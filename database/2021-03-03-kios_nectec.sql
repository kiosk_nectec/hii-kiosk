-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 01, 2021 at 11:20 AM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kios_nectec`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_address`
--

CREATE TABLE `t_address` (
  `KIOSKCODE` varchar(9) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `PID` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `CID` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ADDRESSTYPE` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ROOMNO` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `CONDO` varchar(75) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `HOUSENO` varchar(75) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOISUB` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOIMAIN` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ROAD` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `VILLNAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `VILLAGE` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `TAMBON` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `AMPUR` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `CHANGWAT` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `D_UPDATE` datetime NOT NULL,
  `Delete_flag` int(10) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_address`
--

INSERT INTO `t_address` (`KIOSKCODE`, `PID`, `CID`, `ADDRESSTYPE`, `ROOMNO`, `CONDO`, `HOUSENO`, `SOISUB`, `SOIMAIN`, `ROAD`, `VILLNAME`, `VILLAGE`, `TAMBON`, `AMPUR`, `CHANGWAT`, `D_UPDATE`, `Delete_flag`, `Create_date`) VALUES
('', '631a6302da8adba', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-08 16:20:14'),
('', '9f353a07f70857f', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-08 17:11:32'),
('', '885e55b0eb6cbd5', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-18 15:12:03'),
('', 'ec32ef4b0548a66', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-20 17:42:35'),
('', '233f19ed2568836', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-20 21:04:32'),
('', '303e49ae9ce5c54', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-26 18:34:42'),
('', '52da11ad4c6c786', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-27 19:35:24'),
('', '4420b1389fa32f2', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-27 19:35:26'),
('', 'add905a63319cb7', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-27 19:53:38'),
('', '55118dea8531012', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-02-05 14:16:49'),
('', 'b3984bf07036349', '', '', 'test', NULL, '9/2', '', '', '', NULL, 'หม', 'ตำ', 'อำ', 'จั', '0000-00-00 00:00:00', 0, '2021-02-18 17:07:56');

-- --------------------------------------------------------

--
-- Table structure for table `t_config`
--

CREATE TABLE `t_config` (
  `ID` int(11) NOT NULL,
  `RNAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `AGE` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_con_blood_pressure`
--

CREATE TABLE `t_con_blood_pressure` (
  `ID` int(11) NOT NULL,
  `SBP_min` int(11) NOT NULL,
  `SBP_max` int(11) NOT NULL,
  `DBP_min` int(11) NOT NULL,
  `DBP_max` int(11) NOT NULL,
  `Code` varchar(50) NOT NULL,
  `display` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation_2` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Importance` int(11) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `Device_style` text NOT NULL,
  `Results_style` text NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_con_blood_pressure`
--

INSERT INTO `t_con_blood_pressure` (`ID`, `SBP_min`, `SBP_max`, `DBP_min`, `DBP_max`, `Code`, `display`, `recommendation`, `recommendation_2`, `Importance`, `Image`, `Device_style`, `Results_style`, `Delete_flag`, `Create_date`) VALUES
(1, 0, 119, 0, 79, 'N', 'เหมาะสม', 'ควบคุมอาหาร ออกกำลังกาย วัดความดันโลหิตอย่างสม่ำเสมอ', '', 1, 'assets/kiosk/emoji/icon_emo_ok.png', '{\r\n\"recommendation\":{\"top\":\"509px\",\"line-height\":\"55px\",\"color\":\"#448841\"},\r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},\r\n\"recommendation_num\":{\"color\":\"rgb(112, 172, 71)\"},\r\n\"display\":{\"color\":\"#70AC47\",\"left\":\"895px\",\"width\":\"400px\",\"font-size\":\"85px\",\"top\":\"420px\"},\r\n\"emojiposition\":{ \"left\":\"710px\",\"top\":\"280px\"}\r\n}', '{\r\n\"recommendation_num\":{\"color\":\"#70AD47\",\"top\":\"513px\",\"left\":\"1412px\"},\r\n\"recommendation\":{\"top\":\"509px\",\"line-height\":\"55px\",\"color\":\"#448841\"},\r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},\r\n\"display\":{\"color\":\"#70AC47\"}\r\n}', 0, '2021-01-15 16:36:19'),
(2, 120, 129, 80, 84, 'N', 'ปกติ', 'ควบคุมอาหาร ออกกำลังกาย วัดความดันโลหิตอย่างสม่ำเสมอ', '', 2, 'assets/kiosk/emoji/icon_emo_ok.png', '{\r\n\"recommendation\":{\"top\":\"509px\",\"line-height\":\"55px\",\"color\":\"#448841\"},\r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},\r\n\"recommendation_num\":{\"color\":\"rgb(112, 172, 71)\"},\r\n\"display\":{\"color\":\"#70AC47\",\"left\":\"1035px\",\"width\":\"400px\",\"font-size\":\"85px\",\"top\":\"420px\"},\r\n\"emojiposition\":{ \"left\":\"710px\",\"top\":\"280px\"}\r\n}', '{\r\n\"recommendation_num\":{\"color\":\"#70AD47\",\"top\":\"513px\",\"left\":\"1412px\"},\r\n\"recommendation\":{\"top\":\"509px\",\"line-height\":\"55px\",\"color\":\"#448841\"},\r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},\r\n\"display\":{\"color\":\"#70AC47\"}\r\n}', 0, '2021-01-15 16:36:27'),
(3, 130, 139, 85, 89, 'H', 'สูง', 'ปรึกษาแพทย์', '', 3, 'assets/kiosk/emoji/icon_emo_over1.png', '{ \r\n\"recommendation\":{\"color\":\"#848841\",\"left\":\"1000px\",\"font-size\": \"85px\",\"top\":\"555px\"},\r\n\"recommendation_2\":{\"color\":\"\"},\r\n\"recommendation_bg\":{\"background-color\":\"#FFE97A\"},\r\n\"recommendation_num\":{\"color\":\"rgb(217 218 0)\"},\r\n\"display\":{\"color\":\"#747606\",\"left\":\"1095px\",\"width\":\"400px\",\"font-size\":\"85px\",\"top\":\"420px\"},\r\n\"emojiposition\":{ \"left\":\"710px\",\"top\":\"280px\"}\r\n}', '{\r\n\"recommendation_num\":{\"color\":\"#747606\",\"top\":\"513px\",\"left\":\"1412px\"},\r\n\"recommendation\":{\"color\":\"#848841\"}, \r\n\"recommendation_2\":{\"color\":\"\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FFE97A\"}, \r\n\"display\":{\"color\":\"#747606\"} \r\n}', 0, '2021-01-15 16:38:57'),
(4, 140, 159, 90, 99, 'HU', 'สูงมาก', 'พบแพทย์ ', ' (โรคความดันฯ ระยะเริ่มแรก)', 4, 'assets/kiosk/emoji/icon_emo_over2.png', '{ \r\n\"recommendation\":{\"color\":\"#FF8D00\"}, \r\n\"recommendation_2\":{\"color\":\"#BA957F\",\"width\":\"521px\"}, \r\n\"recommendation_num\":{\"color\":\"rgb(242, 101, 17)\"},\r\n\"recommendation_bg\":{\"background-color\":\"#FAF4D5\"}, \r\n\"display\":{\"color\":\"#F26511\",\"left\":\"950px\",\"width\":\"400px\",\"font-size\":\"85px\",\"top\":\"420px\"},\r\n\"emojiposition\":{ \"left\":\"710px\",\"top\":\"280px\"}\r\n}', '{\r\n\"recommendation_num\":{\"color\":\"#F26511\",\"top\":\"513px\",\"left\":\"1412px\"},\r\n\"recommendation\":{\"color\":\"#FF8D00\"}, \r\n\"recommendation_2\":{\"color\":\"#BA957F\",\"width\":\"521px\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FAF4D5\"}, \r\n\"display\":{\"color\":\"#F26511\"} \r\n}', 0, '2021-01-15 16:49:01'),
(5, 160, 179, 100, 109, 'HH', 'สูงวิกฤต', 'พบแพทย์', '(โรคความดันฯ ระยะที่ 2)', 5, 'assets/kiosk/emoji/icon_emo_over4.png', '{ \"recommendation\":{\"color\":\"#FF0100\",\"left\":\"1035px\"},\r\n \"recommendation_2\":{\"color\":\"#9A6565\",\"left\":\"1250px\"},\r\n\"recommendation_num\":{\"color\":\"rgb(118, 6, 6)\"},\r\n \"recommendation_bg\":{\"background-color\":\"#FFEBE3\"},\r\n \"display\":{\"color\":\"#760606\",\"left\":\"870px\",\"width\":\"400px\",\"font-size\":\"85px\",\"top\":\"420px\"},\r\n\"emojiposition\":{ \"left\":\"710px\",\"top\":\"280px\"}\r\n}', '{\r\n\"recommendation_num\":{\"color\":\"#FF0100\",\"top\":\"513px\",\"left\":\"1412px\"},\r\n\"recommendation\":{\"color\":\"#FF0100\"}, \r\n\"recommendation_2\":{\"color\":\"#9A6565\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FFEBE3\"}, \r\n\"display\":{\"color\":\"#760606\"} \r\n}', 0, '2021-01-15 16:49:01'),
(6, 180, 999, 110, 999, 'HH', 'สูงวิกฤต', 'พบแพทย์', ' (โรคความดันฯ รุนแรง)', 6, 'assets/kiosk/emoji/icon_emo_over4.png', '{ \"recommendation\":{\"color\":\"#FF0100\",\"left\":\"1035px\"},\r\n \"recommendation_2\":{\"color\":\"#9A6565\",\"left\":\"1250px\"},\r\n\"recommendation_num\":{\"color\":\"rgb(118, 6, 6)\"},\r\n \"recommendation_bg\":{\"background-color\":\"#FFEBE3\"},\r\n \"display\":{\"color\":\"#760606\",\"left\":\"870px\",\"width\":\"400px\",\"font-size\":\"85px\",\"top\":\"420px\"},\r\n\"emojiposition\":{ \"left\":\"710px\",\"top\":\"280px\"}\r\n}', '{\r\n\"recommendation_num\":{\"color\":\"#FF0100\",\"top\":\"513px\",\"left\":\"1412px\"},\r\n\"recommendation\":{\"color\":\"#FF0100\"}, \r\n\"recommendation_2\":{\"color\":\"#9A6565\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FFEBE3\"}, \r\n\"display\":{\"color\":\"#760606\"} \r\n}', 0, '2021-01-15 16:52:31');

-- --------------------------------------------------------

--
-- Table structure for table `t_con_bmi`
--

CREATE TABLE `t_con_bmi` (
  `ID` int(11) NOT NULL,
  `Min` decimal(10,1) NOT NULL,
  `Max` decimal(10,1) NOT NULL,
  `Code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `display` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Device_style` text NOT NULL,
  `Results_style` text NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_con_bmi`
--

INSERT INTO `t_con_bmi` (`ID`, `Min`, `Max`, `Code`, `display`, `recommendation`, `Image`, `Device_style`, `Results_style`, `Delete_flag`, `Create_date`) VALUES
(1, 0.0, 18.4, 'L', 'ต่ำ', 'เกณฑ์น้ำหนักน้อยหรือผอม', 'assets/kiosk/emoji/icon_emo_down1.png', '{ \r\n\"recommendation_num\":{\"color\":\"#2dacd1\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#E3F3FF\"},\r\n\"display\":{\"color\":\"#00B0F0\"},\r\n\"emojiposition\":{ \"left\":\"450px\"}\r\n}', '{\r\n\"recommendation_num\":{\"color\":\"#9A6565\"} \r\n}', 0, '2021-01-15 19:32:24'),
(2, 18.5, 22.9, 'N', 'ปกติ', 'อยู่ในเกณฑ์ปกติ', 'assets/kiosk/emoji/icon_emo_ok.png', '{ \r\n\"recommendation_num\":{\"color\":\"#70AD47\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},\r\n\"display\":{\"color\":\"#70AC47\"},\r\n\"emojiposition\":{ \"left\":\"450px\"}\r\n}', '{\r\n\"recommendation_num\":{\"color\":\"#70AD47\"} \r\n}', 0, '2021-01-15 19:32:24'),
(3, 23.0, 24.9, 'H', 'สูง', 'น้ำหนักเกิน', 'assets/kiosk/emoji/icon_emo_over1.png', '{ \r\n\"recommendation_num\":{\"color\":\"rgb(217 218 0)\"}, \r\n\"recommendation_bg\":{\"background-color\":\"rgb(250, 244, 213)\"},\r\n\"display\":{\"color\":\"#747606\"},\r\n\"emojiposition\":{ \"left\":\"450px\"}\r\n}', '{\r\n\"recommendation_num\":{\"color\":\"#747606\"} \r\n}', 0, '2021-01-15 19:32:24'),
(4, 25.0, 29.9, 'HU', 'สูงมาก', 'โรคอ้วนระดับที่ 1', 'assets/kiosk/emoji/icon_emo_over2.png', '{ \r\n\"recommendation_num\":{\"color\":\"rgb(255 193 1)\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FAF4D5\"},\r\n\"display\":{\"color\":\"#F26511\"},\r\n\"emojiposition\":{ \"left\":\"450px\"}\r\n}', '{\r\n\"recommendation_num\":{\"color\":\"#F26511\"} \r\n}', 0, '2021-01-15 19:32:24'),
(5, 30.0, 99.0, 'HH', 'สูงวิกฤต', 'โรคอ้วนระดับที่ 2', 'assets/kiosk/emoji/icon_emo_over4.png', '{ \r\n\"recommendation_num\":{\"color\":\"#FF0100\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FFEBE3\"},\r\n\"display\":{\"color\":\"#760606\"},\r\n\"emojiposition\":{ \"left\":\"450px\"}\r\n}', '{\r\n\"recommendation_num\":{\"color\":\"#FF0100\"} \r\n}', 0, '2021-01-15 19:32:24');

-- --------------------------------------------------------

--
-- Table structure for table `t_con_oxygen`
--

CREATE TABLE `t_con_oxygen` (
  `ID` int(11) NOT NULL,
  `Min` int(11) NOT NULL,
  `Max` int(11) NOT NULL,
  `Code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `display` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Device_style` text NOT NULL,
  `Results_style` text NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_con_oxygen`
--

INSERT INTO `t_con_oxygen` (`ID`, `Min`, `Max`, `Code`, `display`, `recommendation`, `Image`, `Device_style`, `Results_style`, `Delete_flag`, `Create_date`) VALUES
(1, 0, 87, 'LU', 'ต่ำมาก', '(ต่ำมากกว่าปกติ)', 'assets/kiosk/emoji/icon_emo_over4.png', '{ \r\n\"recommendation\":{\"color\":\"#FF0100\",\"left\":\"810px\",\"width\":\"1000px\"}, \r\n\"recommendation_num\":{\"color\":\"rgb(255, 1, 0)\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FFEBE3\"}, \r\n\"display\":{\"color\":\"#760606\",\"left\":\"1085px\",\"width\":\"440px\",\"top\":\"600px\",\"font-size\":\"150px\"},\r\n\"emojiposition\":{ \"left\":\"1365px\"} \r\n}', '{ \r\n\"recommendation_num\":{\"color\":\"#FF0100\"}\r\n}', 0, '2021-01-15 17:38:16'),
(2, 88, 94, 'L', 'ต่ำ', '(ต่ำกว่าปกติ)', 'assets/kiosk/emoji/icon_emo_down1.png', '{ \r\n\"recommendation\":{\"color\":\"#417588\",\"left\":\"720px\",\"width\":\"1000px\"}, \r\n\"recommendation_num\":{\"color\":\"#9A6565\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#E3F3FF\"}, \r\n\"display\":{\"color\":\"#00B0F0\",\"left\":\"1055px\",\"width\":\"260px\",\"top\":\"600px\",\"font-size\":\"150px\"},\r\n\"emojiposition\":{ \"left\":\"1365px\"} \r\n}', '{ \r\n\"recommendation_num\":{\"color\":\"#9A6565\"}\r\n}', 0, '2021-01-15 17:38:16'),
(3, 95, 100, 'N', 'ปกติ', '(ปกติ)', 'assets/kiosk/emoji/icon_emo_ok.png', '{ \r\n\"recommendation\":{\"color\":\"#448841\",\"left\":\"855px\",\"width\":\"1000px\"}, \r\n\"recommendation_num\":{\"color\":\"#448841\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"}, \r\n\"display\":{\"color\":\"#70AC47\",\"left\":\"1115px\",\"width\":\"260px\",\"top\":\"600px\",\"font-size\":\"150px\"},\r\n\"emojiposition\":{ \"left\":\"1490px\"} \r\n}', '{ \r\n\"recommendation_num\":{\"color\":\"#70AD47\"}\r\n}', 0, '2021-01-15 17:38:16');

-- --------------------------------------------------------

--
-- Table structure for table `t_con_pulserate`
--

CREATE TABLE `t_con_pulserate` (
  `ID` int(11) NOT NULL,
  `Min` int(11) NOT NULL,
  `Max` int(11) NOT NULL,
  `Code` varchar(50) NOT NULL,
  `display` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Image` varchar(255) NOT NULL,
  `Device_style` text NOT NULL,
  `Results_style` text NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_con_pulserate`
--

INSERT INTO `t_con_pulserate` (`ID`, `Min`, `Max`, `Code`, `display`, `recommendation`, `Image`, `Device_style`, `Results_style`, `Delete_flag`, `Create_date`) VALUES
(1, 0, 59, 'L', 'ต่ำ', '(ชีพจรเต้นช้ากว่าปกติ)', 'assets/kiosk/emoji/icon_emo_down1.png', '{ \"recommendation\":{\"color\":\"#417588\",\"left\":\"1310px\"},  \r\n\"recommendation_num\":{\"color\":\"#2dacd1\"},\r\n\"recommendation_bg\":{\"background-color\":\"#E3F3FF\"},  \"display\":{\"color\":\"#00B0F0\",\"left\":\"810px\",\"width\":\"250px\",\"font-size\":\"100px\"},\r\n\"emojiposition\":{ \"left\":\"870px\",\"top\":\"630px\"} \r\n}', '{ \"recommendation\":{\"color\":\"#417588\"}, \"recommendation_num\":{\"color\":\"#9A6565\"}, \"recommendation_bg\":{\"background-color\":\"#E3F3FF\"}, \"display\":{\"color\":\"#00B0F0\"} }', 0, '2021-01-15 17:29:43'),
(2, 60, 100, 'N', 'ปกติ', '(ชีพจรเต้นปกติ)', 'assets/kiosk/emoji/icon_emo_ok.png', '{ \"recommendation\":{\"color\":\"#448841\",\"left\":\"1335px\",\"width\":\"435px\",\"font-size\":\"60px\"},\r\n \"recommendation_num\":{\"color\":\"#70AD47\"},\r\n \"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},\r\n \"display\":{\"color\":\"#70AC47\",\"left\":\"725px\",\"width\":\"250px\",\"font-size\":\"100px\"},\r\n\"emojiposition\":{ \"left\":\"870px\",\"top\":\"630px\"} \r\n}', '{ \"recommendation\":{\"color\":\"#448841\"}, \"recommendation_num\":{\"color\":\"#70AD47\"}, \"recommendation_bg\":{\"background-color\":\"#E3FFE3\"}, \"display\":{\"color\":\"#70AC47\"} }', 0, '2021-01-15 17:29:43'),
(3, 110, 999, 'H', 'สูง', '(ชีพจรเต้นสูงกว่าปกติ)', 'assets/kiosk/emoji/icon_emo_over4.png', '{ \"recommendation\":{\"color\":\"#FF0100\",\"left\":\"1315px\"}, \r\n\"recommendation_num\":{\"color\":\"#FF0100\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FFEBE3\"}, \r\n\"display\":{\"color\":\"#760606\",\"left\":\"800px\",\"width\":\"250px\",\"font-size\":\"100px\"},\r\n\"emojiposition\":{ \"left\":\"870px\",\"top\":\"630px\"} \r\n }', '{ \"recommendation\":{\"color\":\"#FF0100\"}, \"recommendation_num\":{\"color\":\"#FF0100\"}, \"recommendation_bg\":{\"background-color\":\"#FFEBE3\"}, \"display\":{\"color\":\"#760606\"} }', 0, '2021-01-15 17:29:43');

-- --------------------------------------------------------

--
-- Table structure for table `t_con_temperature`
--

CREATE TABLE `t_con_temperature` (
  `ID` int(11) NOT NULL,
  `Min` decimal(10,1) NOT NULL,
  `Max` decimal(10,1) NOT NULL,
  `Code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `display` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Device_style` text NOT NULL,
  `Results_style` text NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_con_temperature`
--

INSERT INTO `t_con_temperature` (`ID`, `Min`, `Max`, `Code`, `display`, `recommendation`, `Image`, `Device_style`, `Results_style`, `Delete_flag`, `Create_date`) VALUES
(1, 0.0, 36.4, 'L', 'ต่ำ', '(ต่ำกว่าปกติ)', 'assets/kiosk/emoji/icon_emo_down1.png', '{  \r\n\"recommendation\":{\"color\":\"#417588\",\"left\":\"990px\",\"width\":\"655px\"},  \"recommendation_num\":{\"color\":\"#2dacd1\"},  \r\n\"recommendation_bg\":{\"background-color\":\"#E3F3FF\"},  \r\n\"display\":{\"color\":\"#00B0F0\",\"left\":\"895px\",\"width\":\"580px\",\"top\":\"645px\",\"font-size\":\"120px\"},\r\n\"emojiposition\":{ \"left\":\"1520px\",\"top\":\"630px\"}\r\n }', '{  \r\n \"recommendation_num\":{\"color\":\"#9A6565\"}\r\n}', 0, '2021-01-15 17:08:24'),
(2, 36.5, 37.5, 'N', 'ปกติ', '(ปกติ)', 'assets/kiosk/emoji/icon_emo_ok.png', '{ \"recommendation\":{\"color\":\"#448841\",\"left\":\"1165px\"},  \r\n\"recommendation_num\":{\"color\":\"#70AD47\"},  \r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},  \r\n\"display\":{\"color\":\"#70AC47\",\"left\":\"945px\",\"width\":\"580px\",\"top\":\"645px\",\"font-size\":\"120px\"},\r\n\"emojiposition\":{ \"left\":\"1520px\",\"top\":\"630px\"}\r\n }', '{   \r\n\"recommendation_num\":{\"color\":\"#70AD47\"}\r\n}', 0, '2021-01-15 17:08:24'),
(3, 37.6, 38.5, 'H', 'สูง', '(มีไข้ต่ำ)', 'assets/kiosk/emoji/icon_emo_over1.png', '\r\n{ \"recommendation\":{\"color\":\"#848841\",\"l1eft\":\"1180px\",\"width\":\"460px\"}, \r\n\"recommendation_num\":{\"color\":\"rgb(217 218 0)\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FFE97A\"}, \r\n\"display\":{\"color\":\"#747606\",\"left\":\"900px\",\"width\":\"580px\",\"top\":\"645px\",\"font-size\":\"120px\"},\r\n\"emojiposition\":{ \"left\":\"1520px\",\"top\":\"630px\"}\r\n }', '{ \r\n\"recommendation_num\":{\"color\":\"#747606\"}\r\n}', 0, '2021-01-15 17:19:47'),
(4, 38.6, 39.5, 'HU', 'สูงมาก', '(มีไข้ปานกลาง)', 'assets/kiosk/emoji/icon_emo_over2.png', '{ \"recommendation\":{\"color\":\"#FF8D00\",\"width\":\"755px\",\"left\":\"985px\"}, \r\n\"recommendation_num\":{\"color\":\"rgb(242, 101, 17)\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FAF4D5\"}, \r\n\"display\":{\"color\":\"#F26511\",\"left\":\"1005px\",\"width\":\"580px\",\"top\":\"645px\",\"font-size\":\"120px\"},\r\n\"emojiposition\":{ \"left\":\"1520px\",\"top\":\"630px\"}\r\n }', '{  \r\n\"recommendation_num\":{\"color\":\"#F26511\"} \r\n}', 0, '2021-01-15 17:19:47'),
(5, 39.6, 99.9, 'HH', 'สูงวิกฤต', '(มีไข้สูง)', 'assets/kiosk/emoji/icon_emo_over4.png', '{ \"recommendation\":{\"color\":\"#FF0100\",\"left\":\"1160px\",\"width\":\"470px\"}, \r\n\"recommendation_2\":{\"color\":\"rgb(118, 6, 6)\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FFEBE3\"}, \r\n\"display\":{\"color\":\"#760606\",\"left\":\"1055px\",\"width\":\"580px\",\"top\":\"645px\",\"font-size\":\"120px\"},\r\n\"emojiposition\":{ \"left\":\"1520px\",\"top\":\"630px\"}\r\n }', '{ \r\n\"recommendation_num\":{\"color\":\"#FF0100\"}\r\n}', 0, '2021-01-15 17:19:47');

-- --------------------------------------------------------

--
-- Table structure for table `t_devices`
--

CREATE TABLE `t_devices` (
  `id` int(11) NOT NULL,
  `device` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `active_flag` int(11) NOT NULL,
  `route` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_devices`
--

INSERT INTO `t_devices` (`id`, `device`, `active_flag`, `route`) VALUES
(1, 'เครื่องชั่งน้ำหนัก', 1, 'devices/weighing-machines'),
(2, 'เครื่องวัดความดันโลหิต', 1, 'devices/blood-pressure'),
(3, 'เครื่องวัดเปอร์เซ็นต์ออกซิเจนในเลือด อัตราการเต้นหัวใจ', 1, 'devices/pulse-oximeter'),
(4, 'เครื่องวัดอุณหภูมิร่างกาย', 1, 'devices/thermometer');

-- --------------------------------------------------------

--
-- Table structure for table `t_person`
--

CREATE TABLE `t_person` (
  `KIOSKCODE` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `PID` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `CID` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRENAME` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `LNAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SEX` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `BIRTH` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MSTATUS` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NATION` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `ABOGROUP` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RHGROUP` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PASSPORT` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TELEPHONE` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MOBILE` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHOTO_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHOTO_URL1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHOTO_URL2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHOTO_URL3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `D_UPDATE` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_request_log`
--

CREATE TABLE `t_request_log` (
  `id` int(11) NOT NULL,
  `request_url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `request_time` datetime NOT NULL,
  `response_time` datetime NOT NULL,
  `status_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `error_message` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `response_message` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_request_log`
--

INSERT INTO `t_request_log` (`id`, `request_url`, `request_time`, `response_time`, `status_code`, `error_message`, `response_message`) VALUES
(1, 'http://128.199.73.202/middleware/read/readeridcard', '2021-03-01 12:01:14', '2021-03-01 12:01:14', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(2, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 12:02:28', '2021-03-01 12:02:28', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(3, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 12:03:04', '2021-03-01 12:03:04', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(4, 'http://128.199.73.202/middleware/read/readeridcard', '2021-03-01 12:06:39', '2021-03-01 12:06:39', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(5, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 12:09:58', '2021-03-01 12:09:58', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(6, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 12:11:24', '2021-03-01 12:11:24', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(7, 'http://128.199.73.202/middleware/read/readeridcard', '2021-03-01 12:12:00', '2021-03-01 12:12:00', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(8, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 12:12:27', '2021-03-01 12:12:27', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(9, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 12:28:01', '2021-03-01 12:28:01', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(10, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 12:28:31', '2021-03-01 12:28:32', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(11, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 12:29:24', '2021-03-01 12:29:24', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(12, 'http://128.199.73.202/middleware/read/readeridcard', '2021-03-01 12:30:36', '2021-03-01 12:30:37', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(13, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 12:30:49', '2021-03-01 12:30:49', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(14, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 12:31:57', '2021-03-01 12:31:57', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(15, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 12:33:11', '2021-03-01 12:33:11', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(16, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 12:38:21', '2021-03-01 12:38:21', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(17, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 12:39:34', '2021-03-01 12:40:19', '0', 'Operation timed out after 45000 milliseconds with 0 bytes received', '0'),
(18, 'http://128.199.73.202/middleware/stop/pressuregauge', '2021-03-01 12:40:20', '2021-03-01 12:40:20', '200', '', '{\n    \"data\": [\n        {\n            \"status\": \"success\"\n        }\n    ]\n}'),
(19, 'http://128.199.73.202/middleware/stop/pressuregauge', '2021-03-01 12:40:20', '2021-03-01 12:40:20', '200', '', '{\n    \"data\": [\n        {\n            \"status\": \"success\"\n        }\n    ]\n}'),
(20, 'http://128.199.73.202/middleware/stop/pressuregauge', '2021-03-01 12:40:20', '2021-03-01 12:40:20', '200', '', '{\n    \"data\": [\n        {\n            \"status\": \"success\"\n        }\n    ]\n}'),
(21, 'http://128.199.73.202/middleware/stop/pressuregauge', '2021-03-01 12:40:20', '2021-03-01 12:40:20', '200', '', '{\n    \"data\": [\n        {\n            \"status\": \"success\"\n        }\n    ]\n}'),
(22, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 12:40:31', '2021-03-01 12:41:16', '0', 'Operation timed out after 45000 milliseconds with 0 bytes received', '0'),
(23, 'http://128.199.73.202/middleware/stop/pressuregauge', '2021-03-01 12:41:16', '2021-03-01 12:41:16', '200', '', '{\n    \"data\": [\n        {\n            \"status\": \"success\"\n        }\n    ]\n}'),
(24, 'http://128.199.73.202/middleware/stop/pressuregauge', '2021-03-01 12:41:17', '2021-03-01 12:41:17', '200', '', '{\n    \"data\": [\n        {\n            \"status\": \"success\"\n        }\n    ]\n}'),
(25, 'http://128.199.73.202/middleware/stop/pressuregauge', '2021-03-01 12:41:17', '2021-03-01 12:41:17', '200', '', '{\n    \"data\": [\n        {\n            \"status\": \"success\"\n        }\n    ]\n}'),
(26, 'http://128.199.73.202/middleware/stop/pressuregauge', '2021-03-01 12:41:17', '2021-03-01 12:41:17', '200', '', '{\n    \"data\": [\n        {\n            \"status\": \"success\"\n        }\n    ]\n}'),
(27, 'http://128.199.73.202/middleware/stop/pressuregauge', '2021-03-01 12:41:17', '2021-03-01 12:41:17', '200', '', '{\n    \"data\": [\n        {\n            \"status\": \"success\"\n        }\n    ]\n}'),
(28, 'http://128.199.73.202/middleware/stop/pressuregauge', '2021-03-01 12:41:17', '2021-03-01 12:41:17', '200', '', '{\n    \"data\": [\n        {\n            \"status\": \"success\"\n        }\n    ]\n}'),
(29, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 12:41:30', '2021-03-01 12:42:05', '503', '', '<HTML><HEAD>\r\n<TITLE>Network Error</TITLE>\r\n</HEAD>\r\n<BODY>\r\n<FONT face=\"Helvetica\">\r\n<big><strong></strong></big><BR>\r\n</FONT>\r\n<blockquote>\r\n<TABLE border=0 cellPadding=1 width=\"80%\">\r\n<TR><TD>\r\n<FONT face=\"Helvetica\">\r\n<big>Network Error (tcp_error)</big>\r\n<BR>\r\n<BR>\r\n</FONT>\r\n</TD></TR>\r\n<TR><TD>\r\n<FONT face=\"Helvetica\">\r\nA communication error occurred: \"\"\r\n</FONT>\r\n</TD></TR>\r\n<TR><TD>\r\n<FONT face=\"Helvetica\">\r\nThe Web Server may be down, too busy, or experiencing other problems preventing it from responding to requests. You may wish to try again at a later time.\r\n</FONT>\r\n</TD></TR>\r\n<TR><TD>\r\n<FONT face=\"Helvetica\" SIZE=2>\r\n<BR>\r\nFor assistance, contact your network support team.\r\n</FONT>\r\n</TD></TR>\r\n</TABLE>\r\n</blockquote>\r\n</FONT>\r\n</BODY></HTML>\r\n'),
(30, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 12:42:22', '2021-03-01 12:42:22', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(31, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 12:42:24', '2021-03-01 12:42:24', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(32, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 12:45:11', '2021-03-01 12:45:56', '0', 'Operation timed out after 45000 milliseconds with 0 bytes received', '0'),
(33, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 12:52:27', '2021-03-01 12:52:27', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(34, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 12:59:18', '2021-03-01 12:59:18', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(35, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 13:05:51', '2021-03-01 13:05:52', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(36, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 13:10:29', '2021-03-01 13:10:29', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(37, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 13:13:33', '2021-03-01 13:13:33', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(38, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 13:19:08', '2021-03-01 13:19:38', '503', '', '<HTML><HEAD><meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\"><TITLE>503 Service Unavailable</TITLE></HEAD><BODY><H1>503 Service Unavailable</H1></BODY></HTML>'),
(39, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 13:19:45', '2021-03-01 13:19:45', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(40, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 13:24:11', '2021-03-01 13:24:11', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(41, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 13:26:36', '2021-03-01 13:26:37', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(42, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 14:06:46', '2021-03-01 14:06:46', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(43, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 14:10:08', '2021-03-01 14:10:08', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(44, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 14:12:49', '2021-03-01 14:13:34', '0', 'Operation timed out after 45000 milliseconds with 0 bytes received', '0'),
(45, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 14:15:12', '2021-03-01 14:15:12', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(46, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 14:15:45', '2021-03-01 14:15:45', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(47, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 14:17:00', '2021-03-01 14:17:00', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(48, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 14:19:54', '2021-03-01 14:19:54', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(49, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 14:22:32', '2021-03-01 14:22:32', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(50, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 14:25:15', '2021-03-01 14:25:15', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(51, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 14:44:12', '2021-03-01 14:44:12', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(52, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 14:45:07', '2021-03-01 14:45:07', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(53, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 14:49:35', '2021-03-01 14:49:36', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(54, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 15:11:51', '2021-03-01 15:11:51', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(55, 'http://128.199.73.202/middleware/read/readeridcard', '2021-03-01 15:12:57', '2021-03-01 15:12:58', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(56, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 15:13:14', '2021-03-01 15:13:59', '0', 'Operation timed out after 45000 milliseconds with 0 bytes received', '0'),
(57, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 15:14:04', '2021-03-01 15:14:49', '0', 'Operation timed out after 45000 milliseconds with 0 bytes received', '0'),
(58, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 15:14:55', '2021-03-01 15:14:55', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(59, 'http://128.199.73.202/middleware/read/bloodoxygenmeter', '2021-03-01 15:15:30', '2021-03-01 15:15:30', '200', '', '{\n    \"data\": [\n        {\n            \"PRbpm\": \"82\",\n            \"SpO2%\": \"97\"\n        }\n    ]\n}'),
(60, 'http://128.199.73.202/middleware/read/readeridcard', '2021-03-01 15:16:08', '2021-03-01 15:16:08', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(61, 'http://128.199.73.202/middleware/read/readeridcard', '2021-03-01 15:16:10', '2021-03-01 15:16:11', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(62, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 15:16:22', '2021-03-01 15:16:22', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(63, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 15:16:34', '2021-03-01 15:16:34', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(64, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 15:16:40', '2021-03-01 15:17:25', '0', 'Operation timed out after 45000 milliseconds with 0 bytes received', '0'),
(65, 'http://128.199.73.202/middleware/read/bloodoxygenmeter', '2021-03-01 15:17:25', '2021-03-01 15:17:25', '200', '', '{\n    \"data\": [\n        {\n            \"PRbpm\": \"82\",\n            \"SpO2%\": \"97\"\n        }\n    ]\n}'),
(66, 'http://128.199.73.202/middleware/stop/weighingmachine', '2021-03-01 15:17:25', '2021-03-01 15:18:10', '0', 'Operation timed out after 45000 milliseconds with 0 bytes received', '0'),
(67, 'http://128.199.73.202/middleware/read/bloodoxygenmeter', '2021-03-01 15:18:45', '2021-03-01 15:18:45', '200', '', '{\n    \"data\": [\n        {\n            \"PRbpm\": \"82\",\n            \"SpO2%\": \"97\"\n        }\n    ]\n}'),
(68, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-03-01 15:20:25', '2021-03-01 15:20:25', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(69, 'http://128.199.73.202/middleware/read/bloodoxygenmeter', '2021-03-01 15:23:53', '2021-03-01 15:23:53', '200', '', '{\n    \"data\": [\n        {\n            \"PRbpm\": \"82\",\n            \"SpO2%\": \"97\"\n        }\n    ]\n}'),
(70, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 15:25:48', '2021-03-01 15:25:48', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(71, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 15:25:54', '2021-03-01 15:25:54', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(72, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 15:28:36', '2021-03-01 15:28:36', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(73, 'http://128.199.73.202/middleware/read/readeridcard', '2021-03-01 15:30:22', '2021-03-01 15:30:22', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(74, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 15:30:35', '2021-03-01 15:30:35', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(75, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 15:30:56', '2021-03-01 15:30:56', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(76, 'http://128.199.73.202/middleware/read/bloodoxygenmeter', '2021-03-01 15:31:52', '2021-03-01 15:31:52', '200', '', '{\n    \"data\": [\n        {\n            \"PRbpm\": \"82\",\n            \"SpO2%\": \"97\"\n        }\n    ]\n}'),
(77, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-03-01 15:33:56', '2021-03-01 15:34:41', '0', 'Operation timed out after 45000 milliseconds with 0 bytes received', '0'),
(78, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-03-01 15:35:10', '2021-03-01 15:35:55', '0', 'Operation timed out after 45000 milliseconds with 0 bytes received', '0'),
(79, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 16:20:00', '2021-03-01 16:20:00', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(80, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 16:20:29', '2021-03-01 16:20:29', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(81, 'http://128.199.73.202/middleware/read/readeridcard', '2021-03-01 16:22:28', '2021-03-01 16:22:28', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(82, 'http://128.199.73.202/middleware/read/readeridcard', '2021-03-01 16:23:45', '2021-03-01 16:23:45', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(83, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 16:26:50', '2021-03-01 16:26:50', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(84, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 16:42:31', '2021-03-01 16:42:31', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(85, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 16:46:37', '2021-03-01 16:46:37', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(86, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 16:47:26', '2021-03-01 16:47:26', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(87, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 16:48:27', '2021-03-01 16:48:27', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(88, 'http://128.199.73.202/middleware/read/readeridcard', '2021-03-01 16:49:22', '2021-03-01 16:49:22', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(89, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 16:49:36', '2021-03-01 16:49:36', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(90, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 16:49:54', '2021-03-01 16:49:54', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(91, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 16:51:08', '2021-03-01 16:51:08', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(92, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 16:51:53', '2021-03-01 16:51:53', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(93, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 16:53:08', '2021-03-01 16:53:08', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(94, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 16:55:35', '2021-03-01 16:55:36', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(95, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 16:59:11', '2021-03-01 16:59:12', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(96, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 16:59:37', '2021-03-01 16:59:37', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(97, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 16:59:48', '2021-03-01 16:59:48', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(98, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 17:00:34', '2021-03-01 17:00:34', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(99, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 17:00:41', '2021-03-01 17:00:41', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(100, 'http://128.199.73.202/middleware/read/bloodoxygenmeter', '2021-03-01 17:01:16', '2021-03-01 17:01:16', '200', '', '{\n    \"data\": [\n        {\n            \"PRbpm\": \"82\",\n            \"SpO2%\": \"97\"\n        }\n    ]\n}'),
(101, 'http://128.199.73.202/middleware/read/bloodoxygenmeter', '2021-03-01 17:02:46', '2021-03-01 17:02:46', '200', '', '{\n    \"data\": [\n        {\n            \"PRbpm\": \"82\",\n            \"SpO2%\": \"97\"\n        }\n    ]\n}'),
(102, 'http://128.199.73.202/middleware/read/bloodoxygenmeter', '2021-03-01 17:12:00', '2021-03-01 17:12:01', '200', '', '{\n    \"data\": [\n        {\n            \"PRbpm\": \"82\",\n            \"SpO2%\": \"97\"\n        }\n    ]\n}'),
(103, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 17:25:55', '2021-03-01 17:25:55', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(104, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 17:27:38', '2021-03-01 17:27:38', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(105, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 17:27:45', '2021-03-01 17:27:46', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(106, 'http://128.199.73.202/middleware/stop/bloodoxygenmeter', '2021-03-01 17:31:41', '2021-03-01 17:31:41', '200', '', '{\n    \"data\": [\n        {\n            \"status\": \"success\"\n        }\n    ]\n}'),
(107, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-03-01 17:31:47', '2021-03-01 17:31:47', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(108, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 17:33:47', '2021-03-01 17:33:47', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(109, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 17:34:36', '2021-03-01 17:35:21', '0', 'Operation timed out after 45000 milliseconds with 0 bytes received', '0'),
(110, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 17:35:32', '2021-03-01 17:36:17', '0', 'Operation timed out after 45000 milliseconds with 0 bytes received', '0'),
(111, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 17:36:32', '2021-03-01 17:37:17', '0', 'Operation timed out after 45000 milliseconds with 0 bytes received', '0'),
(112, 'http://128.199.73.202/middleware/stop/weighingmachine', '2021-03-01 17:37:17', '2021-03-01 17:37:17', '200', '', '{\n    \"data\": [\n        {\n            \"status\": \"success\"\n        }\n    ]\n}'),
(113, 'http://128.199.73.202/middleware/stop/weighingmachine', '2021-03-01 17:37:17', '2021-03-01 17:37:17', '200', '', '{\n    \"data\": [\n        {\n            \"status\": \"success\"\n        }\n    ]\n}'),
(114, 'http://128.199.73.202/middleware/stop/weighingmachine', '2021-03-01 17:37:17', '2021-03-01 17:37:17', '200', '', '{\n    \"data\": [\n        {\n            \"status\": \"success\"\n        }\n    ]\n}'),
(115, 'http://128.199.73.202/middleware/stop/weighingmachine', '2021-03-01 17:37:18', '2021-03-01 17:37:18', '200', '', '{\n    \"data\": [\n        {\n            \"status\": \"success\"\n        }\n    ]\n}'),
(116, 'http://128.199.73.202/middleware/stop/weighingmachine', '2021-03-01 17:37:18', '2021-03-01 17:37:18', '200', '', '{\n    \"data\": [\n        {\n            \"status\": \"success\"\n        }\n    ]\n}'),
(117, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 17:37:20', '2021-03-01 17:38:05', '0', 'Operation timed out after 45000 milliseconds with 0 bytes received', '0'),
(118, 'http://128.199.73.202/middleware/stop/weighingmachine', '2021-03-01 17:38:05', '2021-03-01 17:38:50', '0', 'Operation timed out after 45000 milliseconds with 0 bytes received', '0'),
(119, 'http://128.199.73.202/middleware/stop/weighingmachine', '2021-03-01 17:38:50', '2021-03-01 17:39:20', '429', '', '{\n    \"success\": false,\n    \"title\": \"Internal Server Error\",\n    \"message\": \"Rate limit exceeded\"\n}'),
(120, 'http://128.199.73.202/middleware/stop/weighingmachine', '2021-03-01 17:39:20', '2021-03-01 17:39:20', '429', '', '{\n    \"success\": false,\n    \"title\": \"Internal Server Error\",\n    \"message\": \"Rate limit exceeded\"\n}'),
(121, 'http://128.199.73.202/middleware/stop/weighingmachine', '2021-03-01 17:39:20', '2021-03-01 17:39:21', '429', '', '{\n    \"success\": false,\n    \"title\": \"Internal Server Error\",\n    \"message\": \"Rate limit exceeded\"\n}'),
(122, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 17:41:20', '2021-03-01 17:41:20', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(123, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 17:41:54', '2021-03-01 17:41:54', '429', '', '{\n    \"success\": false,\n    \"title\": \"Internal Server Error\",\n    \"message\": \"Rate limit exceeded\"\n}'),
(124, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 17:42:02', '2021-03-01 17:42:02', '429', '', '{\n    \"success\": false,\n    \"title\": \"Internal Server Error\",\n    \"message\": \"Rate limit exceeded\"\n}'),
(125, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 17:42:10', '2021-03-01 17:42:10', '429', '', '{\n    \"success\": false,\n    \"title\": \"Internal Server Error\",\n    \"message\": \"Rate limit exceeded\"\n}'),
(126, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 17:42:25', '2021-03-01 17:42:25', '429', '', '{\n    \"success\": false,\n    \"title\": \"Internal Server Error\",\n    \"message\": \"Rate limit exceeded\"\n}'),
(127, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 17:42:28', '2021-03-01 17:42:28', '429', '', '{\n    \"success\": false,\n    \"title\": \"Internal Server Error\",\n    \"message\": \"Rate limit exceeded\"\n}'),
(128, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 17:42:52', '2021-03-01 17:42:52', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(129, 'http://128.199.73.202/middleware/read/bloodoxygenmeter', '2021-03-01 17:48:28', '2021-03-01 17:48:28', '200', '', '{\n    \"data\": [\n        {\n            \"PRbpm\": \"82\",\n            \"SpO2%\": \"97\"\n        }\n    ]\n}'),
(130, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-03-01 17:49:04', '2021-03-01 17:49:04', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(131, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 18:05:35', '2021-03-01 18:05:35', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(132, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 18:07:33', '2021-03-01 18:07:33', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(133, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 18:08:15', '2021-03-01 18:09:00', '0', 'Operation timed out after 45000 milliseconds with 0 bytes received', '0'),
(134, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 18:09:40', '2021-03-01 18:10:25', '0', 'Operation timed out after 45000 milliseconds with 0 bytes received', '0'),
(135, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 18:10:56', '2021-03-01 18:10:56', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(136, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 18:11:05', '2021-03-01 18:11:05', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}');

-- --------------------------------------------------------

--
-- Table structure for table `t_service`
--

CREATE TABLE `t_service` (
  `KIOSKCODE` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `USER_TYPE` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `PID` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_SERV` date NOT NULL,
  `TIME_SERV` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `BTEMP` decimal(10,1) DEFAULT NULL,
  `BTEMPCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SBP` int(11) DEFAULT NULL,
  `DBP` int(11) DEFAULT NULL,
  `BPCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PR` int(11) DEFAULT NULL,
  `PRCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RR` int(11) DEFAULT NULL,
  `RRCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OXYGEN` int(11) DEFAULT NULL,
  `OXYGENCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WEIGHT` int(11) DEFAULT NULL,
  `HEIGHT` decimal(10,1) DEFAULT NULL,
  `BMI` int(11) DEFAULT NULL,
  `BMICODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BMR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `D_UPDATE` datetime NOT NULL,
  `SESSION_ID` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_con_blood_pressure`
--
ALTER TABLE `t_con_blood_pressure`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_con_bmi`
--
ALTER TABLE `t_con_bmi`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_con_oxygen`
--
ALTER TABLE `t_con_oxygen`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_con_pulserate`
--
ALTER TABLE `t_con_pulserate`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_con_temperature`
--
ALTER TABLE `t_con_temperature`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_devices`
--
ALTER TABLE `t_devices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_person`
--
ALTER TABLE `t_person`
  ADD PRIMARY KEY (`KIOSKCODE`,`PID`);

--
-- Indexes for table `t_request_log`
--
ALTER TABLE `t_request_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_service`
--
ALTER TABLE `t_service`
  ADD PRIMARY KEY (`SESSION_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_con_blood_pressure`
--
ALTER TABLE `t_con_blood_pressure`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `t_con_bmi`
--
ALTER TABLE `t_con_bmi`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `t_con_oxygen`
--
ALTER TABLE `t_con_oxygen`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_con_pulserate`
--
ALTER TABLE `t_con_pulserate`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_con_temperature`
--
ALTER TABLE `t_con_temperature`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `t_devices`
--
ALTER TABLE `t_devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `t_request_log`
--
ALTER TABLE `t_request_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
