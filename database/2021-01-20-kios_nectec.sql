-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 20, 2021 at 10:55 AM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kios_nectec`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_address`
--

CREATE TABLE `t_address` (
  `KIOSKCODE` varchar(9) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `PID` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `CID` varchar(13) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ADDRESSTYPE` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ROOMNO` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `CONDO` varchar(75) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `HOUSENO` varchar(75) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOISUB` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOIMAIN` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ROAD` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `VILLNAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `VILLAGE` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `TAMBON` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `AMPUR` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `CHANGWAT` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `D_UPDATE` datetime NOT NULL,
  `Delete_flag` int(10) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_address`
--

INSERT INTO `t_address` (`KIOSKCODE`, `PID`, `CID`, `ADDRESSTYPE`, `ROOMNO`, `CONDO`, `HOUSENO`, `SOISUB`, `SOIMAIN`, `ROAD`, `VILLNAME`, `VILLAGE`, `TAMBON`, `AMPUR`, `CHANGWAT`, `D_UPDATE`, `Delete_flag`, `Create_date`) VALUES
('', '631a6302da8adba', '', '', 'test', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-08 16:20:14'),
('', '9f353a07f70857f', '', '', 'test', NULL, '9/2', '', '', '', NULL, 'หม', 'ตำ', 'อำ', 'จั', '0000-00-00 00:00:00', 0, '2021-01-08 17:11:32'),
('', '885e55b0eb6cbd5', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-18 15:12:03'),
('', 'ec32ef4b0548a66', '', '', 'test', NULL, '9/2', '', '', '', NULL, 'หม', 'ตำ', 'อำ', 'จั', '0000-00-00 00:00:00', 0, '2021-01-20 17:42:35');

-- --------------------------------------------------------

--
-- Table structure for table `t_config`
--

CREATE TABLE `t_config` (
  `ID` int(11) NOT NULL,
  `RNAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `AGE` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_con_blood_pressure`
--

CREATE TABLE `t_con_blood_pressure` (
  `ID` int(11) NOT NULL,
  `SBP_min` int(11) NOT NULL,
  `SBP_max` int(11) NOT NULL,
  `DBP_min` int(11) NOT NULL,
  `DBP_max` int(11) NOT NULL,
  `Code` varchar(50) NOT NULL,
  `display` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation_2` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Image` varchar(255) NOT NULL,
  `Device_style` text NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_con_blood_pressure`
--

INSERT INTO `t_con_blood_pressure` (`ID`, `SBP_min`, `SBP_max`, `DBP_min`, `DBP_max`, `Code`, `display`, `recommendation`, `recommendation_2`, `Image`, `Device_style`, `Delete_flag`, `Create_date`) VALUES
(1, 0, 119, 0, 79, 'N', 'เหมาะสม', 'ควบคุมอาหาร ออกกำลังกาย วัดความดันโลหิตอย่างสม่ำเสมอ', '', 'assets/kiosk/emoji/icon_emo_ok.svg', '{\r\n\"recommendation\":{\"top\":\"509px\",\"line-height\":\"55px\",\"color\":\"#448841\"},\r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},\r\n\"display\":{\"color\":\"#70AC47\"}\r\n}', 0, '2021-01-15 16:36:19'),
(2, 120, 129, 80, 84, 'N', 'ปกติ', 'ควบคุมอาหาร ออกกำลังกาย วัดความดันโลหิตอย่างสม่ำเสมอ', '', 'assets/kiosk/emoji/icon_emo_ok.svg', '{\r\n\"recommendation\":{\"top\":\"509px\",\"line-height\":\"55px\",\"color\":\"#448841\"},\r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},\r\n\"display\":{\"color\":\"#70AC47\"}\r\n}', 0, '2021-01-15 16:36:27'),
(3, 130, 139, 85, 89, 'H', 'สูง', 'ปรึกษาแพทย์', '', 'assets/kiosk/emoji/icon_emo_over1.svg', '{ \"recommendation\":{\"color\":\"#848841\"}, \"recommendation_2\":{\"color\":\"\"}, \"recommendation_bg\":{\"background-color\":\"#FFE97A\"}, \"display\":{\"color\":\"#747606\"} }', 0, '2021-01-15 16:38:57'),
(4, 140, 159, 90, 99, 'HU', 'สูงมาก', 'พบแพทย์ ', ' (โรคความดันฯ ระยะเริ่มแรก)', 'assets/kiosk/emoji/icon_emo_over2.svg', '{ \r\n\"recommendation\":{\"color\":\"#FF8D00\"}, \r\n\"recommendation_2\":{\"color\":\"#BA957F\",\"width\":\"521px\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FAF4D5\"}, \r\n\"display\":{\"color\":\"#F26511\"} \r\n}', 0, '2021-01-15 16:49:01'),
(5, 160, 179, 100, 109, 'HH', 'สูงวิกฤต', 'พบแพทย์', '(โรคความดันฯ ระยะที่ 2)', 'assets/kiosk/emoji/icon_emo_over4.svg', '{ \"recommendation\":{\"color\":\"#FF0100\"}, \"recommendation_2\":{\"color\":\"#9A6565\"}, \"recommendation_bg\":{\"background-color\":\"#FFEBE3\"}, \"display\":{\"color\":\"#760606\"} }', 0, '2021-01-15 16:49:01'),
(6, 180, 999, 110, 999, 'HH', 'สูงวิกฤต', 'พบแพทย์', ' (โรคความดันฯ รุนแรง)', 'assets/kiosk/emoji/icon_emo_over4.svg', '{ \"recommendation\":{\"color\":\"#FF0100\"}, \"recommendation_2\":{\"color\":\"#9A6565\"}, \"recommendation_bg\":{\"background-color\":\"#FFEBE3\"}, \"display\":{\"color\":\"#760606\"} }', 0, '2021-01-15 16:52:31');

-- --------------------------------------------------------

--
-- Table structure for table `t_con_bmi`
--

CREATE TABLE `t_con_bmi` (
  `ID` int(11) NOT NULL,
  `Min` decimal(10,1) NOT NULL,
  `Max` decimal(10,1) NOT NULL,
  `Code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `display` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_con_bmi`
--

INSERT INTO `t_con_bmi` (`ID`, `Min`, `Max`, `Code`, `display`, `recommendation`, `Image`, `Delete_flag`, `Create_date`) VALUES
(1, 0.0, 18.4, 'L', 'ต่ำ', 'เกณฑ์น้ำหนักน้อยหรือผอม', 'assets/kiosk/emoji/icon_emo_down1.svg', 1, '2021-01-15 19:32:24'),
(2, 18.5, 22.9, 'N', 'ปกติ', 'อยู่ในเกณฑ์ปกติ', 'assets/kiosk/emoji/icon_emo_ok.svg', 1, '2021-01-15 19:32:24'),
(3, 23.0, 24.9, 'H', 'สูง', 'น้ำหนักเกิน', 'assets/kiosk/emoji/icon_emo_over1.svg', 1, '2021-01-15 19:32:24'),
(4, 25.0, 29.9, 'HU', 'สูงมาก', 'โรคอ้วนระดับที่ 1', 'assets/kiosk/emoji/icon_emo_over2.svg', 1, '2021-01-15 19:32:24'),
(5, 30.0, 99.0, 'HH', 'สูงวิกฤต', 'โรคอ้วนระดับที่ 2', 'assets/kiosk/emoji/icon_emo_over4.svg', 1, '2021-01-15 19:32:24');

-- --------------------------------------------------------

--
-- Table structure for table `t_con_oxygen`
--

CREATE TABLE `t_con_oxygen` (
  `ID` int(11) NOT NULL,
  `Min` int(11) NOT NULL,
  `Max` int(11) NOT NULL,
  `Code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `display` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Device_style` text NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_con_oxygen`
--

INSERT INTO `t_con_oxygen` (`ID`, `Min`, `Max`, `Code`, `display`, `recommendation`, `Image`, `Device_style`, `Delete_flag`, `Create_date`) VALUES
(1, 0, 87, 'LU', 'ต่ำมาก', '(ต่ำมากกว่าปกติ)', 'assets/kiosk/emoji/icon_emo_over4.svg', '{ \r\n\"recommendation\":{\"color\":\"#FF0100\"}, \r\n\"recommendation_num\":{\"color\":\"#FF0100\"}, \"recommendation_bg\":{\"background-color\":\"#FFEBE3\"}, \"display\":{\"color\":\"#760606\",\"width\":\"150px\",\"left\": \"853px\"} \r\n}', 0, '2021-01-15 17:38:16'),
(2, 88, 94, 'L', 'ต่ำ', '(ต่ำกว่าปกติ)', 'assets/kiosk/emoji/icon_emo_down1.svg', '{ \r\n\"recommendation\":{\"color\":\"#417588\",\"left\":\"906px\"}, \r\n\"recommendation_num\":{\"color\":\"#9A6565\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#E3F3FF\"}, \"display\":{\"color\":\"#00B0F0\",\"left\":\"833px\"} \r\n}', 0, '2021-01-15 17:38:16'),
(3, 95, 100, 'N', 'ปกติ', '(ปกติ)', 'assets/kiosk/emoji/icon_emo_ok.svg', '{ \r\n\"recommendation\":{\"color\":\"#448841\",\"left\":\"945px\"}, \r\n\"recommendation_num\":{\"color\":\"#70AD47\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"}, \r\n\"display\":{\"color\":\"#70AC47\",\"left\":\"893px\"} \r\n}', 0, '2021-01-15 17:38:16');

-- --------------------------------------------------------

--
-- Table structure for table `t_con_pulserate`
--

CREATE TABLE `t_con_pulserate` (
  `ID` int(11) NOT NULL,
  `Min` int(11) NOT NULL,
  `Max` int(11) NOT NULL,
  `Code` varchar(50) NOT NULL,
  `display` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Image` varchar(255) NOT NULL,
  `Device_style` text NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_con_pulserate`
--

INSERT INTO `t_con_pulserate` (`ID`, `Min`, `Max`, `Code`, `display`, `recommendation`, `Image`, `Device_style`, `Delete_flag`, `Create_date`) VALUES
(1, 0, 59, 'L', 'ต่ำ', '(ชีพจรเต้นช้ากว่าปกติ)', 'assets/kiosk/emoji/icon_emo_down1.svg', '{ \"recommendation\":{\"color\":\"#417588\"}, \"recommendation_num\":{\"color\":\"#9A6565\"}, \"recommendation_bg\":{\"background-color\":\"#E3F3FF\"}, \"display\":{\"color\":\"#00B0F0\"} }', 0, '2021-01-15 17:29:43'),
(2, 60, 100, 'N', 'ปกติ', '(ชีพจรเต้นปกติ)', 'assets/kiosk/emoji/icon_emo_ok.svg', '{ \"recommendation\":{\"color\":\"#448841\"}, \"recommendation_num\":{\"color\":\"#70AD47\"}, \"recommendation_bg\":{\"background-color\":\"#E3FFE3\"}, \"display\":{\"color\":\"#70AC47\"} }', 0, '2021-01-15 17:29:43'),
(3, 110, 999, 'H', 'สูง', '(ชีพจรเต้นสูงกว่าปกติ)', 'assets/kiosk/emoji/icon_emo_over4.svg', '{ \"recommendation\":{\"color\":\"#FF0100\"}, \"recommendation_num\":{\"color\":\"#FF0100\"}, \"recommendation_bg\":{\"background-color\":\"#FFEBE3\"}, \"display\":{\"color\":\"#760606\"} }', 0, '2021-01-15 17:29:43');

-- --------------------------------------------------------

--
-- Table structure for table `t_con_temperature`
--

CREATE TABLE `t_con_temperature` (
  `ID` int(11) NOT NULL,
  `Min` decimal(10,1) NOT NULL,
  `Max` decimal(10,1) NOT NULL,
  `Code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `display` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Device_style` text NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_con_temperature`
--

INSERT INTO `t_con_temperature` (`ID`, `Min`, `Max`, `Code`, `display`, `recommendation`, `Image`, `Device_style`, `Delete_flag`, `Create_date`) VALUES
(1, 0.0, 36.4, 'L', 'ต่ำ', '(ต่ำกว่าปกติ)', 'assets/kiosk/emoji/icon_emo_down1.svg', '{  \"recommendation\":{\"color\":\"#417588\",\"left\":\"978px\",\"width\":\"280px\"},  \"recommendation_num\":{\"color\":\"#9A6565\"},  \"recommendation_bg\":{\"background-color\":\"#E3F3FF\"},  \"display\":{\"color\":\"#00B0F0\",\"left\":\"800px\"} \r\n }', 0, '2021-01-15 17:08:24'),
(2, 36.5, 37.5, 'N', 'ปกติ', '(ปกติ)', 'assets/kiosk/emoji/icon_emo_ok.svg', '{ \"recommendation\":{\"color\":\"#448841\",\"left\":\"1015px\"},  \"recommendation_num\":{\"color\":\"#70AD47\"},  \"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},  \"display\":{\"color\":\"#70AC47\",\"left\":\"832px\"}  }', 0, '2021-01-15 17:08:24'),
(3, 37.6, 38.5, 'H', 'สูงมาก', '(มีไข้ต่ำ)', 'assets/kiosk/emoji/icon_emo_over1.svg', '{ \"recommendation\":{\"color\":\"#848841\"}, \"recommendation_num\":{\"color\":\"#747606\"}, \"recommendation_bg\":{\"background-color\":\"#FFE97A\"}, \"display\":{\"color\":\"#747606\",\"left\":\"868px\"} }', 0, '2021-01-15 17:19:47'),
(4, 38.6, 39.5, 'HU', 'สูงมาก', '(มีไข้ปานกลาง)', 'assets/kiosk/emoji/icon_emo_over2.svg', '{ \"recommendation\":{\"color\":\"#FF8D00\",\"width\":\"322px\"}, \"recommendation_num\":{\"color\":\"#F26511\"}, \"recommendation_bg\":{\"background-color\":\"#FAF4D5\"}, \"display\":{\"color\":\"#F26511\",\"left\":\"864px\"} }', 0, '2021-01-15 17:19:47'),
(5, 39.6, 99.9, 'HH', 'สูงวิกฤต', '(มีไข้สูง)', 'assets/kiosk/emoji/icon_emo_over4.svg', '{ \"recommendation\":{\"color\":\"#FF0100\"}, \"recommendation_2\":{\"color\":\"#9A6565\"}, \"recommendation_bg\":{\"background-color\":\"#FFEBE3\"}, \"display\":{\"color\":\"#760606\",\"left\":\"872px\"} }', 0, '2021-01-15 17:19:47');

-- --------------------------------------------------------

--
-- Table structure for table `t_devices`
--

CREATE TABLE `t_devices` (
  `id` int(11) NOT NULL,
  `device` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `active_flag` int(11) NOT NULL,
  `route` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_devices`
--

INSERT INTO `t_devices` (`id`, `device`, `active_flag`, `route`) VALUES
(1, 'เครื่องชั่งน้ำหนัก', 1, 'devices/weighing-machines'),
(2, 'เครื่องวัดความดันโลหิต', 1, 'devices/blood-pressure'),
(3, 'เครื่องวัดเปอร์เซ็นต์ออกซิเจนในเลือด อัตราการเต้นหัวใจ', 1, 'devices/pulse-oximeter'),
(4, 'เครื่องวัดอุณหภูมิร่างกาย', 1, 'devices/thermometer');

-- --------------------------------------------------------

--
-- Table structure for table `t_person`
--

CREATE TABLE `t_person` (
  `KIOSKCODE` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `PID` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `CID` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRENAME` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `LNAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SEX` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `BIRTH` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MSTATUS` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NATION` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `ABOGROUP` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RHGROUP` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PASSPORT` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TELEPHONE` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MOBILE` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHOTO_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHOTO_URL1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHOTO_URL2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHOTO_URL3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `D_UPDATE` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_person`
--

INSERT INTO `t_person` (`KIOSKCODE`, `PID`, `CID`, `PRENAME`, `NAME`, `LNAME`, `SEX`, `BIRTH`, `MSTATUS`, `NATION`, `ABOGROUP`, `RHGROUP`, `PASSPORT`, `TELEPHONE`, `MOBILE`, `PHOTO_ID`, `PHOTO_URL1`, `PHOTO_URL2`, `PHOTO_URL3`, `D_UPDATE`) VALUES
('1000', 'ec32ef4b0548a66c0b65519c56a85c3b', '1209700350000', 'นาย', 'สมชาย', 'ดีใจ', '', '19910527', NULL, '', NULL, NULL, NULL, NULL, NULL, 'http://128.199.73.202/hii-asset/1209700000.png', NULL, NULL, NULL, '2021-01-20 10:45:02');

-- --------------------------------------------------------

--
-- Table structure for table `t_service`
--

CREATE TABLE `t_service` (
  `KIOSKCODE` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `USER_TYPE` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `PID` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_SERV` date NOT NULL,
  `TIME_SERV` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `BTEMP` decimal(10,2) DEFAULT NULL,
  `BTEMPCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SBP` int(11) DEFAULT NULL,
  `DBP` int(11) DEFAULT NULL,
  `BPCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PR` int(11) DEFAULT NULL,
  `PRCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RR` int(11) DEFAULT NULL,
  `RRCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OXYGEN` int(11) DEFAULT NULL,
  `OXYGENCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WEIGHT` int(11) DEFAULT NULL,
  `HEIGHT` int(11) DEFAULT NULL,
  `BMI` int(11) DEFAULT NULL,
  `BMICODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BMR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `D_UPDATE` date NOT NULL,
  `SESSION_ID` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_service`
--

INSERT INTO `t_service` (`KIOSKCODE`, `USER_TYPE`, `PID`, `DATE_SERV`, `TIME_SERV`, `BTEMP`, `BTEMPCODE`, `SBP`, `DBP`, `BPCODE`, `PR`, `PRCODE`, `RR`, `RRCODE`, `OXYGEN`, `OXYGENCODE`, `WEIGHT`, `HEIGHT`, `BMI`, `BMICODE`, `BMR`, `D_UPDATE`, `SESSION_ID`) VALUES
('1000', '1', '', '2021-01-19', '15:59:', 0.00, '', 0, 0, '', 0, '', 82, '', 0, '', 0, 0, 0, '', '', '2021-01-19', '0Ppsi71xo6NqQ'),
('1000', '1', '', '2021-01-20', '14:05:', 0.00, '', 0, 0, '', 0, '', 0, '', 0, '', 0, 0, 0, '', '', '2021-01-20', '0r9viZGt6IPyG'),
('1000', '1', '', '2021-01-20', '14:48:', 36.20, '', 0, 0, '', 0, '', 0, '', 0, '', 0, 0, 0, '', '', '2021-01-20', '32uqJO6XnixwO'),
('1000', '1', '', '2021-01-20', '12:33:', 0.00, '', 132, 87, '', 120, '', 0, '', 0, '', 0, 0, 0, '', '', '2021-01-20', '33IvtYJ6W8YXc'),
('1000', '1', '9f353a07f70857fca99b7bfa88045e4c', '2021-01-19', '11:11:', 0.00, '', 125, 82, '', 80, '', 0, '', 0, '', 0, 0, 0, '', '', '2021-01-19', '4kz9VopVOdhWT'),
('1000', '1', '', '2021-01-19', '17:55:', 0.00, '', 125, 82, '', 112, '', 0, '', 0, '', 0, 0, 0, '', '', '2021-01-19', '5sd3LWDUWK1ZQ'),
('1000', '1', '', '2021-01-20', '14:35:', 37.80, '', 150, 92, '', 50, '', 82, '', 0, '', 0, 0, 0, '', '', '2021-01-20', '63a2xvTDOkm9Y'),
('1000', '1', '', '2021-01-19', '20:13:', 0.00, '', 125, 82, '', 40, '', 0, '', 0, '', 0, 0, 0, '', '', '2021-01-19', '77xTQWbLlcygc'),
('1000', '1', '', '2021-01-19', '16:05:', 0.00, '', 0, 0, '', 0, '', 82, '', 0, '', 0, 0, 0, '', '', '2021-01-19', '7i2dfrLeDECYi'),
('1000', '1', '9f353a07f70857fca99b7bfa88045e4c', '2021-01-18', '14:32:', 0.00, '', 0, 0, '', 0, '', 0, '', 0, '', 0, 0, 0, '', '', '2021-01-18', '8jswbyoO1DjmF'),
('1000', '1', '', '2021-01-20', '00:02:', 35.90, '', 0, 0, '', 0, '', 0, '', 0, '', 0, 0, 0, '', '', '2021-01-20', 'a7E5E7rRsWM84'),
('1000', '1', '', '2021-01-19', '18:13:', 0.00, '', 125, 82, '', 80, '', 0, '', 0, '', 0, 0, 0, '', '', '2021-01-19', 'dZkeaOyaQkSrT'),
('1000', '1', '', '2021-01-18', '21:15:', 0.00, '', 125, 82, '', 80, '', 0, '', 0, '', 0, 0, 0, '', '', '2021-01-18', 'FNFu3WJ4uVPiX'),
('1000', '1', '', '2021-01-19', '23:33:', 35.90, '', 0, 0, '', 0, '', 0, '', 0, '', 0, 0, 0, '', '', '2021-01-19', 'g4tWDWJJCXAAq'),
('1000', '1', '', '2021-01-20', '10:44:', 0.00, '', 0, 0, '', 0, '', 82, '', 0, '', 0, 0, 0, '', '', '2021-01-20', 'GAyWqnFx3kJoz'),
('1000', '1', '9f353a07f70857fca99b7bfa88045e4c', '2021-01-20', '16:11:', 0.00, '', 132, 87, '', 50, '', 0, '', 0, '', 0, 0, 0, '', '', '2021-01-20', 'GdiTy8OzImrks'),
('1000', '1', '9f353a07f70857fca99b7bfa88045e4c', '2021-01-18', '19:17:', 0.00, '', 0, 0, '', 0, '', 0, '', 0, '', 0, 0, 0, '', '', '2021-01-18', 'GMbFNSF4hfCMS'),
('1000', '1', '9f353a07f70857fca99b7bfa88045e4c', '2021-01-18', '16:39:', 0.00, '', 0, 0, '', 0, '', 82, '', 0, '', 0, 0, 0, '', '', '2021-01-18', 'hIVfNoLBD13Gm'),
('1000', '1', '', '2021-01-20', '14:32:', 0.00, '', 0, 0, '', 0, '', 0, '', 0, '', 0, 0, 0, '', '', '2021-01-20', 'HKCPuuhK5yy0P'),
('1000', '1', '9f353a07f70857fca99b7bfa88045e4c', '2021-01-20', '14:00:', 0.00, '', 190, 200, '', 120, '', 0, '', 0, '', 0, 0, 0, '', '', '2021-01-20', 'Ht96PidJYNt5a'),
('1000', '1', '', '2021-01-20', '16:07:', 0.00, '', 132, 87, '', 50, '', 0, '', 0, '', 0, 0, 0, '', '', '2021-01-20', 'kCWzC1ilssPRz'),
('1000', '1', '', '2021-01-20', '15:17:', 0.00, '', 0, 0, '', 0, '', 82, '', 0, '', 0, 0, 0, '', '', '2021-01-20', 'kebJMVAI2nAqb'),
('1000', '1', '', '2021-01-18', '16:28:', 0.00, '', 0, 0, '', 0, '', 0, '', 0, '', 0, 0, 0, '', '', '2021-01-18', 'KF9IrVdGzoE46'),
('1000', '1', '', '2021-01-20', '13:59:', 0.00, '', 190, 200, '', 120, '', 0, '', 0, '', 0, 0, 0, '', '', '2021-01-20', 'Lo96wjj6QTvR0'),
('1000', '1', '', '2021-01-19', '18:33:', 0.00, '', 125, 82, '', 40, '', 0, '', 0, '', 0, 0, 0, '', '', '2021-01-19', 'M66hHS3tTBFOA'),
('1000', '1', '9f353a07f70857fca99b7bfa88045e4c', '2021-01-18', '16:55:', 0.00, '', 0, 0, '', 0, '', 82, '', 0, '', 0, 0, 0, '', '', '2021-01-18', 'mDuk96F633Qng'),
('1000', '1', '', '2021-01-19', '18:30:', 0.00, '', 125, 82, '', 80, '', 0, '', 0, '', 0, 0, 0, '', '', '2021-01-19', 'MpYCLnIchGDCA'),
('1000', '1', '', '2021-01-20', '11:53:', 0.00, '', 125, 82, '', 40, '', 82, '', 0, '', 0, 0, 0, '', '', '2021-01-20', 'nfwfAboFfzeoi'),
('1000', '1', '', '2021-01-18', '17:04:', 35.90, '', 0, 0, '', 0, '', 0, '', 0, '', 0, 0, 0, '', '', '2021-01-18', 'nj7xhh4C8ui30'),
('1000', '1', '', '2021-01-20', '14:46:', 36.80, '', 0, 0, '', 0, '', 0, '', 0, '', 0, 0, 0, '', '', '2021-01-20', 'NkETZsWqMDXVc'),
('1000', '1', '', '2021-01-19', '14:56:', 0.00, '', 125, 82, '', 80, '', 0, '', 0, '', 0, 0, 0, '', '', '2021-01-19', 'OkVFKxalw0zmc'),
('1000', '1', '', '2021-01-18', '15:52:', 35.90, '', 0, 0, '', 0, '', 0, '', 0, '', 0, 0, 0, '', '', '2021-01-18', 'PjMV6iv9rGGyH'),
('1000', '1', '', '2021-01-20', '14:52:', 0.00, '', 132, 87, '', 50, '', 0, '', 0, '', 0, 0, 0, '', '', '2021-01-20', 'pZeCVKLsGLjm9'),
('1000', '1', '', '2021-01-19', '22:35:', 0.00, '', 0, 0, '', 0, '', 82, '', 0, '', 0, 0, 0, '', '', '2021-01-19', 'qAAd0ki3n1J6g'),
('1000', '1', '9f353a07f70857fca99b7bfa88045e4c', '2021-01-20', '14:12:', 40.20, '', 170, 105, '', 120, '', 82, '', 0, '', 0, 0, 0, '', '', '2021-01-20', 'S8CcXuaYHPhkf'),
('1000', '1', 'ec32ef4b0548a66c0b65519c56a85c3b', '2021-01-20', '17:43:', 37.80, '', 0, 0, '', 0, '', 0, '', 0, '', 0, 0, 0, '', '', '2021-01-20', 'SI8hUbaYlWnTq'),
('1000', '1', '', '2021-01-20', '11:42:', 40.20, '', 0, 0, '', 0, '', 0, '', 0, '', 0, 0, 0, '', '', '2021-01-20', 'VJEsMD6AWWwXE'),
('1000', '1', '', '2021-01-14', '04:12:', 35.94, '', 125, 95, '', 80, '', 82, '', 97, '', 0, 0, 0, '', '', '2021-01-14', 'XhaiekkJvnCML'),
('1000', '1', '', '2021-01-18', '16:53:', 0.00, '', 0, 0, '', 0, '', 82, '', 0, '', 0, 0, 0, '', '', '2021-01-18', 'Yb8rEHDWrR8DH'),
('1000', '1', '', '2021-01-19', '13:39:', 0.00, '', 125, 82, '', 80, '', 0, '', 0, '', 0, 0, 0, '', '', '2021-01-19', 'yWJYfXoIQF4wZ'),
('1000', '1', '', '2021-01-15', '18:01:', 0.00, '', 0, 0, '', 0, '', 82, '', 0, '', 0, 0, 0, '', '', '2021-01-15', 'Z0LHFFnhWwxvJ'),
('1000', '1', '', '2021-01-19', '11:38:', 0.00, '', 125, 82, '', 80, '', 82, '', 0, '', 0, 0, 0, '', '', '2021-01-19', 'zcQ1dOn4nzuUt');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_con_blood_pressure`
--
ALTER TABLE `t_con_blood_pressure`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_con_bmi`
--
ALTER TABLE `t_con_bmi`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_con_oxygen`
--
ALTER TABLE `t_con_oxygen`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_con_pulserate`
--
ALTER TABLE `t_con_pulserate`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_con_temperature`
--
ALTER TABLE `t_con_temperature`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_devices`
--
ALTER TABLE `t_devices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_person`
--
ALTER TABLE `t_person`
  ADD PRIMARY KEY (`KIOSKCODE`,`PID`);

--
-- Indexes for table `t_service`
--
ALTER TABLE `t_service`
  ADD PRIMARY KEY (`SESSION_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_con_blood_pressure`
--
ALTER TABLE `t_con_blood_pressure`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `t_con_bmi`
--
ALTER TABLE `t_con_bmi`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `t_con_oxygen`
--
ALTER TABLE `t_con_oxygen`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_con_pulserate`
--
ALTER TABLE `t_con_pulserate`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_con_temperature`
--
ALTER TABLE `t_con_temperature`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `t_devices`
--
ALTER TABLE `t_devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
