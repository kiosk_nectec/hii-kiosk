-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 01, 2021 at 08:13 AM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kios_nectec`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_address`
--

CREATE TABLE `t_address` (
  `KIOSKCODE` varchar(9) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `PID` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `CID` varchar(13) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ADDRESSTYPE` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ROOMNO` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `CONDO` varchar(75) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `HOUSENO` varchar(75) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOISUB` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOIMAIN` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ROAD` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `VILLNAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `VILLAGE` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `TAMBON` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `AMPUR` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `CHANGWAT` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `D_UPDATE` datetime NOT NULL,
  `Delete_flag` int(10) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_address`
--

INSERT INTO `t_address` (`KIOSKCODE`, `PID`, `CID`, `ADDRESSTYPE`, `ROOMNO`, `CONDO`, `HOUSENO`, `SOISUB`, `SOIMAIN`, `ROAD`, `VILLNAME`, `VILLAGE`, `TAMBON`, `AMPUR`, `CHANGWAT`, `D_UPDATE`, `Delete_flag`, `Create_date`) VALUES
('', '631a6302da8adba', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-08 16:20:14'),
('', '9f353a07f70857f', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-08 17:11:32'),
('', '885e55b0eb6cbd5', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-18 15:12:03'),
('', 'ec32ef4b0548a66', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-20 17:42:35'),
('', '233f19ed2568836', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-20 21:04:32'),
('', '303e49ae9ce5c54', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-26 18:34:42'),
('', '52da11ad4c6c786', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-27 19:35:24'),
('', '4420b1389fa32f2', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-27 19:35:26'),
('', 'add905a63319cb7', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-27 19:53:38'),
('', '55118dea8531012', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-02-05 14:16:49'),
('', 'b3984bf07036349', '', '', 'test', NULL, '9/2', '', '', '', NULL, 'หม', 'ตำ', 'อำ', 'จั', '0000-00-00 00:00:00', 0, '2021-02-18 17:07:56');

-- --------------------------------------------------------

--
-- Table structure for table `t_config`
--

CREATE TABLE `t_config` (
  `ID` int(11) NOT NULL,
  `RNAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `AGE` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_con_blood_pressure`
--

CREATE TABLE `t_con_blood_pressure` (
  `ID` int(11) NOT NULL,
  `SBP_min` int(11) NOT NULL,
  `SBP_max` int(11) NOT NULL,
  `DBP_min` int(11) NOT NULL,
  `DBP_max` int(11) NOT NULL,
  `Code` varchar(50) NOT NULL,
  `display` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation_2` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Importance` int(11) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `Device_style` text NOT NULL,
  `Results_style` text NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_con_blood_pressure`
--

INSERT INTO `t_con_blood_pressure` (`ID`, `SBP_min`, `SBP_max`, `DBP_min`, `DBP_max`, `Code`, `display`, `recommendation`, `recommendation_2`, `Importance`, `Image`, `Device_style`, `Results_style`, `Delete_flag`, `Create_date`) VALUES
(1, 0, 119, 0, 79, 'N', 'เหมาะสม', 'ควบคุมอาหาร ออกกำลังกาย วัดความดันโลหิตอย่างสม่ำเสมอ', '', 1, 'assets/kiosk/emoji/icon_emo_ok.png', '{\r\n\"recommendation\":{\"top\":\"509px\",\"line-height\":\"55px\",\"color\":\"#448841\"},\r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},\r\n\"display\":{\"color\":\"#70AC47\",\"left\":\"895px\",\"width\":\"400px\",\"font-size\":\"85px\",\"top\":\"420px\"},\r\n\"emojiposition\":{ \"left\":\"730px\",\"top\":\"375px\"}\r\n}', '{\r\n\"recommendation_num\":{\"color\":\"#70AD47\",\"top\":\"513px\",\"left\":\"1412px\"},\r\n\"recommendation\":{\"top\":\"509px\",\"line-height\":\"55px\",\"color\":\"#448841\"},\r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},\r\n\"display\":{\"color\":\"#70AC47\"}\r\n}', 0, '2021-01-15 16:36:19'),
(2, 120, 129, 80, 84, 'N', 'ปกติ', 'ควบคุมอาหาร ออกกำลังกาย วัดความดันโลหิตอย่างสม่ำเสมอ', '', 2, 'assets/kiosk/emoji/icon_emo_ok.png', '{\r\n\"recommendation\":{\"top\":\"509px\",\"line-height\":\"55px\",\"color\":\"#448841\"},\r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},\r\n\"display\":{\"color\":\"#70AC47\",\"left\":\"1035px\",\"width\":\"400px\",\"font-size\":\"85px\",\"top\":\"420px\"},\r\n\"emojiposition\":{ \"left\":\"865px\",\"top\":\"375px\"}\r\n}', '{\r\n\"recommendation_num\":{\"color\":\"#70AD47\",\"top\":\"513px\",\"left\":\"1412px\"},\r\n\"recommendation\":{\"top\":\"509px\",\"line-height\":\"55px\",\"color\":\"#448841\"},\r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},\r\n\"display\":{\"color\":\"#70AC47\"}\r\n}', 0, '2021-01-15 16:36:27'),
(3, 130, 139, 85, 89, 'H', 'สูง', 'ปรึกษาแพทย์', '', 3, 'assets/kiosk/emoji/icon_emo_over1.png', '{ \r\n\"recommendation\":{\"color\":\"#848841\",\"left\":\"1000px\",\"font-size\": \"85px\",\"top\":\"555px\"},\r\n\"recommendation_2\":{\"color\":\"\"},\r\n\"recommendation_bg\":{\"background-color\":\"#FFE97A\"},\r\n\"display\":{\"color\":\"#747606\",\"left\":\"1275px\",\"width\":\"400px\",\"font-size\":\"85px\",\"top\":\"420px\"},\r\n\"emojiposition\":{ \"left\":\"910px\",\"top\":\"375px\"}\r\n ', '{\r\n\"recommendation_num\":{\"color\":\"#747606\",\"top\":\"513px\",\"left\":\"1412px\"},\r\n\"recommendation\":{\"color\":\"#848841\"}, \r\n\"recommendation_2\":{\"color\":\"\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FFE97A\"}, \r\n\"display\":{\"color\":\"#747606\"} \r\n}', 0, '2021-01-15 16:38:57'),
(4, 140, 159, 90, 99, 'HU', 'สูงมาก', 'พบแพทย์ ', ' (โรคความดันฯ ระยะเริ่มแรก)', 4, 'assets/kiosk/emoji/icon_emo_over2.png', '{ \r\n\"recommendation\":{\"color\":\"#FF8D00\"}, \r\n\"recommendation_2\":{\"color\":\"#BA957F\",\"width\":\"521px\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FAF4D5\"}, \r\n\"display\":{\"color\":\"#F26511\",\"left\":\"950px\",\"width\":\"400px\",\"font-size\":\"85px\",\"top\":\"420px\"},\r\n\"emojiposition\":{ \"left\":\"770px\",\"top\":\"375px\"}\r\n}', '{\r\n\"recommendation_num\":{\"color\":\"#F26511\",\"top\":\"513px\",\"left\":\"1412px\"},\r\n\"recommendation\":{\"color\":\"#FF8D00\"}, \r\n\"recommendation_2\":{\"color\":\"#BA957F\",\"width\":\"521px\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FAF4D5\"}, \r\n\"display\":{\"color\":\"#F26511\"} \r\n}', 0, '2021-01-15 16:49:01'),
(5, 160, 179, 100, 109, 'HH', 'สูงวิกฤต', 'พบแพทย์', '(โรคความดันฯ ระยะที่ 2)', 5, 'assets/kiosk/emoji/icon_emo_over4.png', '{ \"recommendation\":{\"color\":\"#FF0100\",\"left\":\"1035px\"},\r\n \"recommendation_2\":{\"color\":\"#9A6565\",\"left\":\"1250px\"},\r\n \"recommendation_bg\":{\"background-color\":\"#FFEBE3\"},\r\n \"display\":{\"color\":\"#760606\",\"left\":\"870px\",\"width\":\"400px\",\"font-size\":\"85px\",\"top\":\"420px\"},\r\n\"emojiposition\":{ \"left\":\"690px\",\"top\":\"375px\"}\r\n}', '{\r\n\"recommendation_num\":{\"color\":\"#FF0100\",\"top\":\"513px\",\"left\":\"1412px\"},\r\n\"recommendation\":{\"color\":\"#FF0100\"}, \r\n\"recommendation_2\":{\"color\":\"#9A6565\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FFEBE3\"}, \r\n\"display\":{\"color\":\"#760606\"} \r\n}', 0, '2021-01-15 16:49:01'),
(6, 180, 999, 110, 999, 'HH', 'สูงวิกฤต', 'พบแพทย์', ' (โรคความดันฯ รุนแรง)', 6, 'assets/kiosk/emoji/icon_emo_over4.png', '\r\n{ \"recommendation\":{\"color\":\"#FF0100\",\"left\":\"1035px\"},\r\n \"recommendation_2\":{\"color\":\"#9A6565\",\"left\":\"1250px\"},\r\n \"recommendation_bg\":{\"background-color\":\"#FFEBE3\"},\r\n \"display\":{\"color\":\"#760606\",\"left\":\"870px\",\"width\":\"400px\",\"font-size\":\"85px\",\"top\":\"420px\"},\r\n\"emojiposition\":{ \"left\":\"690px\",\"top\":\"375px\"}\r\n}', '{\r\n\"recommendation_num\":{\"color\":\"#FF0100\",\"top\":\"513px\",\"left\":\"1412px\"},\r\n\"recommendation\":{\"color\":\"#FF0100\"}, \r\n\"recommendation_2\":{\"color\":\"#9A6565\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FFEBE3\"}, \r\n\"display\":{\"color\":\"#760606\"} \r\n}', 0, '2021-01-15 16:52:31');

-- --------------------------------------------------------

--
-- Table structure for table `t_con_bmi`
--

CREATE TABLE `t_con_bmi` (
  `ID` int(11) NOT NULL,
  `Min` decimal(10,1) NOT NULL,
  `Max` decimal(10,1) NOT NULL,
  `Code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `display` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Device_style` text NOT NULL,
  `Results_style` text NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_con_bmi`
--

INSERT INTO `t_con_bmi` (`ID`, `Min`, `Max`, `Code`, `display`, `recommendation`, `Image`, `Device_style`, `Results_style`, `Delete_flag`, `Create_date`) VALUES
(1, 0.0, 18.4, 'L', 'ต่ำ', 'เกณฑ์น้ำหนักน้อยหรือผอม', 'assets/kiosk/emoji/icon_emo_down1.png', '{ \r\n\"recommendation_num\":{\"color\":\"#9A6565\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#E3F3FF\"},\r\n\"display\":{\"color\":\"#00B0F0\"},\r\n\"emojiposition\":{ \"left\":\"777px\"}\r\n}\r\n', '{\r\n\"recommendation_num\":{\"color\":\"#9A6565\"} \r\n}', 0, '2021-01-15 19:32:24'),
(2, 18.5, 22.9, 'N', 'ปกติ', 'อยู่ในเกณฑ์ปกติ', 'assets/kiosk/emoji/icon_emo_ok.png', '{ \r\n\"recommendation_num\":{\"color\":\"#70AD47\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},\r\n\"display\":{\"color\":\"#70AC47\"},\r\n\"emojiposition\":{ \"left\":\"735px\"}\r\n}', '{\r\n\"recommendation_num\":{\"color\":\"#70AD47\"} \r\n}', 0, '2021-01-15 19:32:24'),
(3, 23.0, 24.9, 'H', 'สูง', 'น้ำหนักเกิน', 'assets/kiosk/emoji/icon_emo_over1.png', '{ \r\n\"recommendation_num\":{\"color\":\"#747606\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#E6E65F\"},\r\n\"display\":{\"color\":\"#747606\"},\r\n\"emojiposition\":{ \"left\":\"747px\"}\r\n}', '{\r\n\"recommendation_num\":{\"color\":\"#747606\"} \r\n}', 0, '2021-01-15 19:32:24'),
(4, 25.0, 29.9, 'HU', 'สูงมาก', 'โรคอ้วนระดับที่ 1', 'assets/kiosk/emoji/icon_emo_over2.png', '{ \r\n\"recommendation_num\":{\"color\":\"#F26511\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FAF4D5\"},\r\n\"display\":{\"color\":\"#F26511\"}\r\n}', '{\r\n\"recommendation_num\":{\"color\":\"#F26511\"} \r\n}', 0, '2021-01-15 19:32:24'),
(5, 30.0, 99.0, 'HH', 'สูงวิกฤต', 'โรคอ้วนระดับที่ 2', 'assets/kiosk/emoji/icon_emo_over4.png', '{ \r\n\"recommendation_num\":{\"color\":\"#FF0100\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FFEBE3\"},\r\n\"display\":{\"color\":\"#760606\"}\r\n}', '{\r\n\"recommendation_num\":{\"color\":\"#FF0100\"} \r\n}', 0, '2021-01-15 19:32:24');

-- --------------------------------------------------------

--
-- Table structure for table `t_con_oxygen`
--

CREATE TABLE `t_con_oxygen` (
  `ID` int(11) NOT NULL,
  `Min` int(11) NOT NULL,
  `Max` int(11) NOT NULL,
  `Code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `display` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Device_style` text NOT NULL,
  `Results_style` text NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_con_oxygen`
--

INSERT INTO `t_con_oxygen` (`ID`, `Min`, `Max`, `Code`, `display`, `recommendation`, `Image`, `Device_style`, `Results_style`, `Delete_flag`, `Create_date`) VALUES
(1, 0, 87, 'LU', 'ต่ำมาก', '(ต่ำมากกว่าปกติ)', 'assets/kiosk/emoji/icon_emo_over4.png', '{ \r\n\"recommendation\":{\"color\":\"#FF0100\",\"left\":\"810px\",\"width\":\"1000px\"}, \r\n\"recommendation_num\":{\"color\":\"#FF0100\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FFEBE3\"}, \r\n\"display\":{\"color\":\"#760606\",\"left\":\"1085px\",\"width\":\"440px\",\"top\":\"600px\",\"font-size\":\"150px\"},\r\n\"emojiposition\":{ \"left\":\"1585px\"} \r\n}', '{ \r\n\"recommendation_num\":{\"color\":\"#FF0100\"}\r\n}', 0, '2021-01-15 17:38:16'),
(2, 88, 94, 'L', 'ต่ำ', '(ต่ำกว่าปกติ)', 'assets/kiosk/emoji/icon_emo_down1.png', '{ \r\n\"recommendation\":{\"color\":\"#417588\",\"left\":\"720px\",\"width\":\"1000px\"}, \r\n\"recommendation_num\":{\"color\":\"#9A6565\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#E3F3FF\"}, \r\n\"display\":{\"color\":\"#00B0F0\",\"left\":\"1055px\",\"width\":\"260px\",\"top\":\"600px\",\"font-size\":\"150px\"},\r\n\"emojiposition\":{ \"left\":\"1365px\"} \r\n}', '{ \r\n\"recommendation_num\":{\"color\":\"#9A6565\"}\r\n}', 0, '2021-01-15 17:38:16'),
(3, 95, 100, 'N', 'ปกติ', '(ปกติ)', 'assets/kiosk/emoji/icon_emo_ok.png', '{ \r\n\"recommendation\":{\"color\":\"#448841\",\"left\":\"855px\",\"width\":\"1000px\"}, \r\n\"recommendation_num\":{\"color\":\"#448841\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"}, \r\n\"display\":{\"color\":\"#70AC47\",\"left\":\"1115px\",\"width\":\"260px\",\"top\":\"600px\",\"font-size\":\"150px\"},\r\n\"emojiposition\":{ \"left\":\"1490px\"} \r\n}', '{ \r\n\"recommendation_num\":{\"color\":\"#70AD47\"}\r\n}', 0, '2021-01-15 17:38:16');

-- --------------------------------------------------------

--
-- Table structure for table `t_con_pulserate`
--

CREATE TABLE `t_con_pulserate` (
  `ID` int(11) NOT NULL,
  `Min` int(11) NOT NULL,
  `Max` int(11) NOT NULL,
  `Code` varchar(50) NOT NULL,
  `display` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Image` varchar(255) NOT NULL,
  `Device_style` text NOT NULL,
  `Results_style` text NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_con_pulserate`
--

INSERT INTO `t_con_pulserate` (`ID`, `Min`, `Max`, `Code`, `display`, `recommendation`, `Image`, `Device_style`, `Results_style`, `Delete_flag`, `Create_date`) VALUES
(1, 0, 59, 'L', 'ต่ำ', '(ชีพจรเต้นช้ากว่าปกติ)', 'assets/kiosk/emoji/icon_emo_down1.png', '{ \"recommendation\":{\"color\":\"#417588\",\"left\":\"1310px\"},  \"recommendation_num\":{\"color\":\"#9A6565\"},  \"recommendation_bg\":{\"background-color\":\"#E3F3FF\"},  \"display\":{\"color\":\"#00B0F0\",\"left\":\"810px\",\"width\":\"250px\",\"font-size\":\"100px\"},\r\n\"emojiposition\":{ \"left\":\"600px\",\"top\":\"650px\"} \r\n}', '{ \"recommendation\":{\"color\":\"#417588\"}, \"recommendation_num\":{\"color\":\"#9A6565\"}, \"recommendation_bg\":{\"background-color\":\"#E3F3FF\"}, \"display\":{\"color\":\"#00B0F0\"} }', 0, '2021-01-15 17:29:43'),
(2, 60, 100, 'N', 'ปกติ', '(ชีพจรเต้นปกติ)', 'assets/kiosk/emoji/icon_emo_ok.png', '{ \"recommendation\":{\"color\":\"#448841\",\"left\":\"1335px\",\"width\":\"435px\",\"font-size\":\"60px\"},\r\n \"recommendation_num\":{\"color\":\"#70AD47\"},\r\n \"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},\r\n \"display\":{\"color\":\"#70AC47\",\"left\":\"725px\",\"width\":\"250px\",\"font-size\":\"100px\"},\r\n\"emojiposition\":{ \"left\":\"495px\",\"top\":\"650px\"} \r\n}', '{ \"recommendation\":{\"color\":\"#448841\"}, \"recommendation_num\":{\"color\":\"#70AD47\"}, \"recommendation_bg\":{\"background-color\":\"#E3FFE3\"}, \"display\":{\"color\":\"#70AC47\"} }', 0, '2021-01-15 17:29:43'),
(3, 110, 999, 'H', 'สูง', '(ชีพจรเต้นสูงกว่าปกติ)', 'assets/kiosk/emoji/icon_emo_over4.png', '{ \"recommendation\":{\"color\":\"#FF0100\",\"left\":\"1315px\"}, \r\n\"recommendation_num\":{\"color\":\"#FF0100\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FFEBE3\"}, \r\n\"display\":{\"color\":\"#760606\",\"left\":\"800px\",\"width\":\"250px\",\"font-size\":\"100px\"},\r\n\"emojiposition\":{ \"left\":\"600px\",\"top\":\"650px\"} \r\n }', '{ \"recommendation\":{\"color\":\"#FF0100\"}, \"recommendation_num\":{\"color\":\"#FF0100\"}, \"recommendation_bg\":{\"background-color\":\"#FFEBE3\"}, \"display\":{\"color\":\"#760606\"} }', 0, '2021-01-15 17:29:43');

-- --------------------------------------------------------

--
-- Table structure for table `t_con_temperature`
--

CREATE TABLE `t_con_temperature` (
  `ID` int(11) NOT NULL,
  `Min` decimal(10,1) NOT NULL,
  `Max` decimal(10,1) NOT NULL,
  `Code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `display` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Device_style` text NOT NULL,
  `Results_style` text NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_con_temperature`
--

INSERT INTO `t_con_temperature` (`ID`, `Min`, `Max`, `Code`, `display`, `recommendation`, `Image`, `Device_style`, `Results_style`, `Delete_flag`, `Create_date`) VALUES
(1, 0.0, 36.4, 'L', 'ต่ำ', '(ต่ำกว่าปกติ)', 'assets/kiosk/emoji/icon_emo_down1.png', '{  \r\n\"recommendation\":{\"color\":\"#417588\",\"left\":\"990px\",\"width\":\"655px\"},  \"recommendation_num\":{\"color\":\"#9A6565\"},  \r\n\"recommendation_bg\":{\"background-color\":\"#E3F3FF\"},  \r\n\"display\":{\"color\":\"#00B0F0\",\"left\":\"895px\",\"width\":\"580px\",\"top\":\"645px\",\"font-size\":\"120px\"},\r\n\"emojiposition\":{ \"left\":\"1385px\",\"top\":\"630px\"}\r\n }', '{  \r\n \"recommendation_num\":{\"color\":\"#9A6565\"}\r\n}', 0, '2021-01-15 17:08:24'),
(2, 36.5, 37.5, 'N', 'ปกติ', '(ปกติ)', 'assets/kiosk/emoji/icon_emo_ok.png', '{ \"recommendation\":{\"color\":\"#448841\",\"left\":\"1165px\"},  \r\n\"recommendation_num\":{\"color\":\"#70AD47\"},  \r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},  \r\n\"display\":{\"color\":\"#70AC47\",\"left\":\"945px\",\"width\":\"580px\",\"top\":\"645px\",\"font-size\":\"120px\"},\r\n\"emojiposition\":{ \"left\":\"1465px\",\"top\":\"630px\"}\r\n }', '{   \r\n\"recommendation_num\":{\"color\":\"#70AD47\"}\r\n}', 0, '2021-01-15 17:08:24'),
(3, 37.6, 38.5, 'H', 'สูง', '(มีไข้ต่ำ)', 'assets/kiosk/emoji/icon_emo_over1.png', '{ \"recommendation\":{\"color\":\"#848841\",\"l1eft\":\"1180px\",\"width\":\"460px\"}, \r\n\"recommendation_num\":{\"color\":\"#747606\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FFE97A\"}, \r\n\"display\":{\"color\":\"#747606\",\"left\":\"900px\",\"width\":\"580px\",\"top\":\"645px\",\"font-size\":\"120px\"},\r\n\"emojiposition\":{ \"left\":\"1405px\",\"top\":\"630px\"}\r\n }', '{ \r\n\"recommendation_num\":{\"color\":\"#747606\"}\r\n}', 0, '2021-01-15 17:19:47'),
(4, 38.6, 39.5, 'HU', 'สูงมาก', '(มีไข้ปานกลาง)', 'assets/kiosk/emoji/icon_emo_over2.png', '{ \"recommendation\":{\"color\":\"#FF8D00\",\"width\":\"755px\",\"left\":\"985px\"}, \r\n\"recommendation_num\":{\"color\":\"#F26511\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FAF4D5\"}, \r\n\"display\":{\"color\":\"#F26511\",\"left\":\"1005px\",\"width\":\"580px\",\"top\":\"645px\",\"font-size\":\"120px\"},\r\n\"emojiposition\":{ \"left\":\"1550px\",\"top\":\"630px\"}\r\n }', '{  \r\n\"recommendation_num\":{\"color\":\"#F26511\"} \r\n}', 0, '2021-01-15 17:19:47'),
(5, 39.6, 99.9, 'HH', 'สูงวิกฤต', '(มีไข้สูง)', 'assets/kiosk/emoji/icon_emo_over4.png', '{ \"recommendation\":{\"color\":\"#FF0100\",\"left\":\"1160px\",\"width\":\"470px\"}, \r\n\"recommendation_2\":{\"color\":\"#9A6565\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FFEBE3\"}, \r\n\"display\":{\"color\":\"#760606\",\"left\":\"1055px\",\"width\":\"580px\",\"top\":\"645px\",\"font-size\":\"120px\"},\r\n\"emojiposition\":{ \"left\":\"1610px\",\"top\":\"630px\"}\r\n }', '{ \r\n\"recommendation_num\":{\"color\":\"#FF0100\"}\r\n}', 0, '2021-01-15 17:19:47');

-- --------------------------------------------------------

--
-- Table structure for table `t_devices`
--

CREATE TABLE `t_devices` (
  `id` int(11) NOT NULL,
  `device` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `active_flag` int(11) NOT NULL,
  `route` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_devices`
--

INSERT INTO `t_devices` (`id`, `device`, `active_flag`, `route`) VALUES
(1, 'เครื่องชั่งน้ำหนัก', 1, 'devices/weighing-machines'),
(2, 'เครื่องวัดความดันโลหิต', 1, 'devices/blood-pressure'),
(3, 'เครื่องวัดเปอร์เซ็นต์ออกซิเจนในเลือด อัตราการเต้นหัวใจ', 1, 'devices/pulse-oximeter'),
(4, 'เครื่องวัดอุณหภูมิร่างกาย', 1, 'devices/thermometer');

-- --------------------------------------------------------

--
-- Table structure for table `t_person`
--

CREATE TABLE `t_person` (
  `KIOSKCODE` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `PID` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `CID` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRENAME` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `LNAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SEX` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `BIRTH` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MSTATUS` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NATION` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `ABOGROUP` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RHGROUP` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PASSPORT` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TELEPHONE` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MOBILE` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHOTO_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHOTO_URL1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHOTO_URL2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHOTO_URL3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `D_UPDATE` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_person`
--

INSERT INTO `t_person` (`KIOSKCODE`, `PID`, `CID`, `PRENAME`, `NAME`, `LNAME`, `SEX`, `BIRTH`, `MSTATUS`, `NATION`, `ABOGROUP`, `RHGROUP`, `PASSPORT`, `TELEPHONE`, `MOBILE`, `PHOTO_ID`, `PHOTO_URL1`, `PHOTO_URL2`, `PHOTO_URL3`, `D_UPDATE`) VALUES
('1000', 'b3984bf070363495e16fc4bb02c68cc3', '1209700350000', '003', 'สมชาย', 'ดีใจ', '1', '19910527', NULL, '', NULL, NULL, NULL, NULL, NULL, 'http://128.199.73.202/hii-asset/1209700000.png', NULL, NULL, NULL, '2021-03-01 07:35:33');

-- --------------------------------------------------------

--
-- Table structure for table `t_request_log`
--

CREATE TABLE `t_request_log` (
  `id` int(11) NOT NULL,
  `request_url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `request_time` datetime NOT NULL,
  `response_time` datetime NOT NULL,
  `status_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `error_message` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `response_message` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_request_log`
--

INSERT INTO `t_request_log` (`id`, `request_url`, `request_time`, `response_time`, `status_code`, `error_message`, `response_message`) VALUES
(1, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-02-25 13:51:16', '2021-02-25 13:51:16', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(2, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-02-25 13:51:21', '2021-02-25 13:51:22', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(3, 'http://128.199.73.202/middleware/read/bloodoxygenmeter', '2021-02-25 13:51:51', '2021-02-25 13:51:51', '200', '', '{\n    \"data\": [\n        {\n            \"PRbpm\": \"82\",\n            \"SpO2%\": \"97\"\n        }\n    ]\n}'),
(4, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-02-25 14:48:57', '2021-02-25 14:48:57', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(5, 'http://128.199.73.202/middleware/stop/bloodoxygenmeter', '2021-02-25 14:52:03', '2021-02-25 14:52:03', '200', '', '{\n    \"data\": [\n        {\n            \"status\": \"success\"\n        }\n    ]\n}'),
(6, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-02-25 14:52:06', '2021-02-25 14:52:06', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(7, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-02-25 14:55:24', '2021-02-25 14:55:24', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(8, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-02-25 14:55:32', '2021-02-25 14:55:32', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(9, 'http://128.199.73.202/middleware/read/readeridcard', '2021-02-25 14:56:17', '2021-02-25 14:56:18', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(10, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-02-25 14:56:38', '2021-02-25 14:56:38', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(11, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-02-25 14:56:43', '2021-02-25 14:56:43', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(12, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-02-25 14:56:54', '2021-02-25 14:56:54', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(13, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-02-25 15:05:46', '2021-02-25 15:05:46', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(14, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-02-25 15:11:03', '2021-02-25 15:11:03', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(15, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-02-25 15:11:07', '2021-02-25 15:11:07', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(16, 'http://128.199.73.202/middleware/stop/weighingmachine', '2021-02-25 15:11:09', '2021-02-25 15:11:09', '200', '', '{\n    \"data\": [\n        {\n            \"status\": \"success\"\n        }\n    ]\n}'),
(17, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-02-25 15:11:10', '2021-02-25 15:11:11', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(18, 'http://128.199.73.202/middleware/stop/weighingmachine', '2021-02-25 15:11:13', '2021-02-25 15:11:13', '200', '', '{\n    \"data\": [\n        {\n            \"status\": \"success\"\n        }\n    ]\n}'),
(19, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-02-25 15:11:14', '2021-02-25 15:11:14', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(20, 'http://128.199.73.202/middleware/read/readeridcard', '2021-02-25 15:11:21', '2021-02-25 15:11:21', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(21, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-02-25 15:11:32', '2021-02-25 15:11:33', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(22, 'http://128.199.73.202/middleware/stop/weighingmachine', '2021-02-25 15:11:34', '2021-02-25 15:11:34', '200', '', '{\n    \"data\": [\n        {\n            \"status\": \"success\"\n        }\n    ]\n}'),
(23, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-02-25 15:11:35', '2021-02-25 15:11:35', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(24, 'http://128.199.73.202/middleware/stop/weighingmachine', '2021-02-25 15:11:37', '2021-02-25 15:11:37', '200', '', '{\n    \"data\": [\n        {\n            \"status\": \"success\"\n        }\n    ]\n}'),
(25, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-02-25 15:11:38', '2021-02-25 15:11:39', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(26, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-02-25 15:18:19', '2021-02-25 15:18:20', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(27, 'http://128.199.73.202/middleware/read/bloodoxygenmeter', '2021-02-25 15:18:53', '2021-02-25 15:18:53', '200', '', '{\n    \"data\": [\n        {\n            \"PRbpm\": \"82\",\n            \"SpO2%\": \"97\"\n        }\n    ]\n}'),
(28, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-02-25 15:20:02', '2021-02-25 15:20:02', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(29, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-02-25 15:20:39', '2021-02-25 15:20:39', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(30, 'http://128.199.73.202/middleware/read/readeridcard', '2021-02-25 15:21:21', '2021-02-25 15:21:21', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(31, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-02-25 15:21:47', '2021-02-25 15:21:48', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(32, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-02-25 15:21:48', '2021-02-25 15:21:48', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(33, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-02-25 15:21:49', '2021-02-25 15:21:50', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(34, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-02-25 15:28:15', '2021-02-25 15:28:16', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(35, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-02-25 15:28:48', '2021-02-25 15:28:49', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(36, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-02-25 15:29:01', '2021-02-25 15:29:01', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(37, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-02-25 15:29:48', '2021-02-25 15:29:48', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(38, 'http://128.199.73.202/middleware/read/readeridcard', '2021-02-25 15:31:45', '2021-02-25 15:31:45', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(39, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-02-25 15:31:57', '2021-02-25 15:31:57', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(40, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-02-25 15:32:04', '2021-02-25 15:32:04', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(41, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-02-25 15:32:29', '2021-02-25 15:32:29', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(42, 'http://128.199.73.202/middleware/read/readeridcard', '2021-02-25 15:54:58', '2021-02-25 15:54:58', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(43, 'http://128.199.73.202/middleware/read/readeridcard', '2021-02-25 15:57:41', '2021-02-25 15:57:41', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(44, 'http://128.199.73.202/middleware/read/readeridcard', '2021-02-25 16:00:09', '2021-02-25 16:00:09', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(45, 'http://128.199.73.202/middleware/read/readeridcard', '2021-02-25 16:00:24', '2021-02-25 16:00:24', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(46, 'http://128.199.73.202/middleware/read/readeridcard', '2021-02-25 16:01:40', '2021-02-25 16:01:40', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(47, 'http://128.199.73.202/middleware/stop/readeridcard', '2021-02-25 16:08:02', '2021-02-25 16:08:02', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"success\"\n        }\n    ]\n}'),
(48, 'http://128.199.73.202/middleware/read/readeridcard', '2021-02-25 16:07:53', '2021-02-25 16:08:23', '503', '', '<HTML><HEAD><meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\"><TITLE>503 Service Unavailable</TITLE></HEAD><BODY><H1>503 Service Unavailable</H1></BODY></HTML>'),
(49, 'http://128.199.73.202/middleware/read/readeridcard', '2021-02-25 16:08:46', '2021-02-25 16:08:47', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(50, 'http://128.199.73.202/middleware/read/readeridcard', '2021-02-25 16:09:18', '2021-02-25 16:09:18', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(51, 'http://128.199.73.202/middleware/stop/readeridcard', '2021-02-25 16:09:19', '2021-02-25 16:09:49', '503', '', '<HTML><HEAD><meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\"><TITLE>503 Service Unavailable</TITLE></HEAD><BODY><H1>503 Service Unavailable</H1></BODY></HTML>'),
(52, 'http://128.199.73.202/middleware/stop/readeridcard', '2021-02-25 16:09:49', '2021-02-25 16:09:49', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"success\"\n        }\n    ]\n}'),
(53, 'http://128.199.73.202/middleware/stop/readeridcard', '2021-02-25 16:09:49', '2021-02-25 16:09:49', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"success\"\n        }\n    ]\n}'),
(54, 'http://128.199.73.202/middleware/stop/readeridcard', '2021-02-25 16:09:50', '2021-02-25 16:09:50', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"success\"\n        }\n    ]\n}'),
(55, 'http://128.199.73.202/middleware/stop/readeridcard', '2021-02-25 16:09:50', '2021-02-25 16:10:20', '503', '', '<HTML><HEAD><meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\"><TITLE>503 Service Unavailable</TITLE></HEAD><BODY><H1>503 Service Unavailable</H1></BODY></HTML>'),
(56, 'http://128.199.73.202/middleware/stop/readeridcard', '2021-02-25 16:10:20', '2021-02-25 16:10:20', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"success\"\n        }\n    ]\n}'),
(57, 'http://128.199.73.202/middleware/stop/readeridcard', '2021-02-25 16:10:20', '2021-02-25 16:10:20', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"success\"\n        }\n    ]\n}'),
(58, 'http://128.199.73.202/middleware/stop/readeridcard', '2021-02-25 16:10:20', '2021-02-25 16:10:20', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"success\"\n        }\n    ]\n}'),
(59, 'http://128.199.73.202/middleware/stop/readeridcard', '2021-02-25 16:10:20', '2021-02-25 16:10:20', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"success\"\n        }\n    ]\n}'),
(60, 'http://128.199.73.202/middleware/stop/readeridcard', '2021-02-25 16:10:20', '2021-02-25 16:10:20', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"success\"\n        }\n    ]\n}'),
(61, 'http://128.199.73.202/middleware/stop/readeridcard', '2021-02-25 16:10:20', '2021-02-25 16:10:20', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"success\"\n        }\n    ]\n}'),
(62, 'http://128.199.73.202/middleware/stop/readeridcard', '2021-02-25 16:10:20', '2021-02-25 16:10:20', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"success\"\n        }\n    ]\n}'),
(63, 'http://128.199.73.202/middleware/stop/readeridcard', '2021-02-25 16:10:21', '2021-02-25 16:10:21', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"success\"\n        }\n    ]\n}'),
(64, 'http://128.199.73.202/middleware/read/readeridcard', '2021-02-25 16:10:36', '2021-02-25 16:10:36', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(65, 'http://128.199.73.202/middleware/read/readeridcard', '2021-02-25 16:19:34', '2021-02-25 16:19:34', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(66, 'http://128.199.73.202/middleware/read/readeridcard', '2021-02-25 16:20:48', '2021-02-25 16:20:48', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(67, 'http://128.199.73.202/middleware/read/readeridcard', '2021-02-25 16:32:06', '2021-02-25 16:32:06', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(68, 'http://128.199.73.202/middleware/read/readeridcard', '2021-02-25 17:23:01', '2021-02-25 17:23:01', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(69, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-02-25 17:23:14', '2021-02-25 17:23:14', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(70, 'http://128.199.73.202/middleware/read/readeridcard', '2021-03-01 09:36:50', '2021-03-01 09:37:35', '0', 'Operation timed out after 45000 milliseconds with 0 bytes received', '0'),
(71, 'http://128.199.73.202/middleware/read/readeridcard', '2021-03-01 09:37:59', '2021-03-01 09:38:44', '0', 'Operation timed out after 45000 milliseconds with 0 bytes received', '0'),
(72, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 09:39:07', '2021-03-01 09:39:08', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(73, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 09:39:43', '2021-03-01 09:39:43', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(74, 'http://128.199.73.202/middleware/read/bloodoxygenmeter', '2021-03-01 09:40:21', '2021-03-01 09:41:06', '0', 'Operation timed out after 45000 milliseconds with 0 bytes received', '0'),
(75, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-03-01 09:41:14', '2021-03-01 09:41:14', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(76, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 09:42:25', '2021-03-01 09:42:25', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(77, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-03-01 09:45:32', '2021-03-01 09:45:32', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(78, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 09:47:12', '2021-03-01 09:47:12', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(79, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 09:50:45', '2021-03-01 09:50:45', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(80, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-03-01 09:50:52', '2021-03-01 09:50:52', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(81, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 09:54:46', '2021-03-01 09:54:46', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(82, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-03-01 09:54:54', '2021-03-01 09:55:39', '0', 'Operation timed out after 45000 milliseconds with 0 bytes received', '0'),
(83, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 10:15:48', '2021-03-01 10:15:48', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(84, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-03-01 10:15:55', '2021-03-01 10:15:56', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(85, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 10:20:26', '2021-03-01 10:20:26', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(86, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-03-01 10:20:34', '2021-03-01 10:21:19', '0', 'Operation timed out after 45000 milliseconds with 0 bytes received', '0'),
(87, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 10:36:20', '2021-03-01 10:36:20', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(88, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-03-01 10:36:27', '2021-03-01 10:36:27', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(89, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 10:40:39', '2021-03-01 10:40:39', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(90, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-03-01 10:40:54', '2021-03-01 10:40:54', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(91, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 10:42:34', '2021-03-01 10:42:34', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(92, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-03-01 10:42:42', '2021-03-01 10:43:27', '0', 'Operation timed out after 45000 milliseconds with 0 bytes received', '0'),
(93, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-03-01 10:43:55', '2021-03-01 10:44:40', '0', 'Operation timed out after 45000 milliseconds with 0 bytes received', '0'),
(94, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-03-01 10:45:02', '2021-03-01 10:45:12', '503', '', '<HTML><HEAD>\r\n<TITLE>Network Error</TITLE>\r\n</HEAD>\r\n<BODY>\r\n<FONT face=\"Helvetica\">\r\n<big><strong></strong></big><BR>\r\n</FONT>\r\n<blockquote>\r\n<TABLE border=0 cellPadding=1 width=\"80%\">\r\n<TR><TD>\r\n<FONT face=\"Helvetica\">\r\n<big>Network Error (tcp_error)</big>\r\n<BR>\r\n<BR>\r\n</FONT>\r\n</TD></TR>\r\n<TR><TD>\r\n<FONT face=\"Helvetica\">\r\nA communication error occurred: \"\"\r\n</FONT>\r\n</TD></TR>\r\n<TR><TD>\r\n<FONT face=\"Helvetica\">\r\nThe Web Server may be down, too busy, or experiencing other problems preventing it from responding to requests. You may wish to try again at a later time.\r\n</FONT>\r\n</TD></TR>\r\n<TR><TD>\r\n<FONT face=\"Helvetica\" SIZE=2>\r\n<BR>\r\nFor assistance, contact your network support team.\r\n</FONT>\r\n</TD></TR>\r\n</TABLE>\r\n</blockquote>\r\n</FONT>\r\n</BODY></HTML>\r\n'),
(95, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 10:46:11', '2021-03-01 10:46:12', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(96, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-03-01 10:46:20', '2021-03-01 10:46:20', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(97, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 11:00:48', '2021-03-01 11:00:48', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(98, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-03-01 11:00:55', '2021-03-01 11:01:12', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(99, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 11:06:33', '2021-03-01 11:06:33', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(100, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-03-01 11:06:59', '2021-03-01 11:06:59', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(101, 'http://128.199.73.202/middleware/read/readeridcard', '2021-03-01 11:08:32', '2021-03-01 11:08:32', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(102, 'http://128.199.73.202/middleware/read/readeridcard', '2021-03-01 11:11:52', '2021-03-01 11:11:52', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(103, 'http://128.199.73.202/middleware/read/readeridcard', '2021-03-01 11:13:03', '2021-03-01 11:13:03', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(104, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 11:13:32', '2021-03-01 11:13:33', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(105, 'http://128.199.73.202/middleware/read/readeridcard', '2021-03-01 11:17:45', '2021-03-01 11:17:45', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(106, 'http://128.199.73.202/middleware/read/readeridcard', '2021-03-01 11:19:03', '2021-03-01 11:19:03', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(107, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 11:19:17', '2021-03-01 11:19:17', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(108, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-03-01 11:19:26', '2021-03-01 11:19:26', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(109, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 11:21:19', '2021-03-01 11:21:19', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(110, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-03-01 11:21:39', '2021-03-01 11:21:39', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(111, 'http://128.199.73.202/middleware/read/readeridcard', '2021-03-01 11:43:54', '2021-03-01 11:43:54', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(112, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 11:44:10', '2021-03-01 11:44:10', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(113, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 11:51:17', '2021-03-01 11:52:02', '0', 'Operation timed out after 45000 milliseconds with 0 bytes received', '0'),
(114, 'http://128.199.73.202/middleware/read/readeridcard', '2021-03-01 11:55:06', '2021-03-01 11:55:06', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(115, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 11:55:31', '2021-03-01 11:55:31', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(116, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 11:56:30', '2021-03-01 11:56:30', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(117, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-03-01 12:03:25', '2021-03-01 12:03:25', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}');
INSERT INTO `t_request_log` (`id`, `request_url`, `request_time`, `response_time`, `status_code`, `error_message`, `response_message`) VALUES
(118, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-03-01 12:13:53', '2021-03-01 12:13:54', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(119, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-03-01 12:17:30', '2021-03-01 12:17:31', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(120, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-03-01 12:20:19', '2021-03-01 12:20:19', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(121, 'http://128.199.73.202/middleware/read/infraredcameraheatdetection', '2021-03-01 12:24:28', '2021-03-01 12:24:28', '200', '', '{\n    \"data\": [\n        {\n            \"value\": \"35.94\",\n            \"picture\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/temp_picture.png\"\n        }\n    ]\n}'),
(122, 'http://128.199.73.202/middleware/read/readeridcard', '2021-03-01 12:27:49', '2021-03-01 12:27:49', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(123, 'http://128.199.73.202/middleware/read/readeridcard', '2021-03-01 12:42:17', '2021-03-01 12:43:02', '0', 'Operation timed out after 45000 milliseconds with 0 bytes received', '0'),
(124, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 12:43:23', '2021-03-01 12:43:23', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(125, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 12:43:58', '2021-03-01 12:44:43', '0', 'Operation timed out after 45000 milliseconds with 0 bytes received', '0'),
(126, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 12:44:59', '2021-03-01 12:45:44', '0', 'Operation timed out after 45000 milliseconds with 0 bytes received', '0'),
(127, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 12:49:18', '2021-03-01 12:49:18', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(128, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 12:57:38', '2021-03-01 12:57:38', '200', '', '{\n    \"data\": [\n        {\n            \"systolic\": 125,\n            \"diastolic\": 95,\n            \"pulse\": 80\n        }\n    ]\n}'),
(129, 'http://128.199.73.202/middleware/read/readeridcard', '2021-03-01 14:35:24', '2021-03-01 14:35:25', '200', '', '{\n    \"id_card\": \"1209700350000\",\n    \"titlenameth\": \"\\u0e19\\u0e32\\u0e22\",\n    \"firstnameth\": \"\\u0e2a\\u0e21\\u0e0a\\u0e32\\u0e22\",\n    \"lastnameth\": \"\\u0e14\\u0e35\\u0e43\\u0e08\",\n    \"titlenameEn\": \"Mr.\",\n    \"firstnameEn\": \"Somchai\",\n    \"lastnameEn\": \"Deejai\",\n    \"birthday\": \"25340527\",\n    \"home\": \"9\\/2\",\n    \"moo\": \"\\u0e2b\\u0e21\\u0e39\\u0e48\\u0e17\\u0e35\\u0e48 4\",\n    \"trok\": \"\",\n    \"soi\": \"\",\n    \"road\": \"\",\n    \"tumbon\": \"\\u0e15\\u0e33\\u0e1a\\u0e25\\u0e1a\\u0e49\\u0e32\\u0e19\\u0e43\\u0e2b\\u0e0d\\u0e48\",\n    \"amphoe\": \"\\u0e2d\\u0e33\\u0e40\\u0e20\\u0e2d\\u0e40\\u0e21\\u0e37\\u0e2d\\u0e07\",\n    \"province\": \"\\u0e08\\u0e31\\u0e07\\u0e2b\\u0e27\\u0e31\\u0e14\\u0e23\\u0e30\\u0e22\\u0e2d\\u0e07\",\n    \"photo\": \"http:\\/\\/128.199.73.202\\/hii-asset\\/1209700000.png\"\n}'),
(130, 'http://128.199.73.202/middleware/read/weighingmachine', '2021-03-01 14:35:49', '2021-03-01 14:35:49', '200', '', '{\n    \"data\": [\n        {\n            \"weight_total\": \"80\",\n            \"weight_prepare\": \"60\",\n            \"rfid\": \"yes\",\n            \"rfid_no\": \"00123\"\n        }\n    ]\n}'),
(131, 'http://128.199.73.202/middleware/read/pressuregauge', '2021-03-01 14:35:56', '2021-03-01 14:36:41', '0', 'Operation timed out after 45000 milliseconds with 0 bytes received', '0'),
(132, 'http://128.199.73.202/middleware/read/bloodoxygenmeter', '2021-03-01 14:37:48', '2021-03-01 14:37:48', '200', '', '{\n    \"data\": [\n        {\n            \"PRbpm\": \"82\",\n            \"SpO2%\": \"97\"\n        }\n    ]\n}');

-- --------------------------------------------------------

--
-- Table structure for table `t_service`
--

CREATE TABLE `t_service` (
  `KIOSKCODE` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `USER_TYPE` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `PID` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_SERV` date NOT NULL,
  `TIME_SERV` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `BTEMP` decimal(10,1) DEFAULT NULL,
  `BTEMPCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SBP` int(11) DEFAULT NULL,
  `DBP` int(11) DEFAULT NULL,
  `BPCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PR` int(11) DEFAULT NULL,
  `PRCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RR` int(11) DEFAULT NULL,
  `RRCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OXYGEN` int(11) DEFAULT NULL,
  `OXYGENCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WEIGHT` int(11) DEFAULT NULL,
  `HEIGHT` decimal(10,1) DEFAULT NULL,
  `BMI` int(11) DEFAULT NULL,
  `BMICODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BMR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `D_UPDATE` datetime NOT NULL,
  `SESSION_ID` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_service`
--

INSERT INTO `t_service` (`KIOSKCODE`, `USER_TYPE`, `PID`, `DATE_SERV`, `TIME_SERV`, `BTEMP`, `BTEMPCODE`, `SBP`, `DBP`, `BPCODE`, `PR`, `PRCODE`, `RR`, `RRCODE`, `OXYGEN`, `OXYGENCODE`, `WEIGHT`, `HEIGHT`, `BMI`, `BMICODE`, `BMR`, `D_UPDATE`, `SESSION_ID`) VALUES
('1000', '0', '', '2021-03-01', '10:16:01', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 111.1, 49, 'HH', '-', '2021-03-01 10:16:01', '0HtbaK4O7gtj0'),
('1000', '', '', '2021-02-24', '11:17:08', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-24 11:17:08', '0mbu0fOiMFo2e'),
('1000', '0', '', '2021-02-25', '15:20:44', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 111.1, NULL, NULL, NULL, '2021-02-25 15:20:44', '0wfnQGpyApn5A'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-23', '14:18:46', 35.9, 'L', 125, 95, 'HU', 80, 'N', 82, 'N', 97, 'N', 60, 111.1, 49, 'HH', '1246', '2021-02-23 14:18:46', '1bdEVtVU6ecGI'),
('1000', '0', '', '2021-03-01', '12:03:30', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 111.1, NULL, NULL, NULL, '2021-03-01 12:03:30', '1sjed52RJmeXH'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-23', '17:49:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 111.1, 49, 'HH', '1246', '2021-02-23 17:49:14', '2JT4Va4h6fhha'),
('1000', '', '', '2021-02-25', '12:37:35', 35.9, 'L', 125, 95, 'HU', 80, 'N', 82, 'N', 97, 'N', 60, 169.0, 21, 'N', '1409', '2021-02-25 12:37:35', '2Opz5UZzWQFex'),
('1000', '', '', '2021-02-24', '16:04:08', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-24 16:04:08', '440mpl1Fp4Eo6'),
('1000', '', '', '2021-02-24', '14:06:56', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-24 14:06:56', '4dx0GRaxWzkuf'),
('1000', '', '', '2021-02-24', '16:13:55', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-24 16:13:55', '5o0HQy3aQCt4A'),
('1000', '0', '', '2021-02-23', '14:08:16', NULL, NULL, 125, 95, 'HU', 80, 'N', 82, 'N', 97, 'N', 60, 169.0, 21, 'N', '-', '2021-02-23 14:08:16', '5oX7Eyr9CivWR'),
('1000', '0', '', '2021-03-01', '10:36:32', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 111.1, 49, 'HH', '-', '2021-03-01 10:36:32', '5Z9JqOQhvyBEj'),
('1000', '', '', '2021-02-25', '12:30:57', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-25 12:30:57', '5ZqyfL0cfIfTe'),
('1000', '', '', '2021-02-24', '17:37:53', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-02-24 17:37:53', '6dco4M4N8FaQx'),
('1000', '0', '', '2021-02-23', '12:19:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 111.1, 49, 'HH', '-', '2021-02-23 12:19:38', '6imU2oTxYDe4G'),
('1000', '', '', '2021-03-01', '11:44:12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 169.0, 21, 'N', '1409', '2021-03-01 11:44:12', '6uYtxl5u61P3b'),
('1000', '0', '', '2021-02-23', '13:28:26', NULL, NULL, 125, 95, 'HU', 80, 'N', NULL, NULL, NULL, NULL, 60, 152.0, 26, 'HU', '-', '2021-02-23 13:28:26', '7a55Dj8iyPvso'),
('1000', '0', '', '2021-02-23', '11:51:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 111.6, 48, 'HH', '-', '2021-02-23 11:51:58', '87Oqvt1vegdWP'),
('1000', '', '', '2021-02-24', '11:34:44', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-24 11:34:44', '8MKqt0x1EquoQ'),
('1000', '', '', '2021-02-24', '11:31:34', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-24 11:31:34', '9UltF0KRRcLin'),
('1000', '0', '', '2021-02-23', '13:08:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 169.0, 21, 'N', '-', '2021-02-23 13:08:50', 'aAAMzIOANaLXU'),
('1000', '', '', '2021-03-01', '12:57:51', NULL, NULL, 125, 95, 'HU', 80, 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-01 12:57:51', 'ABHyr3SEMuQrp'),
('1000', '', '', '2021-02-25', '15:20:07', 35.9, 'L', 125, 95, 'HU', 80, 'N', 82, 'N', 97, 'N', 60, 169.0, 21, 'N', '1409', '2021-02-25 15:20:07', 'AcgfCmFZ2PxzH'),
('1000', '0', '', '2021-03-01', '10:20:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 111.1, 49, 'HH', '-', '2021-03-01 10:20:28', 'aOluQnXi84oZ9'),
('1000', '', '', '2021-02-24', '17:14:22', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-24 17:14:22', 'C8aBarxGOmlch'),
('1000', '', '', '2021-03-01', '11:55:38', NULL, NULL, 125, 95, 'HU', 80, 'N', NULL, NULL, NULL, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-03-01 11:55:38', 'cbhU7j7AqHzzf'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-23', '17:04:04', 35.9, 'L', 125, 95, 'HU', 80, 'N', 82, 'N', 97, 'N', 60, 111.1, 49, 'HH', '1246', '2021-02-23 17:04:04', 'cfttzP8xQSHxz'),
('1000', '', '', '2021-02-25', '17:26:09', NULL, NULL, 125, 95, 'HU', 80, 'N', NULL, NULL, NULL, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-02-25 17:26:09', 'cnlklNUdMZx8g'),
('1000', '', '', '2021-02-24', '15:42:55', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-24 15:42:55', 'd0aZ7tNZPYdsu'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-23', '11:40:57', 35.9, 'L', 125, 95, 'HU', 80, 'N', 82, 'N', 97, 'N', 60, 111.1, 49, 'HH', '1246', '2021-02-23 11:40:57', 'egcIF0OjT6TN6'),
('1000', '0', '', '2021-02-23', '14:12:35', 35.9, 'L', 125, 95, 'HU', 80, 'N', 82, 'N', 97, 'N', 60, 169.0, 21, 'N', '-', '2021-02-23 14:12:35', 'EgTmOYGw72SEL'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-23', '17:00:58', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, 82, 'N', 97, 'N', NULL, 111.1, NULL, NULL, NULL, '2021-02-23 17:00:58', 'Ekjm788DkQGa9'),
('1000', '0', '', '2021-03-01', '12:13:59', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 111.1, NULL, NULL, NULL, '2021-03-01 12:13:59', 'EOgUHCTQdeUOn'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-23', '18:22:30', NULL, NULL, 125, 95, 'HU', 80, 'N', 82, 'N', 97, 'N', 60, 169.0, 21, 'N', '1536', '2021-02-23 18:22:30', 'eqbAPvwc9wMel'),
('1000', '0', '', '2021-03-01', '11:21:44', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 111.1, 49, 'HH', '-', '2021-03-01 11:21:44', 'ewNlAABRO21xN'),
('1000', '', '', '2021-02-25', '14:56:59', 35.9, 'L', 125, 95, 'HU', 80, 'N', NULL, NULL, NULL, NULL, 60, 169.0, 21, 'N', '1409', '2021-02-25 14:56:59', 'G3mIZRwiNHdy5'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-23', '18:07:18', 35.9, 'L', 125, 95, 'HU', 80, 'N', 82, 'N', 97, 'N', 60, 169.0, 21, 'N', '1536', '2021-02-23 18:07:18', 'GaK9Em9pfohch'),
('1000', '0', '', '2021-02-23', '12:50:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 169.0, 21, 'N', '-', '2021-02-23 12:50:19', 'GGcXNCZ80mjDp'),
('1000', '0', '', '2021-02-25', '12:18:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 168.0, 21, 'N', '-', '2021-02-25 12:18:36', 'GkOEZN2626s25'),
('1000', '', '', '2021-02-25', '15:29:53', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-25 15:29:53', 'gVf5AWgJJhUE6'),
('1000', '', '', '2021-03-01', '14:37:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 82, 'N', 97, 'N', 60, 169.0, 21, 'N', '1409', '2021-03-01 14:37:51', 'HE9uOx6QaR5Qu'),
('1000', '0', '', '2021-02-23', '11:42:34', 35.9, 'L', 125, 95, 'HU', 80, 'N', NULL, NULL, NULL, NULL, 60, 168.0, 21, 'N', '-', '2021-02-23 11:42:34', 'HnpMsf1CWspJC'),
('1000', '0', '', '2021-02-23', '18:03:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 169.0, 21, 'N', '-', '2021-02-23 18:03:19', 'hot98B3XKvpIc'),
('1000', '0', '', '2021-02-23', '14:14:42', 35.9, 'L', 125, 95, 'HU', 80, 'N', 82, 'N', 97, 'N', 60, 169.0, 21, 'N', '-', '2021-02-23 14:14:42', 'hVYUB0ZBw0ws8'),
('1000', '0', '', '2021-03-01', '10:46:25', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 111.1, 49, 'HH', '-', '2021-03-01 10:46:25', 'hYeLatnAyXbIG'),
('1000', '0', '', '2021-02-25', '14:49:02', 35.9, 'L', 125, 95, 'HU', 80, 'N', 82, 'N', 97, 'N', 60, 169.8, 21, 'N', '-', '2021-02-25 14:49:02', 'i2htr2roC1hML'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-23', '16:14:36', NULL, NULL, 125, 95, 'HU', 80, 'N', 82, 'N', 97, 'N', 60, 111.1, 49, 'HH', '1246', '2021-02-23 16:14:36', 'ijDo28HmCOr84'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-23', '13:28:56', NULL, NULL, 125, 95, 'HU', 80, 'N', NULL, NULL, NULL, NULL, 60, 111.1, 49, 'HH', '1246', '2021-02-23 13:28:56', 'iSEdfhGXZjo32'),
('1000', '', '', '2021-02-24', '17:48:55', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-02-24 17:48:55', 'iUUJTGweqSg94'),
('1000', '0', '', '2021-03-01', '09:54:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 111.1, 49, 'HH', '-', '2021-03-01 09:54:48', 'j3ZrV38CU8pRn'),
('1000', '0', '', '2021-02-25', '12:33:16', 35.9, 'L', 125, 95, 'HU', 80, 'N', 82, 'N', 97, 'N', 60, 169.0, 21, 'N', '-', '2021-02-25 12:33:16', 'Jl1flYxcOUxer'),
('1000', '0', '', '2021-03-01', '10:40:59', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 111.1, 49, 'HH', '-', '2021-03-01 10:40:59', 'jO4zwk3xLtb3b'),
('1000', '0', '', '2021-02-25', '12:22:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 169.0, 21, 'N', '-', '2021-02-25 12:22:40', 'JT0gesAajUgxn'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-23', '19:05:07', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-02-23 19:05:07', 'JZHphyaKo60OB'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-23', '13:32:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 82, 'N', 97, 'N', NULL, 111.1, NULL, NULL, NULL, '2021-02-23 13:32:51', 'KdI8IbEGzPMsP'),
('1000', '0', '', '2021-02-23', '12:40:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 169.0, 21, 'N', '-', '2021-02-23 12:40:45', 'l2HqvVHu6lunk'),
('1000', '0', '', '2021-02-25', '15:05:52', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-02-25 15:05:52', 'lPMQ4hjiqy60H'),
('1000', '0', '', '2021-02-23', '14:11:15', NULL, NULL, 125, 95, 'HU', 80, 'N', 82, 'N', 97, 'N', 60, 169.0, 21, 'N', '-', '2021-02-23 14:11:15', 'LtgAlkCmrJZi5'),
('1000', '0', '', '2021-02-24', '16:44:55', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 111.1, NULL, NULL, NULL, '2021-02-24 16:44:55', 'LViZW29rfN7K6'),
('1000', '0', '', '2021-02-23', '12:25:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 169.0, 21, 'N', '-', '2021-02-23 12:25:50', 'LYTheoOjRyB6Y'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-23', '19:13:46', NULL, NULL, 125, 95, 'HU', 80, 'N', 82, 'N', 97, 'N', 60, 169.0, 21, 'N', '1536', '2021-02-23 19:13:46', 'M13nanSwDAsCr'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-23', '18:04:57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 169.0, 21, 'N', '1536', '2021-02-23 18:04:57', 'mifnHfEkxxwz9'),
('1000', '0', '', '2021-02-23', '14:23:45', 35.9, 'L', 125, 95, 'HU', 80, 'N', 82, 'N', 97, 'N', 60, 169.0, 21, 'N', '-', '2021-02-23 14:23:45', 'mmj0e9zDrFN9V'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-23', '18:04:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 169.0, 21, 'N', '1536', '2021-02-23 18:04:01', 'mNLVLb9kZRz78'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-23', '18:11:31', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-02-23 18:11:31', 'mpmqlhYMH4SFK'),
('1000', '0', '', '2021-02-25', '14:52:11', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-02-25 14:52:11', 'n6JKLkgFRKMyU'),
('1000', '0', '', '2021-02-23', '16:12:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 169.0, 21, 'N', '-', '2021-02-23 16:12:06', 'n9g4oLP0SoDPk'),
('1000', '', '', '2021-03-01', '11:19:31', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 169.0, 21, 'N', '1409', '2021-03-01 11:19:31', 'NjaLDzC43V4PH'),
('1000', '0', '', '2021-02-23', '16:00:16', 35.9, 'L', 125, 95, 'HU', 80, 'N', 82, 'N', 97, 'N', 60, 190.0, 17, 'L', '-', '2021-02-23 16:00:16', 'NkwU096hQEvIm'),
('1000', '', '', '2021-02-24', '17:17:40', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-02-24 17:17:40', 'nqu71EdaxadAV'),
('1000', '0', '', '2021-03-01', '10:42:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 111.1, 49, 'HH', '-', '2021-03-01 10:42:36', 'NSqVUFqWU1HfO'),
('1000', '', '', '2021-02-24', '11:40:36', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-24 11:40:36', 'NygyzeNGGVZ8q'),
('1000', '0', '', '2021-02-23', '19:07:07', NULL, NULL, 125, 95, 'HU', 80, 'N', 82, 'N', 97, 'N', 60, 169.0, 21, 'N', '-', '2021-02-23 19:07:07', 'ohCdutpM6WMse'),
('1000', '', '', '2021-02-24', '16:57:20', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-02-24 16:57:20', 'ohMFw2BH6kDqn'),
('1000', '0', '', '2021-03-01', '09:50:57', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 111.1, 49, 'HH', '-', '2021-03-01 09:50:57', 'op8HdH5CuGoS7'),
('1000', '0', '', '2021-03-01', '11:01:17', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 111.1, 49, 'HH', '-', '2021-03-01 11:01:17', 'OQSRgYHBD3VxS'),
('1000', '0', '', '2021-03-01', '11:07:04', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 111.1, 49, 'HH', '-', '2021-03-01 11:07:04', 'oYT03CKpuGXzu'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-23', '16:18:35', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 111.1, NULL, NULL, NULL, '2021-02-23 16:18:35', 'P5qtmpSnikWiJ'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-23', '19:16:07', 35.9, 'L', 125, 95, 'HU', 80, 'N', 82, 'N', 97, 'N', 60, 169.0, 21, 'N', '1536', '2021-02-23 19:16:07', 'P8iOFiUdCNK8e'),
('1000', '', '', '2021-02-23', '15:25:08', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-23 15:25:08', 'PgicNfwWk9OIK'),
('1000', '0', '', '2021-02-25', '15:32:34', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 111.1, NULL, NULL, NULL, '2021-02-25 15:32:34', 'PM21h483OJV4r'),
('1000', '0', '', '2021-02-23', '18:24:44', 35.9, 'L', 125, 95, 'HU', 80, 'N', 82, 'N', 97, 'N', 60, 168.0, 21, 'N', '-', '2021-02-23 18:24:44', 'pwPZ6zX4kqPpt'),
('1000', '', '', '2021-02-24', '17:06:05', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-02-24 17:06:05', 'Q6W4oepAqIjXz'),
('1000', '0', '', '2021-02-23', '18:46:32', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-02-23 18:46:32', 'qlV6tLwBhzuUD'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-23', '16:25:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 82, 'N', 97, 'N', NULL, 111.1, NULL, NULL, NULL, '2021-02-23 16:25:38', 'qP7BrYzW8EzUW'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-23', '18:28:24', NULL, NULL, 125, 95, 'HU', 80, 'N', NULL, NULL, NULL, NULL, 60, 169.0, 21, 'N', '1536', '2021-02-23 18:28:24', 'QSKoNoXk6b007'),
('1000', '0', '', '2021-02-23', '19:03:18', NULL, NULL, 125, 95, 'HU', 80, 'N', 82, 'N', 97, 'N', 60, 169.0, 21, 'N', '-', '2021-02-23 19:03:18', 'RbN4oXdFuHL57'),
('1000', '', '', '2021-02-23', '15:07:08', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-23 15:07:08', 'rHJ2sxRPyAmdy'),
('1000', '', '', '2021-03-01', '12:24:33', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-01 12:24:33', 'S3cMXA3IkmeBl'),
('1000', '0', '', '2021-02-24', '16:14:58', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 111.1, NULL, NULL, NULL, '2021-02-24 16:14:58', 'sFYuqrvUKQ7iy'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-23', '13:06:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 111.1, 49, 'HH', '1246', '2021-02-23 13:06:35', 'sMplbQrNe49ki'),
('1000', '', '', '2021-02-23', '12:40:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, NULL, 0, NULL, '-', '2021-02-23 12:40:14', 'SsXh8CAGB1YTP'),
('1000', '0', '', '2021-02-24', '10:20:52', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 169.0, 21, 'N', '-', '2021-02-24 10:20:52', 'T6cb5qFiaI8sb'),
('1000', '0', '', '2021-02-24', '15:45:25', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 111.1, NULL, NULL, NULL, '2021-02-24 15:45:25', 'TbPWbF4IaIKFy'),
('1000', '0', '', '2021-02-23', '12:31:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 169.0, 21, 'N', '-', '2021-02-23 12:31:56', 'THPavwnS6nyic'),
('1000', '0', '', '2021-02-23', '18:10:47', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-02-23 18:10:47', 'THSB96fH2ZqBv'),
('1000', '0', '', '2021-03-01', '11:56:38', NULL, NULL, 125, 95, 'HU', 80, 'N', NULL, NULL, NULL, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-03-01 11:56:38', 'tJM8OW5XZhpPb'),
('1000', '0', '', '2021-03-01', '09:47:14', 35.9, 'L', 125, 95, 'HU', 80, 'N', NULL, NULL, NULL, NULL, 60, 111.1, 49, 'HH', '-', '2021-03-01 09:47:14', 'tlSqgNz9YB242'),
('1000', '', '', '2021-02-24', '17:19:15', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-02-24 17:19:15', 'U51SdAU8ajO5j'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-23', '13:08:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 111.1, 49, 'HH', '1246', '2021-02-23 13:08:27', 'UEtoAiufnDHhY'),
('1000', '0', '', '2021-02-23', '11:44:31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 168.0, 21, 'N', '-', '2021-02-23 11:44:31', 'ujo0TW8kHj91C'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-23', '18:01:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 111.1, 49, 'HH', '1246', '2021-02-23 18:01:01', 'UT6klOreOdchL'),
('1000', '0', '', '2021-02-25', '14:55:37', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 169.0, 21, 'N', '-', '2021-02-25 14:55:37', 'v1aGUBPFd2jyI'),
('1000', '0', '', '2021-02-23', '18:26:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 82, 'N', 97, 'N', NULL, 169.0, NULL, NULL, NULL, '2021-02-23 18:26:07', 'V1xPrncEBXwcS'),
('1000', '', '', '2021-02-24', '13:18:11', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-24 13:18:11', 'VBEkuk9vO8AT6'),
('1000', '0', '', '2021-02-23', '13:31:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 82, 'N', 97, 'N', NULL, 159.0, NULL, NULL, NULL, '2021-02-23 13:31:58', 'vH24MNfhbipho'),
('1000', '0', '', '2021-02-23', '14:26:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 82, 'N', 97, 'N', NULL, 169.0, NULL, NULL, NULL, '2021-02-23 14:26:52', 'VHTTtAcwiznLH'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-23', '18:09:14', 35.9, 'L', 125, 95, 'HU', 80, 'N', 82, 'N', 97, 'N', 60, 169.0, 21, 'N', '1536', '2021-02-23 18:09:14', 'WABdCKppXukyT'),
('1000', '0', '', '2021-02-23', '13:05:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 169.0, 21, 'N', '-', '2021-02-23 13:05:05', 'WcDFAmF5vRb1x'),
('1000', '', '', '2021-02-24', '16:54:08', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-02-24 16:54:08', 'XFv5Pi3E1RjOg'),
('1000', '', '', '2021-03-01', '11:13:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 169.0, 21, 'N', '1409', '2021-03-01 11:13:35', 'xULFNymmBMxNF'),
('1000', '', '', '2021-02-24', '17:21:51', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-02-24 17:21:51', 'XZUMp8Zvjl0wE'),
('1000', '', '', '2021-02-24', '17:09:25', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-02-24 17:09:25', 'y4ebD0jW5ZUhX'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-23', '18:31:46', 35.9, 'L', 125, 95, 'HU', 80, 'N', 82, 'N', 97, 'N', 60, 169.0, 21, 'N', '1536', '2021-02-23 18:31:46', 'yjbr3DJWQESob'),
('1000', '0', '', '2021-02-23', '12:48:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 169.0, 21, 'N', '-', '2021-02-23 12:48:18', 'yko4pVCw8aEV5'),
('1000', '', '', '2021-02-24', '10:20:15', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-24 10:20:15', 'yzTLHLAzPeA5O'),
('1000', '0', '', '2021-02-25', '15:11:16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 169.0, 21, 'N', '-', '2021-02-25 15:11:16', 'z5gy5HEQxNZh7'),
('1000', '', '', '2021-02-25', '15:32:09', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 169.0, 21, 'N', '1409', '2021-02-25 15:32:09', 'z6k0GGzHrgNuK'),
('1000', '', '', '2021-02-23', '14:51:18', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-23 14:51:18', 'ZkzsmO3eCLYZc'),
('1000', '', '', '2021-02-23', '15:56:32', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-23 15:56:32', 'zxhq93VU1NgAs');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_con_blood_pressure`
--
ALTER TABLE `t_con_blood_pressure`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_con_bmi`
--
ALTER TABLE `t_con_bmi`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_con_oxygen`
--
ALTER TABLE `t_con_oxygen`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_con_pulserate`
--
ALTER TABLE `t_con_pulserate`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_con_temperature`
--
ALTER TABLE `t_con_temperature`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_devices`
--
ALTER TABLE `t_devices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_person`
--
ALTER TABLE `t_person`
  ADD PRIMARY KEY (`KIOSKCODE`,`PID`);

--
-- Indexes for table `t_request_log`
--
ALTER TABLE `t_request_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_service`
--
ALTER TABLE `t_service`
  ADD PRIMARY KEY (`SESSION_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_con_blood_pressure`
--
ALTER TABLE `t_con_blood_pressure`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `t_con_bmi`
--
ALTER TABLE `t_con_bmi`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `t_con_oxygen`
--
ALTER TABLE `t_con_oxygen`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_con_pulserate`
--
ALTER TABLE `t_con_pulserate`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_con_temperature`
--
ALTER TABLE `t_con_temperature`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `t_devices`
--
ALTER TABLE `t_devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `t_request_log`
--
ALTER TABLE `t_request_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
