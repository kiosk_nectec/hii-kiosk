-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 24, 2021 at 08:45 AM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kios_nectec`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_person`
--

CREATE TABLE `t_person` (
  `KIOSKCODE` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `PID` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `CID` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRENAME` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `LNAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SEX` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `BIRTH` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MSTATUS` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NATION` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `ABOGROUP` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RHGROUP` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PASSPORT` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TELEPHONE` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MOBILE` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REGISTER_FLAG` tinyint(1) NOT NULL,
  `CONDITION_FLAG` tinyint(1) NOT NULL,
  `PHOTO_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHOTO_FACELIFT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHOTO_URL1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHOTO_URL2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHOTO_URL3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `D_UPDATE` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_person`
--
ALTER TABLE `t_person`
  ADD PRIMARY KEY (`KIOSKCODE`,`PID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
