-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 04, 2021 at 04:02 AM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kios_nectec`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_address`
--

CREATE TABLE `t_address` (
  `KIOSKCODE` varchar(9) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `PID` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `CID` varchar(13) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ADDRESSTYPE` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ROOMNO` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `CONDO` varchar(75) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `HOUSENO` varchar(75) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOISUB` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOIMAIN` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ROAD` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `VILLNAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `VILLAGE` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `TAMBON` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `AMPUR` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `CHANGWAT` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `D_UPDATE` datetime NOT NULL,
  `Delete_flag` int(10) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_config`
--

CREATE TABLE `t_config` (
  `ID` int(11) NOT NULL,
  `RNAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `AGE` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_con_bmi`
--

CREATE TABLE `t_con_bmi` (
  `ID` int(11) NOT NULL,
  `Min` int(11) NOT NULL,
  `Max` int(11) NOT NULL,
  `Code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `display` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_con_bool_pressure`
--

CREATE TABLE `t_con_bool_pressure` (
  `ID` int(11) NOT NULL,
  `SBP_min` int(11) NOT NULL,
  `SBP_max` int(11) NOT NULL,
  `DBP_min` int(11) NOT NULL,
  `DBP_max` int(11) NOT NULL,
  `Code` varchar(50) NOT NULL,
  `display` varchar(50) NOT NULL,
  `recommendation` varchar(100) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_con_oxygen`
--

CREATE TABLE `t_con_oxygen` (
  `ID` int(11) NOT NULL,
  `Min` int(11) NOT NULL,
  `Max` int(11) NOT NULL,
  `Code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `display` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_con_pulserate`
--

CREATE TABLE `t_con_pulserate` (
  `ID` int(11) NOT NULL,
  `Min` int(11) NOT NULL,
  `Max` int(11) NOT NULL,
  `Code` varchar(50) NOT NULL,
  `display` varchar(50) NOT NULL,
  `recommendation` varchar(100) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_con_temperature`
--

CREATE TABLE `t_con_temperature` (
  `ID` int(11) NOT NULL,
  `Mix` int(11) NOT NULL,
  `Max` int(11) NOT NULL,
  `Code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `display` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_devices`
--

CREATE TABLE `t_devices` (
  `id` int(11) NOT NULL,
  `device` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `active_flag` int(11) NOT NULL,
  `route` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_devices`
--

INSERT INTO `t_devices` (`id`, `device`, `active_flag`, `route`, `Delete_flag`, `Create_date`) VALUES
(1, 'เครื่องชั่งน้ำหนัก', 1, 'devices/weighing-machines', 0, '2020-12-25 12:20:32'),
(2, 'เครื่องวัดความดันโลหิต', 1, 'devices/blood-pressure', 0, '2020-12-25 12:20:32'),
(3, 'เครื่องวัดเปอร์เซ็นต์ออกซิเจนในเลือด อัตราการเต้นหัวใจ', 1, 'devices/pulse-oximeter', 0, '2020-12-25 12:20:32'),
(4, 'เครื่องวัดอุณหภูมิร่างกาย', 1, 'devices/thermometer', 0, '2020-12-25 12:20:32');

-- --------------------------------------------------------

--
-- Table structure for table `t_kiosk`
--

CREATE TABLE `t_kiosk` (
  `ID` int(11) NOT NULL,
  `KIOSKCODE` int(9) NOT NULL,
  `NAMEKIOSK` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ADDRESSKIOSK` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_person`
--

CREATE TABLE `t_person` (
  `KIOSKCODE` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `PID` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `CID` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRENAME` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `LNAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SEX` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `BIRTH` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MSTATUS` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NATION` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `ABOGROUP` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RHGROUP` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PASSPORT` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TELEPHONE` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MOBILE` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHOTO_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHOTO_URL1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHOTO_URL2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHOTO_URL3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `D_UPDATE` datetime NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_service`
--

CREATE TABLE `t_service` (
  `KIOSKCODE` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `USER_TYPE` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `PID` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_SERV` date NOT NULL,
  `TIME_SERV` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `BTEMP` decimal(2,1) DEFAULT NULL,
  `BTEMPCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SBP` int(11) DEFAULT NULL,
  `DBP` int(11) DEFAULT NULL,
  `BPCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PR` int(11) DEFAULT NULL,
  `PRCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RR` int(11) DEFAULT NULL,
  `RRCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OXYGEN` int(11) DEFAULT NULL,
  `OXYGENCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WEIGHT` int(11) DEFAULT NULL,
  `HEIGHT` int(11) DEFAULT NULL,
  `BMI` int(11) DEFAULT NULL,
  `BMICODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BMR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `D_UPDATE` date NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_staff`
--

CREATE TABLE `t_staff` (
  `ID` int(11) NOT NULL,
  `CODE` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_address`
--
ALTER TABLE `t_address`
  ADD PRIMARY KEY (`KIOSKCODE`,`PID`,`CID`);

--
-- Indexes for table `t_con_bool_pressure`
--
ALTER TABLE `t_con_bool_pressure`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_con_oxygen`
--
ALTER TABLE `t_con_oxygen`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_con_pulserate`
--
ALTER TABLE `t_con_pulserate`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_con_temperature`
--
ALTER TABLE `t_con_temperature`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_devices`
--
ALTER TABLE `t_devices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_person`
--
ALTER TABLE `t_person`
  ADD PRIMARY KEY (`KIOSKCODE`,`PID`);

--
-- Indexes for table `t_service`
--
ALTER TABLE `t_service`
  ADD PRIMARY KEY (`KIOSKCODE`,`USER_TYPE`,`PID`,`DATE_SERV`);

--
-- Indexes for table `t_staff`
--
ALTER TABLE `t_staff`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_devices`
--
ALTER TABLE `t_devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
