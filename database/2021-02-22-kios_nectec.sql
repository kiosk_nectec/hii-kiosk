-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 22, 2021 at 11:08 AM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kios_nectec`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_address`
--

CREATE TABLE `t_address` (
  `KIOSKCODE` varchar(9) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `PID` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `CID` varchar(13) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ADDRESSTYPE` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ROOMNO` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `CONDO` varchar(75) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `HOUSENO` varchar(75) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOISUB` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOIMAIN` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ROAD` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `VILLNAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `VILLAGE` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `TAMBON` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `AMPUR` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `CHANGWAT` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `D_UPDATE` datetime NOT NULL,
  `Delete_flag` int(10) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_address`
--

INSERT INTO `t_address` (`KIOSKCODE`, `PID`, `CID`, `ADDRESSTYPE`, `ROOMNO`, `CONDO`, `HOUSENO`, `SOISUB`, `SOIMAIN`, `ROAD`, `VILLNAME`, `VILLAGE`, `TAMBON`, `AMPUR`, `CHANGWAT`, `D_UPDATE`, `Delete_flag`, `Create_date`) VALUES
('', '631a6302da8adba', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-08 16:20:14'),
('', '9f353a07f70857f', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-08 17:11:32'),
('', '885e55b0eb6cbd5', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-18 15:12:03'),
('', 'ec32ef4b0548a66', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-20 17:42:35'),
('', '233f19ed2568836', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-20 21:04:32'),
('', '303e49ae9ce5c54', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-26 18:34:42'),
('', '52da11ad4c6c786', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-27 19:35:24'),
('', '4420b1389fa32f2', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-27 19:35:26'),
('', 'add905a63319cb7', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-27 19:53:38'),
('', '55118dea8531012', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-02-05 14:16:49'),
('', 'b3984bf07036349', '', '', 'test', NULL, '9/2', '', '', '', NULL, 'หม', 'ตำ', 'อำ', 'จั', '0000-00-00 00:00:00', 0, '2021-02-18 17:07:56');

-- --------------------------------------------------------

--
-- Table structure for table `t_config`
--

CREATE TABLE `t_config` (
  `ID` int(11) NOT NULL,
  `RNAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `AGE` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_con_blood_pressure`
--

CREATE TABLE `t_con_blood_pressure` (
  `ID` int(11) NOT NULL,
  `SBP_min` int(11) NOT NULL,
  `SBP_max` int(11) NOT NULL,
  `DBP_min` int(11) NOT NULL,
  `DBP_max` int(11) NOT NULL,
  `Code` varchar(50) NOT NULL,
  `display` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation_2` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Importance` int(11) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `Device_style` text NOT NULL,
  `Results_style` text NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_con_blood_pressure`
--

INSERT INTO `t_con_blood_pressure` (`ID`, `SBP_min`, `SBP_max`, `DBP_min`, `DBP_max`, `Code`, `display`, `recommendation`, `recommendation_2`, `Importance`, `Image`, `Device_style`, `Results_style`, `Delete_flag`, `Create_date`) VALUES
(1, 0, 119, 0, 79, 'N', 'เหมาะสม', 'ควบคุมอาหาร ออกกำลังกาย วัดความดันโลหิตอย่างสม่ำเสมอ', '', 1, 'assets/kiosk/emoji/icon_emo_ok.png', '{\r\n\"recommendation\":{\"top\":\"509px\",\"line-height\":\"55px\",\"color\":\"#448841\"},\r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},\r\n\"display\":{\"color\":\"#70AC47\",\"left\":\"895px\",\"width\":\"400px\",\"font-size\":\"85px\",\"top\":\"420px\"},\r\n\"emojiposition\":{ \"left\":\"730px\",\"top\":\"375px\"}\r\n}', '{\r\n\"recommendation_num\":{\"color\":\"#70AD47\",\"top\":\"513px\",\"left\":\"1412px\"},\r\n\"recommendation\":{\"top\":\"509px\",\"line-height\":\"55px\",\"color\":\"#448841\"},\r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},\r\n\"display\":{\"color\":\"#70AC47\"}\r\n}', 0, '2021-01-15 16:36:19'),
(2, 120, 129, 80, 84, 'N', 'ปกติ', 'ควบคุมอาหาร ออกกำลังกาย วัดความดันโลหิตอย่างสม่ำเสมอ', '', 2, 'assets/kiosk/emoji/icon_emo_ok.png', '{\r\n\"recommendation\":{\"top\":\"509px\",\"line-height\":\"55px\",\"color\":\"#448841\"},\r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},\r\n\"display\":{\"color\":\"#70AC47\",\"left\":\"1035px\",\"width\":\"400px\",\"font-size\":\"85px\",\"top\":\"420px\"},\r\n\"emojiposition\":{ \"left\":\"865px\",\"top\":\"375px\"}\r\n}', '{\r\n\"recommendation_num\":{\"color\":\"#70AD47\",\"top\":\"513px\",\"left\":\"1412px\"},\r\n\"recommendation\":{\"top\":\"509px\",\"line-height\":\"55px\",\"color\":\"#448841\"},\r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},\r\n\"display\":{\"color\":\"#70AC47\"}\r\n}', 0, '2021-01-15 16:36:27'),
(3, 130, 139, 85, 89, 'H', 'สูง', 'ปรึกษาแพทย์', '', 3, 'assets/kiosk/emoji/icon_emo_over1.png', '{ \r\n\"recommendation\":{\"color\":\"#848841\",\"left\":\"1000px\",\"font-size\": \"85px\",\"top\":\"555px\"},\r\n\"recommendation_2\":{\"color\":\"\"},\r\n\"recommendation_bg\":{\"background-color\":\"#FFE97A\"},\r\n\"display\":{\"color\":\"#747606\",\"left\":\"1275px\",\"width\":\"400px\",\"font-size\":\"85px\",\"top\":\"420px\"},\r\n\"emojiposition\":{ \"left\":\"910px\",\"top\":\"375px\"}\r\n ', '{\r\n\"recommendation_num\":{\"color\":\"#747606\",\"top\":\"513px\",\"left\":\"1412px\"},\r\n\"recommendation\":{\"color\":\"#848841\"}, \r\n\"recommendation_2\":{\"color\":\"\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FFE97A\"}, \r\n\"display\":{\"color\":\"#747606\"} \r\n}', 0, '2021-01-15 16:38:57'),
(4, 140, 159, 90, 99, 'HU', 'สูงมาก', 'พบแพทย์ ', ' (โรคความดันฯ ระยะเริ่มแรก)', 4, 'assets/kiosk/emoji/icon_emo_over2.png', '{ \r\n\"recommendation\":{\"color\":\"#FF8D00\"}, \r\n\"recommendation_2\":{\"color\":\"#BA957F\",\"width\":\"521px\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FAF4D5\"}, \r\n\"display\":{\"color\":\"#F26511\",\"left\":\"950px\",\"width\":\"400px\",\"font-size\":\"85px\",\"top\":\"420px\"},\r\n\"emojiposition\":{ \"left\":\"770px\",\"top\":\"375px\"}\r\n}', '{\r\n\"recommendation_num\":{\"color\":\"#F26511\",\"top\":\"513px\",\"left\":\"1412px\"},\r\n\"recommendation\":{\"color\":\"#FF8D00\"}, \r\n\"recommendation_2\":{\"color\":\"#BA957F\",\"width\":\"521px\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FAF4D5\"}, \r\n\"display\":{\"color\":\"#F26511\"} \r\n}', 0, '2021-01-15 16:49:01'),
(5, 160, 179, 100, 109, 'HH', 'สูงวิกฤต', 'พบแพทย์', '(โรคความดันฯ ระยะที่ 2)', 5, 'assets/kiosk/emoji/icon_emo_over4.png', '{ \"recommendation\":{\"color\":\"#FF0100\",\"left\":\"1035px\"},\r\n \"recommendation_2\":{\"color\":\"#9A6565\",\"left\":\"1250px\"},\r\n \"recommendation_bg\":{\"background-color\":\"#FFEBE3\"},\r\n \"display\":{\"color\":\"#760606\",\"left\":\"870px\",\"width\":\"400px\",\"font-size\":\"85px\",\"top\":\"420px\"},\r\n\"emojiposition\":{ \"left\":\"690px\",\"top\":\"375px\"}\r\n}', '{\r\n\"recommendation_num\":{\"color\":\"#FF0100\",\"top\":\"513px\",\"left\":\"1412px\"},\r\n\"recommendation\":{\"color\":\"#FF0100\"}, \r\n\"recommendation_2\":{\"color\":\"#9A6565\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FFEBE3\"}, \r\n\"display\":{\"color\":\"#760606\"} \r\n}', 0, '2021-01-15 16:49:01'),
(6, 180, 999, 110, 999, 'HH', 'สูงวิกฤต', 'พบแพทย์', ' (โรคความดันฯ รุนแรง)', 6, 'assets/kiosk/emoji/icon_emo_over4.png', '\r\n{ \"recommendation\":{\"color\":\"#FF0100\",\"left\":\"1035px\"},\r\n \"recommendation_2\":{\"color\":\"#9A6565\",\"left\":\"1250px\"},\r\n \"recommendation_bg\":{\"background-color\":\"#FFEBE3\"},\r\n \"display\":{\"color\":\"#760606\",\"left\":\"870px\",\"width\":\"400px\",\"font-size\":\"85px\",\"top\":\"420px\"},\r\n\"emojiposition\":{ \"left\":\"690px\",\"top\":\"375px\"}\r\n}', '{\r\n\"recommendation_num\":{\"color\":\"#FF0100\",\"top\":\"513px\",\"left\":\"1412px\"},\r\n\"recommendation\":{\"color\":\"#FF0100\"}, \r\n\"recommendation_2\":{\"color\":\"#9A6565\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FFEBE3\"}, \r\n\"display\":{\"color\":\"#760606\"} \r\n}', 0, '2021-01-15 16:52:31');

-- --------------------------------------------------------

--
-- Table structure for table `t_con_bmi`
--

CREATE TABLE `t_con_bmi` (
  `ID` int(11) NOT NULL,
  `Min` decimal(10,1) NOT NULL,
  `Max` decimal(10,1) NOT NULL,
  `Code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `display` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Device_style` text NOT NULL,
  `Results_style` text NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_con_bmi`
--

INSERT INTO `t_con_bmi` (`ID`, `Min`, `Max`, `Code`, `display`, `recommendation`, `Image`, `Device_style`, `Results_style`, `Delete_flag`, `Create_date`) VALUES
(1, 0.0, 18.4, 'L', 'ต่ำ', 'เกณฑ์น้ำหนักน้อยหรือผอม', 'assets/kiosk/emoji/icon_emo_down1.png', '{ \r\n\"recommendation_num\":{\"color\":\"#9A6565\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#E3F3FF\"},\r\n\"display\":{\"color\":\"#00B0F0\"},\r\n\"emojiposition\":{ \"left\":\"777px\"}\r\n}\r\n', '{\r\n\"recommendation_num\":{\"color\":\"#9A6565\"} \r\n}', 0, '2021-01-15 19:32:24'),
(2, 18.5, 22.9, 'N', 'ปกติ', 'อยู่ในเกณฑ์ปกติ', 'assets/kiosk/emoji/icon_emo_ok.png', '{ \r\n\"recommendation_num\":{\"color\":\"#70AD47\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},\r\n\"display\":{\"color\":\"#70AC47\"},\r\n\"emojiposition\":{ \"left\":\"735px\"}\r\n}', '{\r\n\"recommendation_num\":{\"color\":\"#70AD47\"} \r\n}', 0, '2021-01-15 19:32:24'),
(3, 23.0, 24.9, 'H', 'สูง', 'น้ำหนักเกิน', 'assets/kiosk/emoji/icon_emo_over1.png', '{ \r\n\"recommendation_num\":{\"color\":\"#747606\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#E6E65F\"},\r\n\"display\":{\"color\":\"#747606\"},\r\n\"emojiposition\":{ \"left\":\"747px\"}\r\n}', '{  \r\n\"recommendation_num\":{\"color\":\"#747606\"} \r\n}', 0, '2021-01-15 19:32:24'),
(4, 25.0, 29.9, 'HU', 'สูงมาก', 'โรคอ้วนระดับที่ 1', 'assets/kiosk/emoji/icon_emo_over2.png', '{ \r\n\"recommendation_num\":{\"color\":\"#F26511\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FAF4D5\"},\r\n\"display\":{\"color\":\"#F26511\"}\r\n}', '{   \r\n\"recommendation_num\":{\"color\":\"#F26511\"}  \r\n}', 0, '2021-01-15 19:32:24'),
(5, 30.0, 99.0, 'HH', 'สูงวิกฤต', 'โรคอ้วนระดับที่ 2', 'assets/kiosk/emoji/icon_emo_over4.png', '{ \r\n\"recommendation_num\":{\"color\":\"#FF0100\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FFEBE3\"},\r\n\"display\":{\"color\":\"#760606\"}\r\n}', '{  \r\n\"recommendation_num\":{\"color\":\"#FF0100\"} \r\n}', 0, '2021-01-15 19:32:24');

-- --------------------------------------------------------

--
-- Table structure for table `t_con_oxygen`
--

CREATE TABLE `t_con_oxygen` (
  `ID` int(11) NOT NULL,
  `Min` int(11) NOT NULL,
  `Max` int(11) NOT NULL,
  `Code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `display` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Device_style` text NOT NULL,
  `Results_style` text NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_con_oxygen`
--

INSERT INTO `t_con_oxygen` (`ID`, `Min`, `Max`, `Code`, `display`, `recommendation`, `Image`, `Device_style`, `Results_style`, `Delete_flag`, `Create_date`) VALUES
(1, 0, 87, 'LU', 'ต่ำมาก', '(ต่ำมากกว่าปกติ)', 'assets/kiosk/emoji/icon_emo_over4.png', '{ \r\n\"recommendation\":{\"color\":\"#FF0100\",\"left\":\"810px\",\"width\":\"1000px\"}, \r\n\"recommendation_num\":{\"color\":\"#FF0100\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FFEBE3\"}, \r\n\"display\":{\"color\":\"#760606\",\"left\":\"1085px\",\"width\":\"440px\",\"top\":\"600px\",\"font-size\":\"150px\"},\r\n\"emojiposition\":{ \"left\":\"1585px\"} \r\n}', '{ \r\n\"recommendation_num\":{\"color\":\"#FF0100\"}\r\n}', 0, '2021-01-15 17:38:16'),
(2, 88, 94, 'L', 'ต่ำ', '(ต่ำกว่าปกติ)', 'assets/kiosk/emoji/icon_emo_down1.png', '{ \r\n\"recommendation\":{\"color\":\"#417588\",\"left\":\"720px\",\"width\":\"1000px\"}, \r\n\"recommendation_num\":{\"color\":\"#9A6565\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#E3F3FF\"}, \r\n\"display\":{\"color\":\"#00B0F0\",\"left\":\"1055px\",\"width\":\"260px\",\"top\":\"600px\",\"font-size\":\"150px\"},\r\n\"emojiposition\":{ \"left\":\"1365px\"} \r\n}', '{ \r\n\"recommendation_num\":{\"color\":\"#9A6565\"}\r\n}', 0, '2021-01-15 17:38:16'),
(3, 95, 100, 'N', 'ปกติ', '(ปกติ)', 'assets/kiosk/emoji/icon_emo_ok.png', '{ \r\n\"recommendation\":{\"color\":\"#448841\",\"left\":\"855px\",\"width\":\"1000px\"}, \r\n\"recommendation_num\":{\"color\":\"#448841\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"}, \r\n\"display\":{\"color\":\"#70AC47\",\"left\":\"1115px\",\"width\":\"260px\",\"top\":\"600px\",\"font-size\":\"150px\"},\r\n\"emojiposition\":{ \"left\":\"1490px\"} \r\n}', '{ \r\n\"recommendation_num\":{\"color\":\"#70AD47\"}\r\n}', 0, '2021-01-15 17:38:16');

-- --------------------------------------------------------

--
-- Table structure for table `t_con_pulserate`
--

CREATE TABLE `t_con_pulserate` (
  `ID` int(11) NOT NULL,
  `Min` int(11) NOT NULL,
  `Max` int(11) NOT NULL,
  `Code` varchar(50) NOT NULL,
  `display` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Image` varchar(255) NOT NULL,
  `Device_style` text NOT NULL,
  `Results_style` text NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_con_pulserate`
--

INSERT INTO `t_con_pulserate` (`ID`, `Min`, `Max`, `Code`, `display`, `recommendation`, `Image`, `Device_style`, `Results_style`, `Delete_flag`, `Create_date`) VALUES
(1, 0, 59, 'L', 'ต่ำ', '(ชีพจรเต้นช้ากว่าปกติ)', 'assets/kiosk/emoji/icon_emo_down1.png', '{ \"recommendation\":{\"color\":\"#417588\",\"left\":\"1310px\"},  \"recommendation_num\":{\"color\":\"#9A6565\"},  \"recommendation_bg\":{\"background-color\":\"#E3F3FF\"},  \"display\":{\"color\":\"#00B0F0\",\"left\":\"810px\",\"width\":\"250px\",\"font-size\":\"100px\"},\r\n\"emojiposition\":{ \"left\":\"600px\",\"top\":\"650px\"} \r\n}', '{ \"recommendation\":{\"color\":\"#417588\"}, \"recommendation_num\":{\"color\":\"#9A6565\"}, \"recommendation_bg\":{\"background-color\":\"#E3F3FF\"}, \"display\":{\"color\":\"#00B0F0\"} }', 0, '2021-01-15 17:29:43'),
(2, 60, 100, 'N', 'ปกติ', '(ชีพจรเต้นปกติ)', 'assets/kiosk/emoji/icon_emo_ok.png', '{ \"recommendation\":{\"color\":\"#448841\",\"left\":\"1335px\",\"width\":\"435px\",\"font-size\":\"60px\"},\r\n \"recommendation_num\":{\"color\":\"#70AD47\"},\r\n \"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},\r\n \"display\":{\"color\":\"#70AC47\",\"left\":\"725px\",\"width\":\"250px\",\"font-size\":\"100px\"},\r\n\"emojiposition\":{ \"left\":\"495px\",\"top\":\"650px\"} \r\n}', '{ \"recommendation\":{\"color\":\"#448841\"}, \"recommendation_num\":{\"color\":\"#70AD47\"}, \"recommendation_bg\":{\"background-color\":\"#E3FFE3\"}, \"display\":{\"color\":\"#70AC47\"} }', 0, '2021-01-15 17:29:43'),
(3, 110, 999, 'H', 'สูง', '(ชีพจรเต้นสูงกว่าปกติ)', 'assets/kiosk/emoji/icon_emo_over4.png', '{ \"recommendation\":{\"color\":\"#FF0100\",\"left\":\"1315px\"}, \r\n\"recommendation_num\":{\"color\":\"#FF0100\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FFEBE3\"}, \r\n\"display\":{\"color\":\"#760606\",\"left\":\"800px\",\"width\":\"250px\",\"font-size\":\"100px\"},\r\n\"emojiposition\":{ \"left\":\"600px\",\"top\":\"650px\"} \r\n }', '{ \"recommendation\":{\"color\":\"#FF0100\"}, \"recommendation_num\":{\"color\":\"#FF0100\"}, \"recommendation_bg\":{\"background-color\":\"#FFEBE3\"}, \"display\":{\"color\":\"#760606\"} }', 0, '2021-01-15 17:29:43');

-- --------------------------------------------------------

--
-- Table structure for table `t_con_temperature`
--

CREATE TABLE `t_con_temperature` (
  `ID` int(11) NOT NULL,
  `Min` decimal(10,1) NOT NULL,
  `Max` decimal(10,1) NOT NULL,
  `Code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `display` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Device_style` text NOT NULL,
  `Results_style` text NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_con_temperature`
--

INSERT INTO `t_con_temperature` (`ID`, `Min`, `Max`, `Code`, `display`, `recommendation`, `Image`, `Device_style`, `Results_style`, `Delete_flag`, `Create_date`) VALUES
(1, 0.0, 36.4, 'L', 'ต่ำ', '(ต่ำกว่าปกติ)', 'assets/kiosk/emoji/icon_emo_down1.png', '{  \r\n\"recommendation\":{\"color\":\"#417588\",\"left\":\"990px\",\"width\":\"655px\"},  \"recommendation_num\":{\"color\":\"#9A6565\"},  \r\n\"recommendation_bg\":{\"background-color\":\"#E3F3FF\"},  \r\n\"display\":{\"color\":\"#00B0F0\",\"left\":\"895px\",\"width\":\"580px\",\"top\":\"645px\",\"font-size\":\"120px\"},\r\n\"emojiposition\":{ \"left\":\"1385px\",\"top\":\"630px\"}\r\n }', '{  \r\n \"recommendation_num\":{\"color\":\"#9A6565\"}\r\n}', 0, '2021-01-15 17:08:24'),
(2, 36.5, 37.5, 'N', 'ปกติ', '(ปกติ)', 'assets/kiosk/emoji/icon_emo_ok.png', '{ \"recommendation\":{\"color\":\"#448841\",\"left\":\"1165px\"},  \r\n\"recommendation_num\":{\"color\":\"#70AD47\"},  \r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},  \r\n\"display\":{\"color\":\"#70AC47\",\"left\":\"945px\",\"width\":\"580px\",\"top\":\"645px\",\"font-size\":\"120px\"},\r\n\"emojiposition\":{ \"left\":\"1465px\",\"top\":\"630px\"}\r\n }', '{   \r\n\"recommendation_num\":{\"color\":\"#70AD47\"}\r\n}', 0, '2021-01-15 17:08:24'),
(3, 37.6, 38.5, 'H', 'สูง', '(มีไข้ต่ำ)', 'assets/kiosk/emoji/icon_emo_over1.png', '{ \"recommendation\":{\"color\":\"#848841\",\"l1eft\":\"1180px\",\"width\":\"460px\"}, \r\n\"recommendation_num\":{\"color\":\"#747606\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FFE97A\"}, \r\n\"display\":{\"color\":\"#747606\",\"left\":\"900px\",\"width\":\"580px\",\"top\":\"645px\",\"font-size\":\"120px\"},\r\n\"emojiposition\":{ \"left\":\"1405px\",\"top\":\"630px\"}\r\n }', '{ \r\n\"recommendation_num\":{\"color\":\"#747606\"}\r\n}', 0, '2021-01-15 17:19:47'),
(4, 38.6, 39.5, 'HU', 'สูงมาก', '(มีไข้ปานกลาง)', 'assets/kiosk/emoji/icon_emo_over2.png', '{ \"recommendation\":{\"color\":\"#FF8D00\",\"width\":\"755px\",\"left\":\"985px\"}, \r\n\"recommendation_num\":{\"color\":\"#F26511\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FAF4D5\"}, \r\n\"display\":{\"color\":\"#F26511\",\"left\":\"1005px\",\"width\":\"580px\",\"top\":\"645px\",\"font-size\":\"120px\"},\r\n\"emojiposition\":{ \"left\":\"1550px\",\"top\":\"630px\"}\r\n }', '{  \r\n\"recommendation_num\":{\"color\":\"#F26511\"} \r\n}', 0, '2021-01-15 17:19:47'),
(5, 39.6, 99.9, 'HH', 'สูงวิกฤต', '(มีไข้สูง)', 'assets/kiosk/emoji/icon_emo_over4.png', '{ \"recommendation\":{\"color\":\"#FF0100\",\"left\":\"1160px\",\"width\":\"470px\"}, \r\n\"recommendation_2\":{\"color\":\"#9A6565\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FFEBE3\"}, \r\n\"display\":{\"color\":\"#760606\",\"left\":\"1055px\",\"width\":\"580px\",\"top\":\"645px\",\"font-size\":\"120px\"},\r\n\"emojiposition\":{ \"left\":\"1610px\",\"top\":\"630px\"}\r\n }', '{ \r\n\"recommendation_num\":{\"color\":\"#FF0100\"}\r\n}', 0, '2021-01-15 17:19:47');

-- --------------------------------------------------------

--
-- Table structure for table `t_devices`
--

CREATE TABLE `t_devices` (
  `id` int(11) NOT NULL,
  `device` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `active_flag` int(11) NOT NULL,
  `route` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_devices`
--

INSERT INTO `t_devices` (`id`, `device`, `active_flag`, `route`) VALUES
(1, 'เครื่องชั่งน้ำหนัก', 1, 'devices/weighing-machines'),
(2, 'เครื่องวัดความดันโลหิต', 1, 'devices/blood-pressure'),
(3, 'เครื่องวัดเปอร์เซ็นต์ออกซิเจนในเลือด อัตราการเต้นหัวใจ', 1, 'devices/pulse-oximeter'),
(4, 'เครื่องวัดอุณหภูมิร่างกาย', 1, 'devices/thermometer');

-- --------------------------------------------------------

--
-- Table structure for table `t_person`
--

CREATE TABLE `t_person` (
  `KIOSKCODE` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `PID` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `CID` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRENAME` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `LNAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SEX` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `BIRTH` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MSTATUS` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NATION` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `ABOGROUP` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RHGROUP` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PASSPORT` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TELEPHONE` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MOBILE` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHOTO_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHOTO_URL1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHOTO_URL2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHOTO_URL3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `D_UPDATE` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_person`
--

INSERT INTO `t_person` (`KIOSKCODE`, `PID`, `CID`, `PRENAME`, `NAME`, `LNAME`, `SEX`, `BIRTH`, `MSTATUS`, `NATION`, `ABOGROUP`, `RHGROUP`, `PASSPORT`, `TELEPHONE`, `MOBILE`, `PHOTO_ID`, `PHOTO_URL1`, `PHOTO_URL2`, `PHOTO_URL3`, `D_UPDATE`) VALUES
('1000', 'b3984bf070363495e16fc4bb02c68cc3', '1209700350000', '003', 'สมชาย', 'ดีใจ', '1', '19910527', NULL, '', NULL, NULL, NULL, NULL, NULL, 'http://128.199.73.202/hii-asset/1209700000.png', NULL, NULL, NULL, '2021-02-22 10:34:25');

-- --------------------------------------------------------

--
-- Table structure for table `t_service`
--

CREATE TABLE `t_service` (
  `KIOSKCODE` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `USER_TYPE` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `PID` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_SERV` date NOT NULL,
  `TIME_SERV` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `BTEMP` decimal(10,1) DEFAULT NULL,
  `BTEMPCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SBP` int(11) DEFAULT NULL,
  `DBP` int(11) DEFAULT NULL,
  `BPCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PR` int(11) DEFAULT NULL,
  `PRCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RR` int(11) DEFAULT NULL,
  `RRCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OXYGEN` int(11) DEFAULT NULL,
  `OXYGENCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WEIGHT` int(11) DEFAULT NULL,
  `HEIGHT` decimal(10,1) DEFAULT NULL,
  `BMI` int(11) DEFAULT NULL,
  `BMICODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BMR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `D_UPDATE` datetime NOT NULL,
  `SESSION_ID` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_service`
--

INSERT INTO `t_service` (`KIOSKCODE`, `USER_TYPE`, `PID`, `DATE_SERV`, `TIME_SERV`, `BTEMP`, `BTEMPCODE`, `SBP`, `DBP`, `BPCODE`, `PR`, `PRCODE`, `RR`, `RRCODE`, `OXYGEN`, `OXYGENCODE`, `WEIGHT`, `HEIGHT`, `BMI`, `BMICODE`, `BMR`, `D_UPDATE`, `SESSION_ID`) VALUES
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-22', '15:04:19', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 170.0, 21, 'N', '1541', '2021-02-22 15:04:19', '1nNAtPAqnEGN9'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-22', '13:47:39', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 170.0, 21, 'N', '1541', '2021-02-22 13:47:39', '283ndrySyVwxF'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-19', '17:46:13', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 170.0, 21, 'N', '1541', '2021-02-19 17:46:13', '3NirI7mvkSmSZ'),
('1000', '0', '', '2021-02-22', '14:36:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 160.0, 23, 'H', '-', '2021-02-22 14:36:07', '3S0l0MxNUifMW'),
('1000', '0', '', '2021-02-22', '11:19:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 169.0, 21, 'N', '-', '2021-02-22 11:19:24', '7A85dVwZMWDb6'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-22', '13:38:45', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 170.0, 21, 'N', '1541', '2021-02-22 13:38:45', '9Vukj2647cHMv'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-22', '17:52:24', 35.9, 'L', 125, 95, 'HU', 80, 'N', 82, 'N', 97, 'N', 60, 170.0, 21, 'N', '1541', '2021-02-22 17:52:24', 'AaWwrIAjRQTfz'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-19', '17:51:42', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 170.0, 21, 'N', '1541', '2021-02-19 17:51:42', 'aiyfnN5tUqn1P'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-22', '15:26:58', 35.9, 'L', 125, 95, 'HU', 80, 'N', 82, 'N', 97, 'N', 60, 170.0, 21, 'N', '1541', '2021-02-22 15:26:58', 'BcoUVPyvOTv89'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-22', '11:28:56', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 170.0, 21, 'N', '1541', '2021-02-22 11:28:56', 'bi7GXZN2g1yT9'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-22', '14:19:23', 35.9, 'L', 125, 95, 'HU', 80, 'N', 82, 'N', 97, 'N', 60, 170.0, 21, 'N', '1541', '2021-02-22 14:19:23', 'DIMwagoN2njaS'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-19', '18:01:43', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 170.0, 21, 'N', '1541', '2021-02-19 18:01:43', 'e7T74mwY7NY2y'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-22', '14:53:13', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 170.0, 21, 'N', '1541', '2021-02-22 14:53:13', 'eOD6q4RAZcmKY'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-22', '11:46:13', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 170.0, 21, 'N', '1541', '2021-02-22 11:46:13', 'FMyXNUWHFB8ev'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-22', '16:34:01', 35.9, 'L', 125, 95, 'HU', 80, 'N', 82, 'N', 97, 'N', 60, 170.0, 21, 'N', '1541', '2021-02-22 16:34:01', 'gO32HzXMhLVR2'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-22', '17:18:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 170.0, 21, 'N', '1541', '2021-02-22 17:18:06', 'hLSmmfnXITCLS'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-22', '12:03:26', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 170.0, 21, 'N', '1541', '2021-02-22 12:03:26', 'I02VYQTT7R1bK'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-22', '14:34:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 170.0, 21, 'N', '1541', '2021-02-22 14:34:07', 'jfIpNiJNGj8Fo'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-22', '12:13:35', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 170.0, 21, 'N', '1541', '2021-02-22 12:13:35', 'JWgq7V4BMRPPP'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-22', '13:12:11', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 170.0, 21, 'N', '1541', '2021-02-22 13:12:11', 'kyZWowSl2bQZt'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-22', '14:46:41', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 170.0, 21, 'N', '1541', '2021-02-22 14:46:41', 'lG6WT6WTYYVfY'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-22', '13:49:56', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 170.0, 21, 'N', '1541', '2021-02-22 13:49:56', 'LxJ8slErvVWgw'),
('1000', '0', '', '2021-02-22', '14:40:41', 35.9, 'L', 125, 95, 'HU', 80, 'N', NULL, NULL, NULL, NULL, 60, 169.0, 21, 'N', '-', '2021-02-22 14:40:41', 'mI716ORlyNZtB'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-22', '15:00:32', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 170.0, 21, 'N', '1541', '2021-02-22 15:00:32', 'NWe5ubeMeSfCm'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-19', '17:41:02', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 170.0, 21, 'N', '1541', '2021-02-19 17:41:02', 'O6j3P66fA3wgd'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-19', '17:39:13', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 170.0, 21, 'N', '1541', '2021-02-19 17:39:13', 'pP6hg7DepEcNn'),
('1000', '0', '', '2021-02-22', '17:55:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 169.0, 21, 'N', '-', '2021-02-22 17:55:20', 'ptBZd902D3H5n'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-22', '13:45:42', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 170.0, 21, 'N', '1541', '2021-02-22 13:45:42', 'PUYhKzIPuBCcQ'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-22', '11:55:22', 35.9, 'L', 125, 95, 'HU', 80, 'N', NULL, NULL, NULL, NULL, 60, 170.0, 21, 'N', '1541', '2021-02-22 11:55:22', 'PxzT4JPIwuoLz'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-22', '15:06:55', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 170.0, 21, 'N', '1541', '2021-02-22 15:06:55', 'qIAq523cln61n'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-19', '17:19:50', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 170.0, 21, 'N', '1541', '2021-02-19 17:19:50', 'rZcqlotqWnwtm'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-22', '13:42:27', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 170.0, 21, 'N', '1541', '2021-02-22 13:42:27', 'T2EghrI7VgsmX'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-19', '17:59:38', 35.9, 'L', 125, 95, 'HU', 80, 'N', NULL, NULL, NULL, NULL, 60, 170.0, 21, 'N', '1541', '2021-02-19 17:59:38', 'u7LloCwupID5t'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-22', '17:09:11', 35.9, 'L', 125, 95, 'HU', 80, 'N', 82, 'N', 97, 'N', 60, 170.0, 21, 'N', '1541', '2021-02-22 17:09:11', 'Wnrhn3WDqrUzt'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-22', '12:10:11', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 170.0, 21, 'N', '1541', '2021-02-22 12:10:11', 'Wr0Q3UhaywkcC'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-22', '12:12:14', 35.9, 'L', 125, 95, 'HU', 80, 'N', NULL, NULL, NULL, NULL, 60, 170.0, 21, 'N', '1541', '2021-02-22 12:12:14', 'WSV3GJLG22bhM'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-19', '17:53:22', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 170.0, 21, 'N', '1541', '2021-02-19 17:53:22', 'y5XTph8ltubCw'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-19', '11:47:43', 35.9, 'L', 125, 95, 'HU', 80, 'N', 82, 'N', 97, 'N', 60, 170.0, 21, 'N', '1541', '2021-02-19 11:47:43', 'zsRMNa86mv6y6'),
('1000', '1', 'b3984bf070363495e16fc4bb02c68cc3', '2021-02-22', '15:42:30', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 170.0, NULL, NULL, NULL, '2021-02-22 15:42:30', 'zxkTn3MH3ocUp');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_con_blood_pressure`
--
ALTER TABLE `t_con_blood_pressure`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_con_bmi`
--
ALTER TABLE `t_con_bmi`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_con_oxygen`
--
ALTER TABLE `t_con_oxygen`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_con_pulserate`
--
ALTER TABLE `t_con_pulserate`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_con_temperature`
--
ALTER TABLE `t_con_temperature`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_devices`
--
ALTER TABLE `t_devices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_person`
--
ALTER TABLE `t_person`
  ADD PRIMARY KEY (`KIOSKCODE`,`PID`);

--
-- Indexes for table `t_service`
--
ALTER TABLE `t_service`
  ADD PRIMARY KEY (`SESSION_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_con_blood_pressure`
--
ALTER TABLE `t_con_blood_pressure`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `t_con_bmi`
--
ALTER TABLE `t_con_bmi`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `t_con_oxygen`
--
ALTER TABLE `t_con_oxygen`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_con_pulserate`
--
ALTER TABLE `t_con_pulserate`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_con_temperature`
--
ALTER TABLE `t_con_temperature`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `t_devices`
--
ALTER TABLE `t_devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
