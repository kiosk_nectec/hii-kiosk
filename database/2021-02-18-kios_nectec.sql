-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 18, 2021 at 03:38 AM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kios_nectec`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_address`
--

CREATE TABLE `t_address` (
  `KIOSKCODE` varchar(9) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `PID` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `CID` varchar(13) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ADDRESSTYPE` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ROOMNO` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `CONDO` varchar(75) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `HOUSENO` varchar(75) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOISUB` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `SOIMAIN` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ROAD` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `VILLNAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `VILLAGE` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `TAMBON` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `AMPUR` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `CHANGWAT` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `D_UPDATE` datetime NOT NULL,
  `Delete_flag` int(10) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_address`
--

INSERT INTO `t_address` (`KIOSKCODE`, `PID`, `CID`, `ADDRESSTYPE`, `ROOMNO`, `CONDO`, `HOUSENO`, `SOISUB`, `SOIMAIN`, `ROAD`, `VILLNAME`, `VILLAGE`, `TAMBON`, `AMPUR`, `CHANGWAT`, `D_UPDATE`, `Delete_flag`, `Create_date`) VALUES
('', '631a6302da8adba', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-08 16:20:14'),
('', '9f353a07f70857f', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-08 17:11:32'),
('', '885e55b0eb6cbd5', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-18 15:12:03'),
('', 'ec32ef4b0548a66', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-20 17:42:35'),
('', '233f19ed2568836', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-20 21:04:32'),
('', '303e49ae9ce5c54', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-26 18:34:42'),
('', '52da11ad4c6c786', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-27 19:35:24'),
('', '4420b1389fa32f2', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-27 19:35:26'),
('', 'add905a63319cb7', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-01-27 19:53:38'),
('', '55118dea8531012', '', '', 'test', NULL, '', '', '', '', NULL, '', '', '', '', '0000-00-00 00:00:00', 0, '2021-02-05 14:16:49');

-- --------------------------------------------------------

--
-- Table structure for table `t_config`
--

CREATE TABLE `t_config` (
  `ID` int(11) NOT NULL,
  `RNAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `AGE` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_con_blood_pressure`
--

CREATE TABLE `t_con_blood_pressure` (
  `ID` int(11) NOT NULL,
  `SBP_min` int(11) NOT NULL,
  `SBP_max` int(11) NOT NULL,
  `DBP_min` int(11) NOT NULL,
  `DBP_max` int(11) NOT NULL,
  `Code` varchar(50) NOT NULL,
  `display` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation_2` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Importance` int(11) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `Device_style` text NOT NULL,
  `Results_style` text NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_con_blood_pressure`
--

INSERT INTO `t_con_blood_pressure` (`ID`, `SBP_min`, `SBP_max`, `DBP_min`, `DBP_max`, `Code`, `display`, `recommendation`, `recommendation_2`, `Importance`, `Image`, `Device_style`, `Results_style`, `Delete_flag`, `Create_date`) VALUES
(1, 0, 119, 0, 79, 'N', 'เหมาะสม', 'ควบคุมอาหาร ออกกำลังกาย วัดความดันโลหิตอย่างสม่ำเสมอ', '', 1, 'assets/kiosk/emoji/icon_emo_ok.png', '{\r\n\"recommendation\":{\"top\":\"509px\",\"line-height\":\"55px\",\"color\":\"#448841\"},\r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},\r\n\"display\":{\"color\":\"#70AC47\",\"left\":\"895px\",\"width\":\"400px\",\"font-size\":\"85px\",\"top\":\"420px\"},\r\n\"emojiposition\":{ \"left\":\"730px\",\"top\":\"375px\"}\r\n}', '{\r\n\"recommendation\":{\"top\":\"509px\",\"line-height\":\"55px\",\"color\":\"#448841\"},\r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},\r\n\"display\":{\"color\":\"#70AC47\"}\r\n}', 0, '2021-01-15 16:36:19'),
(2, 120, 129, 80, 84, 'N', 'ปกติ', 'ควบคุมอาหาร ออกกำลังกาย วัดความดันโลหิตอย่างสม่ำเสมอ', '', 2, 'assets/kiosk/emoji/icon_emo_ok.png', '{\r\n\"recommendation\":{\"top\":\"509px\",\"line-height\":\"55px\",\"color\":\"#448841\"},\r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},\r\n\"display\":{\"color\":\"#70AC47\",\"left\":\"1035px\",\"width\":\"400px\",\"font-size\":\"85px\",\"top\":\"420px\"},\r\n\"emojiposition\":{ \"left\":\"865px\",\"top\":\"375px\"}\r\n}', '{\r\n\"recommendation\":{\"top\":\"509px\",\"line-height\":\"55px\",\"color\":\"#448841\"},\r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},\r\n\"display\":{\"color\":\"#70AC47\"}\r\n}', 0, '2021-01-15 16:36:27'),
(3, 130, 139, 85, 89, 'H', 'สูง', 'ปรึกษาแพทย์', '', 3, 'assets/kiosk/emoji/icon_emo_over1.png', '{ \r\n\"recommendation\":{\"color\":\"#848841\",\"left\":\"1000px\",\"font-size\": \"85px\",\"top\":\"555px\"},\r\n\"recommendation_2\":{\"color\":\"\"},\r\n\"recommendation_bg\":{\"background-color\":\"#FFE97A\"},\r\n\"display\":{\"color\":\"#747606\",\"left\":\"1275px\",\"width\":\"400px\",\"font-size\":\"85px\",\"top\":\"420px\"},\r\n\"emojiposition\":{ \"left\":\"910px\",\"top\":\"375px\"}\r\n ', '{ \"recommendation\":{\"color\":\"#848841\"}, \"recommendation_2\":{\"color\":\"\"}, \"recommendation_bg\":{\"background-color\":\"#FFE97A\"}, \"display\":{\"color\":\"#747606\"} }', 0, '2021-01-15 16:38:57'),
(4, 140, 159, 90, 99, 'HU', 'สูงมาก', 'พบแพทย์ ', ' (โรคความดันฯ ระยะเริ่มแรก)', 4, 'assets/kiosk/emoji/icon_emo_over2.png', '{ \r\n\"recommendation\":{\"color\":\"#FF8D00\"}, \r\n\"recommendation_2\":{\"color\":\"#BA957F\",\"width\":\"521px\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FAF4D5\"}, \r\n\"display\":{\"color\":\"#F26511\",\"left\":\"950px\",\"width\":\"400px\",\"font-size\":\"85px\",\"top\":\"420px\"},\r\n\"emojiposition\":{ \"left\":\"770px\",\"top\":\"375px\"}\r\n}', '{ \r\n\"recommendation\":{\"color\":\"#FF8D00\"}, \r\n\"recommendation_2\":{\"color\":\"#BA957F\",\"width\":\"521px\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FAF4D5\"}, \r\n\"display\":{\"color\":\"#F26511\"} \r\n}', 0, '2021-01-15 16:49:01'),
(5, 160, 179, 100, 109, 'HH', 'สูงวิกฤต', 'พบแพทย์', '(โรคความดันฯ ระยะที่ 2)', 5, 'assets/kiosk/emoji/icon_emo_over4.png', '{ \"recommendation\":{\"color\":\"#FF0100\",\"left\":\"1035px\"},\r\n \"recommendation_2\":{\"color\":\"#9A6565\",\"left\":\"1250px\"},\r\n \"recommendation_bg\":{\"background-color\":\"#FFEBE3\"},\r\n \"display\":{\"color\":\"#760606\",\"left\":\"870px\",\"width\":\"400px\",\"font-size\":\"85px\",\"top\":\"420px\"},\r\n\"emojiposition\":{ \"left\":\"690px\",\"top\":\"375px\"}\r\n}', '{ \"recommendation\":{\"color\":\"#FF0100\"}, \"recommendation_2\":{\"color\":\"#9A6565\"}, \"recommendation_bg\":{\"background-color\":\"#FFEBE3\"}, \"display\":{\"color\":\"#760606\"} }', 0, '2021-01-15 16:49:01'),
(6, 180, 999, 110, 999, 'HH', 'สูงวิกฤต', 'พบแพทย์', ' (โรคความดันฯ รุนแรง)', 6, 'assets/kiosk/emoji/icon_emo_over4.png', '\r\n{ \"recommendation\":{\"color\":\"#FF0100\",\"left\":\"1035px\"},\r\n \"recommendation_2\":{\"color\":\"#9A6565\",\"left\":\"1250px\"},\r\n \"recommendation_bg\":{\"background-color\":\"#FFEBE3\"},\r\n \"display\":{\"color\":\"#760606\",\"left\":\"870px\",\"width\":\"400px\",\"font-size\":\"85px\",\"top\":\"420px\"},\r\n\"emojiposition\":{ \"left\":\"690px\",\"top\":\"375px\"}\r\n}', '{ \"recommendation\":{\"color\":\"#FF0100\"}, \"recommendation_2\":{\"color\":\"#9A6565\"}, \"recommendation_bg\":{\"background-color\":\"#FFEBE3\"}, \"display\":{\"color\":\"#760606\"} }', 0, '2021-01-15 16:52:31');

-- --------------------------------------------------------

--
-- Table structure for table `t_con_bmi`
--

CREATE TABLE `t_con_bmi` (
  `ID` int(11) NOT NULL,
  `Min` decimal(10,1) NOT NULL,
  `Max` decimal(10,1) NOT NULL,
  `Code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `display` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_con_bmi`
--

INSERT INTO `t_con_bmi` (`ID`, `Min`, `Max`, `Code`, `display`, `recommendation`, `Image`, `Delete_flag`, `Create_date`) VALUES
(1, 0.0, 18.4, 'L', 'ต่ำ', 'เกณฑ์น้ำหนักน้อยหรือผอม', 'assets/kiosk/emoji/icon_emo_down1.png', 1, '2021-01-15 19:32:24'),
(2, 18.5, 22.9, 'N', 'ปกติ', 'อยู่ในเกณฑ์ปกติ', 'assets/kiosk/emoji/icon_emo_ok.png', 1, '2021-01-15 19:32:24'),
(3, 23.0, 24.9, 'H', 'สูง', 'น้ำหนักเกิน', 'assets/kiosk/emoji/icon_emo_over1.png', 1, '2021-01-15 19:32:24'),
(4, 25.0, 29.9, 'HU', 'สูงมาก', 'โรคอ้วนระดับที่ 1', 'assets/kiosk/emoji/icon_emo_over2.png', 1, '2021-01-15 19:32:24'),
(5, 30.0, 99.0, 'HH', 'สูงวิกฤต', 'โรคอ้วนระดับที่ 2', 'assets/kiosk/emoji/icon_emo_over4.png', 1, '2021-01-15 19:32:24');

-- --------------------------------------------------------

--
-- Table structure for table `t_con_oxygen`
--

CREATE TABLE `t_con_oxygen` (
  `ID` int(11) NOT NULL,
  `Min` int(11) NOT NULL,
  `Max` int(11) NOT NULL,
  `Code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `display` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Device_style` text NOT NULL,
  `Results_style` text NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_con_oxygen`
--

INSERT INTO `t_con_oxygen` (`ID`, `Min`, `Max`, `Code`, `display`, `recommendation`, `Image`, `Device_style`, `Results_style`, `Delete_flag`, `Create_date`) VALUES
(1, 0, 87, 'LU', 'ต่ำมาก', '(ต่ำมากกว่าปกติ)', 'assets/kiosk/emoji/icon_emo_over4.png', '{ \r\n\"recommendation\":{\"color\":\"#FF0100\",\"left\":\"810px\",\"width\":\"1000px\"}, \r\n\"recommendation_num\":{\"color\":\"#FF0100\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FFEBE3\"}, \r\n\"display\":{\"color\":\"#760606\",\"left\":\"1085px\",\"width\":\"440px\",\"top\":\"600px\",\"font-size\":\"150px\"},\r\n\"emojiposition\":{ \"left\":\"1585px\"} \r\n}', '{ \r\n\"recommendation_num\":{\"color\":\"#FF0100\"}\r\n}', 0, '2021-01-15 17:38:16'),
(2, 88, 94, 'L', 'ต่ำ', '(ต่ำกว่าปกติ)', 'assets/kiosk/emoji/icon_emo_down1.png', '{ \r\n\"recommendation\":{\"color\":\"#417588\",\"left\":\"720px\",\"width\":\"1000px\"}, \r\n\"recommendation_num\":{\"color\":\"#9A6565\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#E3F3FF\"}, \r\n\"display\":{\"color\":\"#00B0F0\",\"left\":\"1055px\",\"width\":\"260px\",\"top\":\"600px\",\"font-size\":\"150px\"},\r\n\"emojiposition\":{ \"left\":\"1365px\"} \r\n}', '{ \r\n\"recommendation_num\":{\"color\":\"#9A6565\"}\r\n}', 0, '2021-01-15 17:38:16'),
(3, 95, 100, 'N', 'ปกติ', '(ปกติ)', 'assets/kiosk/emoji/icon_emo_ok.png', '{ \r\n\"recommendation\":{\"color\":\"#448841\",\"left\":\"855px\",\"width\":\"1000px\"}, \r\n\"recommendation_num\":{\"color\":\"#448841\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"}, \r\n\"display\":{\"color\":\"#70AC47\",\"left\":\"1115px\",\"width\":\"260px\",\"top\":\"600px\",\"font-size\":\"150px\"},\r\n\"emojiposition\":{ \"left\":\"1490px\"} \r\n}', '{ \r\n\"recommendation_num\":{\"color\":\"#70AD47\"}\r\n}', 0, '2021-01-15 17:38:16');

-- --------------------------------------------------------

--
-- Table structure for table `t_con_pulserate`
--

CREATE TABLE `t_con_pulserate` (
  `ID` int(11) NOT NULL,
  `Min` int(11) NOT NULL,
  `Max` int(11) NOT NULL,
  `Code` varchar(50) NOT NULL,
  `display` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Image` varchar(255) NOT NULL,
  `Device_style` text NOT NULL,
  `Results_style` text NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_con_pulserate`
--

INSERT INTO `t_con_pulserate` (`ID`, `Min`, `Max`, `Code`, `display`, `recommendation`, `Image`, `Device_style`, `Results_style`, `Delete_flag`, `Create_date`) VALUES
(1, 0, 59, 'L', 'ต่ำ', '(ชีพจรเต้นช้ากว่าปกติ)', 'assets/kiosk/emoji/icon_emo_down1.png', '{ \"recommendation\":{\"color\":\"#417588\",\"left\":\"1310px\"},  \"recommendation_num\":{\"color\":\"#9A6565\"},  \"recommendation_bg\":{\"background-color\":\"#E3F3FF\"},  \"display\":{\"color\":\"#00B0F0\",\"left\":\"810px\",\"width\":\"250px\",\"font-size\":\"100px\"},\r\n\"emojiposition\":{ \"left\":\"600px\",\"top\":\"650px\"} \r\n}', '{ \"recommendation\":{\"color\":\"#417588\"}, \"recommendation_num\":{\"color\":\"#9A6565\"}, \"recommendation_bg\":{\"background-color\":\"#E3F3FF\"}, \"display\":{\"color\":\"#00B0F0\"} }', 0, '2021-01-15 17:29:43'),
(2, 60, 100, 'N', 'ปกติ', '(ชีพจรเต้นปกติ)', 'assets/kiosk/emoji/icon_emo_ok.png', '{ \"recommendation\":{\"color\":\"#448841\",\"left\":\"1335px\",\"width\":\"435px\",\"font-size\":\"60px\"},\r\n \"recommendation_num\":{\"color\":\"#70AD47\"},\r\n \"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},\r\n \"display\":{\"color\":\"#70AC47\",\"left\":\"725px\",\"width\":\"250px\",\"font-size\":\"100px\"},\r\n\"emojiposition\":{ \"left\":\"495px\",\"top\":\"650px\"} \r\n}', '{ \"recommendation\":{\"color\":\"#448841\"}, \"recommendation_num\":{\"color\":\"#70AD47\"}, \"recommendation_bg\":{\"background-color\":\"#E3FFE3\"}, \"display\":{\"color\":\"#70AC47\"} }', 0, '2021-01-15 17:29:43'),
(3, 110, 999, 'H', 'สูง', '(ชีพจรเต้นสูงกว่าปกติ)', 'assets/kiosk/emoji/icon_emo_over4.png', '{ \"recommendation\":{\"color\":\"#FF0100\",\"left\":\"1315px\"}, \r\n\"recommendation_num\":{\"color\":\"#FF0100\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FFEBE3\"}, \r\n\"display\":{\"color\":\"#760606\",\"left\":\"800px\",\"width\":\"250px\",\"font-size\":\"100px\"},\r\n\"emojiposition\":{ \"left\":\"600px\",\"top\":\"650px\"} \r\n }', '{ \"recommendation\":{\"color\":\"#FF0100\"}, \"recommendation_num\":{\"color\":\"#FF0100\"}, \"recommendation_bg\":{\"background-color\":\"#FFEBE3\"}, \"display\":{\"color\":\"#760606\"} }', 0, '2021-01-15 17:29:43');

-- --------------------------------------------------------

--
-- Table structure for table `t_con_temperature`
--

CREATE TABLE `t_con_temperature` (
  `ID` int(11) NOT NULL,
  `Min` decimal(10,1) NOT NULL,
  `Max` decimal(10,1) NOT NULL,
  `Code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `display` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `recommendation` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Device_style` text NOT NULL,
  `Results_style` text NOT NULL,
  `Delete_flag` int(9) NOT NULL,
  `Create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_con_temperature`
--

INSERT INTO `t_con_temperature` (`ID`, `Min`, `Max`, `Code`, `display`, `recommendation`, `Image`, `Device_style`, `Results_style`, `Delete_flag`, `Create_date`) VALUES
(1, 0.0, 36.4, 'L', 'ต่ำ', '(ต่ำกว่าปกติ)', 'assets/kiosk/emoji/icon_emo_down1.png', '{  \r\n\"recommendation\":{\"color\":\"#417588\",\"left\":\"990px\",\"width\":\"655px\"},  \"recommendation_num\":{\"color\":\"#9A6565\"},  \r\n\"recommendation_bg\":{\"background-color\":\"#E3F3FF\"},  \r\n\"display\":{\"color\":\"#00B0F0\",\"left\":\"895px\",\"width\":\"580px\",\"top\":\"645px\",\"font-size\":\"120px\"},\r\n\"emojiposition\":{ \"left\":\"1385px\",\"top\":\"630px\"}\r\n }', '{  \r\n \"recommendation_num\":{\"color\":\"#9A6565\"}\r\n}', 0, '2021-01-15 17:08:24'),
(2, 36.5, 37.5, 'N', 'ปกติ', '(ปกติ)', 'assets/kiosk/emoji/icon_emo_ok.png', '{ \"recommendation\":{\"color\":\"#448841\",\"left\":\"1165px\"},  \r\n\"recommendation_num\":{\"color\":\"#70AD47\"},  \r\n\"recommendation_bg\":{\"background-color\":\"#E3FFE3\"},  \r\n\"display\":{\"color\":\"#70AC47\",\"left\":\"945px\",\"width\":\"580px\",\"top\":\"645px\",\"font-size\":\"120px\"},\r\n\"emojiposition\":{ \"left\":\"1465px\",\"top\":\"630px\"}\r\n }', '{   \r\n\"recommendation_num\":{\"color\":\"#70AD47\"}\r\n}', 0, '2021-01-15 17:08:24'),
(3, 37.6, 38.5, 'H', 'สูง', '(มีไข้ต่ำ)', 'assets/kiosk/emoji/icon_emo_over1.png', '{ \"recommendation\":{\"color\":\"#848841\",\"l1eft\":\"1180px\",\"width\":\"460px\"}, \r\n\"recommendation_num\":{\"color\":\"#747606\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FFE97A\"}, \r\n\"display\":{\"color\":\"#747606\",\"left\":\"900px\",\"width\":\"580px\",\"top\":\"645px\",\"font-size\":\"120px\"},\r\n\"emojiposition\":{ \"left\":\"1405px\",\"top\":\"630px\"}\r\n }', '{ \r\n\"recommendation_num\":{\"color\":\"#747606\"}\r\n}', 0, '2021-01-15 17:19:47'),
(4, 38.6, 39.5, 'HU', 'สูงมาก', '(มีไข้ปานกลาง)', 'assets/kiosk/emoji/icon_emo_over2.png', '{ \"recommendation\":{\"color\":\"#FF8D00\",\"width\":\"755px\",\"left\":\"985px\"}, \r\n\"recommendation_num\":{\"color\":\"#F26511\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FAF4D5\"}, \r\n\"display\":{\"color\":\"#F26511\",\"left\":\"1005px\",\"width\":\"580px\",\"top\":\"645px\",\"font-size\":\"120px\"},\r\n\"emojiposition\":{ \"left\":\"1550px\",\"top\":\"630px\"}\r\n }', '{  \r\n\"recommendation_num\":{\"color\":\"#F26511\"} \r\n}', 0, '2021-01-15 17:19:47'),
(5, 39.6, 99.9, 'HH', 'สูงวิกฤต', '(มีไข้สูง)', 'assets/kiosk/emoji/icon_emo_over4.png', '{ \"recommendation\":{\"color\":\"#FF0100\",\"left\":\"1160px\",\"width\":\"470px\"}, \r\n\"recommendation_2\":{\"color\":\"#9A6565\"}, \r\n\"recommendation_bg\":{\"background-color\":\"#FFEBE3\"}, \r\n\"display\":{\"color\":\"#760606\",\"left\":\"1055px\",\"width\":\"580px\",\"top\":\"645px\",\"font-size\":\"120px\"},\r\n\"emojiposition\":{ \"left\":\"1610px\",\"top\":\"630px\"}\r\n }', '{ \r\n\"recommendation_num\":{\"color\":\"#FF0100\"}\r\n}', 0, '2021-01-15 17:19:47');

-- --------------------------------------------------------

--
-- Table structure for table `t_devices`
--

CREATE TABLE `t_devices` (
  `id` int(11) NOT NULL,
  `device` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `active_flag` int(11) NOT NULL,
  `route` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_devices`
--

INSERT INTO `t_devices` (`id`, `device`, `active_flag`, `route`) VALUES
(1, 'เครื่องชั่งน้ำหนัก', 1, 'devices/weighing-machines'),
(2, 'เครื่องวัดความดันโลหิต', 1, 'devices/blood-pressure'),
(3, 'เครื่องวัดเปอร์เซ็นต์ออกซิเจนในเลือด อัตราการเต้นหัวใจ', 1, 'devices/pulse-oximeter'),
(4, 'เครื่องวัดอุณหภูมิร่างกาย', 1, 'devices/thermometer');

-- --------------------------------------------------------

--
-- Table structure for table `t_person`
--

CREATE TABLE `t_person` (
  `KIOSKCODE` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `PID` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `CID` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRENAME` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `LNAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SEX` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `BIRTH` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MSTATUS` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NATION` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `ABOGROUP` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RHGROUP` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PASSPORT` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TELEPHONE` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MOBILE` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHOTO_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHOTO_URL1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHOTO_URL2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHOTO_URL3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `D_UPDATE` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_person`
--

INSERT INTO `t_person` (`KIOSKCODE`, `PID`, `CID`, `PRENAME`, `NAME`, `LNAME`, `SEX`, `BIRTH`, `MSTATUS`, `NATION`, `ABOGROUP`, `RHGROUP`, `PASSPORT`, `TELEPHONE`, `MOBILE`, `PHOTO_ID`, `PHOTO_URL1`, `PHOTO_URL2`, `PHOTO_URL3`, `D_UPDATE`) VALUES
('1000', '55118dea8531012cf15a78dd62bf8f4b', '', '', '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '2021-02-05 12:15:22'),
('1000', 'add905a63319cb72fc6936a5cc4d55f0', '1209700350000', '003', 'สมชาย', 'ดีใจ', '1', '19910527', NULL, '', NULL, NULL, NULL, NULL, NULL, 'http://128.199.73.202/hii-asset/1209700000.png', NULL, NULL, NULL, '2021-02-17 10:26:12');

-- --------------------------------------------------------

--
-- Table structure for table `t_service`
--

CREATE TABLE `t_service` (
  `KIOSKCODE` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `USER_TYPE` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `PID` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_SERV` date NOT NULL,
  `TIME_SERV` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `BTEMP` decimal(10,1) DEFAULT NULL,
  `BTEMPCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SBP` int(11) DEFAULT NULL,
  `DBP` int(11) DEFAULT NULL,
  `BPCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PR` int(11) DEFAULT NULL,
  `PRCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RR` int(11) DEFAULT NULL,
  `RRCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OXYGEN` int(11) DEFAULT NULL,
  `OXYGENCODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WEIGHT` int(11) DEFAULT NULL,
  `HEIGHT` decimal(10,1) DEFAULT NULL,
  `BMI` int(11) DEFAULT NULL,
  `BMICODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BMR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `D_UPDATE` datetime NOT NULL,
  `SESSION_ID` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_service`
--

INSERT INTO `t_service` (`KIOSKCODE`, `USER_TYPE`, `PID`, `DATE_SERV`, `TIME_SERV`, `BTEMP`, `BTEMPCODE`, `SBP`, `DBP`, `BPCODE`, `PR`, `PRCODE`, `RR`, `RRCODE`, `OXYGEN`, `OXYGENCODE`, `WEIGHT`, `HEIGHT`, `BMI`, `BMICODE`, `BMR`, `D_UPDATE`, `SESSION_ID`) VALUES
('1000', '1', '', '2021-02-08', '18:37:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 82, NULL, 97, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-08 18:37:52', '0ZSyWUwwsbtRt'),
('1000', '1', '', '2021-02-01', '20:11:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 82, NULL, 82, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-01 20:11:55', '18yiRrttqIm7h'),
('1000', '1', '', '2021-02-08', '13:37:42', NULL, NULL, 125, 95, NULL, 80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-08 13:37:42', '19FYzjmw7iJEf'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-01-27', '19:57:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-01-27 19:57:49', '1hGSodkbmSXEX'),
('1000', '1', '', '2021-02-08', '18:34:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 82, NULL, 97, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-08 18:34:22', '1Lbo4qwXcdQTt'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-04', '11:20:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59, 169.0, NULL, NULL, NULL, '2021-02-04 11:20:10', '27yih0ajsGpF9'),
('1000', '1', '', '2021-02-17', '11:45:17', NULL, NULL, 125, 95, 'HU', 112, 'H', 82, 'N', 97, 'N', NULL, NULL, NULL, NULL, NULL, '2021-02-17 11:45:17', '3slmZQhqbzzA7'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-17', '12:12:16', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 168.0, NULL, NULL, NULL, '2021-02-17 12:12:16', '4FKprgIw5VyWs'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-01', '18:14:01', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 170.0, NULL, NULL, NULL, '2021-02-01 18:14:01', '4PUaPiOhjuykl'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-01', '18:09:09', 35.9, NULL, 125, 95, NULL, 50, NULL, 82, NULL, 82, NULL, 59, 170.0, NULL, NULL, NULL, '2021-02-01 18:09:09', '4syW0uPrQftY7'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-01-28', '16:18:32', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 170.0, NULL, NULL, NULL, '2021-01-28 16:18:32', '5GAUoo5rllS61'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-01-27', '19:53:47', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, '2021-01-27 19:53:47', '5MSl76UkYH8hc'),
('1000', '1', '', '2021-02-17', '14:27:51', 39.4, 'HU', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-17 14:27:51', '7QBa3WOuXUOl3'),
('1000', '1', '', '2021-02-02', '12:47:38', NULL, NULL, 125, 95, NULL, 70, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-02 12:47:38', '7qIuW6LK03a17'),
('1000', '1', '', '2021-02-06', '00:04:05', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, 82, NULL, 97, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-06 00:04:05', '80E7uWW6S0S4M'),
('1000', '1', '', '2021-02-17', '10:21:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '2021-02-17 10:21:07', '84swacSe16VrD'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-01-28', '12:52:09', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-01-28 12:52:09', '8ejICvjcgvDzT'),
('1000', '1', '', '2021-02-01', '16:52:18', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-01 16:52:18', '8vRg8rs5HFo0G'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-06', '00:31:23', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 168.0, NULL, NULL, NULL, '2021-02-06 00:31:23', '8zjxANFCaFD4g'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-01-28', '13:31:07', 35.9, NULL, 125, 95, NULL, 50, NULL, 82, NULL, 82, NULL, NULL, 170.0, NULL, NULL, NULL, '2021-01-28 13:31:07', '9eP9vsQa7p8Dn'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-01', '20:28:29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 82, NULL, 82, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-02-01 20:28:29', '9zYQzzJJzAluz'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-06', '00:38:47', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 82, NULL, 97, NULL, NULL, 168.0, NULL, NULL, NULL, '2021-02-06 00:38:47', 'a6zsDh3jcj4Dg'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-03', '14:06:43', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-03 14:06:43', 'AbNDbnBtOqfuk'),
('1000', '1', '', '2021-02-05', '18:05:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 82, NULL, 97, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-05 18:05:14', 'ANULWnHAZmLoJ'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-05', '14:23:55', NULL, NULL, 125, 95, NULL, 80, NULL, NULL, NULL, NULL, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-02-05 14:23:55', 'aZBUPq27J4l9b'),
('1000', '1', '', '2021-02-02', '12:43:56', NULL, NULL, 125, 95, NULL, 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-02 12:43:56', 'b2L5JuuLYOQ3z'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-04', '11:15:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59, 169.0, NULL, NULL, NULL, '2021-02-04 11:15:36', 'BLp14tsEXJtB1'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-03', '16:40:44', 35.9, NULL, 125, 95, NULL, 70, NULL, 82, NULL, 97, NULL, 59, 169.0, NULL, NULL, NULL, '2021-02-03 16:40:44', 'bO7l9vRg5eFeN'),
('1000', '1', '', '2021-02-03', '14:01:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59, NULL, NULL, NULL, NULL, '2021-02-03 14:01:36', 'bqI9N3ATX3w9m'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-02', '12:37:50', NULL, NULL, 125, 95, NULL, 50, NULL, NULL, NULL, NULL, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-02-02 12:37:50', 'BXWGGDAbXYyHE'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-02', '14:19:51', 35.9, NULL, 125, 95, NULL, 70, NULL, 82, NULL, 82, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-02-02 14:19:51', 'CC3n37PGwkFFD'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-01', '17:51:06', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 170.0, NULL, NULL, NULL, '2021-02-01 17:51:06', 'CDrOcajA0WIk2'),
('1000', '1', '', '2021-02-17', '15:50:23', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-17 15:50:23', 'cL4MOv4JmeUt1'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-03', '14:39:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 82, NULL, 97, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-02-03 14:39:43', 'CMrlmkhkcgfpl'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-01', '19:29:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59, 169.0, NULL, NULL, NULL, '2021-02-01 19:29:42', 'COipLIhzofujY'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-04', '17:11:16', 35.9, NULL, 125, 95, NULL, 80, NULL, 82, NULL, 97, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-02-04 17:11:16', 'Cuhi1v9DLSi68'),
('1000', '1', '', '2021-02-05', '17:18:55', NULL, NULL, 125, 95, NULL, 80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-05 17:18:55', 'cVWgZIoWStJLG'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-01-29', '13:49:37', NULL, NULL, 125, 95, NULL, 50, NULL, NULL, NULL, NULL, NULL, NULL, 170.0, NULL, NULL, NULL, '2021-01-29 13:49:37', 'D8YmffpqC4n0S'),
('1000', '1', '', '2021-02-05', '18:15:59', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-05 18:15:59', 'dIrVuym4x55OE'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-03', '11:55:03', 35.9, NULL, 125, 95, NULL, 70, NULL, 82, NULL, 82, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-03 11:55:03', 'DKM5UA9h8RFTZ'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-01-28', '11:15:20', 35.9, NULL, 125, 95, NULL, 50, NULL, 82, NULL, 82, NULL, 59, 169.0, NULL, NULL, NULL, '2021-01-28 11:15:20', 'DKvBgDleGdzt7'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-01-28', '12:07:31', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-01-28 12:07:31', 'DrBN2SZc6Hsfn'),
('1000', '1', '', '2021-02-15', '13:30:27', NULL, NULL, NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-15 13:30:27', 'EeutvHnpRc47G'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-16', '10:43:54', NULL, NULL, 125, 95, 'HU', 112, 'H', NULL, NULL, NULL, NULL, NULL, 168.0, NULL, NULL, NULL, '2021-02-16 10:43:54', 'EJUu2aKbEm0uA'),
('1000', '1', '', '2021-02-17', '11:22:47', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 82, 'N', 97, 'N', NULL, NULL, NULL, NULL, NULL, '2021-02-17 11:22:47', 'EpCked3urbHVk'),
('1000', '1', '', '2021-02-06', '00:30:02', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-06 00:30:02', 'FkBCI9JqwYe0g'),
('1000', '1', '', '2021-02-01', '11:20:15', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-01 11:20:15', 'FUE03sysszIF8'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-01', '20:34:47', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59, 169.0, NULL, NULL, NULL, '2021-02-01 20:34:47', 'fVWZDWxQLNcE8'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-01-27', '21:48:22', 35.9, NULL, 125, 95, NULL, 50, NULL, 82, NULL, 82, NULL, 59, 169.0, NULL, NULL, NULL, '2021-01-27 21:48:22', 'Gfkmqi53DV0GX'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-05', '14:45:25', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-05 14:45:25', 'Gj5jDOffSic6V'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-05', '13:06:29', 35.9, NULL, 125, 95, NULL, 80, NULL, 82, NULL, 97, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-02-05 13:06:29', 'HfdXrN15ZzYPO'),
('1000', '1', '', '2021-02-09', '14:50:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 82, NULL, 97, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-09 14:50:35', 'HkkofvO9p5bQH'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-05', '18:19:43', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 168.0, NULL, NULL, NULL, '2021-02-05 18:19:43', 'hri4BNMC5l7Po'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-02', '10:21:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59, 169.0, NULL, NULL, NULL, '2021-02-02 10:21:49', 'Hyhs2VHFzXHOA'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-01', '18:24:50', 35.9, NULL, 125, 95, NULL, 50, NULL, 82, NULL, 82, NULL, 59, 169.0, NULL, NULL, NULL, '2021-02-01 18:24:50', 'iwIK6kx0cWWca'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-01-29', '16:17:42', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 170.0, NULL, NULL, NULL, '2021-01-29 16:17:42', 'iyvYxbwJ05Tud'),
('1000', '1', '', '2021-02-15', '18:47:26', NULL, NULL, 125, 95, 'HU', 112, 'H', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-15 18:47:26', 'JDnk9tAq1U09R'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-01-27', '22:42:36', 35.9, NULL, 125, 95, NULL, 50, NULL, 82, NULL, 82, NULL, 59, 169.0, NULL, NULL, NULL, '2021-01-27 22:42:36', 'JJMT8DrPszrjZ'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-01-28', '12:55:29', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-01-28 12:55:29', 'K37z3es4CDCQV'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-15', '16:32:48', NULL, NULL, 0, 101, 'HH', 80, 'N', NULL, NULL, NULL, NULL, NULL, 168.0, NULL, NULL, NULL, '2021-02-15 16:32:48', 'KkgJHKAyJbBul'),
('1000', '1', '', '2021-02-01', '11:59:27', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-01 11:59:27', 'kkzUo8AEUgJla'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-04', '11:25:05', NULL, NULL, 125, 95, NULL, 80, NULL, 82, NULL, 97, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-02-04 11:25:05', 'KshmgU5SHZq0z'),
('1000', '1', '4420b1389fa32f24f2d80e1a457fda84', '2021-01-27', '19:36:38', 35.9, NULL, 125, 95, NULL, 50, NULL, 82, NULL, 82, NULL, 59, 0.0, NULL, NULL, NULL, '2021-01-27 19:36:38', 'l1Wr5ubTxV0mI'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-01', '17:25:28', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 170.0, NULL, NULL, NULL, '2021-02-01 17:25:28', 'lemP6ACXHSFst'),
('1000', '1', '', '2021-02-17', '11:36:37', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-17 11:36:37', 'LgcyvRVF913n8'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-03', '18:03:21', NULL, NULL, 125, 95, NULL, 70, NULL, 82, NULL, 97, NULL, 59, 169.0, NULL, NULL, NULL, '2021-02-03 18:03:21', 'Ljyvuz2S9souG'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-08', '10:32:08', 35.9, NULL, 125, 95, NULL, 80, NULL, 82, NULL, 97, NULL, NULL, 168.0, NULL, NULL, NULL, '2021-02-08 10:32:08', 'lOLEidAplO3wE'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-05', '15:02:47', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 168.0, NULL, NULL, NULL, '2021-02-05 15:02:47', 'LrHIVG5MZo9ZX'),
('1000', '1', '', '2021-01-29', '16:14:21', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-29 16:14:21', 'LUAltJ6JJHAGG'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-05', '22:26:31', NULL, NULL, 125, 95, NULL, 80, NULL, NULL, NULL, NULL, NULL, NULL, 168.0, NULL, NULL, NULL, '2021-02-05 22:26:31', 'M0Uc8iYs2uu8u'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-04', '11:21:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59, 169.0, NULL, NULL, NULL, '2021-02-04 11:21:06', 'mhqtTtjqcV9mn'),
('1000', '1', '303e49ae9ce5c54bb82e9cacbe585f6b', '2021-01-27', '19:08:22', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, '2021-01-27 19:08:22', 'MoPXLm4ALqAmB'),
('1000', '1', '', '2021-02-08', '16:09:53', 35.9, NULL, 125, 95, NULL, 80, NULL, 82, NULL, 97, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-08 16:09:53', 'n9L0d5SLVgEhK'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-01-27', '22:46:09', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-01-27 22:46:09', 'nBEBBlEid07JQ'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-05', '14:38:43', 35.9, NULL, 125, 95, NULL, 80, NULL, 82, NULL, 97, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-02-05 14:38:43', 'NZozgPhwcnOyX'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-03', '14:30:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 82, NULL, 97, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-03 14:30:46', 'pkl458K9jssFS'),
('1000', '1', '', '2021-02-09', '13:41:41', NULL, NULL, 0, 117, NULL, 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-09 13:41:41', 'qDogkteQa2gea'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-01-28', '12:13:10', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-01-28 12:13:10', 'sgY61qIXGIgqQ'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-15', '14:03:13', NULL, NULL, 0, 101, 'HH', 80, 'N', NULL, NULL, NULL, NULL, 0, 168.0, NULL, NULL, NULL, '2021-02-15 14:03:13', 'Syn9OLOzJgqbz'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-01-27', '19:56:03', 35.9, NULL, 125, 95, NULL, 50, NULL, NULL, NULL, NULL, NULL, 59, 0.0, NULL, NULL, NULL, '2021-01-27 19:56:03', 'TA52r8OpOyRDT'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-03', '14:46:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 82, NULL, 97, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-02-03 14:46:28', 'tp5rl3DHHesMj'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-02', '10:16:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59, 169.0, NULL, NULL, NULL, '2021-02-02 10:16:08', 'TvY50LG8m5QKj'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-01-28', '11:55:17', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-01-28 11:55:17', 'u5IXIwlYLTO8p'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-01-27', '19:56:31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, '2021-01-27 19:56:31', 'UBFSNxHr6Ws4z'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-03', '13:22:17', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, 82, NULL, 82, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-03 13:22:17', 'uPFqNh2YSFJOs'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-01-28', '10:59:50', 35.9, NULL, 125, 95, NULL, 50, NULL, NULL, NULL, NULL, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-01-28 10:59:50', 'uZiOhZ3uYFZDN'),
('1000', '1', '', '2021-01-29', '14:00:39', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-29 14:00:39', 'uZUmaRSDsKhyS'),
('1000', '1', '', '2021-02-08', '11:04:09', NULL, NULL, 125, 95, NULL, 80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-08 11:04:09', 'vbO5FJaC5c9jk'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-01-28', '16:21:31', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 170.0, NULL, NULL, NULL, '2021-01-28 16:21:31', 'vPWuKBQRfjmIm'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-01-27', '20:02:33', 35.9, NULL, 125, 95, NULL, 50, NULL, NULL, NULL, NULL, NULL, 59, 169.0, NULL, NULL, NULL, '2021-01-27 20:02:33', 'vw44t7qm5Qeot'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-03', '14:40:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 82, NULL, 97, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-02-03 14:40:42', 'W4sXgnnF8wTQV'),
('1000', '1', '303e49ae9ce5c54bb82e9cacbe585f6b', '2021-01-27', '19:10:55', 35.9, NULL, 125, 95, NULL, 50, NULL, 82, NULL, 82, NULL, 59, 0.0, NULL, NULL, NULL, '2021-01-27 19:10:55', 'Wdwld15lmKgIO'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-01-29', '13:34:35', NULL, NULL, 125, 95, NULL, 50, NULL, NULL, NULL, NULL, NULL, NULL, 170.0, NULL, NULL, NULL, '2021-01-29 13:34:35', 'WgrNUkcPOaHXe'),
('1000', '1', '', '2021-02-09', '14:22:54', 39.7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-09 14:22:54', 'WguBeiwxDViCz'),
('1000', '1', '', '2021-02-04', '15:53:19', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-04 15:53:19', 'WOrSBqhwYjlCE'),
('1000', '1', '', '2021-02-17', '14:56:00', 35.9, 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-17 14:56:00', 'WpsItpN3chGAV'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-01-28', '14:43:33', 35.9, NULL, 125, 95, NULL, 50, NULL, 82, NULL, 82, NULL, NULL, 170.0, NULL, NULL, NULL, '2021-01-28 14:43:33', 'Wzh7qRXzpJizw'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-01', '16:05:21', 35.9, NULL, 125, 95, NULL, 50, NULL, NULL, NULL, NULL, NULL, NULL, 170.0, NULL, NULL, NULL, '2021-02-01 16:05:21', 'XGojhggBtAFO3'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-01-28', '11:50:53', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, 82, NULL, 82, NULL, 59, 169.0, NULL, NULL, NULL, '2021-01-28 11:50:53', 'xGPEyQe9fWP3M'),
('1000', '1', '', '2021-02-05', '23:40:26', NULL, NULL, 125, 95, NULL, 80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-05 23:40:26', 'xmBK32KQg36sB'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-05', '18:48:06', 35.9, NULL, 125, 95, NULL, 80, NULL, 82, NULL, 97, NULL, NULL, 168.0, NULL, NULL, NULL, '2021-02-05 18:48:06', 'yCE5mDVEri20A'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-01-29', '16:16:22', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 170.0, NULL, NULL, NULL, '2021-01-29 16:16:22', 'yd6K1rqkzeTXG'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-02', '12:55:21', 35.9, NULL, 125, 95, NULL, 70, NULL, 82, NULL, 82, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-02-02 12:55:21', 'yLg56oYY33aA1'),
('1000', '1', '', '2021-02-05', '17:27:58', NULL, NULL, 125, 95, NULL, 80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-05 17:27:58', 'YTBtkMNnDCdZu'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-02-15', '14:25:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 82, 'N', 97, 'N', NULL, NULL, NULL, NULL, NULL, '2021-02-15 14:25:43', 'Yu3F0KGWupVzC'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-01-28', '11:25:45', 35.9, NULL, 125, 95, NULL, 50, NULL, 82, NULL, 82, NULL, NULL, 169.0, NULL, NULL, NULL, '2021-01-28 11:25:45', 'zFJQJxrs3J5Dr'),
('1000', '1', '303e49ae9ce5c54bb82e9cacbe585f6b', '2021-01-27', '19:04:31', 35.9, NULL, 125, 95, NULL, 50, NULL, 82, NULL, 82, NULL, 59, 0.0, NULL, NULL, NULL, '2021-01-27 19:04:31', 'ZiW5uSvwKJFxw'),
('1000', '1', 'add905a63319cb72fc6936a5cc4d55f0', '2021-01-27', '21:55:10', 35.9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59, 169.0, NULL, NULL, NULL, '2021-01-27 21:55:10', 'zmU5XoVTTWBKU'),
('1000', '1', '', '2021-02-08', '18:38:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 82, NULL, 97, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-08 18:38:59', 'zRcyW1bd096r1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_con_blood_pressure`
--
ALTER TABLE `t_con_blood_pressure`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_con_bmi`
--
ALTER TABLE `t_con_bmi`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_con_oxygen`
--
ALTER TABLE `t_con_oxygen`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_con_pulserate`
--
ALTER TABLE `t_con_pulserate`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_con_temperature`
--
ALTER TABLE `t_con_temperature`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `t_devices`
--
ALTER TABLE `t_devices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_person`
--
ALTER TABLE `t_person`
  ADD PRIMARY KEY (`KIOSKCODE`,`PID`);

--
-- Indexes for table `t_service`
--
ALTER TABLE `t_service`
  ADD PRIMARY KEY (`SESSION_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_con_blood_pressure`
--
ALTER TABLE `t_con_blood_pressure`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `t_con_bmi`
--
ALTER TABLE `t_con_bmi`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `t_con_oxygen`
--
ALTER TABLE `t_con_oxygen`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_con_pulserate`
--
ALTER TABLE `t_con_pulserate`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_con_temperature`
--
ALTER TABLE `t_con_temperature`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `t_devices`
--
ALTER TABLE `t_devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
