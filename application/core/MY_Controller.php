<?php

class MY_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        error_reporting(0);
    }

    public function HelloWorld()
    {
        return 'Hello World';
    }

    public function StatusActiveOption()
    {
        $list_insert = [];
        $item['Status'] = 'active';
        array_push($list_insert, $item);
        $item['Status'] = 'suspended';
        array_push($list_insert, $item);

        return $list_insert;
    }

    public function TypeLoginOption()
    {
        $list_insert = [];
        $item['value'] = 'admin';
        $item['show_value'] = 'Administrator';
        array_push($list_insert, $item);
        $item['value'] = 'supplier';
        $item['show_value'] = 'Supplier';
        array_push($list_insert, $item);
        $item['value'] = 'owner';
        $item['show_value'] = 'Service center/ Hub/ CS';
        array_push($list_insert, $item);

        return $list_insert;
    }

    public function DropDrowTypeOption()
    {
        $list_insert = [];
        $item['Type'] = 'SERVICE CENTER';
        array_push($list_insert, $item);
        $item['Type'] = 'HUB';
        array_push($list_insert, $item);
        $item['Type'] = 'CUSTOMER SERVICE';
        array_push($list_insert, $item);

        return $list_insert;
    }
}
