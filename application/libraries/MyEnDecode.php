<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class MyEnDecode
{
    public function __construct()
    {
        //parent::__construct();
    }

    public function mysql_aes_key($string, $key)
    {
        // echo 1;

        try {
            $plaintext = $string;
			$ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
			$iv = openssl_random_pseudo_bytes($ivlen);
			$ciphertext_raw = openssl_encrypt($plaintext, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
			$hmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
			$ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );
            return $ciphertext;
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";

            return $key;
        }
    }

    public function aes_decrypt($ciphertext, $key)
    {
        try {
           /*$data = base64_decode($ciphertext);
            $iv = substr($data, 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));

            $decrypted = rtrim(
                mcrypt_decrypt(
                    MCRYPT_RIJNDAEL_128,
                    hash('sha256', $key, true),
                    substr($data, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC)),
                    MCRYPT_MODE_CBC,
                    $iv
                ),
                "\0"
            );
            return $decrypted;
			*/
			
			$c = base64_decode($ciphertext);
			$ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
			$iv = substr($c, 0, $ivlen);
			$hmac = substr($c, $ivlen, $sha2len=32);
			$ciphertext_raw = substr($c, $ivlen+$sha2len);
			$original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
			$calcmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
			if (hash_equals($hmac, $calcmac))//PHP 5.6+ timing attack safe comparison
			{
				//echo $original_plaintext."\n";
			} 
			return $original_plaintext;
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";

            return $key;
        }
    }

   
}
