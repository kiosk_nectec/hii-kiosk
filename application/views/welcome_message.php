<!DOCTYPE html>
<html lang="en" ng-app="myApp">

<head>
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <title>Kios Nectec</title>

    <link href="https://fonts.googleapis.com/css2?family=Prompt:wght@600&display=swap" rel="stylesheet">

    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon-16x16.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon-96x96.png">

    <!-- inject:css -->
    <link rel="stylesheet" href="<?php echo base_url('/app/main.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('/app/kiosk.css'); ?>">
    <!-- endinject -->
    <base href="<?php echo base_url(); ?>" />
    <style>
    html,
    body {
        margin: 0;
        height: 100%;
        overflow: hidden
    }

    .tag-sactive {
        background-color: #0275d8;
        border-radius: 10px;
        padding: 3px;
        color: #fff;
    }

    .tag-ssuspended {
        background-color: #d9534f;
        border-radius: 10px;
        padding: 3px;
        color: #fff;
    }
    </style>
</head>

<body>
    <div class="body-bg"></div>
    <main ng-if="$pageFinishedLoading" ng-class="{ 'menu-collapsed': $baSidebarService.isMenuCollapsed() }">

        <div class="al-main"
            style="margin: 0px;padding:0px;height:1080px;width:1920px; background-image: url(assets/kiosk/bg_main.svg);">
            <div class="al-content" style="margin: 0px;padding:0px;">
                <div ui-view autoscroll="true" autoscroll-body-top></div>
            </div>
        </div>

        <footer class="al-footer clearfix">
            <div class="al-footer-main clearfix">
            </div>
        </footer>

        <back-top></back-top>
    </main>

    <div id="preloader" ng-show="!$pageFinishedLoading">
        <div></div>
    </div>

    <script src="<?php echo base_url('/theme/lib/jquery.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/jquery-ui.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/jquery.easing.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/jquery.easypiechart.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/Chart.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/amcharts.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/responsive.min.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/serial.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/funnel.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/pie.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/gantt.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/amstock.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/ammap.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/worldLow.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/angular.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/angular-route.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/jquery.slimscroll.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/angular-slimscroll.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/smart-table.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/angular-toastr.tpls.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/angular-touch.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/sortable.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/dropdown.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/bootstrap-select.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/bootstrap-switch.js'); ?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('/theme/lib/moment.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/fullcalendar.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/leaflet-src.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/angular-progress-button-styles.min.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/angular-ui-router.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/angular-chart.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/chartist.min.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/angular-chartist.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/eve.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/raphael.min.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/mocha.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/morris.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/angular-morris-chart.min.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/ion.rangeSlider.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/ui-bootstrap-tpls.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/angular-animate.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/rangy-core.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/rangy-classapplier.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/rangy-highlighter.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/rangy-selectionsaverestore.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/rangy-serializer.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/rangy-textrange.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/textAngular.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/textAngular-sanitize.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/textAngularSetup.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/xeditable.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/jstree.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/ngJsTree.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/select.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/select2.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/angular-cookies.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/angular-translate.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/lib/angular-select2.js'); ?>"></script>

    <!-- inject:js -->
    <script src="<?php echo base_url('/app/pages/pages.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/theme.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/components/components.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/inputs/inputs.module.js'); ?>"></script>

    <script src="<?php echo base_url('/app/pages/home/home.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/pages/home/homeCtrl.js'); ?>"></script>

    <script src="<?php echo base_url('/app/pages/authen/authen.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/pages/authen/authenCtrl.js'); ?>"></script>

    <script src="<?php echo base_url('/app/pages/idcard/idcard.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/pages/idcard/idcardCtrl.js'); ?>"></script>

    <script src="<?php echo base_url('/app/pages/profile/profile.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/pages/profile/profileCtrl.js'); ?>"></script>

    <script src="<?php echo base_url('/app/pages/authen_admin/authen_admin.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/pages/authen_admin/authen_adminCtrl.js'); ?>"></script>

    <script src="<?php echo base_url('/app/pages/authen_password/authen_password.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/pages/authen_password/authen_passwordCtrl.js'); ?>"></script>

    <script src="<?php echo base_url('/app/pages/authen_qr/authen_qr.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/pages/authen_qr/authen_qrCtrl.js'); ?>"></script>

    <script src="<?php echo base_url('/app/pages/staff/staff.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/pages/staff/staffCtrl.js'); ?>"></script>

    <script src="<?php echo base_url('/app/pages/staff_set/staff_set.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/pages/staff_set/staff_setCtrl.js'); ?>"></script>

    <script src="<?php echo base_url('/app/pages/staff_face_regis/staff_face_regis.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/pages/staff_face_regis/staff_face_regisCtrl.js'); ?>"></script>

    <script src="<?php echo base_url('/app/pages/staff_card_regis/staff_card_regis.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/pages/staff_card_regis/staff_card_regisCtrl.js'); ?>"></script>

    <script src="<?php echo base_url('/app/pages/staff_new_regis/staff_new_regis.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/pages/staff_new_regis/staff_new_regisCtrl.js'); ?>"></script>

    <script src="<?php echo base_url('/app/pages/staff_list/staff_list.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/pages/staff_list/staff_listCtrl.js'); ?>"></script>

    <script src="<?php echo base_url('/app/pages/other_services/other_services.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/pages/other_services/other_servicesCtrl.js'); ?>"></script>

    <script src="<?php echo base_url('/app/pages/detail/detail.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/pages/detail/detailCtrl.js'); ?>"></script>

    <script src="<?php echo base_url('/app/pages/numpad/numpad.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/pages/numpad/numpadCtrl.js'); ?>"></script>

    <script src="<?php echo base_url('/app/pages/face/face.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/pages/face/faceCtrl.js'); ?>"></script>

    <script src="<?php echo base_url('/app/pages/results_unknown/results_unknown.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/pages/results_unknown/results_unknownCtrl.js'); ?>"></script>

    <script src="<?php echo base_url('/app/pages/profile_new/profile_new.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/pages/profile_new/profile_newCtrl.js'); ?>"></script>

    <script src="<?php echo base_url('/app/pages/error_device/error_device.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/pages/error_device/error_deviceCtrl.js'); ?>"></script>

    <!-- start : page devices -->
    <script src="<?php echo base_url('/app/pages/devices/devices.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/pages/devices/weighing-machines/weighingMachines.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/pages/devices/weighing-machines/weighingMachinesCtrl.js'); ?>"></script>
    <script src="<?php echo base_url('/app/pages/devices/blood-pressure/bloodPressure.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/pages/devices/blood-pressure/bloodPressureCtrl.js'); ?>"></script>
    <script src="<?php echo base_url('/app/pages/devices/pulse-oximeter/pulseOximeter.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/pages/devices/pulse-oximeter/pulseOximeterCtrl.js'); ?>"></script>
    <script src="<?php echo base_url('/app/pages/devices/thermometer/thermometer.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/pages/devices/thermometer/thermometerCtrl.js'); ?>"></script>

    <script src="<?php echo base_url('/app/pages/results/results.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/pages/results/resultsCtrl.js'); ?>"></script>

    <script src="<?php echo base_url('/app/app.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/theme.config.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/theme.configProvider.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/theme.constants.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/theme.run.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/theme.service.js'); ?>"></script>

    <script src="<?php echo base_url('/app/theme/components/toastrLibConfig.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/directives/animatedChange.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/directives/autoExpand.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/directives/autoFocus.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/directives/includeWithScope.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/directives/ionSlider.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/directives/ngFileSelect.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/directives/scrollPosition.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/directives/trackWidth.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/directives/zoomIn.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/services/baProgressModal.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/services/baUtil.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/services/fileReader.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/services/preloader.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/services/stopableInterval.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/components/backTop/backTop.directive.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/components/baPanel/baPanel.directive.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/components/baPanel/baPanel.service.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/components/baPanel/baPanelBlur.directive.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/components/baPanel/baPanelBlurHelper.service.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/components/baPanel/baPanelSelf.directive.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/components/baSidebar/baSidebar.directive.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/components/baSidebar/baSidebar.service.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/components/baSidebar/BaSidebarCtrl.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/components/baSidebar/baSidebarHelpers.directive.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/components/baWizard/baWizard.directive.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/components/baWizard/baWizardCtrl.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/components/baWizard/baWizardStep.directive.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/components/contentTop/contentTop.directive.js'); ?>"></script>

    <!-- swith language -->

    <script src="<?php echo base_url('/app/theme/components/pageTop/pageTop.directive.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/components/progressBarRound/progressBarRound.directive.js'); ?>">
    </script>
    <script src="<?php echo base_url('/app/theme/components/widgets/widgets.directive.js'); ?>"></script>

    <script src="<?php echo base_url('/app/theme/components/dialogModal/confirm-dialog.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/components/dialogModal/error-dialog.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/components/dialogModal/message-dialog.js'); ?>"></script>

    <script type="text/javascript" src="<?php echo base_url('/app/share/smartTable/stPageSelect.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('/app/share/smartTable/stSelectMultiple.js'); ?>"></script>


    <script src="<?php echo base_url('/app/theme/filters/image/appImage.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/filters/image/kameleonImg.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/filters/image/profilePicture.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/filters/text/removeHtml.js'); ?>"></script>
    <script src="<?php echo base_url('/app/theme/components/backTop/lib/jquery.backTop.min.js'); ?>"></script>
    <!-- endinject -->
    <script src="<?php echo base_url('/assets/webcamjs-master/webcam.js'); ?>"></script>

    <!-- app service start-->
    <script src="<?php echo base_url('/app/services/services.module.js'); ?>"></script>
    <script src="<?php echo base_url('/app/services/baseService.js'); ?>"></script>

    <script src="<?php echo base_url('/app/services/authenticationService.js'); ?>"></script>
    <script src="<?php echo base_url('/app/services/devicesService.js'); ?>"></script>
    <script src="<?php echo base_url('/app/services/personService.js'); ?>"></script>
    <script src="<?php echo base_url('/app/services/d_serviceService.js'); ?>"></script>
    <script src="<?php echo base_url('/app/services/conBloodPressureService.js'); ?>"></script>
    <script src="<?php echo base_url('/app/services/conPulseRateService.js'); ?>"></script>
    <script src="<?php echo base_url('/app/services/conOxygenService.js'); ?>"></script>
    <script src="<?php echo base_url('/app/services/conTemperatureService.js'); ?>"></script>
    <script src="<?php echo base_url('/app/services/conWeighingMachinesService.js'); ?>"></script>
    <script src="<?php echo base_url('/app/services/webcamService.js'); ?>"></script>
    <script src="<?php echo base_url('/app/services/detailService.js'); ?>"></script>
    <script src="<?php echo base_url('/app/services/staffService.js'); ?>"></script>
    <script src="<?php echo base_url('/app/services/conLoginAdminService.js'); ?>"></script>
    <script src="<?php echo base_url('/app/services/BackgroundService.js'); ?>"></script>
    <script src="<?php echo base_url('theme/lib/Chart.min.js'); ?>"></script>
    <script src="<?php echo base_url('theme/lib/chartjs-plugin-datalabels.js'); ?>"></script>
    <script src="<?php echo base_url('theme/lib/instascan.min.js'); ?>"></script>
    <script src="<?php echo base_url('theme/lib/jquery.min.js'); ?>"></script>

    <script src="<?php echo base_url('/app/components/button/nextButtonComponents.js'); ?>"></script>
    <script src="<?php echo base_url('/app/components/button/stopButtonComponents.js'); ?>"></script>
    <script src="<?php echo base_url('/app/components/button/skipButtonComponents.js'); ?>"></script>

    <!-- app service end -->
    <script src="<?php echo base_url('/theme/bower_components/qrcode-generator/qrcode.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/bower_components/qrcode-generator/qrcode_UTF8.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/bower_components/angular-qrcode/angular-qrcode.js'); ?>"></script>

    <script src="<?php echo base_url('/theme/bower_components/videogular/videogular.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/bower_components/videogular-controls/vg-controls.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/bower_components/videogular-overlay-play/vg-overlay-play.js'); ?>">
    </script>
    <script src="<?php echo base_url('/theme/bower_components/videogular-poster/vg-poster.js'); ?>"></script>
    <script src="<?php echo base_url('/theme/bower_components/videogular-buffering/vg-buffering.js'); ?>"></script>
    <!-- inject:partials -->
    <!-- angular templates will be automatically converted in js and inserted here -->
    <!-- endinject -->
    <!-- endbuild -->

</body>

</html>