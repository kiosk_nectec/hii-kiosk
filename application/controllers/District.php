<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class District extends CI_Controller {
	public function __construct() {
        parent::__construct();
		if(! $this->session->userdata('validated')){
            redirect('login');
        }
    }
	 
	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar');
		$this->load->view('setting/settingcompany_view'); 
		$this->load->view('share/footer');
	}
    
    public function getdistrictComboList(){
	 
		try{ 
			$this->load->model('districtModel','',TRUE);
			$result['status'] = true;
			$result['message'] = $this->districtModel->getdistrictComboList();
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}

	public function getdistrictComborelate(){
	 
		try{ 
			$this->load->model('districtModel','',TRUE);
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$amphures =  isset($dataPost['amphures'])?$dataPost['amphures']:0;
			$result['status'] = true;
			$result['message'] = $this->districtModel->getdistrictComborelate($amphures);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	
}
