<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Person extends MY_Controller
{
    // private $url = 'http://128.199.73.202/middleware/';
    public function __construct()
    {
        parent::__construct();
        // if (!$this->session->userdata('validated')) {
        //     redirect('login');
        // }
    }

    public function index()
    {
    }

    public function getPerson()
    {
        try {
            $this->load->model('PersonModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->PersonModel->getPerson($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function getPersonLoginFace()
    {
        try {
            $this->load->model('PersonModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->PersonModel->getPersonLoginFace($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function savePerson()
    {
        try {
            $this->load->model('PersonModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->PersonModel->savePerson($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function loginfaceApi()
    {
        try {
            $this->load->model('PersonModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->PersonModel->loginfaceApi($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function confirmfaceApi()
    {
        try {
            $this->load->model('PersonModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->PersonModel->confirmfaceApi($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function ChkConditionFlagPerson()
    {
        try {
            $this->load->model('PersonModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->PersonModel->ChkConditionFlagPerson($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function ChkHeightFlagPerson()
    {
        try {
            $this->load->model('PersonModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->PersonModel->ChkHeightFlagPerson($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}
