<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Detail extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // if (!$this->session->userdata('validated')) {
        //     redirect('login');
        // }
    }

    public function index()
    {
    }

    public function GetDataGraph()
    {
        try {
            $this->load->model('DetailModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->DetailModel->GetDataGraph($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}
