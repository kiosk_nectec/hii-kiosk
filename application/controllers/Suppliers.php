<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Suppliers extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // if (!$this->session->userdata('validated')) {
        //     redirect('login');
        // }
    }

    public function index()
    {
    }

    public function getSuppliersComboList()
    {
        try {
            $this->load->model('SuppliersModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->SuppliersModel->getSuppliersComboList($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function saveSuppliers()
    {
        try {
            $this->load->model('SuppliersModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->SuppliersModel->saveSuppliers($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function deleteSuppliers()
    {
        try {
            $this->load->model('SuppliersModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->SuppliersModel->deleteSuppliers($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}
