<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Amphures extends CI_Controller {
	public function __construct() {
        parent::__construct();
		if(! $this->session->userdata('validated')){
            redirect('login');
        }
    }
	 
	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar');
		$this->load->view('setting/settingcompany_view'); 
		$this->load->view('share/footer');
	}
    
    public function getamphuresComboList(){
	 
		try{ 
			$this->load->model('amphuresModel','',TRUE);
			$result['status'] = true;
			$result['message'] = $this->amphuresModel->getamphuresComboList();
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}

	public function getamphuresComborelate(){
	 
		try{ 
			$this->load->model('amphuresModel','',TRUE);
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$province =  isset($dataPost['province'])?$dataPost['province']:0;
			$result['status'] = true;
			$result['message'] = $this->amphuresModel->getamphuresComborelate($province);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	
}
