<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Location extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // if (!$this->session->userdata('validated')) {
        //     redirect('login');
        // }
    }

    public function index()
    {
    }

    public function getLocationComboList()
    {
        try {
            $this->load->model('LocationModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->LocationModel->getLocationComboList($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function saveLocation()
    {
        try {
            $this->load->model('LocationModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->LocationModel->saveLocation($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function deleteLocation()
    {
        try {
            $this->load->model('LocationModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->LocationModel->deleteLocation($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}
