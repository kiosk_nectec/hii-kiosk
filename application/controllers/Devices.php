<?php

ini_set('max_execution_time', '300');

set_time_limit(300);
date_default_timezone_set('Asia/Bangkok');
defined('BASEPATH') or exit('No direct script access allowed');

class Devices extends MY_Controller
{
    // private $url = 'http://128.199.73.202/middleware/';
    public function __construct()
    {
        parent::__construct();
        // if (!$this->session->userdata('validated')) {
        //     redirect('login');
        // }
    }

    public function index()
    {
    }

    public function getDevices()
    {
        try {
            $this->load->model('DevicesModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->DevicesModel->getDevices($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function getTemperature()
    {
        try {
            $this->load->model('DevicesModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->DevicesModel->getTemperature($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function api()
    {
        $func = $this->uri->segment(3, 0);
        $device = $this->uri->segment(4, 0);

        // sleep(50);
        $reqTime = date('Y-m-d H:i:s');
        $curl = curl_init();

        $url = $this->config->item('base_api_url') . $func . '/' . $device;
        // $url = 'http://www.devdeethailand.com/hii-kiosk/api/GetSession/delay';
        $timeout = $device == 'pressuregauge' ? 120 : 45;
        curl_setopt_array($curl, [
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_TIMEOUT => $timeout,
            CURLOPT_RETURNTRANSFER => 1,

            CURLOPT_URL => $url,

            CURLOPT_USERAGENT => 'Codular Sample cURL Request',
        ]);
        $result = curl_exec($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $time = curl_getinfo($curl, CURLINFO_TOTAL_TIME);
        $error = curl_error($curl);
        $resTime = date('Y-m-d H:i:s');
        $modelData = ['request_url' => $url, 'request_time' => $reqTime, 'response_time' => $resTime, 'status_code' => $statusCode, 'error_message' => $error, 'response_message' => $result];
        $this->load->model('LineNotiFyModel', '', true);
        if ($modelData['status_code'] != 200) {
            $this->LineNotiFyModel->alert_line_notify($modelData, false, 'UserAction');
        } else {
            $this->LineNotiFyModel->alert_line_notify($modelData, true, 'UserAction');
        }
        if ($device == 'readeridcard') {

            $this->LineNotiFyModel->alert_line_notify_idcard($modelData);
        }
        $this->db->insert('t_request_log', $modelData);
        if (curl_errno($curl)) {
            $result = [];

            $result['success'] = false;
            echo json_encode($result, JSON_UNESCAPED_UNICODE);
            exit();
        } elseif ($statusCode != 200) {
            $result = [];
            $result['success'] = false;
            echo json_encode($result, JSON_UNESCAPED_UNICODE);
            exit();
        }
        // $result = json_decode($result, true);
        // if(isset($result['success'])){
        //     $result['status'] =false;
        // }
        // print_r($result);die();

        // Close request to clear up some resources
        curl_close($curl);

        // $result = json_encode($result, JSON_UNESCAPED_UNICODE);
        $result2 = str_replace("'", '"', $result);
        $result2 = str_replace('SpO2%', 'SpO2', $result2);
        // echo $result2;die();
        // $result2 = json_decode($result2, true);
        if (isset($result2['success'])) {
            $result2['status'] = false;
        }

        echo $result2;

        // echo $result2 = json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function setActiveDevice()
    {
        try {
            $this->load->model('DevicesModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->DevicesModel->setActiveDevice($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function SaveSettingScreen()
    {
        try {
            $this->load->model('DevicesModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->DevicesModel->SaveSettingScreen($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function GetSettingScreen()
    {
        try {
            $this->load->model('DevicesModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->DevicesModel->GetSettingScreen($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function GetChkRfid()
    {
        try {
            $this->load->model('DevicesModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            // print_r($dataPost);
            $result = $this->DevicesModel->GetChkRfid($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function SaveRFID()
    {
        try {
            $this->load->model('DevicesModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->DevicesModel->SaveRFID($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function DeleteRFID()
    {
        try {
            $this->load->model('DevicesModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->DevicesModel->DeleteRFID($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function GetListRFID()
    {
        try {
            $this->load->model('DevicesModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->DevicesModel->GetListRFID($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function chkReaderFlag()
    {
        try {
            $this->load->model('DevicesModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->DevicesModel->chkReaderFlag($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function updateReaderFlag()
    {
        try {
            $this->load->model('ApiModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            // print_r($dataPost);
            // die();
            $dataModel = isset($dataPost['reader_flag']) ? $dataPost['reader_flag'] : 'N';
            $result = $this->ApiModel->updateReaderFlag($dataModel);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function SetWeighingmachine()
    {
        try {
            $this->load->model('DevicesModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->DevicesModel->SetWeighingmachine($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
    public function SaveSettingTemp()
    {
        try {
            $this->load->model('DevicesModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->DevicesModel->SaveSettingTemp($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function GetSettingTemp()
    {
        try {
            $this->load->model('DevicesModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->DevicesModel->GetSettingTemp($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}
