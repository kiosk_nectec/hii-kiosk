<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Owners extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // if (!$this->session->userdata('validated')) {
        //     redirect('login');
        // }
    }

    public function index()
    {
    }

    public function getOwnersComboList()
    {
        try {
            $this->load->model('OwnersModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->OwnersModel->getOwnersComboList($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function saveOwners()
    {
        try {
            $this->load->model('OwnersModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->OwnersModel->saveOwners($dataPost);     
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function deleteOwners()
    {
        try {
            $this->load->model('OwnersModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->OwnersModel->deleteOwners($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}
