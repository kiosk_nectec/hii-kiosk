<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Causedby extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // if (!$this->session->userdata('validated')) {
        //     redirect('login');
        // }
    }

    public function index()
    {
    }

    public function getCausedbyComboList()
    {
        try {
            $this->load->model('CausedbyModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->CausedbyModel->getCausedbyComboList($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function saveCausedby()
    {
        try {
            $this->load->model('CausedbyModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->CausedbyModel->saveCausedby($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function deleteCausedby()
    {
        try {
            $this->load->model('CausedbyModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->CausedbyModel->deleteCausedby($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}
