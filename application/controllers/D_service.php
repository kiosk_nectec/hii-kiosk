<?php

defined('BASEPATH') or exit('No direct script access allowed');

class D_service extends MY_Controller
{
    // private $url = 'http://128.199.73.202/middleware/';
    public function __construct()
    {
        parent::__construct();
        // if (!$this->session->userdata('validated')) {
        //     redirect('login');
        // }
    }

    public function index()
    {
    }

    public function getCurrentD_service()
    {
        try {
            $this->load->model('D_serviceModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->D_serviceModel->getCurrentD_service($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function saveD_service()
    {

        try {
            $this->load->model('D_serviceModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->D_serviceModel->saveD_service($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}
