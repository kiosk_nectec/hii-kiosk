<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Api extends MY_Controller
{
    // private $url = 'http://128.199.73.202/middleware/';
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Bangkok");
        // if (!$this->session->userdata('validated')) {
        //     redirect('login');
        // }
    }

    public function index()
    {
    }

    public function GetSession()
    {
        try {
            $this->load->model('D_serviceModel', '', true);
            $result = $this->D_serviceModel->getLastSession();
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        // print_r($result);
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
    public function update_temperature()
    {
        $result = [];
        try {
          
            $session = isset($_GET['session']) ? $_GET['session'] : "";
            $temperature = isset($_GET['temperature']) ? $_GET['temperature'] : "";
            if ($session && $temperature) {
                $this->db->insert('t_temp_temperature', array("session" => $session, "temperature" => $temperature, "create_date" => date("Y-m-d H:i:s")));
                $result['status'] = true;
                $result['message'] = 'success';

                // $this->response($result, 200);
            } else {
                $result['status'] = false;
                $result['message'] = 'session and temperature is require.';
                // $this->response($result, 200);
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] =  $this->mycatch->catch_message($ex);
            // $this->response($result, 500);
        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}
