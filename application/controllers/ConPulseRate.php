<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ConPulseRate extends MY_Controller
{
    // private $url = 'http://128.199.73.202/middleware/';
    public function __construct()
    {
        parent::__construct();
        // if (!$this->session->userdata('validated')) {
        //     redirect('login');
        // }
    }

    public function index()
    {
    }

    public function getConPulseRate()
    {
        try {
            $this->load->model('ConPulseRateModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->ConPulseRateModel->getConPulseRate($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

  
}
