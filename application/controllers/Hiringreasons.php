<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Hiringreasons extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // if (!$this->session->userdata('validated')) {
        //     redirect('login');
        // }
    }

    public function index()
    {
    }

    public function getHiringreasonsComboList()
    {
        try {
            $this->load->model('HiringreasonsModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->HiringreasonsModel->getHiringreasonsComboList($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function saveHiringreasons()
    {
        try {
            $this->load->model('HiringreasonsModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->HiringreasonsModel->saveHiringreasons($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function deleteHiringreasons()
    {
        try {
            $this->load->model('HiringreasonsModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->HiringreasonsModel->deleteHiringreasons($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}
