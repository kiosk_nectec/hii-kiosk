<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Province extends CI_Controller {
	public function __construct() {
        parent::__construct();
		if(! $this->session->userdata('validated')){
            redirect('login');
        }
    }
	 
	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar');
		$this->load->view('setting/settingcompany_view'); 
		$this->load->view('share/footer');
	}
    
    public function getprovinceComboList(){
	 
		try{ 
			$this->load->model('provinceModel','',TRUE);
			$result['status'] = true;
			$result['message'] = $this->provinceModel->getprovinceComboList();
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
}
