<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Fuelcharge extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // if (!$this->session->userdata('validated')) {
        //     redirect('login');
        // }
    }

    public function index()
    {
    }

    public function getFuelchargeComboList()
    {
        try {
            $this->load->model('FuelchargeModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->FuelchargeModel->getFuelchargeComboList($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function saveFuelcharge()
    {
        try {
            $this->load->model('FuelchargeModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->FuelchargeModel->saveFuelcharge($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function deleteFuelcharge()
    {
        try {
            $this->load->model('FuelchargeModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->FuelchargeModel->deleteFuelcharge($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}
