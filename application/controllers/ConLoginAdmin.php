<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ConLoginAdmin extends MY_Controller
{
    // private $url = 'http://128.199.73.202/middleware/';
    public function __construct()
    {
        parent::__construct();
        // if (!$this->session->userdata('validated')) {
        //     redirect('login');
        // }
    }

    public function index()
    {
    }

    public function getConLoginAdmin()
    {
        try {
            $this->load->model('ConLoginAdminModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->ConLoginAdminModel->getConLoginAdmin($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

  
}
