<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH.'libraries/REST_Controller.php';
class UpdateReaderFlag extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('MyCatch');
        date_default_timezone_set('Asia/Bangkok');
    }

    public function index_post()
    {
        $this->load->model('ApiModel', '', true);
        $dataPost = isset($_POST['reader_flag']) ? $_POST['reader_flag'] : '';
        $result = $this->ApiModel->updateReaderFlag($dataPost);
        $this->response($result, 200);
    }
}
