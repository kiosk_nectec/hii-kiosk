<?php
defined('BASEPATH') OR exit('No direct script access allowed');

 
class Example extends MY_Controller {
 
	public function __construct() {
        parent::__construct();        
		// if(! $this->session->userdata('validated')){
        //     redirect('login');
        // }
    }
	
	public function index()
	{
		
	}
	
	public function ex_api() {
		$device_id = $this->uri->segment(3, 0);
	
		switch ($device_id) {
			case '1':
				$result['data']['systolic'] = "100";
				$result['data']['diastolic'] = "89";
				$result['data']['pulse'] = "20";
			
			  break;
			case '2':
				$result['data'] = "not found";
			  break;
			case '3':
				$result['data']['value'] = "36.2";
			  break;
			case '4':
			
				$result['data']['titlename'] = "นาย";
				$result['data']['firstname'] = "ปรัชญา";
				$result['data']['lastname'] = "อินพุ่ม";
				$result['data']['id_card'] = "1102200133xxx";
				$result['data']['birthday'] = "17/05/38";
				$result['data']['address'] = "2 ซอย 4";
				$result['data']['issue_date'] = "17/05/38";
				$result['data']['card_expiration'] = "17/05/38";
				$result['data']['photo'] = "/images/demo.png";

		
				break;
			default:
				$result['data'] = "not found";
				break;
		  }
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}

