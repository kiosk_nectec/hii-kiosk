<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Staff extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function SearchPerson()
    {
        try {
            $this->load->model('StaffModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->StaffModel->SearchPerson($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}
