<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Rejectreasons extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // if (!$this->session->userdata('validated')) {
        //     redirect('login');
        // }
    }

    public function index()
    {
    }

    public function getRejectreasonsComboList()
    {
        try {
            $this->load->model('RejectreasonsModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->RejectreasonsModel->getRejectreasonsComboList($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function saveRejectreasons()
    {
        try {
            $this->load->model('RejectreasonsModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->RejectreasonsModel->saveRejectreasons($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function deleteRejectreasons()
    {
        try {
            $this->load->model('RejectreasonsModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->RejectreasonsModel->deleteRejectreasons($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}
