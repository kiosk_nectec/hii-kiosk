<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Background extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getFileVideoComboList()
    {
        try {
            $this->load->model('BackgroundModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->BackgroundModel->getFileVideoComboList($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function getFileImageComboList()
    {
        try {
            $this->load->model('BackgroundModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->BackgroundModel->getFileImageComboList($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function getAllFileComboList()
    {
        try {
            $this->load->model('BackgroundModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result['messageImg'] = $this->BackgroundModel->getFileImageComboList($dataPost);
            $result['messageVideo'] = $this->BackgroundModel->getFileVideoComboList($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function alert_line_notify()
    {
        date_default_timezone_set('Asia/Bangkok');
        $date = date('Y-m-d H:i:s');
        // $Token = '7JP3dxsRP4bB6rsXVOuIpQSIOZ2fzGlTomYL9QW7eHN';
        $Token = 'CVSubhQqXZ9RaL82FyLm7cAM4faynBSlMKGGVLeSXTc';
        $message = ' วันที่ '.$date.' , มีการเข้า ScreenServer';
        // call line api

        $lineapi = $Token; // ใส่ token key ที่ได้มา
        $mms = trim($message); // ข้อความที่ต้องการส่ง
        date_default_timezone_set('Asia/Bangkok');
        $chOne = curl_init();
        curl_setopt($chOne, CURLOPT_URL, 'https://notify-api.line.me/api/notify');
        // SSL USE
        curl_setopt($chOne, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($chOne, CURLOPT_SSL_VERIFYPEER, 0);
        //POST
        curl_setopt($chOne, CURLOPT_POST, 1);
        curl_setopt($chOne, CURLOPT_POSTFIELDS, "message=$mms");
        curl_setopt($chOne, CURLOPT_FOLLOWLOCATION, 1);
        $headers = ['Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer '.$lineapi.''];
        curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($chOne, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($chOne);
        //Check error
        if (curl_error($chOne)) {
            // echo 'error:' . curl_error($chOne);
        } else {
            $result_ = json_decode($result, true);
            // echo "status : ".$result_['status']; echo "message : ". $result_['message'];
        // redirect('/menu_list/ordered','refresh');
        }
        curl_close($chOne);
    }
}
