<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Account extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // if (!$this->session->userdata('validated')) {
        //     redirect('login');
        // }
    }

    public function index()
    {
    }

    public function getAccountComboList()
    {
        try {
            $this->load->model('AccountModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->AccountModel->getAccountComboList($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function saveAccount()
    {
        try {
            $this->load->model('AccountModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->AccountModel->saveAccount($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function deleteAccount()
    {
        try {
            $this->load->model('AccountModel', '', true);
            $dataPost = json_decode($this->input->raw_input_stream, true);
            $result = $this->AccountModel->deleteAccount($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}
