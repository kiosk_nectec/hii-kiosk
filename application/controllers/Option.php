<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Option extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // if (!$this->session->userdata('validated')) {
        //     redirect('login');
        // }
    }

    public function getStatusActiveOption()
    {
        try {
            $result['status'] = true;
            $result['message'] = $this->StatusActiveOption();
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function getTypeLoginOption()
    {
        try {
            $result['status'] = true;
            $result['message'] = $this->getTypeLoginOption();
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function getDropDrowTypeOption()
    {
        try {
            $result['status'] = true;
            $result['message'] = $this->DropDrowTypeOption();
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}
