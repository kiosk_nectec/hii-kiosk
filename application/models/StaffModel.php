<?php

class StaffModel extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Bangkok');
        $this->load->library('MyEnDecode');
    }

    public function SearchPerson($dataPost)
    {
        try {
            include './application/models/convert/TitleNameTH.php';

            $dataModel['Name'] = isset($dataPost['Name']) ? $dataPost['Name'] : '';
            // print_r($dataModel);
            // die();
            $nResult = $this->SQL_getPerson($dataModel);
            $listrs = [];
            foreach ($nResult as $rs) {
                $item['PID'] = $rs['PID'];
                $item['CID'] = $this->myendecode->aes_decrypt($rs['CID'], 'hii-kiosk');
                foreach ($prename as $data) {
                    if ($data['namecode'] == $rs['PRENAME']) {
                        $item['titlenameth'] = $data['rename'];
                        break;
                    }
                }
                if (null != $rs && null == $rs['PHOTO_FACELIFT']) {
                    $item['profile_img'] = $rs['PHOTO_ID'];
                } else {
                    $item['profile_img'] = $rs['PHOTO_FACELIFT'];
                }
                $item['NAME'] = $rs['NAME'];
                $item['LNAME'] = $rs['LNAME'];
                $item['BIRTH'] = $rs['BIRTH'];
                array_push($listrs, $item);
            }
            $result['status'] = true;
            $result['message'] = $listrs;
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_getPerson($dataModel)
    {
        $sql = 'SELECT t1.KIOSKCODE,t1.PID,t1.CID,t1.PRENAME,t1.`NAME`,t1.LNAME,t1.SEX,t1.BIRTH,t1.PHOTO_ID,t1.PHOTO_FACELIFT FROM t_person t1 WHERE 0=0 ';
        $sql = $this->SQL_searchPerson($dataModel, $sql);
        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function SQL_searchPerson($dataModel, $sql)
    {
        if (isset($dataModel['Name']) && $dataModel['Name'] != '') {
            $sql .= " AND NAME like '%".$dataModel['Name']."%' ";
        }

        return $sql;
    }
}
