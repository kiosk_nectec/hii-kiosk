<?php

class ApiModel extends CI_Model
{
    private $tbl_name = 't_idcard_flag';

    public function __construct()
    {
        parent::__construct();
    }

    public function updateReaderFlag($dataPost)
    {
        try {
            $DataModel['reader_flag'] = isset($dataPost) ? strtoupper($dataPost) : 'N';
            $Wording = 'No';
            if ($DataModel['reader_flag'] == 'Y') {
                $Wording = 'Yes';
            } else {
                $Wording = 'No';
            }
            $uResult = $this->SQL_updateReaderFlag($DataModel);
            if ($uResult) {
                $result['status'] = true;
                $result['message'] = 'Update '.$Wording.' Success';
            } else {
                $result['status'] = false;
                $result['message'] = 'Update '.$Wording.' Fail';
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_updateReaderFlag($DataModel)
    {
        return $this->db->update($this->tbl_name, $DataModel);
    }
}
