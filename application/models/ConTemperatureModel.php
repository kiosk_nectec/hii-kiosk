<?php

class ConTemperatureModel extends MY_Model
{
    private $tbl_name = 't_con_temperature';

    public function __construct()
    {
        parent::__construct();
    }

    public function getConTemperature($dataPost)
    {
        try {
            $result['status'] = true;
            $result['message'] = $this->SQL_getConTemperature($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }

        return $result;
    }

    public function SQL_getConTemperature($dataModel)
    {
        $sql = "SELECT * From " . $this->tbl_name . " Where Delete_flag = 0";

        $sql = $this->SQL_searchConTemperature($dataModel, $sql);

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function SQL_searchConTemperature($dataModel, $sql)
    {

        if (isset($dataModel['active_flag']) && $dataModel['active_flag'] != '') {
            $sql .= " and active_flag ='" . $dataModel['active_flag'] . "'";
        }
        return $sql;
    }
    

}
