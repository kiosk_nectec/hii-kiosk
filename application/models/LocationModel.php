<?php

class LocationModel extends MY_Model
{
    private $tbl_name = 'location';

    public function __construct()
    {
        parent::__construct();
    }

    public function getLocationComboList($dataPost)
    {
        try {
            $PageIndex = isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
            $PageSize = isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 10;
            $direction = isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : '';
            $SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : 'asc';

            $offset = ($PageIndex - 1) * $PageSize;

            $result['status'] = true;
            $result['message'] = $this->SQL_getLocationComboList($DataModel, $PageSize, $offset, $direction, $SortOrder);

            $result['totalRecords'] = $this->SQL_getLocationTotalList($DataModel);
            $result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_getLocationComboList($DataModel, $limit = 10, $offset = 0, $Order = '', $direction = 'asc')
    {
        $sql = 'SELECT * From '.$this->tbl_name.' Where DeleteFlag = 0';
        if ($Order != '') {
            $sql .= ' ORDER BY '.$Order.' '.$direction;
        }
        $sql .= " LIMIT $offset, $limit";
        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function SQL_getLocationTotalList($DataModel)
    {
        $sql = 'SELECT * From '.$this->tbl_name.' Where DeleteFlag = 0';

        $query = $this->db->query($sql);

        return $query->num_rows();
    }

    public function saveLocation($dataPost)
    {
        try {
            $DataModel['LocationId'] = isset($dataPost['LocationId']) ? $dataPost['LocationId'] : 0;
            $DataModel['Code'] = isset($dataPost['Code']) ? $dataPost['Code'] : '';
            $DataModel['OwnerId'] = isset($dataPost['OwnerId']) ? $dataPost['OwnerId'] : null;
            $DataModel['Description'] = isset($dataPost['Description']) ? $dataPost['Description'] : '';
            $DataModel['PUDCostcenter'] = isset($dataPost['PUDCostcenter']) ? $dataPost['PUDCostcenter'] : '';
            $DataModel['LOPCostcenter'] = isset($dataPost['LOPCostcenter']) ? $dataPost['LOPCostcenter'] : '';
            $DataModel['Address1'] = isset($dataPost['Address1']) ? $dataPost['Address1'] : '';
            $DataModel['Address2'] = isset($dataPost['Address2']) ? $dataPost['Address2'] : '';
            $DataModel['Postcode'] = isset($dataPost['Postcode']) ? $dataPost['Postcode'] : '';
            $DataModel['Status'] = isset($dataPost['Status']) ? $dataPost['Status'] : '';
            // $DataModel['UpdateBy'] = $this->session->userdata('name');
            $DataModel['UpdateBy'] = 'admin';
            $DataModel['UpdateDate'] = date('Y-m-d H:i:s');
            if ($DataModel['LocationId'] == 0) {
                // ChkCode
                $ChkCode = $this->SQL_ChkCode($DataModel['Code']);
                if (null != $ChkCode && count($ChkCode) > 0) {
                    $result['status'] = false;
                    $result['message'] = 'Code is duplicated';
                    echo json_encode($result, JSON_UNESCAPED_UNICODE);
                    exit();
                }
            }
            if (strlen($DataModel['Postcode']) != 5) {
                $result['status'] = false;
                $result['message'] = 'Must be a 5-digit code.';
                echo json_encode($result, JSON_UNESCAPED_UNICODE);
                exit();
            }
            if ($DataModel['LocationId'] == 0) {
                $DataModel['CreateDate'] = date('Y-m-d H:i:s');
                $nResult = $this->SQL_insertLocation($DataModel);
                if ($nResult > 0) {
                    $result['status'] = true;
                    $result['message'] = $this->lang->line('SAVESUCCESS');
                } else {
                    $result['status'] = false;
                    $result['message'] = $this->lang->line('SAVEFAIL');
                }
            } else {
                $uResult = $this->SQL_updateLocation($DataModel);
                if ($uResult) {
                    $result['status'] = true;
                    $result['message'] = $this->lang->line('UPDATESUCCESS');
                } else {
                    $result['status'] = false;
                    $result['message'] = $this->lang->line('UPDATEFAIL');
                }
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_ChkCode($DataModel)
    {
        $sql = 'SELECT * From '.$this->tbl_name.' Where Code = "'.$DataModel.'"';
        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function SQL_insertLocation($DataModel)
    {
        $this->db->insert($this->tbl_name, $DataModel);

        return $this->db->insert_id();
    }

    public function SQL_updateLocation($DataModel)
    {
        $this->db->where('LocationId', $DataModel['LocationId']);

        return $this->db->update($this->tbl_name, $DataModel);
    }

    public function deleteLocation($dataPost)
    {
        try {
            $DataModel['LocationId'] = isset($dataPost['LocationId']) ? $dataPost['LocationId'] : 0;
            $nResult = $this->SQL_deleteLocation($DataModel);
            if ($nResult) {
                $result['status'] = true;
                $result['message'] = $this->lang->line('DELETESUCCESS');
            } else {
                $result['status'] = false;
                $result['message'] = $this->lang->line('DELETEFAIL');
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_deleteLocation($DataModel)
    {
        $this->db->where('LocationId', $DataModel['LocationId']);
        $DataModel = [
            'DeleteFlag' => 1,
        ];

        return $this->db->update($this->tbl_name, $DataModel);
    }
}
