<?php

class DetailModel extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function GetDataGraph($dataPost)
    {
        try {
            $DataType = isset($dataPost['type']) ? $dataPost['type'] : '';
            $tapId = isset($dataPost['tapId']) ? $dataPost['tapId'] : '';
            $DataModel['USER_TYPE'] = isset($dataPost['usertype']) ? $dataPost['usertype'] : '';
            $DataModel['PID'] = isset($dataPost['PID']) ? $dataPost['PID'] : '';
            $DataModel['date'] = isset($dataPost['date']) ? $dataPost['date'] : date('d-m-Y');
            // $DataModel['USER_TYPE'] = '1';
            // $DataModel['PID'] = '2021000011';

            $result['status'] = true;
            $result['message'] = $this->SQL_GetDataGraph($DataModel, $DataType, $tapId);
            $result['standard'] = $this->SQL_GetStandard($DataModel, $DataType, $tapId);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_GetDataGraph($DataModel, $DataType, $tapId)
    {
        $sql = 'SELECT *,';
        // if ($tapId == 'menu-2' && ($DataType == 'MONTH' || $DataType == 'YEAR')) {
        //     $sql .= 'Max(D_UPDATE),';
        // }
        $sql .= "TIME_FORMAT(TIME_SERV, '%H:%i') AS TIME,
                    DATE_FORMAT(DATE_SERV,'%d') AS DAY,
                    DATE_FORMAT(DATE_SERV,'%m') AS MONTH,
                    DATE_FORMAT(DATE_SERV,'%Y') AS YEAR
                FROM
                    `t_service` 
                WHERE
                    PID = '".$DataModel['PID']."' AND USER_TYPE = '".$DataModel['USER_TYPE']."'";
        if ($DataType == 'DAY') {
            $DataModel['DATE'] = date('d-m-Y', strtotime($DataModel['date']));
            $sql .= " AND DATE_FORMAT( DATE_SERV, '%d-%m-%Y' ) = '".$DataModel['DATE']."'";
            if ($tapId == 'menu-1') {
                $sql .= ' AND WEIGHT IS NOT NULL';
            } elseif ($tapId == 'menu-2') {
                $sql .= ' AND SBP IS NOT NULL AND DBP IS NOT NULL';
            } elseif ($tapId == 'menu-3') {
                $sql .= ' AND OXYGEN IS NOT NULL AND OXYGEN BETWEEN 90 AND 100';
            } elseif ($tapId == 'menu-4') {
                $sql .= ' AND BTEMP IS NOT NULL AND BTEMP BETWEEN 35 AND 40';
            }
        } elseif ($DataType == 'MONTH') {
            $DataModel['DATE'] = date('m-Y', strtotime($DataModel['date']));
            $sql .= " AND DATE_FORMAT( DATE_SERV, '%m-%Y' ) = '".$DataModel['DATE']."'";
            if ($tapId == 'menu-1') {
                $sql .= ' AND WEIGHT IS NOT NULL';
            } elseif ($tapId == 'menu-2') {
                $sql .= ' AND SBP IS NOT NULL AND DBP IS NOT NULL';
            } elseif ($tapId == 'menu-3') {
                $sql .= ' AND OXYGEN IS NOT NULL AND OXYGEN BETWEEN 90 AND 100';
            } elseif ($tapId == 'menu-4') {
                $sql .= ' AND BTEMP IS NOT NULL AND BTEMP BETWEEN 35 AND 40';
            }
        } elseif ($DataType == 'YEAR') {
            $DataModel['DATE'] = date('Y', strtotime($DataModel['date']));
            $sql .= " AND DATE_FORMAT( DATE_SERV, '%Y' ) = '".$DataModel['DATE']."'";
            if ($tapId == 'menu-1') {
                $sql .= ' AND WEIGHT IS NOT NULL';
            } elseif ($tapId == 'menu-2') {
                $sql .= ' AND SBP IS NOT NULL AND DBP IS NOT NULL';
            } elseif ($tapId == 'menu-3') {
                $sql .= ' AND OXYGEN IS NOT NULL AND OXYGEN BETWEEN 90 AND 100';
            } elseif ($tapId == 'menu-4') {
                $sql .= ' AND BTEMP IS NOT NULL AND BTEMP BETWEEN 35 AND 40';
            }
        } else {
        }
        $sql .= ' ORDER BY D_UPDATE ASC';
        // print_r($sql);
        // die();
        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function SQL_GetStandard($DataModel, $DataType, $tapId)
    {
        $sql .= 'SELECT * FROM `t_standard_value` WHERE 0 = 0';
        if ($tapId == 'menu-1') {
            $sql .= ' AND STANDARD_NAME = "WEIGHT"';
        } elseif ($tapId == 'menu-2') {
            $sql .= ' AND STANDARD_NAME = "BP"';
        } elseif ($tapId == 'menu-3') {
            $sql .= ' AND STANDARD_NAME = "OXYGEN"';
        } elseif ($tapId == 'menu-4') {
            $sql .= ' AND STANDARD_NAME = "BTEMP"';
        }
        $sql .= 'LIMIT 1';
        $query = $this->db->query($sql);

        return $query->result_array();
    }
}
