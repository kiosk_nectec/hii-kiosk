<?php

class FuelchargeModel extends MY_Model
{
    private $tbl_name = 'customer_fuelchargerate';

    public function __construct()
    {
        parent::__construct();
    }

    public function getFuelchargeComboList($dataPost)
    {
        try {
            $PageIndex = isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
            $PageSize = isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 10;
            $direction = isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : '';
            $SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : 'asc';

            $offset = ($PageIndex - 1) * $PageSize;

            $result['status'] = true;
            $result['message'] = $this->SQL_getFuelchargeComboList($DataModel, $PageSize, $offset, $direction, $SortOrder);

            $result['totalRecords'] = $this->SQL_getFuelchargeTotalList($DataModel);
            $result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_getFuelchargeComboList($DataModel, $limit = 10, $offset = 0, $Order = '', $direction = 'asc')
    {
        $sql = 'SELECT * From '.$this->tbl_name.' Where DeleteFlag = 0';
        $sql .= ' ORDER BY EffYear DESC,EffMonth DESC';
        $sql .= " LIMIT $offset, $limit";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function SQL_getFuelchargeTotalList($DataModel)
    {
        $sql = 'SELECT * From '.$this->tbl_name.' Where DeleteFlag = 0';

        $query = $this->db->query($sql);

        return $query->num_rows();
    }

    public function saveFuelcharge($dataPost)
    {
        try {
            $DataModel['RateId'] = isset($dataPost['RateId']) ? $dataPost['RateId'] : 0;
            $DataModel['EffYear'] = isset($dataPost['EffYear']) ? $dataPost['EffYear'] : '';
            $DataModel['EffMonth'] = isset($dataPost['EffMonth']) ? $dataPost['EffMonth'] : '';
            $DataModel['ChargeRate'] = isset($dataPost['ChargeRate']) ? $dataPost['ChargeRate'] : '';
            $DataModel['CurrentRate'] = isset($dataPost['CurrentRate']) ? $dataPost['CurrentRate'] : '';
            // $DataModel['UpdateBy'] = $this->session->userdata('name');
            $DataModel['UpdateBy'] = 'admin';
            $DataModel['UpdateDate'] = date('Y-m-d H:i:s');
            if ($DataModel['EffYear'] < 2016) {
                $result['status'] = false;
                $result['message'] = 'Year must be starting from 2016.';
                echo json_encode($result, JSON_UNESCAPED_UNICODE);
                exit();
            }
            // ChkYearMonth
            $ChkYearMonth = $this->SQL_ChkYearMonth($DataModel);
            if (null != $ChkYearMonth && count($ChkYearMonth) > 0) {
                $result['status'] = false;
                $result['message'] = 'Year/Month is duplicated';
                echo json_encode($result, JSON_UNESCAPED_UNICODE);
                exit();
            }
            if ($DataModel['RateId'] == 0) {
                $DataModel['CreateDate'] = date('Y-m-d H:i:s');
                $nResult = $this->SQL_insertFuelcharge($DataModel);
                if ($nResult > 0) {
                    $result['status'] = true;
                    $result['message'] = $this->lang->line('SAVESUCCESS');
                } else {
                    $result['status'] = false;
                    $result['message'] = $this->lang->line('SAVEFAIL');
                }
            } else {
                $uResult = $this->SQL_updateFuelcharge($DataModel);
                if ($uResult) {
                    $result['status'] = true;
                    $result['message'] = $this->lang->line('UPDATESUCCESS');
                } else {
                    $result['status'] = false;
                    $result['message'] = $this->lang->line('UPDATEFAIL');
                }
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_ChkYearMonth($DataModel)
    {
        $sql = 'SELECT * From '.$this->tbl_name.' Where EffYear = "'.$DataModel['EffYear'].'" AND EffMonth = "'.$DataModel['EffYear'].'"';
        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function SQL_insertFuelcharge($DataModel)
    {
        $this->db->insert($this->tbl_name, $DataModel);

        return $this->db->insert_id();
    }

    public function SQL_updateFuelcharge($DataModel)
    {
        $this->db->where('RateId', $DataModel['RateId']);

        return $this->db->update($this->tbl_name, $DataModel);
    }

    public function deleteFuelcharge($dataPost)
    {
        try {
            $DataModel['RateId'] = isset($dataPost['RateId']) ? $dataPost['RateId'] : 0;
            $nResult = $this->SQL_deleteFuelcharge($DataModel);
            if ($nResult) {
                $result['status'] = true;
                $result['message'] = $this->lang->line('DELETESUCCESS');
            } else {
                $result['status'] = false;
                $result['message'] = $this->lang->line('DELETEFAIL');
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_deleteFuelcharge($DataModel)
    {
        $this->db->where('RateId', $DataModel['RateId']);
        $DataModel = [
            'DeleteFlag' => 1,
        ];

        return $this->db->update($this->tbl_name, $DataModel);
    }
}
