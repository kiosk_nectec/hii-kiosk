<?php

class BackgroundModel extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getFileVideoComboList()
    {
        try {
            $dir = glob('backgroundvideo/video/*.*');
            $result['status'] = true;
            $result['message'] = $dir;
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function getFileImageComboList()
    {
        try {
            $this->load->model('DevicesModel', '', true);
            $sScreen = $this->DevicesModel->GetSettingScreen(null);
            $dir = glob('backgroundimage/img/*.*');
            $result['status'] = true;
            $result['message'] = $dir;
            $result['sScreen'] = $sScreen['message'];
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }
}
