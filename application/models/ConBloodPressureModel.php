<?php

class ConBloodPressureModel extends MY_Model
{
    private $tbl_name = 't_con_blood_pressure';

    public function __construct()
    {
        parent::__construct();
    }

    public function getConBloodPressure($dataPost)
    {
        try {
            $result['status'] = true;
            $result['message'] = $this->SQL_getConBloodPressure($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_getConBloodPressure($dataModel)
    {
        $sql = 'SELECT * From '.$this->tbl_name.' Where Delete_flag = 0';

        $sql = $this->SQL_searchConBloodPressure($dataModel, $sql);

        $sql .= ' ORDER BY Importance ASC';

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function SQL_searchConBloodPressure($dataModel, $sql)
    {
        if (isset($dataModel['active_flag']) && $dataModel['active_flag'] != '') {
            $sql .= " and active_flag ='".$dataModel['active_flag']."'";
        }

        return $sql;
    }

    public function getApiBloodPressureStatus()
    {
        try {
            $func = $this->uri->segment(3, 0);
            $device = $this->uri->segment(4, 0);
            $reqTime = date('Y-m-d H:i:s');
            $curl = curl_init();

            $timeout = $device == 'pressuregauge' ? 120 : 45;
            $url = $this->config->item('base_api_url').'status/pressuregauge';
            // Assign POST data
            curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            ]);
            $result = curl_exec($curl);
            $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $time = curl_getinfo($curl, CURLINFO_TOTAL_TIME);
            $error = curl_error($curl);
            $resTime = date('Y-m-d H:i:s');
            $modelData = ['request_url' => $url, 'request_time' => $reqTime, 'response_time' => $resTime, 'status_code' => $statusCode, 'error_message' => $error, 'response_message' => $result];
            $this->load->model('LineNotiFyModel', '', true);
            if ($modelData['status_code'] != 200) {
                $this->LineNotiFyModel->alert_line_notify($modelData, false, 'UserAction', 'UserAction');
            } else {
                $this->LineNotiFyModel->alert_line_notify($modelData, true, 'UserAction', 'UserAction');
            }
            $this->db->insert('t_request_log', $modelData);
            if (curl_errno($curl)) {
                $result = [];

                $result['success'] = false;
                echo json_encode($result, JSON_UNESCAPED_UNICODE);
                exit();
            } elseif ($statusCode != 200) {
                $result = [];
                $result['success'] = false;
                echo json_encode($result, JSON_UNESCAPED_UNICODE);
                exit();
            }

            curl_close($curl);
        } catch (Exception $ex) {
            $result = $ex;
        }

        return json_decode($result, true);
    }
}
