<?php

class LineNotiFyModel extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    // 7JP3dxsRP4bB6rsXVOuIpQSIOZ2fzGlTomYL9QW7eHN
    public function alert_line_notify($DataModel, $status, $action, $faceApi = false)
    {
        date_default_timezone_set('Asia/Bangkok');
        $date = date('Y-m-d H:i:s');

        if ($status == true) {
            if ($faceApi) {
                $Token = 'Vg2pfq2pocJP5VkndqDzolwqzNWdbEsDeqZg5mrISop';
                if (strstr($DataModel['response_message'], '"status":"false"') != '') {
                    $message = 'มีการ Login ด้วยใบไม่หน้าสำเร็จ, วันที่ '.$date.' , และ มีค่า return เป็นปกติ : request_url = '.$DataModel['request_url'].', status_code = '.$DataModel['status_code'].', error_message = '.$DataModel['error_message'].', response_message = '.$DataModel['response_message'].', action = '.$action;
                } else {
                    $message = 'มีการ Login ด้วยใบหน้าสำเร็จ, วันที่ '.$date.' , และ มีค่า return เป็นปกติ : request_url = '.$DataModel['request_url'].', status_code = '.$DataModel['status_code'].', error_message = '.$DataModel['error_message'].', response_message = '.$DataModel['response_message'].', action = '.$action;
                }
            } else {
                // $Token = '7JP3dxsRP4bB6rsXVOuIpQSIOZ2fzGlTomYL9QW7eHN';
                $Token = 'CVSubhQqXZ9RaL82FyLm7cAM4faynBSlMKGGVLeSXTc';
                $message = ' วันที่ '.$date.' , มีค่า return เป็นปกติ : request_url = '.$DataModel['request_url'].', status_code = '.$DataModel['status_code'].', error_message = '.$DataModel['error_message'].', response_message = '.$DataModel['response_message'].', action = '.$action;
            }
        } else {
            if ($faceApi) {
                $Token = 'Vg2pfq2pocJP5VkndqDzolwqzNWdbEsDeqZg5mrISop';
                $message = 'เกิดการทำงานที่ผิดพลาดของ faceApi, วันที่ '.$date.' , เนื่องจาก มีค่า return ที่เป็น failed : request_url = '.$DataModel['request_url'].', status_code = '.$DataModel['status_code'].', error_message = '.$DataModel['error_message'].', response_message = '.$DataModel['response_message'].', action = '.$action;
            } else {
                // $Token = '7JP3dxsRP4bB6rsXVOuIpQSIOZ2fzGlTomYL9QW7eHN';
                $Token = '6dGsq7ZxexFz8xXH0qC8YeViq6Uq1ck8y6GR9yOuP1O';
                $message = ' วันที่ '.$date.' , มีค่า return ที่เป็น failed : request_url = '.$DataModel['request_url'].', status_code = '.$DataModel['status_code'].', error_message = '.$DataModel['error_message'].', response_message = '.$DataModel['response_message'].', action = '.$action;
            }
        }
        // call line api

        $lineapi = $Token; // ใส่ token key ที่ได้มา
        $mms = trim($message); // ข้อความที่ต้องการส่ง
        date_default_timezone_set('Asia/Bangkok');
        $chOne = curl_init();
        curl_setopt($chOne, CURLOPT_URL, 'https://notify-api.line.me/api/notify');
        // SSL USE
        curl_setopt($chOne, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($chOne, CURLOPT_SSL_VERIFYPEER, 0);
        //POST
        curl_setopt($chOne, CURLOPT_POST, 1);
        curl_setopt($chOne, CURLOPT_POSTFIELDS, "message=$mms");
        curl_setopt($chOne, CURLOPT_FOLLOWLOCATION, 1);
        $headers = ['Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer '.$lineapi.''];
        curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($chOne, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($chOne);
        //Check error
        if (curl_error($chOne)) {
            // echo 'error:' . curl_error($chOne);
        } else {
            $result_ = json_decode($result, true);
            // echo "status : ".$result_['status']; echo "message : ". $result_['message'];
        // redirect('/menu_list/ordered','refresh');
        }
        curl_close($chOne);
    }
    public function alert_line_notify_idcard($DataModel)
    {
        date_default_timezone_set('Asia/Bangkok');
        $date = date('Y-m-d H:i:s');
        $Token = 'YSWD1vCf6s6U5ttJHb3o2TR7PXdyGD1NM26ARqGCHGR';
        $message = ' วันที่ ' . $date . ' , มีการเรียก function readeridcard และมีค่า return เป็น : request_url = ' . $DataModel['request_url'] . ', status_code = ' . $DataModel['status_code'] . ', error_message = ' . $DataModel['error_message'] . ', response_message = ' . $DataModel['response_message'] . ', action = readeridcard';
        // echo "1";
        // call line api

        $lineapi = $Token; // ใส่ token key ที่ได้มา
        $mms = trim($message); // ข้อความที่ต้องการส่ง
        date_default_timezone_set('Asia/Bangkok');
        $chOne = curl_init();
        curl_setopt($chOne, CURLOPT_URL, 'https://notify-api.line.me/api/notify');
        // SSL USE
        curl_setopt($chOne, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($chOne, CURLOPT_SSL_VERIFYPEER, 0);
        //POST
        curl_setopt($chOne, CURLOPT_POST, 1);
        curl_setopt($chOne, CURLOPT_POSTFIELDS, "message=$mms");
        curl_setopt($chOne, CURLOPT_FOLLOWLOCATION, 1);
        $headers = ['Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer ' . $lineapi . ''];
        curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($chOne, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($chOne);
        //Check error
        if (curl_error($chOne)) {
            // echo 'error:' . curl_error($chOne);
        } else {
            $result_ = json_decode($result, true);
            // echo "status : ".$result_['status']; echo "message : ". $result_['message'];
            // redirect('/menu_list/ordered','refresh');
        }
        curl_close($chOne);
    }
}
