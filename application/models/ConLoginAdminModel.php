<?php

class ConLoginAdminModel extends MY_Model
{
    private $tbl_name = 't_login';

    public function __construct()
    {
        parent::__construct();
    }

    public function getConLoginAdmin($dataPost)
    {
        try {
            // print_r($dataPost);
            // die();
            $result['status'] = true;
            $result['message'] = $this->SQL_getConLoginAdmin($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_getConLoginAdmin($dataModel)
    {
        $sql = 'SELECT * From '.$this->tbl_name." WHERE password = '".$dataModel['password']."'";

        // print_r($dataModel);

        $query = $this->db->query($sql);

        return $query->result_array();
    }
}
