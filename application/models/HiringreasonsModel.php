<?php

class HiringreasonsModel extends MY_Model
{
    private $tbl_name = 'hiringreason';

    public function __construct()
    {
        parent::__construct();
    }

    public function getHiringreasonsComboList($dataPost)
    {
        try {
            $PageIndex = isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
            $PageSize = isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 10;
            $direction = isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : '';
            $SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : 'asc';

            $offset = ($PageIndex - 1) * $PageSize;

            $result['status'] = true;
            $result['message'] = $this->SQL_getHiringreasonsComboList($DataModel, $PageSize, $offset, $direction, $SortOrder);

            $result['totalRecords'] = $this->SQL_getHiringreasonsTotalList($DataModel);
            $result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_getHiringreasonsComboList($DataModel, $limit = 10, $offset = 0, $Order = '', $direction = 'asc')
    {
        $sql = 'SELECT * From '.$this->tbl_name.' Where DeleteFlag = 0';
        if ($Order != '') {
            $sql .= ' ORDER BY '.$Order.' '.$direction;
        }
        $sql .= " LIMIT $offset, $limit";
        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function SQL_getHiringreasonsTotalList($DataModel)
    {
        $sql = 'SELECT * From '.$this->tbl_name.' Where DeleteFlag = 0';

        $query = $this->db->query($sql);

        return $query->num_rows();
    }

    public function saveHiringreasons($dataPost)
    {
        try {
            $DataModel['ReasonId'] = isset($dataPost['ReasonId']) ? $dataPost['ReasonId'] : 0;
            $DataModel['Code'] = isset($dataPost['Code']) ? $dataPost['Code'] : '';
            $DataModel['Status'] = isset($dataPost['Status']) ? $dataPost['Status'] : '';
            $DataModel['Description'] = isset($dataPost['Description']) ? $dataPost['Description'] : '';
            // $DataModel['UpdateBy'] = $this->session->userdata('name');
            $DataModel['UpdateBy'] = 'admin';
            $DataModel['UpdateDate'] = date('Y-m-d H:i:s');
            if ($DataModel['ReasonId'] == 0) {
                // ChkCode
                $ChkCode = $this->SQL_ChkCode($DataModel['Code']);
                if (null != $ChkCode && count($ChkCode) > 0) {
                    $result['status'] = false;
                    $result['message'] = 'Code is duplicated';
                    echo json_encode($result, JSON_UNESCAPED_UNICODE);
                    exit();
                }
                $DataModel['CreateDate'] = date('Y-m-d H:i:s');
                $nResult = $this->SQL_insertHiringreasons($DataModel);
                if ($nResult > 0) {
                    $result['status'] = true;
                    $result['message'] = $this->lang->line('SAVESUCCESS');
                } else {
                    $result['status'] = false;
                    $result['message'] = $this->lang->line('SAVEFAIL');
                }
            } else {
                $uResult = $this->SQL_updateHiringreasons($DataModel);
                if ($uResult) {
                    $result['status'] = true;
                    $result['message'] = $this->lang->line('UPDATESUCCESS');
                } else {
                    $result['status'] = false;
                    $result['message'] = $this->lang->line('UPDATEFAIL');
                }
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_ChkCode($DataModel)
    {
        $sql = 'SELECT * From '.$this->tbl_name.' Where Code = "'.$DataModel.'"';
        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function SQL_insertHiringreasons($DataModel)
    {
        $this->db->insert($this->tbl_name, $DataModel);

        return $this->db->insert_id();
    }

    public function SQL_updateHiringreasons($DataModel)
    {
        $this->db->where('ReasonId', $DataModel['ReasonId']);

        return $this->db->update($this->tbl_name, $DataModel);
    }

    public function deleteHiringreasons($dataPost)
    {
        try {
            $DataModel['ReasonId'] = isset($dataPost['ReasonId']) ? $dataPost['ReasonId'] : 0;
            $nResult = $this->SQL_deleteHiringreasons($DataModel);
            if ($nResult) {
                $result['status'] = true;
                $result['message'] = $this->lang->line('DELETESUCCESS');
            } else {
                $result['status'] = false;
                $result['message'] = $this->lang->line('DELETEFAIL');
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_deleteHiringreasons($DataModel)
    {
        $this->db->where('ReasonId', $DataModel['ReasonId']);
        $DataModel = [
            'DeleteFlag' => 1,
        ];

        return $this->db->update($this->tbl_name, $DataModel);
    }
}
