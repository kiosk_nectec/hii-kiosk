<?php

class DevicesModel extends MY_Model
{
    private $tbl_name = 't_devices';
    private $tbl_t_setting_screen = 't_setting_screen';
    private $tbl_t_setting_temp = 't_setting_temp';
    private $tbl_t_rfid = 't_rfid';

    public function __construct()
    {
        parent::__construct();
    }

    public function getDevices($dataPost)
    {
        try {
            $result['status'] = true;
            $result['message'] = $this->SQL_getDevices($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }

        return $result;
    }

    public function SQL_getDevices($dataModel)
    {
        $sql = 'SELECT * From ' . $this->tbl_name . ' Where 0 = 0';

        $sql = $this->SQL_searchDevices($dataModel, $sql);

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function SQL_searchDevices($dataModel, $sql)
    {
        if (isset($dataModel['active_flag']) && $dataModel['active_flag'] != '') {
            $sql .= " and active_flag ='" . $dataModel['active_flag'] . "'";
        }

        return $sql;
    }

    public function getTemperature($dataPost)
    {
        try {
            $result['status'] = true;
            $result['message'] = $this->SQL_getTemperature($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }

        return $result;
    }

    public function SQL_getTemperature($dataModel)
    {
        $sql = 'SELECT * From t_temp_temperature Where 0 = 0';

        $sql = $this->SQL_searchTemperature($dataModel, $sql);
        $sql .= ' Order by create_date desc limit 1';
        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function SQL_searchTemperature($dataModel, $sql)
    {
        if (isset($dataModel['session']) && $dataModel['session'] != '') {
            $sql .= " and session ='" . $dataModel['session'] . "' and TIME_TO_SEC(TIMEDIFF(now(),create_date)) <=20 ";
        }
        // echo  $sql;
        return $sql;
    }

    public function setActiveDevice($dataPost)
    {
        try {
            $DataModel['id'] = isset($dataPost['device']) ? $dataPost['device'] : 0;
            $DataModel['active_flag'] = isset($dataPost['active_flag']) ? $dataPost['active_flag'] : 0;
            $result['status'] = true;
            $result['message'] = $this->SQL_setActiveDevice($DataModel);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }

        return $result;
    }

    public function SQL_setActiveDevice($DataModel)
    {
        $this->db->where('id', $DataModel['id']);

        return $this->db->update($this->tbl_name, $DataModel);
    }

    public function SaveSettingScreen($dataPost)
    {
        try {
            // $DataModel['ID'] = isset($dataPost['ID']) ? $dataPost['ID'] : 0;
            // POWER = 0 คือ ปิด POWER = 1 คือ เปิด
            $DataModel['POWER'] = isset($dataPost['POWER']) ? $dataPost['POWER'] : 0;
            // TYPE = 0 คือ image TYPE = 1 คือ video
            $DataModel['TYPE'] = isset($dataPost['TYPE']) ? $dataPost['TYPE'] : 0;
            $DataModel['TIMER'] = isset($dataPost['TIMER']) ? $dataPost['TIMER'] : 0;
            $DataModel['TIME_Interval'] = isset($dataPost['TIME_Interval']) ? $dataPost['TIME_Interval'] : 0;
            $result['status'] = true;
            $result['message'] = $this->SQL_SaveSettingScreen($DataModel);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }

        return $result;
    }

    public function SQL_SaveSettingScreen($DataModel)
    {
        // $this->db->where('ID', $DataModel['ID']);

        return $this->db->update($this->tbl_t_setting_screen, $DataModel);
    }

    public function GetSettingScreen($dataPost)
    {
        try {
            $nResult = $this->SQL_GetSettingScreen();
            // if (null != $nResult && count($nResult) > 0 && $nResult[0]['POWER'] == 1) {
            //     $this->setzeroApi();
            // }
            $result['status'] = true;
            $result['message'] = $nResult;
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }

        return $result;
    }

    public function setzeroApi()
    {
        try {
            $func = $this->uri->segment(3, 0);
            $device = $this->uri->segment(4, 0);
            $reqTime = date('Y-m-d H:i:s');
            $curl = curl_init();
            $url = $this->config->item('base_api_url') . 'setzero/weighingmachine';
            $timeout = $device == 'pressuregauge' ? 120 : 45;

            // Assign POST data
            curl_setopt_array($curl, [
                CURLOPT_CONNECTTIMEOUT => 10,
                CURLOPT_TIMEOUT => $timeout,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $url,
                CURLOPT_USERAGENT => 'Codular Sample cURL Request',
            ]);
            $result = curl_exec($curl);
            $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $time = curl_getinfo($curl, CURLINFO_TOTAL_TIME);
            $error = curl_error($curl);
            $resTime = date('Y-m-d H:i:s');
            $modelData = ['request_url' => $url, 'request_time' => $reqTime, 'response_time' => $resTime, 'status_code' => $statusCode, 'error_message' => $error, 'response_message' => $result];
            $this->load->model('LineNotiFyModel', '', true);
            if ($modelData['status_code'] != 200) {
                $this->LineNotiFyModel->alert_line_notify($modelData, false, 'UserAction');
            } else {
                $this->LineNotiFyModel->alert_line_notify($modelData, true, 'UserAction');
            }
            $this->db->insert('t_request_log', $modelData);
            curl_close($curl);
        } catch (Exception $ex) {
            $result = $ex;
        }

        return $result;
    }

    public function SQL_GetSettingScreen()
    {
        $sql = 'SELECT * From ' . $this->tbl_t_setting_screen . ' Where 0 = 0';

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function GetChkRfid($dataPost)
    {
        // print_r($dataPost);
        // die();
        try {
            $DataModel['RFID'] = isset($dataPost['RFID']) ? $dataPost['RFID'] : 0;
            $result['status'] = true;
            $result['message'] = $this->SQL_GetChkRfid($DataModel);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }

        return $result;
    }

    public function SQL_GetChkRfid($DataModel)
    {
        // print_r($dataPost);
        // die();
        $sql = 'SELECT * From ' . $this->tbl_t_rfid . " Where deleteflag = 0 AND code = '" . $DataModel['RFID'] . "'";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function SaveRFID($dataPost)
    {
        try {
            $DataModel['item'] = isset($dataPost['item']) ? $dataPost['item'] : 0;
            $DataModel['value'] = isset($dataPost['value']) ? $dataPost['value'] : 0;
            $DataModel['code'] = isset($dataPost['code']) ? $dataPost['code'] : 0;

            $DataRFID['RFID'] = isset($dataPost['code']) ? $dataPost['code'] : 0;

            $Rfid = $this->SQL_GetChkRfid($DataRFID);

            if (null != $Rfid && count($Rfid) > 0) {
                $uResult = $this->SQL_UpdateRFID($DataModel);
                if ($uResult > 0) {
                    $result['status'] = true;
                } else {
                    $result['status'] = false;
                }
            } else {
                $DataModel['CreateDate'] = date('Y-m-d H:i:s');
                $nResult = $this->SQL_SaveRFID($DataModel);
                if ($nResult > 0) {
                    $result['status'] = true;
                } else {
                    $result['status'] = false;
                }
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }

        return $result;
    }

    public function SQL_SaveRFID($DataModel)
    {
        return $this->db->insert($this->tbl_t_rfid, $DataModel);
    }

    public function SQL_UpdateRFID($DataModel)
    {
        $this->db->where('code', $DataModel['code']);

        return $this->db->update($this->tbl_t_rfid, $DataModel);
    }

    public function DeleteRFID($dataPost)
    {
        try {
            $DataModel['code'] = isset($dataPost['code']) ? $dataPost['code'] : 0;
            $DataModel['deleteflag'] = 1;
            $dResult = $this->SQL_DeleteRFID($DataModel);
            if ($dResult > 0) {
                $result['status'] = true;
            } else {
                $result['status'] = false;
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }

        return $result;
    }

    public function SQL_DeleteRFID($DataModel)
    {
        $this->db->where('code', $DataModel['code']);

        return $this->db->update($this->tbl_t_rfid, $DataModel);
    }

    public function GetListRFID($dataPost)
    {
        try {
            $DataModel['code'] = isset($dataPost['code']) ? $dataPost['code'] : 0;
            $result['status'] = true;
            $result['message'] = $this->SQL_GetListRFID($DataModel);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }

        return $result;
    }

    public function SQL_GetListRFID()
    {
        $sql = 'SELECT * From ' . $this->tbl_t_rfid . ' Where deleteflag = 0 order by code';

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function chkReaderFlag($dataPost)
    {
        try {
            $result['status'] = true;
            $result['message'] = $this->SQL_chkReaderFlag($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }

        return $result;
    }

    public function SQL_chkReaderFlag($dataModel)
    {
        $sql = 'SELECT * From t_idcard_flag Where 0 = 0';
        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function SetWeighingmachine($data)
    {
        try {
            $reqTime = date('Y-m-d H:i:s');
            $curl = curl_init();

            $timeout = $device == 'pressuregauge' ? 120 : 45;
            $url = $this->config->item('base_api_url') . $data['func'] . '/weighingmachine';
            // Assign POST data
            curl_setopt_array($curl, [
                CURLOPT_CONNECTTIMEOUT => 10,
                CURLOPT_TIMEOUT => $timeout,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $url,
                CURLOPT_USERAGENT => 'Codular Sample cURL Request',
            ]);
            $result = curl_exec($curl);
            $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $time = curl_getinfo($curl, CURLINFO_TOTAL_TIME);
            $error = curl_error($curl);
            $resTime = date('Y-m-d H:i:s');
            $modelData = ['request_url' => $url, 'request_time' => $reqTime, 'response_time' => $resTime, 'status_code' => $statusCode, 'error_message' => $error, 'response_message' => $result];
            $this->load->model('LineNotiFyModel', '', true);
            if ($modelData['status_code'] != 200) {
                $this->LineNotiFyModel->alert_line_notify($modelData, false, $data['action']);
            } else {
                $this->LineNotiFyModel->alert_line_notify($modelData, true, $data['action']);
            }
            $this->db->insert('t_request_log', $modelData);
            if (curl_errno($curl)) {
                $result = [];

                $result['success'] = false;
                echo json_encode($result, JSON_UNESCAPED_UNICODE);
                exit();
            } elseif ($statusCode != 200) {
                $result = [];
                $result['success'] = false;
                echo json_encode($result, JSON_UNESCAPED_UNICODE);
                exit();
            }

            curl_close($curl);
        } catch (Exception $ex) {
            $result = $ex;
        }

        return json_decode(str_replace("'", '"', $result));
    }

    public function SaveSettingTemp($dataPost)
    {
        try {

            // TEMP_ACTIVE = 0 คือ ปิด TEMP_ACTIVE = 1 คือ เปิด
            $DataModel['TEMP_ACTIVE'] = isset($dataPost['TEMP_ACTIVE']) ? $dataPost['TEMP_ACTIVE'] : 0;
            // MATH_SIGN = 0 คือ ลบ MATH_SIGN = 1 คือ บวก
            $DataModel['MATH_SIGN'] = isset($dataPost['MATH_SIGN']) ? $dataPost['MATH_SIGN'] : 0;
            $DataModel['TEMP_VALUE'] = isset($dataPost['TEMP_VALUE']) ? $dataPost['TEMP_VALUE'] : 0;
            $DataModel['TEMP_VALUE_TEXT'] = isset($dataPost['TEMP_VALUE_TEXT']) ? $dataPost['TEMP_VALUE_TEXT'] : "";
            // print_r($DataModel);die();
            $result['status'] = true;
            $result['message'] = $this->SQL_SaveSettingTemp($DataModel);

        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }

        return $result;
    }
    public function SQL_SaveSettingTemp($DataModel)
    {
        $qty = $this->SQL_GetSettingTemp();
        if (count($qty) > 0) {
            return $this->db->update($this->tbl_t_setting_temp, $DataModel);
        } else {
            return $this->db->insert($this->tbl_t_setting_temp, $DataModel);
        }
    }
    public function GetSettingTemp($dataPost)
    {
        try {
            $nResult = $this->SQL_GetSettingTemp();
            $result['status'] = true;
            $result['message'] = $nResult;
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }

        return $result;
    }
    public function SQL_GetSettingTemp()
    {
        $sql = 'SELECT * From ' . $this->tbl_t_setting_temp . ' Where 0 = 0';

        $query = $this->db->query($sql);

        return $query->result_array();
    }
}
