<?php

class AccountModel extends MY_Model
{
    private $tbl_name = 'customer_fuelchargerate';

    public function __construct()
    {
        parent::__construct();
    }

    public function getAccountComboList($dataPost)
    {
        try {
            $PageIndex = isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
            $PageSize = isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 10;
            $direction = isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : '';
            $SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : 'asc';

            $offset = ($PageIndex - 1) * $PageSize;

            $result['status'] = true;
            $result['message'] = $this->SQL_getAccountComboList($DataModel, $PageSize, $offset, $direction, $SortOrder);

            $result['totalRecords'] = $this->SQL_getAccountTotalList($DataModel);
            $result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_getAccountComboList($DataModel, $limit = 10, $offset = 0, $Order = '', $direction = 'asc')
    {
        $sql = 'SELECT
                    acc.LoginId,
                    acc.Username,
                    acc.Displayname,
                    acc.`Status`,
                    acc.LoginType,
                    CASE WHEN acc.LoginType = "admin" THEN
                    "DHL" 
                    WHEN acc.LoginType = "supplier" THEN
                    s.`Company` 
                    WHEN acc.LoginType = "owner" THEN
                    o.OwnerName ELSE "" 
                    END AS OrganizationName,
                    acc.SupplierId,
                    acc.ForcePwdChange,
                    s.`Code` AS SupplierCode,
                    s.`Company` AS SupplierName,
                    o.OwnerId,
                    o.OwnerCode,
                    o.OwnerName 
                FROM
                    `loginaccount` acc
                    LEFT JOIN `supplier` s ON acc.SupplierId = s.SupplierId
                    LEFT JOIN (
                SELECT
                    o1.LoginId,
                    o.`OwnerId`,
                    o.`Code` AS `OwnerCode`,
                    o.`Description` AS `OwnerName` 
                FROM
                    ( SELECT LoginId, Min( OwnerId ) AS `OwnerId` FROM loginaccount_owner GROUP BY LoginId ) AS o1
                    INNER JOIN `owner` o ON o1.OwnerId = o.OwnerId 
                    ) AS o ON acc.LoginId = o.LoginId';
        if ($Order != '') {
            $sql .= ' ORDER BY '.$Order.' '.$direction;
        }
        $sql .= " LIMIT $offset, $limit";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function SQL_getAccountTotalList($DataModel)
    {
        $sql = 'SELECT
                    acc.LoginId,
                    acc.Username,
                    acc.Displayname,
                    acc.`Status`,
                    acc.LoginType,
                    CASE WHEN acc.LoginType = "admin" THEN
                    "DHL" 
                    WHEN acc.LoginType = "supplier" THEN
                    s.`Company` 
                    WHEN acc.LoginType = "owner" THEN
                    o.OwnerName ELSE "" 
                    END AS OrganizationName,
                    acc.SupplierId,
                    acc.ForcePwdChange,
                    s.`Code` AS SupplierCode,
                    s.`Company` AS SupplierName,
                    o.OwnerId,
                    o.OwnerCode,
                    o.OwnerName 
                FROM
                    `loginaccount` acc
                    LEFT JOIN `supplier` s ON acc.SupplierId = s.SupplierId
                    LEFT JOIN (
                SELECT
                    o1.LoginId,
                    o.`OwnerId`,
                    o.`Code` AS `OwnerCode`,
                    o.`Description` AS `OwnerName` 
                FROM
                    ( SELECT LoginId, Min( OwnerId ) AS `OwnerId` FROM loginaccount_owner GROUP BY LoginId ) AS o1
                    INNER JOIN `owner` o ON o1.OwnerId = o.OwnerId 
                    ) AS o ON acc.LoginId = o.LoginId';

        $query = $this->db->query($sql);

        return $query->num_rows();
    }

    public function saveAccount($dataPost)
    {
        try {
            $DataModel['RateId'] = isset($dataPost['RateId']) ? $dataPost['RateId'] : 0;
            $DataModel['EffYear'] = isset($dataPost['EffYear']) ? $dataPost['EffYear'] : '';
            $DataModel['EffMonth'] = isset($dataPost['EffMonth']) ? $dataPost['EffMonth'] : '';
            $DataModel['ChargeRate'] = isset($dataPost['ChargeRate']) ? $dataPost['ChargeRate'] : '';
            $DataModel['CurrentRate'] = isset($dataPost['CurrentRate']) ? $dataPost['CurrentRate'] : '';
            // $DataModel['UpdateBy'] = $this->session->userdata('name');
            $DataModel['UpdateBy'] = 'admin';
            $DataModel['UpdateDate'] = date('Y-m-d H:i:s');
            if ($DataModel['RateId'] == 0) {
                $DataModel['CreateDate'] = date('Y-m-d H:i:s');
                $nResult = $this->SQL_insertAccount($DataModel);
                if ($nResult > 0) {
                    $result['status'] = true;
                    $result['message'] = $this->lang->line('SAVESUCCESS');
                } else {
                    $result['status'] = false;
                    $result['message'] = $this->lang->line('SAVEFAIL');
                }
            } else {
                $uResult = $this->SQL_updateAccount($DataModel);
                if ($uResult) {
                    $result['status'] = true;
                    $result['message'] = $this->lang->line('UPDATESUCCESS');
                } else {
                    $result['status'] = false;
                    $result['message'] = $this->lang->line('UPDATEFAIL');
                }
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_insertAccount($DataModel)
    {
        $this->db->insert($this->tbl_name, $DataModel);

        return $this->db->insert_id();
    }

    public function SQL_updateAccount($DataModel)
    {
        $this->db->where('RateId', $DataModel['RateId']);

        return $this->db->update($this->tbl_name, $DataModel);
    }

    public function deleteAccount($dataPost)
    {
        try {
            $DataModel['RateId'] = isset($dataPost['RateId']) ? $dataPost['RateId'] : 0;
            $nResult = $this->SQL_deleteAccount($DataModel);
            if ($nResult) {
                $result['status'] = true;
                $result['message'] = $this->lang->line('DELETESUCCESS');
            } else {
                $result['status'] = false;
                $result['message'] = $this->lang->line('DELETEFAIL');
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_deleteAccount($DataModel)
    {
        $this->db->where('RateId', $DataModel['RateId']);
        $DataModel = [
            'DeleteFlag' => 1,
        ];

        return $this->db->update($this->tbl_name, $DataModel);
    }
}
