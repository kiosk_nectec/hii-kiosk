<?php

class OwnersModel extends MY_Model
{
    private $tbl_name = 'owner';

    public function __construct()
    {
        parent::__construct();
    }

    public function getOwnersComboList($dataPost)
    {
        try {
            $PageIndex = isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
            $PageSize = isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 10;
            $direction = isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : '';
            $SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : 'asc';

            $offset = ($PageIndex - 1) * $PageSize;

            $result['status'] = true;
            $result['message'] = $this->SQL_getOwnersComboList($PageSize, $offset, $direction, $SortOrder);

            $result['totalRecords'] = $this->SQL_getOwnersTotalList();
            $result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_getOwnersComboList($limit = 10, $offset = 0, $Order = '', $direction = 'asc')
    {
        $sql = 'SELECT * From '.$this->tbl_name.' Where DeleteFlag = 0';

        if ($Order != '') {
            $sql .= ' ORDER BY '.$Order.' '.$direction;
        }
        $sql .= " LIMIT $offset, $limit";
        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function SQL_getOwnersTotalList()
    {
        $sql = 'SELECT * From '.$this->tbl_name.' Where DeleteFlag = 0';

        $query = $this->db->query($sql);

        return $query->num_rows();
    }

    public function saveOwners($dataPost)
    {
        try {
            $DataModel['OwnerId'] = isset($dataPost['OwnerId']) ? $dataPost['OwnerId'] : 0;
            $DataModel['Code'] = isset($dataPost['Code']) ? $dataPost['Code'] : '';
            $DataModel['Description'] = isset($dataPost['Description']) ? $dataPost['Description'] : '';
            $DataModel['Type'] = isset($dataPost['Type']) ? $dataPost['Type'] : '';
            $DataModel['PUDCostcenter'] = isset($dataPost['PUDCostcenter']) ? $dataPost['PUDCostcenter'] : '';
            $DataModel['LOPCostcenter'] = isset($dataPost['LOPCostcenter']) ? $dataPost['LOPCostcenter'] : '';
            $DataModel['Status'] = isset($dataPost['Status']) ? $dataPost['Status'] : '';
            // $DataModel['UpdateBy'] = $this->session->userdata('name');
            $DataModel['UpdateBy'] = 'admin';
            $DataModel['UpdateDate'] = date('Y-m-d H:i:s');
            if ($DataModel['OwnerId'] == 0) {
                // ChkCode
                $ChkCode = $this->SQL_ChkCode($DataModel['Code']);
                if (null != $ChkCode && count($ChkCode) > 0) {
                    $result['status'] = false;
                    $result['message'] = 'Code is duplicated';
                    echo json_encode($result, JSON_UNESCAPED_UNICODE);
                    exit();
                }
            }
            // ChkPUDCostcenter
            $ChkPUDCostcenter = $this->SQL_ChkPUDCostcenter($DataModel['PUDCostcenter']);
            if (null == $ChkPUDCostcenter && count($ChkPUDCostcenter) == 0) {
                $result['status'] = false;
                $result['message'] = 'Unknown PUD cost center';
                echo json_encode($result, JSON_UNESCAPED_UNICODE);
                exit();
            }
            // ChkLOPCostcenter
            $ChkLOPCostcenter = $this->SQL_ChkLOPCostcenter($DataModel['LOPCostcenter']);
            if (null == $ChkLOPCostcenter && count($ChkLOPCostcenter) == 0) {
                $result['status'] = false;
                $result['message'] = 'Unknown LOP cost center';
                echo json_encode($result, JSON_UNESCAPED_UNICODE);
                exit();
            }
            if ($DataModel['OwnerId'] == 0) {
                $DataModel['CreateDate'] = date('Y-m-d H:i:s');
                $nResult = $this->SQL_insertOwners($DataModel);
                if ($nResult > 0) {
                    $result['status'] = true;
                    $result['message'] = $this->lang->line('SAVESUCCESS');
                } else {
                    $result['status'] = false;
                    $result['message'] = $this->lang->line('SAVEFAIL');
                }
            } else {
                $uResult = $this->SQL_updateOwners($DataModel);
                if ($uResult) {
                    $result['status'] = true;
                    $result['message'] = $this->lang->line('UPDATESUCCESS');
                } else {
                    $result['status'] = false;
                    $result['message'] = $this->lang->line('UPDATEFAIL');
                }
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_ChkCode($DataModel)
    {
        $sql = 'SELECT * From '.$this->tbl_name.' Where Code = "'.$DataModel.'"';
        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function SQL_ChkPUDCostcenter($DataModel)
    {
        $sql = 'SELECT * From '.$this->tbl_name.' Where PUDCostcenter = "'.$DataModel.'"';
        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function SQL_ChkLOPCostcenter($DataModel)
    {
        $sql = 'SELECT * From '.$this->tbl_name.' Where LOPCostcenter = "'.$DataModel.'"';
        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function SQL_insertOwners($DataModel)
    {
        $this->db->insert($this->tbl_name, $DataModel);

        return $this->db->insert_id();
    }

    public function SQL_updateOwners($DataModel)
    {
        $this->db->where('OwnerId', $DataModel['OwnerId']);

        return $this->db->update($this->tbl_name, $DataModel);
    }

    public function deleteOwners($dataPost)
    {
        try {
            $DataModel['OwnerId'] = isset($dataPost['OwnerId']) ? $dataPost['OwnerId'] : 0;
            $nResult = $this->SQL_deleteOwners($DataModel);
            if ($nResult) {
                $result['status'] = true;
                $result['message'] = $this->lang->line('DELETESUCCESS');
            } else {
                $result['status'] = false;
                $result['message'] = $this->lang->line('DELETEFAIL');
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_deleteOwners($DataModel)
    {
        $this->db->where('OwnerId', $DataModel['OwnerId']);
        $DataModel = [
            'DeleteFlag' => 1,
        ];

        return $this->db->update($this->tbl_name, $DataModel);
    }
}
