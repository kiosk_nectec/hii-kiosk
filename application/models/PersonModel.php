<?php

class PersonModel extends MY_Model
{
    private $tbl_name = 't_person';

    public function __construct()
    {
        date_default_timezone_set('Asia/Bangkok');
        parent::__construct();
        $this->load->library('MyEnDecode');
    }

    public function getPerson($dataPost)
    {
        try {
            $result['status'] = true;
            $result['message'] = $this->SQL_getPerson($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function getPersonLoginFace($dataPost)
    {
        try {
            include './application/models/convert/TitleNameTH.php';

            $rs = $this->SQL_getPerson($dataPost);
            if (count($rs) > 0) {
                $item['PID'] = $rs[0]['PID'];
                $item['CID'] = $this->myendecode->aes_decrypt($rs[0]['CID'], 'hii-kiosk');
                foreach ($prename as $data) {
                    if ($data['namecode'] == $rs[0]['PRENAME']) {
                        $item['titlenameth'] = $data['rename'];
                        break;
                    }
                }
                if (null != $rs && null == $rs[0]['PHOTO_FACELIFT']) {
                    $item['profile_img'] = $rs[0]['PHOTO_ID'];
                } else {
                    $item['profile_img'] = $rs[0]['PHOTO_FACELIFT'];
                }
                $item['NAME'] = $rs[0]['NAME'];
                $item['LNAME'] = $rs[0]['LNAME'];
                $item['BIRTH'] = $rs[0]['BIRTH'];
                $item['DATE_SERV'] = $rs[0]['DATE_SERV'];
                $item['TIME_SERV'] = $rs[0]['TIME_SERV'];
                // print_r($item);
                // die();
                $result['item'] = $item;
                $result['message'] = $rs;
                $result['status'] = true;
            } else {
                $result['status'] = false;
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_getPerson($dataModel)
    {
        $sql = 'SELECT t1.*,t2.* , t1.PID as PID_face From t_person t1
        LEFT JOIN  t_service t2 on t2.PID= t1.PID and t2.D_UPDATE = ( select max(t3.D_UPDATE) from t_service t3  where t3.PID = t1.PID group by t3.PID)
        WHERE 0=0 ';

        $sql = $this->SQL_searchPerson($dataModel, $sql);

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function SQL_searchPerson($dataModel, $sql)
    {
        if (isset($dataModel['active_flag']) && $dataModel['active_flag'] != '') {
            $sql .= " and active_flag ='".$dataModel['active_flag']."'";
        }

        if (isset($dataModel['PID']) && $dataModel['PID'] != '') {
            $sql .= " and t1.PID ='".$dataModel['PID']."'";
        }

        // echo  $sql;
        return $sql;
    }

    public function savePerson($dataPost)
    {
        include './application/models/convert/TitleNameTH.php';
        // print_r($dataPost);
        // die();
        try {
            $data['person']['KIOSKCODE'] = '1000';
            $data['person']['CID'] = isset($dataPost['id_card']) ? $dataPost['id_card'] : '';

            $data['person']['PRENAME'] = isset($dataPost['titlenameth']) ? $prename[$dataPost['titlenameth']]['namecode'] : '';
            if (null == $data['person']['PRENAME']) {
                $result['status'] = false;
                $result['message'] = 'PRENAME Is Null';
                echo json_encode($result, JSON_UNESCAPED_UNICODE);
                exit();
            }
            $data['person']['NAME'] = isset($dataPost['firstnameth']) ? $dataPost['firstnameth'] : '';
            $data['person']['LNAME'] = isset($dataPost['lastnameth']) ? $dataPost['lastnameth'] : '';
            $data['person']['SEX'] = isset($dataPost['titlenameth']) ? $prename[$dataPost['titlenameth']]['sexcode'] : '';
            $data['person']['BIRTH'] = isset($dataPost['birthday']) ? $dataPost['birthday'] : '';
            $data['person']['PHOTO_ID'] = isset($dataPost['photo']) ? $dataPost['photo'] : '';
            $data['person']['D_UPDATE'] = date('Y-m-d H:i:s');

            $data['address']['KIOSKCODE'] = '1000';
            $data['address']['ROOMNO'] = 'test';
            $data['address']['HOUSENO'] = isset($dataPost['home']) ? $dataPost['home'] : '';
            $data['address']['VILLAGE'] = isset($dataPost['moo']) ? $dataPost['moo'] : '';
            $data['address']['SOISUB'] = isset($dataPost['trok']) ? $dataPost['trok'] : '';
            $data['address']['SOIMAIN'] = isset($dataPost['soi']) ? $dataPost['soi'] : '';
            $data['address']['ROAD'] = isset($dataPost['road']) ? $dataPost['road'] : '';
            $data['address']['TAMBON'] = isset($dataPost['tumbon']) ? $dataPost['tumbon'] : '';
            $data['address']['AMPUR'] = isset($dataPost['amphoe']) ? $dataPost['amphoe'] : '';
            $data['address']['CHANGWAT'] = isset($dataPost['province']) ? $dataPost['province'] : '';
            $data['address']['D_UPDATE'] = date('Y-m-d H:i:s');

            $listPerson = $this->checkDuplicate($data['person']['CID']);
            if ($listPerson[0]['CONDITION_FLAG'] == 0) {
                $data['person']['CONDITION_FLAG'] = isset($dataPost['CONDITION_FLAG']) ? $dataPost['CONDITION_FLAG'] : '';
            } else {
                $data['person']['CONDITION_FLAG'] = $listPerson[0]['CONDITION_FLAG'];
            }

            if ($listPerson[0]['REGISTER_FLAG'] == 0) {
                $data['person']['REGISTER_FLAG'] = 0;
            } else {
                $data['person']['REGISTER_FLAG'] = $listPerson[0]['REGISTER_FLAG'];
            }

            if (count($listPerson) == 0) {
                $PID = $this->generatePID(date('Y'));
                // $PID = $this->generatePID('2019');
                $data['person']['PID'] = $PID;
                $data['address']['PID'] = $data['person']['PID'];
                $data['person']['CID'] = $this->myendecode->mysql_aes_key($data['person']['CID'], 'hii-kiosk');
                $data['address']['CID'] = $data['person']['CID'];
                $this->insert($data);
                $result['message'] = 'insert :'.$data['person']['PID'];
                $result['PID'] = $data['person']['PID'];
                $result['SEX'] = $data['person']['SEX'];
            // if (isset($dataPost['photo'])) {
                //     $url = $dataPost['photo'];
                //     $ext = pathinfo(
                //             parse_url($url, PHP_URL_PATH),
                //             PATHINFO_EXTENSION
                //         );

                //     $imgServerPath = 'assets/person/'.$result['PID'];
                //     $filename = $result['PID'].'.'.$ext;
                //     if (!file_exists($imgServerPath)) {
                //         // echo  $imgServerPath ;
                //         mkdir($imgServerPath, 0777, true);
                //     }
                //     file_put_contents($imgServerPath.'/'.$filename, file_get_contents($url));
                //     $data['person']['PHOTO_ID'] = $imgServerPath.'/'.$filename;
                //     $this->update($data);
                //     $result['profile_img'] = $imgServerPath.'/'.$filename;
                //     // echo SYSDIR;
                //     unlink($this->config->item('base_url_img_for_remove').$dataPost['id_card'].'.'.$ext);
                //     rmdir($this->config->item('base_url_img_for_remove').$imgServerPath);
                // }
            } else {
                $data['person']['PID'] = $listPerson[0]['PID'];
                $data['address']['PID'] = $listPerson[0]['PID'];
                $data['person']['CID'] = $listPerson[0]['CID'];
                $data['address']['CID'] = $listPerson[0]['CID'];
                $this->update($data);
                $result['message'] = 'update :';
                $result['PID'] = $listPerson[0]['PID'];
                $result['SEX'] = $listPerson[0]['SEX'];
            }

            if (isset($dataPost['photo'])) {
                $url = $dataPost['photo'];
                $ext = pathinfo(
                    parse_url($url, PHP_URL_PATH),
                    PATHINFO_EXTENSION
                );

                $imgServerPath = 'assets/person/'.$result['PID'];
                $filename = $result['PID'].'.'.$ext;
                if (!file_exists($imgServerPath)) {
                    // echo  $imgServerPath ;
                    mkdir($imgServerPath, 0777, true);
                }
                file_put_contents($imgServerPath.'/'.$filename, file_get_contents($url));
                $data['person']['PHOTO_ID'] = $imgServerPath.'/'.$filename;
                $this->update($data);
                $result['profile_img'] = $imgServerPath.'/'.$filename;
                // echo SYSDIR;
                unlink($this->config->item('base_url_img_for_remove').$dataPost['id_card'].'.'.$ext);
            }
            if (isset($dataPost['profile_img'])) {
                //ต้องตัดคำว่า นี้ data:image/png;base64, ออกไปก่อน
                $b64 = substr($dataPost['profile_img'], 22);

                // Obtain the original content (usually binary data)
                $bin = base64_decode($b64);

                // Load GD resource from binary data
                $im = imagecreatefromstring($bin);

                // Make sure that the GD library was able to load the image
                // This is important, because you should not miss corrupted or unsupported images
                if (!$im) {
                    $result['status'] = false;
                }

                $img_name = $result['PID'].'.jpg';
                $filelocation_img = 'assets/person/faceApi/'.$result['PID'].'/';

                // Specify the location where you want to save the image
                $img_file = FCPATH.$filelocation_img.$img_name;

                // check path
                $file_pathimgexist = FCPATH.$filelocation_img;
                if (!file_exists($file_pathimgexist)) {
                    mkdir($file_pathimgexist, 0700, true);
                }

                // Save the GD resource as PNG in the best possible quality (no compression)
                // This will strip any metadata or invalid contents (including, the PHP backdoor)
                // To block any possible exploits, consider increasing the compression level
                imagepng($im, $img_file, 0);

                // call Api Face
                $REGISTER_FLAG = $this->db->get_where('t_person', ['PID' => $result['PID'], 'REGISTER_FLAG' => 0])->num_rows();
                if ($REGISTER_FLAG > 0) {
                    $url = $this->config->item('base_api_face_url').'register';
                // $url = $this->config->item('base_api_face_url').'register/face';
                } else {
                    $url = $this->config->item('base_api_face_url').'update';
                    // $url = $this->config->item('base_api_face_url').'update/face';
                }

                // Create a CURLFile object
                if (function_exists('curl_file_create')) { // php 5.5+
                    $cFile = curl_file_create($img_file);
                } else {
                    $cFile = '@'.realpath($img_file);
                }
                $datafaceApi = ['id' => $result['PID'], 'photo' => $cFile];

                $register = $this->faceApi($datafaceApi, $url);
                if (count(json_decode($register, true)['data']) > 0) {
                    if (json_decode($register, true)['data'][0]['status'] == 'success') {
                        $data['person']['PHOTO_FACELIFT'] = $filelocation_img.$img_name;
                        $data['person']['REGISTER_FLAG'] = 1;
                        $this->update($data);
                        $result['status'] = true;
                    } else {
                        $result['status'] = false;
                    }
                } else {
                    $result['status'] = false;
                }
            }
            $listPersongetimg = $this->checkDuplicate($dataPost['id_card']);

            if (null != $listPersongetimg && null == $listPersongetimg[0]['PHOTO_FACELIFT']) {
                $result['profile_img'] = $listPersongetimg[0]['PHOTO_ID'];
            } else {
                $result['profile_img'] = $listPersongetimg[0]['PHOTO_FACELIFT'];
            }
            $result['status'] = true;
        } catch (Exception $ex) {
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function loginfaceApi($dataPost)
    {
        try {
            $photo = isset($dataPost['profile_img']) ? $dataPost['profile_img'] : '';

            if (isset($dataPost['profile_img'])) {
                //ต้องตัดคำว่า นี้ data:image/png;base64, ออกไปก่อน
                $b64 = substr($dataPost['profile_img'], 22);

                // Obtain the original content (usually binary data)
                $bin = base64_decode($b64);

                // Load GD resource from binary data
                $im = imagecreatefromstring($bin);

                // Make sure that the GD library was able to load the image
                // This is important, because you should not miss corrupted or unsupported images
                // if (!$im) {
                //     die('Base64 value is not a valid image');
                // }

                $img_name = 'ImgTempFaceLogin.jpg';
                $filelocation_img = 'assets/person/faceApi/ImgTempFaceLogin/';
                // Specify the location where you want to save the image
                $img_file = FCPATH.$filelocation_img.$img_name;

                // check path
                $file_pathimgexist = FCPATH.$filelocation_img;
                if (!file_exists($file_pathimgexist)) {
                    mkdir($file_pathimgexist, 0700, true);
                }
                // Save the GD resource as PNG in the best possible quality (no compression)
                // This will strip any metadata or invalid contents (including, the PHP backdoor)
                // To block any possible exploits, consider increasing the compression level
                imagepng($im, $img_file, 0);
                // call Api Face

                $url = $this->config->item('base_api_face_url').'recognition';
                // $url = $this->config->item('base_api_face_url').'recognition/face';

                // Create a CURLFile object

                if (function_exists('curl_file_create')) { // php 5.5+
                    $cFile = curl_file_create($img_file);
                } else {
                    $cFile = '@'.realpath($img_file);
                }
                $datafaceApi = ['photo' => $cFile];
                $login = $this->faceApi($datafaceApi, $url);
                if (count(json_decode($login, true)['data']) > 0) {
                    if (json_decode($login, true)['data'][0]['status'] == true) {
                        $result['status'] = true;
                        $result['message'] = json_decode($login, true);
                    } else {
                        $result['status'] = false;
                    }
                } else {
                    $result['status'] = false;
                }
            }
        } catch (Exception $ex) {
        }

        return $result;
    }

    public function faceApi($data, $url)
    {
        try {
            $func = $this->uri->segment(3, 0);
            $device = $this->uri->segment(4, 0);
            $reqTime = date('Y-m-d H:i:s');
            $curl = curl_init();

            $timeout = $device == 'pressuregauge' ? 120 : 45;

            // Assign POST data
            curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $data,
            ]);
            $result = curl_exec($curl);
            $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $time = curl_getinfo($curl, CURLINFO_TOTAL_TIME);
            $error = curl_error($curl);
            $resTime = date('Y-m-d H:i:s');
            $modelData = ['request_url' => $url, 'request_time' => $reqTime, 'response_time' => $resTime, 'status_code' => $statusCode, 'error_message' => $error, 'response_message' => $result];
            $this->load->model('LineNotiFyModel', '', true);
            if ($modelData['status_code'] != 200) {
                $this->LineNotiFyModel->alert_line_notify($modelData, false, 'UserAction', true);
            } else {
                $this->LineNotiFyModel->alert_line_notify($modelData, true, 'UserAction', true);
            }
            $this->db->insert('t_request_log', $modelData);
            if (curl_errno($curl)) {
                $result = [];

                $result['success'] = false;
                echo json_encode($result, JSON_UNESCAPED_UNICODE);
                exit();
            } elseif ($statusCode != 200) {
                $result = [];
                $result['success'] = false;
                echo json_encode($result, JSON_UNESCAPED_UNICODE);
                exit();
            }

            curl_close($curl);
        } catch (Exception $ex) {
            $result = $ex;
        }

        return $result;
    }

    public function confirmfaceApi($dataPost)
    {
        try {
            $dataModel['ListPID'] = isset($dataPost['ListPID']) ? $dataPost['ListPID'] : '';
            $dataModel['CID'] = isset($dataPost['CID']) ? $dataPost['CID'] : '';

            foreach ($dataModel['ListPID'] as $data) {
                $resultPID = $this->SQL_confirmfaceApi($data);
                // print_r($resultPID);
                // die();
                if (null != $resultPID && count($resultPID) > 0) {
                    $resultCID = $this->myendecode->aes_decrypt($resultPID[0]['CID'], 'hii-kiosk');
                    // print_r(substr($resultCID, -4));
                    if (substr($resultCID, -4) == $dataModel['CID']) {
                        $result['message'] = $resultPID;
                        $result['status'] = true;
                        break;
                    } else {
                        $result['message'] = [];
                        $result['status'] = false;
                    }
                } else {
                    $result['message'] = [];
                    $result['status'] = false;
                }
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_confirmfaceApi($data)
    {
        $query = $this->db->get_where('t_person', ['PID' => $data['PID']]);

        return $query->result_array();
    }

    public function generatePID($year)
    {
        $sql = "SELECT LEFT(PID,4) last_pid,MAX(RIGHT(PID,6))+1 running FROM `t_person` WHERE LEFT(PID,4) = '".$year."' GROUP by LEFT(PID,4)";
        $query = $this->db->query($sql);
        $running = $query->result_array();
        $running = $running ? $running[0]['running'] : 1;
        $PID = sprintf('%s%06d', $year, $running);

        return $PID;
    }

    public function checkDuplicate($CID)
    {
        $result = [];

        $query = $this->db->get($this->tbl_name);
        $allUserList = $query->result_array();
        foreach ($allUserList as $data) {
            if ($this->myendecode->aes_decrypt($data['CID'], 'hii-kiosk') == $CID) {
                $result[0] = $data;
                break;
            }
        }

        return  $result;
    }

    public function insert($modelData)
    {
        $this->db->insert($this->tbl_name, $modelData['person']);

        $this->db->insert('t_address', $modelData['address']);

        return $this->db->insert_id();
    }

    public function update($modelData)
    {
        $this->db->where('PID', $modelData['person']['PID']);
        $this->db->update($this->tbl_name, $modelData['person']);

        $this->db->where('PID', $modelData['person']['PID']);

        return $this->db->update('t_address', $modelData['address']);
    }

    public function ChkConditionFlagPerson($dataPost)
    {
        try {
            $data['CID'] = isset($dataPost['CID']) ? $dataPost['CID'] : '';
            $CID = $this->checkDuplicate($data['CID']);
            $result['status'] = true;
            $result['message'] = $this->db->get_where('t_person', ['CID' => $CID[0]['CID'], 'CONDITION_FLAG' => 1])->num_rows();
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function ChkHeightFlagPerson($dataPost)
    {
        try {
            $data['PID'] = isset($dataPost['PID']) ? $dataPost['PID'] : '';
            $result['status'] = true;
            $sql = 'SELECT * From t_person left join  Where DeleteFlag = 0';
            $result['message'] = $this->SQL_getPerson($data);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }
}
