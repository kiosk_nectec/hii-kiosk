<?php

class BackupModel extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function BackUpData($dataPost)
    {
        try {
            $DataModel['ListPerson'] = $this->SQL_get_t_person();
            $DataModel['ListLog'] = $this->SQL_get_t_request_log();
            $DataModel['ListService'] = $this->SQL_get_t_service();

            $result = $this->Api(json_encode($DataModel, JSON_UNESCAPED_UNICODE));

            if ($result['status'] == 200) {
                $UpdateFlagt_person = $this->SQL_update_t_person();
                $UpdateFlagt_request_log = $this->SQL_update_t_request_log();
                $UpdateFlagt_service = $this->SQL_update_t_service();
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_get_t_person()
    {
        $sql = 'SELECT * FROM t_person WHERE sync_flag = 0';

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function SQL_get_t_request_log()
    {
        $sql = 'SELECT * FROM t_request_log WHERE sync_flag = 0';

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function SQL_get_t_service()
    {
        $sql = 'SELECT * FROM t_service WHERE sync_flag = 0';

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function SQL_update_t_person()
    {
        $this->db->set('sync_flag', 1);

        return $this->db->update('t_person');
    }

    public function SQL_update_t_request_log()
    {
        $this->db->set('sync_flag', 1);

        return $this->db->update('t_request_log');
    }

    public function SQL_update_t_service()
    {
        $this->db->set('sync_flag', 1);

        return $this->db->update('t_service');
    }

    public function Api($data)
    {
        try {
            $curl = curl_init();
            curl_setopt_array($curl, [
            CURLOPT_URL => 'http://devdeethailand.com/kios-backup/BackupApi/index',
            CURLOPT_HTTPHEADER => ['Content-Type: application/json'],
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $data,
            ]);
            $nresult = curl_exec($curl);
            $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $error = curl_error($curl);
            if (curl_errno($curl)) {
                $result['data'] = [];
                $result['status'] = $statusCode;
            } elseif ($statusCode != 200) {
                $result['data'] = [];
                $result['status'] = $statusCode;
            } else {
                $result['data'] = $nresult;
                $result['status'] = $statusCode;
            }
            curl_close($curl);
        } catch (Exception $ex) {
            $result = $ex;
        }

        return $result;
    }
}
