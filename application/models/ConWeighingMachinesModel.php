<?php

class ConWeighingMachinesModel extends MY_Model
{
    private $tbl_name = 't_con_bmi';

    public function __construct()
    {
        parent::__construct();
    }

    public function getConWeighingMachines($dataPost)
    {
        try {
            $result['status'] = true;
            $result['message'] = $this->SQL_getConWeighingMachines($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }

        return $result;
    }

    public function SQL_getConWeighingMachines($dataModel)
    {
        $sql = "SELECT * From " . $this->tbl_name . " Where Delete_flag = 0";

        $sql = $this->SQL_searchConWeighingMachines($dataModel, $sql);

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function SQL_searchConWeighingMachines($dataModel, $sql)
    {

        if (isset($dataModel['active_flag']) && $dataModel['active_flag'] != '') {
            $sql .= " and active_flag ='" . $dataModel['active_flag'] . "'";
        }
        return $sql;
    }
    

}
