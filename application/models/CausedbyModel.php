<?php

class CausedbyModel extends MY_Model
{
    private $tbl_name = 'caused_by';

    public function __construct()
    {
        parent::__construct();
    }

    public function getCausedbyComboList($dataPost)
    {
        try {
            $PageIndex = isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
            $PageSize = isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 10;
            $direction = isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : '';
            $SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : 'asc';

            $offset = ($PageIndex - 1) * $PageSize;

            $result['status'] = true;
            $result['message'] = $this->SQL_getCausedbyComboList($DataModel, $PageSize, $offset, $direction, $SortOrder);

            $result['totalRecords'] = $this->SQL_getCausedbyTotalList($DataModel);
            $result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_getCausedbyComboList($DataModel, $limit = 10, $offset = 0, $Order = '', $direction = 'asc')
    {
        $sql = 'SELECT * From '.$this->tbl_name.' Where DeleteFlag = 0';
        if ($Order != '') {
            $sql .= ' ORDER BY '.$Order.' '.$direction;
        }
        $sql .= " LIMIT $offset, $limit";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function SQL_getCausedbyTotalList($DataModel)
    {
        $sql = 'SELECT * From '.$this->tbl_name.' Where DeleteFlag = 0';

        $query = $this->db->query($sql);

        return $query->num_rows();
    }

    public function saveCausedby($dataPost)
    {
        try {
            $DataModel['ReasonId'] = isset($dataPost['ReasonId']) ? $dataPost['ReasonId'] : 0;
            $DataModel['Code'] = isset($dataPost['Code']) ? $dataPost['Code'] : '';
            $DataModel['Status'] = isset($dataPost['Status']) ? $dataPost['Status'] : '';
            $DataModel['Description'] = isset($dataPost['Description']) ? $dataPost['Description'] : '';
            // $DataModel['UpdateBy'] = $this->session->userdata('name');
            $DataModel['UpdateBy'] = 'admin';
            $DataModel['UpdateDate'] = date('Y-m-d H:i:s');
            if ($DataModel['ReasonId'] == 0) {
                $DataModel['CreateDate'] = date('Y-m-d H:i:s');
                $nResult = $this->SQL_insertCausedby($DataModel);
                if ($nResult > 0) {
                    $result['status'] = true;
                    $result['message'] = $this->lang->line('SAVESUCCESS');
                } else {
                    $result['status'] = false;
                    $result['message'] = $this->lang->line('SAVEFAIL');
                }
            } else {
                $uResult = $this->SQL_updateCausedby($DataModel);
                if ($uResult) {
                    $result['status'] = true;
                    $result['message'] = $this->lang->line('UPDATESUCCESS');
                } else {
                    $result['status'] = false;
                    $result['message'] = $this->lang->line('UPDATEFAIL');
                }
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_insertCausedby($DataModel)
    {
        $this->db->insert($this->tbl_name, $DataModel);

        return $this->db->insert_id();
    }

    public function SQL_updateCausedby($DataModel)
    {
        $this->db->where('ReasonId', $DataModel['ReasonId']);

        return $this->db->update($this->tbl_name, $DataModel);
    }

    public function deleteCausedby($dataPost)
    {
        try {
            $DataModel['ReasonId'] = isset($dataPost['ReasonId']) ? $dataPost['ReasonId'] : 0;
            $nResult = $this->SQL_deleteCausedby($DataModel);
            if ($nResult) {
                $result['status'] = true;
                $result['message'] = $this->lang->line('DELETESUCCESS');
            } else {
                $result['status'] = false;
                $result['message'] = $this->lang->line('DELETEFAIL');
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_deleteCausedby($DataModel)
    {
        $this->db->where('ReasonId', $DataModel['ReasonId']);
        $DataModel = [
            'DeleteFlag' => 1,
        ];

        return $this->db->update($this->tbl_name, $DataModel);
    }
}
