<?php

class ConOxygenModel extends MY_Model
{
    private $tbl_name = 't_con_oxygen';

    public function __construct()
    {
        parent::__construct();
    }

    public function getConOxygen($dataPost)
    {
        try {
            $result['status'] = true;
            $result['message'] = $this->SQL_getConOxygen($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_getConOxygen($dataModel)
    {
        $sql = 'SELECT * From '.$this->tbl_name.' Where Delete_flag = 0';

        $sql = $this->SQL_searchConOxygen($dataModel, $sql);

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function SQL_searchConOxygen($dataModel, $sql)
    {
        if (isset($dataModel['active_flag']) && $dataModel['active_flag'] != '') {
            $sql .= " and active_flag ='".$dataModel['active_flag']."'";
        }

        return $sql;
    }
}
