<?php
  
class districtModel extends CI_Model {
	
    private $tbl_name = 't_districts';
	private $id = 'id';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getIncomeNameById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function getdistrictComboList(){
		
		$sql = "SELECT id,zip_code,name_th,amphures_id FROM ". $this->tbl_name . " WHERE deleteflag = 0  ";
		$query = $this->db->query($sql);
		return  $query->result_array();
	}

	public function getdistrictComborelate($amphures){
		
		$sql = "SELECT id,zip_code,name_th,amphures_id 
		FROM ". $this->tbl_name . " WHERE deleteflag = 0 AND amphures_id = ".$amphures;
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
}
?>