<?php

class SuppliersModel extends MY_Model
{
    private $tbl_name = 'supplier';

    public function __construct()
    {
        parent::__construct();
    }

    public function getSuppliersComboList($dataPost)
    {
        try {
            $PageIndex = isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
            $PageSize = isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 10;
            $direction = isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : '';
            $SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : 'asc';

            $offset = ($PageIndex - 1) * $PageSize;

            $result['status'] = true;
            $result['message'] = $this->SQL_getSuppliersComboList($DataModel, $PageSize, $offset, $direction, $SortOrder);

            $result['totalRecords'] = $this->SQL_getSuppliersTotalList($DataModel);
            $result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_getSuppliersComboList($DataModel, $limit = 10, $offset = 0, $Order = '', $direction = 'asc')
    {
        $sql = 'SELECT * From '.$this->tbl_name.' Where DeleteFlag = 0';
        if ($Order != '') {
            $sql .= ' ORDER BY '.$Order.' '.$direction;
        }
        $sql .= " LIMIT $offset, $limit";
        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function SQL_getSuppliersTotalList($DataModel)
    {
        $sql = 'SELECT * From '.$this->tbl_name.' Where DeleteFlag = 0';

        $query = $this->db->query($sql);

        return $query->num_rows();
    }

    public function saveSuppliers($dataPost)
    {
        try {
            $DataModel['SupplierId'] = isset($dataPost['SupplierId']) ? $dataPost['SupplierId'] : 0;
            $DataModel['Code'] = isset($dataPost['Code']) ? $dataPost['Code'] : '';
            $DataModel['Company'] = isset($dataPost['Company']) ? $dataPost['Company'] : '';
            $DataModel['Status'] = isset($dataPost['Status']) ? $dataPost['Status'] : '';
            $DataModel['Firstname'] = isset($dataPost['Firstname']) ? $dataPost['Firstname'] : '';
            $DataModel['Lastname'] = isset($dataPost['Lastname']) ? $dataPost['Lastname'] : '';
            $DataModel['Email'] = isset($dataPost['Email']) ? $dataPost['Email'] : '';
            $DataModel['JobTitle'] = isset($dataPost['JobTitle']) ? $dataPost['JobTitle'] : '';
            $DataModel['BusinessPhone'] = isset($dataPost['BusinessPhone']) ? $dataPost['BusinessPhone'] : '';
            $DataModel['MobilePhone'] = isset($dataPost['MobilePhone']) ? $dataPost['MobilePhone'] : '';
            $DataModel['FaxNumber'] = isset($dataPost['FaxNumber']) ? $dataPost['FaxNumber'] : '';
            $DataModel['Address'] = isset($dataPost['Address']) ? $dataPost['Address'] : '';
            $DataModel['City'] = isset($dataPost['City']) ? $dataPost['City'] : '';
            $DataModel['Province'] = isset($dataPost['Province']) ? $dataPost['Province'] : '';
            $DataModel['Zip'] = isset($dataPost['Zip']) ? $dataPost['Zip'] : '';
            $DataModel['Country'] = isset($dataPost['Country']) ? $dataPost['Country'] : '';
            $DataModel['WebPage'] = isset($dataPost['WebPage']) ? $dataPost['WebPage'] : '';
            $DataModel['Note'] = isset($dataPost['Note']) ? $dataPost['Note'] : '';
            // $DataModel['UpdateBy'] = $this->session->userdata('name');
            $DataModel['UpdateBy'] = 'admin';
            $DataModel['UpdateDate'] = date('Y-m-d H:i:s');
            if ($DataModel['SupplierId'] == 0) {
                // ChkCode
                $ChkCode = $this->SQL_ChkCode($DataModel['Code']);
                if (null != $ChkCode && count($ChkCode) > 0) {
                    $result['status'] = false;
                    $result['message'] = 'Code is duplicated';
                    echo json_encode($result, JSON_UNESCAPED_UNICODE);
                    exit();
                }
            }
            if (strlen($DataModel['Postcode']) != 5) {
                $result['status'] = false;
                $result['message'] = 'Must be a 5-digit code.';
                echo json_encode($result, JSON_UNESCAPED_UNICODE);
                exit();
            }
            if ($DataModel['SupplierId'] == 0) {
                $DataModel['CreateDate'] = date('Y-m-d H:i:s');
                $nResult = $this->SQL_insertSuppliers($DataModel);
                if ($nResult > 0) {
                    $result['status'] = true;
                    $result['message'] = $this->lang->line('SAVESUCCESS');
                } else {
                    $result['status'] = false;
                    $result['message'] = $this->lang->line('SAVEFAIL');
                }
            } else {
                $uResult = $this->SQL_updateSuppliers($DataModel);
                if ($uResult) {
                    $result['status'] = true;
                    $result['message'] = $this->lang->line('UPDATESUCCESS');
                } else {
                    $result['status'] = false;
                    $result['message'] = $this->lang->line('UPDATEFAIL');
                }
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_ChkCode($DataModel)
    {
        $sql = 'SELECT * From '.$this->tbl_name.' Where Code = "'.$DataModel.'"';
        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function SQL_insertSuppliers($DataModel)
    {
        $this->db->insert($this->tbl_name, $DataModel);

        return $this->db->insert_id();
    }

    public function SQL_updateSuppliers($DataModel)
    {
        $this->db->where('SupplierId', $DataModel['SupplierId']);

        return $this->db->update($this->tbl_name, $DataModel);
    }

    public function deleteSuppliers($dataPost)
    {
        try {
            $DataModel['SupplierId'] = isset($dataPost['SupplierId']) ? $dataPost['SupplierId'] : 0;
            $nResult = $this->SQL_deleteSuppliers($DataModel);
            if ($nResult) {
                $result['status'] = true;
                $result['message'] = $this->lang->line('DELETESUCCESS');
            } else {
                $result['status'] = false;
                $result['message'] = $this->lang->line('DELETEFAIL');
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: '.$ex;
        }

        return $result;
    }

    public function SQL_deleteSuppliers($DataModel)
    {
        $this->db->where('SupplierId', $DataModel['SupplierId']);
        $DataModel = [
            'DeleteFlag' => 1,
        ];

        return $this->db->update($this->tbl_name, $DataModel);
    }
}
