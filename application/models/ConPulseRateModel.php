<?php

class ConPulseRateModel extends MY_Model
{
    private $tbl_name = 't_con_pulserate';

    public function __construct()
    {
        parent::__construct();
    }

    public function getConPulseRate($dataPost)
    {
        try {
            $result['status'] = true;
            $result['message'] = $this->SQL_getConPulseRate($dataPost);
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }

        return $result;
    }

    public function SQL_getConPulseRate($dataModel)
    {
        $sql = "SELECT * From " . $this->tbl_name . " Where Delete_flag = 0";

        $sql = $this->SQL_searchConPulseRate($dataModel, $sql);

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function SQL_searchConPulseRate($dataModel, $sql)
    {

        if (isset($dataModel['active_flag']) && $dataModel['active_flag'] != '') {
            $sql .= " and active_flag ='" . $dataModel['active_flag'] . "'";
        }
        return $sql;
    }
    

}
