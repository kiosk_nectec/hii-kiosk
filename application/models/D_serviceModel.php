<?php

class D_serviceModel extends MY_Model
{
    private $tbl_name = 't_service';

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Bangkok');
    }

    public function getCurrentD_service($dataPost)
    {
        include './application/models/convert/TitleNameTH.php';
        $dataModel['SESSION_ID'] = isset($dataPost['session_id']) ? $dataPost['session_id'] : '';
        // $dataModel['SESSION_ID'] = '2erSFleBWjLmg4';
        try {
            $result['status'] = true;
            $result['message'] = $this->SQL_getCurrentD_service($dataModel);

            foreach ($result['message'] as $data) {
                foreach ($prename as $x => $struct) {
                    if ($data['PRENAME'] == $struct['namecode']) {
                        // echo $x;die();
                        $result['message'][0]['PRENAME'] = $x;
                        break;
                    }
                }
            }
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }

        return $result;
    }

    public function SQL_getCurrentD_service($dataModel)
    {
        $sql = "SELECT IF(t2.PRENAME is null,'',t2.PRENAME) as PRENAME,IF(t2.NAME is null,'ไม่ระบุตัวตน',t2.NAME) as NAME, IF(t2.LNAME is null,'',t2.LNAME) as LNAME,DATE_FORMAT(t1.DATE_SERV, '%d/%m/%Y') DATE_SERV,t1.TIME_SERV,t1.BTEMP,t1.SBP,t1.DBP,t1.PR,t1.OXYGEN,t1.WEIGHT,t1.HEIGHT,t1.BMI,t1.BMR,t1.PID From " . $this->tbl_name . " t1 LEFT JOIN t_person t2 on t1.PID = t2.PID Where t1.SESSION_ID = '" . $dataModel['SESSION_ID'] . "'";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function saveD_service($dataPost)
    {
        // print_r($dataPost);die();
        try {
            $data['KIOSKCODE'] = '1000';
            $data['SESSION_ID'] = isset($dataPost['session_id']) ? $dataPost['session_id'] : '';
            $data['PID'] = isset($dataPost['PID']) ? $dataPost['PID'] : '';
            $data['USER_TYPE'] = isset($dataPost['usertype']) ? $dataPost['usertype'] : 0; //อุณหภูมิร่างกาย
            if ($data['PID'] != '') {
                $data['USER_TYPE'] = 1;
            } else {
                $data['USER_TYPE'] = 0;
            }
            $data['DATE_SERV'] = date('Y-m-d'); //วันที่ให้บริการ
            $data['TIME_SERV'] = date('H:i:s'); //เวลาที่ให้บริการ

            $data['BTEMP'] = isset($dataPost['information']['temperature']['value']) ? $dataPost['information']['temperature']['value'] : null; //อุณหภูมิร่างกาย
            $data['TEMPCOMPENSATE'] = isset($dataPost['information']['temperature']['TEMPCOMPENSATE']) ? $dataPost['information']['temperature']['TEMPCOMPENSATE'] : null; //อุณหภูมิร่างกายที่นำมาชดเชย
            $data['BTEMPFROMAPI'] = isset($dataPost['information']['temperature']['BTEMPFROMAPI']) ? $dataPost['information']['temperature']['BTEMPFROMAPI'] : null; //อุณหภูมิร่างกาย จาก api
            $data['BTEMPCODE'] = isset($dataPost['information']['BTEMPCODE']) ? $dataPost['information']['BTEMPCODE'] : null; //แปลผลอุณหภูมิ
            $data['SBP'] = isset($dataPost['information']['bloodPressure']['systolic']) ? $dataPost['information']['bloodPressure']['systolic'] : null; //ความดันตัวบน
            $data['DBP'] = isset($dataPost['information']['bloodPressure']['diastolic']) ? $dataPost['information']['bloodPressure']['diastolic'] : null; //ความดันตัวล่าง
            $data['BPCODE'] = isset($dataPost['information']['BPCODE']) ? $dataPost['information']['BPCODE'] : null; //แปลผลความดัน
            $data['PR'] = isset($dataPost['information']['bloodPressure']['pulse']) ? $dataPost['information']['bloodPressure']['pulse'] : null; //อัตราการเต้นชีพจร
            $data['PRCODE'] = isset($dataPost['information']['PRCODE']) ? $dataPost['information']['PRCODE'] : null; //แปลผลการเต้นชีพจร
            $data['RR'] = isset($dataPost['information']['pulseOximeter']['PRbpm']) ? $dataPost['information']['pulseOximeter']['PRbpm'] : null; //อัตราการหายใจ
            $data['RRCODE'] = isset($dataPost['information']['RRCODE']) ? $dataPost['information']['RRCODE'] : null; //แปลผลอัตราการหายใจ
            $data['OXYGEN'] = isset($dataPost['information']['pulseOximeter']['SpO2']) ? $dataPost['information']['pulseOximeter']['SpO2'] : null; //ค่าความอิ่มตัวของออกซิเจนในเลือด
            $data['OXYGENCODE'] = isset($dataPost['information']['OXYGENCODE']) ? $dataPost['information']['OXYGENCODE'] : null; //แปลค่าความอิ่มตัวของออกซิเจนในเลือด
            $data['WEIGHT'] = isset($dataPost['information']['weight']) ? $dataPost['information']['weight']['weight'] : null; //น้ำหนัก
            $data['HEIGHT'] = isset($dataPost['information']['height']) ? $dataPost['information']['height'] : null; //ส่วนสูง
            $data['BMI'] = isset($dataPost['information']['weight']) ? $dataPost['information']['weight']['BMI'] : null; //ค่าคํานวณ BMI
            $data['BMICODE'] = isset($dataPost['information']['BMICODE']) ? $dataPost['information']['BMICODE'] : null; //แปลผลค่า BMI
            $data['BMR'] = isset($dataPost['information']['weight']) ? $dataPost['information']['weight']['BMR'] : null; //แปลผลค่า BMR

            $data['D_UPDATE'] = date('Y-m-d H:i:s');

            // $data['address']['ROOMNO'] =  "test";
            // $data['address']['HOUSENO'] =  isset($dataPost['home']) ? $dataPost['home'] : "";
            // $data['address']['VILLAGE'] =  isset($dataPost['moo']) ? $dataPost['moo'] : "";
            // $data['address']['SOISUB'] =  isset($dataPost['trok']) ? $dataPost['trok'] : "";
            // $data['address']['SOIMAIN'] =  isset($dataPost['soi']) ? $dataPost['soi'] : "";
            // $data['address']['ROAD'] =  isset($dataPost['road']) ? $dataPost['road'] : "";
            // $data['address']['TAMBON'] =  isset($dataPost['tumbon']) ? $dataPost['tumbon'] : "";
            // $data['address']['AMPUR'] =  isset($dataPost['amphoe']) ? $dataPost['amphoe'] : "";
            // $data['address']['CHANGWAT'] =  isset($dataPost['province']) ? $dataPost['province'] : "";

            $listD_service = $this->checkDuplicate($data['SESSION_ID']);

            if (count($listD_service) == 0) {
                // $data['PID'] = md5(uniqid(rand(), true));
                $this->insert($data);
                $result['message'] = 'insert :' . $data['SESSION_ID'];
            } else {
                $this->update($data);
                $result['message'] = 'update :' . $data['SESSION_ID'];
            }
            $result['status'] = true;
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }

        return $result;
    }

    public function checkDuplicate($SESSION_ID)
    {
        $this->db->where('SESSION_ID', $SESSION_ID);
        $query = $this->db->get($this->tbl_name);

        return $query->result_array();
    }

    public function insert($modelData)
    {
        $this->db->insert($this->tbl_name, $modelData);

        // $this->db->insert("t_address", $modelData['address']);
        return $this->db->insert_id();
    }

    public function update($modelData)
    {
        $this->db->where('SESSION_ID', $modelData['SESSION_ID']);

        return $this->db->update($this->tbl_name, $modelData);

        //     $this->db->where('CID',$modelData['person']['CID']);
        //     return $this->db->update("t_address", $modelData['address']);
    }

    public function getLastSession()
    {
        try {
            $result['status'] = true;
            $result['data'] = $this->SQL_getLastSession();
        } catch (Exception $ex) {
            $result['status'] = false;
            $result['message'] = 'exception: ' . $ex;
        }

        return $result;
    }

    public function SQL_getLastSession()
    {
        $sql = 'SELECT  SESSION_ID, max(D_UPDATE) as D_UPDATE from ' . $this->tbl_name . ' group by SESSION_ID ORDER BY D_UPDATE DESC limit 1';

        $query = $this->db->query($sql);

        return $query->row();
    }
}
