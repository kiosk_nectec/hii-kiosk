<?php

$lang['SAVESUCCESS'] = 'บันทึกสำเร็จ !';
$lang['SAVEFAIL'] = 'บันทึกผิดพลาด !';
$lang['DELETESUCCESS'] = 'ลบสำเร็จ !';
$lang['DELETEFAIL'] = 'ลบผิดพลาด !';
$lang['UPDATESUCCESS'] = 'อัพเดตสำเร็จ !';
$lang['UPDATEFAIL'] = 'อัพเดตผิดพลาด !';
