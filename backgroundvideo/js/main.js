import backgroundVideo from "./background-video.js";

const VIDEO_CONTAINER = document.querySelector("#video_container");
const VIDEO_COVER = document.querySelector("#video_cover");

var tempslide = [];
var listfile = [];
var baseurl = window.location.origin + window.location.pathname;
var xmlHttp = new XMLHttpRequest();
xmlHttp.open(
	"POST",
	baseurl.replace("backgroundvideo/", "") + "Background/getFileVideoComboList",
	false
);
xmlHttp.send(null);
var result = JSON.parse(xmlHttp.responseText);
if (result.status == true) {
	listfile = result.message;

	listfile.forEach(function (entry, index) {
		tempslide.push({
			src: entry.replace("backgroundvideo/", "").replace(".mp4", ""),
			types: ["mp4"],
		});
	});
	tempslide.sort((x, y) => {
		return x.src - y.src;
	});
} else {
	location.href = baseurl.replace("backgroundvideo/", "") + "#/home";
}

new backgroundVideo({
	container: VIDEO_CONTAINER,
	src: tempslide,
	onLoad: () => (VIDEO_COVER.style.display = "none"),
});
// var initWeight;
// var tempweight_total = 0;
// var baseurl = window.location.origin + window.location.pathname;
// initWeight = setInterval(() => {
// 	var xmlHttp = new XMLHttpRequest();
// 	xmlHttp.open(
// 		"POST",
// 		baseurl.replace("backgroundvideo/", "") +
// 			"Devices/api/read/weighingmachine",
// 		false
// 	);
// 	xmlHttp.send(null);
// 	var result = JSON.parse(xmlHttp.responseText);
// 	if (result.success != false) {
// 		if (undefined != result.data[0]["weight_total"]) {
// 			if (tempweight_total == 0) {
// 				tempweight_total = result.data[0]["weight_total"];
// 			} else {
// 				if (tempweight_total == result.data[0]["weight_total"]) {
// 				} else {
// 					clearInterval(initWeight);
// 					location.href = baseurl.replace("backgroundvideo/", "") + "#/home";
// 				}
// 			}
// 		}
// 	}
// }, 5000);
function goTokios() {
	location.href = baseurl.replace("backgroundvideo/", "")+"#/home";
  }

  window.onclick = () => {
	goTokios();
  }
  window.onscroll = () => {
	goTokios();
  }
  window.onkeydown = () => {
	goTokios();
  }
  window.onkeypress = () => {
	goTokios();
  }
  window.onkeyup = () => {
	goTokios();
  }
  window.onmousemove = () => {
	goTokios();
  }
  window.onmousedown = () => {
	goTokios();
  }
  window.onmouseup = () => {
	goTokios();
  }
  window.ontouchstart = () => {
	goTokios();
  }
  window.ontouchmove = () => {
	goTokios();
  }
  window.onfocus = () => {
	goTokios();
  }
