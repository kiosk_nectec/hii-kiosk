(function() {
    "use strict";

    angular.module("myApp.services").component("skipButton", {
        template: ' <div ng-click="$ctrl.skip()" class="btn-yellow">{{$ctrl.text}}</div>',
        controller: skipButtonController,
        bindings: {
            text: "="
        }
    });

    function skipButtonController(baseService, $rootScope, $location) {
        var ctrl = this;
        ctrl.skip = function() {
            ctrl.playAudio();
            baseService.nextDevices()
        };
        ctrl.playAudio = function () {
            var audio = new Audio('./assets/kiosk/audio/click.mp3');
            audio.play();
        };

      
    }
})();