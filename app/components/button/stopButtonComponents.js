(function() {
    "use strict";

    angular.module("myApp.services").component("stopButton", {
        template: '<div ng-click="$ctrl.stop()" class="btn-red">จบการวัด</div>',
        controller: stopButtonController,
        bindings: {
            hero: "="
        }
    });

    function stopButtonController(baseService, $rootScope, $location) {
        var ctrl = this;
        // if ($rootScope.devicesActiveList != undefined) {
        //     let device = $rootScope.devicesActiveList.find(x => x.set == undefined);
        //     if (!device) {
        //         ctrl.lastpage = true
        //     } else {
        //         ctrl.lastpage = false
        //     }
        // }
        ctrl.stop = function() {
            ctrl.playAudio();
            $location.path("/results");
        };
        ctrl.playAudio = function () {
            var audio = new Audio('./assets/kiosk/audio/click.mp3');
            audio.play();
        };
    }
})();