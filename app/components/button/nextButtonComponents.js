(function () {
    'use strict';

    angular.module('myApp.services').component('nextButton', {
        template: '<div ng-click="$ctrl.next()" class="btn-green">{{$ctrl.text}}</div>',
        controller: nextButtonController,
        bindings: {
            btnClass: '<'
        }
    });

    function nextButtonController(baseService, $rootScope) {
        var ctrl = this;
        console.log(ctrl);
        let device = $rootScope.devicesActiveList.find(x => x.set == undefined);
        if (!device) {
            ctrl.text = 'สรุปผล'
        } else {
            ctrl.text = 'ถัดไป'
        }
        ctrl.next = function () {
            ctrl.playAudio();
            baseService.nextDevices();
        };
        ctrl.playAudio = function () {
            var audio = new Audio('./assets/kiosk/audio/click.mp3');
            audio.play();
        };

    }
})();