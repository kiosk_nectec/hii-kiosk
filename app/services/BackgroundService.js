(function() {
    "use strict";
    angular.module("myApp.services").factory("BackgroundService", BackgroundService);

    function BackgroundService(baseService) {
        var servicebase = "Background/";
        //local


        return {
            getFileVideoComboList: function(model, onComplete) {
                baseService.post(servicebase + "getFileVideoComboList", model, function(
                    result
                ) {
                    if (undefined != onComplete) onComplete.call(this, result);
                });
            },

            getFileImageComboList: function(model, onComplete) {
                baseService.post(servicebase + "getFileImageComboList", model, function(
                    result
                ) {
                    if (undefined != onComplete) onComplete.call(this, result);
                });
            },
            getAllFileComboList: function(model, onComplete) {
                baseService.post(servicebase + "getAllFileComboList", model, function(
                    result
                ) {
                    if (undefined != onComplete) onComplete.call(this, result);
                });
            },
            alert_line_notify: function(model, onComplete) {
                baseService.post(servicebase + "alert_line_notify", model, function(
                    result
                ) {
                    if (undefined != onComplete) onComplete.call(this, result);
                });
            },
            BackUpData: function(model, onComplete) {
                baseService.post("Backup/index", model, function(
                    result
                ) {
                    if (undefined != onComplete) onComplete.call(this, result);
                });
            },
        };
    }
})();