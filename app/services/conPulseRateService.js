(function() {
    "use strict";
    angular.module("myApp.services").factory("conPulseRateService", conPulseRateService);

    function conPulseRateService(baseService) {
        var servicebase = "ConPulseRate/";
        //local


        return {
            getConPulseRate: function(model, onComplete) {
                baseService.post(servicebase + "getConPulseRate", model, function(
                    result
                ) {
                    if (undefined != onComplete) onComplete.call(this, result);
                });
            },

        };
    }
})();