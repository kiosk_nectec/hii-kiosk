(function () {
	"use strict";
	angular.module("myApp.services").factory("devicesService", devicesService);

	function devicesService(baseService, d_serviceService, $rootScope) {
		var servicebase = baseService.apiUrl + "Devices/";
		//local
		var apiservice = baseService.apiUrl + "Devices/api";
		//api external
		// var apiservice = "http://128.199.73.202/middleware/read";
		return {
			getActiveDevices: function (model, onComplete) {
				baseService.post(servicebase + "getDevices", {}, function (result) {
					if (undefined != onComplete) onComplete.call(this, result);
				});
			},
			getTemperatureFromGun: function (model, onComplete) {
				baseService.post(
					servicebase + "getTemperature",
					model,
					function (result) {
						if (undefined != onComplete) onComplete.call(this, result);
					}
				);
			},
			getValueDevices1: function (model, onComplete) {
				if (undefined != onComplete) {
					setTimeout(function () {
						onComplete.call(this, { status: true });
					}, 0);
				}
			},
			// เครื่องอ่านบัตรประชาชน Path : /status/4
			getDataIdcardReader: function (model, onComplete) {
				baseService.get(apiservice + "/read/readeridcard", function (result) {
					if (undefined != onComplete) {
						setTimeout(function () {
							onComplete.call(this, result);
						}, 0);
					}
				});
			},
			getDataBloodPressure: function (model, onComplete) {
				baseService.get(
					apiservice + "/read/pressuregauge",
					function (
						///read/pressuregauge
						result
					) {
						if (undefined != onComplete) {
							setTimeout(function () {
								onComplete.call(this, result);
							}, 0);
						}
					}
				);
			},
			getDataIRCamera: function (model, onComplete) {
				baseService.get(
					apiservice + "/read/infraredcameraheatdetection",
					function (result) {
						if (undefined != onComplete) {
							setTimeout(function () {
								onComplete.call(this, result);
							}, 0);
						}
					}
				);
				// if (undefined != onComplete) {
				//     setTimeout(function() {
				//         onComplete.call(this, { status: true });
				//     }, 5000);

				// }
			},
			getDataPulseOximeter: function (model, onComplete) {
				baseService.get(
					apiservice + "/read/bloodoxygenmeter",
					function (result) {
						if (undefined != onComplete) {
							// สำหรับ mock api
							// setTimeout(function () {
							// 	onComplete.call(this, result);
							// }, 5000);
							// สำหรับอุปกรณืจริง
							onComplete.call(this, result);
						}
					}
				);

				// if (undefined != onComplete) {
				//     setTimeout(function() {
				//         onComplete.call(this, { status: true });
				//     }, 5000);

				// }
			},
			getDataWeighingMachines: function (model, onComplete) {
				baseService.get(
					apiservice + "/read/weighingmachine",
					function (result) {
						if (undefined != onComplete) {
							setTimeout(function () {
								onComplete.call(this, result);
							}, 0);
						}
					}
				);

				// if (undefined != onComplete) {
				//     setTimeout(function() {
				//         onComplete.call(this, { status: true });
				//     }, 5000);

				// }
			},
			getStatus: function (model, onComplete) {
				baseService.get(
					apiservice + "/status/" + model.device,
					function (result) {
						if (undefined != onComplete) {
							onComplete.call(this, result);
						}
					}
				);
				// if (undefined != onComplete) {
				//     setTimeout(function() {
				//         onComplete.call(this, { status: true });
				//     }, 5000);

				// }
			},
			stopDevice: function (model, onComplete) {
				baseService.get(
					apiservice + "/stop/" + model.device,
					function (result) {
						if (undefined != onComplete) {
							onComplete.call(this, result);
						}
					}
				);
			},

			setActiveDevice: function (model, onComplete) {
				baseService.post(
					servicebase + "setActiveDevice",
					model,
					function (result) {
						if (undefined != onComplete) onComplete.call(this, result);
					}
				);
			},

			SaveSettingScreen: function (model, onComplete) {
				baseService.post(
					servicebase + "SaveSettingScreen",
					model,
					function (result) {
						if (undefined != onComplete) onComplete.call(this, result);
					}
				);
			},

			GetSettingScreen: function (model, onComplete) {
				baseService.post(
					servicebase + "GetSettingScreen",
					model,
					function (result) {
						if (undefined != onComplete) onComplete.call(this, result);
					}
				);
			},
			GetChkRfid: function (model, onComplete) {
				baseService.post(servicebase + "GetChkRfid", model, function (result) {
					if (undefined != onComplete) onComplete.call(this, result);
				});
			},
			SaveRFID: function (model, onComplete) {
				baseService.post(servicebase + "SaveRFID", model, function (result) {
					if (undefined != onComplete) onComplete.call(this, result);
				});
			},
			DeleteRFID: function (model, onComplete) {
				baseService.post(servicebase + "DeleteRFID", model, function (result) {
					if (undefined != onComplete) onComplete.call(this, result);
				});
			},
			GetListRFID: function (model, onComplete) {
				baseService.post(servicebase + "GetListRFID", model, function (result) {
					if (undefined != onComplete) onComplete.call(this, result);
				});
			},
			chkReaderFlag: function (model, onComplete) {
				baseService.post(
					servicebase + "chkReaderFlag",
					model,
					function (result) {
						if (undefined != onComplete) onComplete.call(this, result);
					}
				);
			},
			updateReaderFlag: function (model, onComplete) {
				baseService.post(
					servicebase + "updateReaderFlag",
					model,
					function (result) {
						if (undefined != onComplete) onComplete.call(this, result);
					}
				);
			},
			SetWeighingmachine: function (model, onComplete) {
				baseService.post(
					servicebase + "SetWeighingmachine",
					model,
					function (result) {
						if (undefined != onComplete) onComplete.call(this, result);
					}
				);
			},
			SaveSettingTemp: function (model, onComplete) {
				baseService.post(
					servicebase + "SaveSettingTemp",
					model,
					function (result) {
						if (undefined != onComplete) onComplete.call(this, result);
					}
				);
			},

			GetSettingTemp: function (model, onComplete) {
				baseService.post(
					servicebase + "GetSettingTemp",
					model,
					function (result) {
						if (undefined != onComplete) onComplete.call(this, result);
					}
				);
			},
		};
	}
})();
