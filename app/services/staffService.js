(function () {
	"use strict";
	angular.module("myApp.services").factory("staffService", staffService);

	function staffService(baseService) {
		var servicebase = "Staff/";
		//local

		return {
			SearchPerson: function (model, onComplete) {
				baseService.post(
					servicebase + "SearchPerson",
					model,
					function (result) {
						if (undefined != onComplete) onComplete.call(this, result);
					}
				);
			},
		};
	}
})();
