(function() {
    "use strict";
    angular.module("myApp.services").factory("personService", personService);

    function personService(baseService) {
        var servicebase = "Person/";
        //local


        return {
            savePerson: function(model, onComplete) {
                baseService.post(servicebase + "savePerson", model, function(
                    result
                ) {
                    if (undefined != onComplete) onComplete.call(this, result);
                });
            },

            getPerson: function(model, onComplete) {
                baseService.post(servicebase + "getPerson", model, function(
                    result
                ) {
                    if (undefined != onComplete) onComplete.call(this, result);
                });
            },

            getPersonLoginFace: function(model, onComplete) {
                baseService.post(servicebase + "getPersonLoginFace", model, function(
                    result
                ) {
                    if (undefined != onComplete) onComplete.call(this, result);
                });
            },

            loginfaceApi: function(model, onComplete) {
                baseService.post(servicebase + "loginfaceApi", model, function(
                    result
                ) {
                    if (undefined != onComplete) onComplete.call(this, result);
                });
            },

            confirmfaceApi: function(model, onComplete) {
                baseService.post(servicebase + "confirmfaceApi", model, function(
                    result
                ) {
                    if (undefined != onComplete) onComplete.call(this, result);
                });
            },

            ChkConditionFlagPerson: function(model, onComplete) {
                baseService.post(servicebase + "ChkConditionFlagPerson", model, function(
                    result
                ) {
                    if (undefined != onComplete) onComplete.call(this, result);
                });
            },

            ChkHeightFlagPerson: function(model, onComplete) {
                baseService.post(servicebase + "ChkHeightFlagPerson", model, function(
                    result
                ) {
                    if (undefined != onComplete) onComplete.call(this, result);
                });
            },
        };
    }
})();