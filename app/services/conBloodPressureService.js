(function() {
    "use strict";
    angular.module("myApp.services").factory("conBloodPressureService", conBloodPressureService);

    function conBloodPressureService(baseService) {
        var servicebase = "ConBloodPressure/";
        //local


        return {
            getConBloodPressure: function(model, onComplete) {
                baseService.post(servicebase + "getConBloodPressure", model, function(
                    result
                ) {
                    if (undefined != onComplete) onComplete.call(this, result);
                });
            },

            getApiBloodPressureStatus: function(model, onComplete) {
                baseService.post(servicebase + "getApiBloodPressureStatus", model, function(
                    result
                ) {
                    if (undefined != onComplete) onComplete.call(this, result);
                });
            },

        };
    }
})();