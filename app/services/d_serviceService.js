(function() {
    "use strict";
    angular.module("myApp.services").factory("d_serviceService", d_serviceService);

    function d_serviceService(baseService) {
        var servicebase = "D_service/";
        //local


        return {
            saveD_service: function(model, onComplete) {
                // alert('D_service');
                baseService.post(servicebase + "saveD_service", model, function(
                    result
                ) {
                    if (undefined != onComplete) onComplete.call(this, result);
                });
            },
            getCurrentD_service: function(model, onComplete) {
                // alert('D_service');
                baseService.post(servicebase + "getCurrentD_service",model, function(
                    result
                ) {
                    if (undefined != onComplete) onComplete.call(this, result);
                });
            },
        };
    }
})();