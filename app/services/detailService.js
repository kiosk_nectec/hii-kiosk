(function () {
	"use strict";
	angular.module("myApp.services").factory("detailService", detailService);

	function detailService(baseService) {
		var servicebase = "Detail/";
		return {
			GetDataGraph: function (model, onComplete) {
				baseService.post(
					servicebase + "GetDataGraph",
					model,
					function (result) {
						if (undefined != onComplete) onComplete.call(this, result);
					}
				);
			},
		};
	}
})();
