(function() {
    "use strict";
    angular.module("myApp.services").factory("conLoginAdminService", conLoginAdminService);

    function conLoginAdminService(baseService) {
        var servicebase = "ConLoginAdmin/";
        //local


        return {
            getConLoginAdmin: function(model, onComplete) {
                baseService.post(servicebase + "getConLoginAdmin", model, function(
                    result
                ) {
                    if (undefined != onComplete) onComplete.call(this, result);
                });
            },

        };
    }
})();