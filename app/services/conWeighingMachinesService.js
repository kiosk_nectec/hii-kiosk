(function() {
    "use strict";
    angular.module("myApp.services").factory("conWeighingMachinesService", conWeighingMachinesService);

    function conWeighingMachinesService(baseService) {
        var servicebase = "ConWeighingMachines/";
        //local


        return {
            getConWeighingMachines: function(model, onComplete) {
                baseService.post(servicebase + "getConWeighingMachines", model, function(
                    result
                ) {
                    if (undefined != onComplete) onComplete.call(this, result);
                });
            },

        };
    }
})();