(function() {
    "use strict";
    angular.module("myApp.services").factory("conOxygenService", conOxygenService);

    function conOxygenService(baseService) {
        var servicebase = "ConOxygen/";
        //local


        return {
            getConOxygen: function(model, onComplete) {
                baseService.post(servicebase + "getConOxygen", model, function(
                    result
                ) {
                    if (undefined != onComplete) onComplete.call(this, result);
                });
            },

            

        };
    }
})();