(function() {
    "use strict";
    angular.module("myApp.services").factory("conTemperatureService", conTemperatureService);

    function conTemperatureService(baseService) {
        var servicebase = "ConTemperature/";
        //local


        return {
            getConTemperature: function(model, onComplete) {
                baseService.post(servicebase + "getConTemperature", model, function(
                    result
                ) {
                    if (undefined != onComplete) onComplete.call(this, result);
                });
            },

        };
    }
})();