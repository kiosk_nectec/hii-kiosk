'use strict';

angular.module('myApp', [
    'ngAnimate',
    'ui.bootstrap',
    'ui.sortable',
    'ui.router',
    'ngTouch',
    'toastr',
    'smart-table',
    "xeditable",
    'ui.slimscroll',
    'ngJsTree',
    'angular-progress-button-styles',
    'pascalprecht.translate',
    'ui.select',
    'ui.select2',
    'ngCookies',
    'myApp.services',
    'BlurAdmin.theme',
    'BlurAdmin.pages',
    "com.2fdevs.videogular",
    "com.2fdevs.videogular.plugins.controls",
    "com.2fdevs.videogular.plugins.overlayplay",
    "com.2fdevs.videogular.plugins.poster",
    "monospaced.qrcode",
]).constant('$configuration', {
    rootpath: $("base").attr("href"),
    loginpath: $("base").attr("href") + "login",
    getLanguage: function() { var lang = localStorage.getItem("AdminLanguage"); return (null == lang) ? 'en' : lang; },
    setLanguage: function(lang) { localStorage.setItem("AdminLanguage", lang); },
    getItemPerPage: function() { var count = parseInt(localStorage.getItem("myAppItemPerPage")); return isNaN(count) ? 5 : count; },
    setItemPerPage: function(count) { localStorage.setItem("myAppItemPerPage", count); },
}).config(function($translateProvider, $configuration) {

    $translateProvider.useStaticFilesLoader({
            prefix: $configuration.rootpath + 'app/locales/locale-',
            suffix: '.json'
        }).useSanitizeValueStrategy('sanitizeParameters') // remove the warning from console log by putting the sanitize strategy
        .preferredLanguage($configuration.getLanguage());
});
angular.module('myApp').directive('disableRightClick', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attr) {
            element.bind('contextmenu', function(e) {
                e.preventDefault();
            })
        }
    }
})
angular.module('myApp').directive('limitTo', function() {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
            var limit = parseInt(attrs.limitTo);
            angular.element(elem).on("keypress", function(e) {
                if (this.value.length == limit) e.preventDefault();
            });
        }
    }
})
angular.module('myApp').directive('moveNextOnMaxlength', function() {
    return {
        restrict: 'A',
        link: function($scope, element) {
            element.on("input", function(e) {
                // console.log(element.val().length,element.attr("maxlength"))
                if(element.val().length == element.attr("maxlength")) {
                    var $nextElement = element.next();
                   
                    if($nextElement.length) {
                        $nextElement[0].focus();
                    }
                }
            });
        }
    }
})