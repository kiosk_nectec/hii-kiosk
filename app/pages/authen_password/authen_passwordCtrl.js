(function () {
	"use strict";

	angular
		.module("BlurAdmin.pages.authen_password")
		.controller("authen_passwordCtrl", homeCtrl);

	/** @ngInject */
	function homeCtrl(
		$scope,
		baseService,
		$rootScope,
		$uibModal,
		$filter,
		$timeout,
		$location,
		$state,
		conLoginAdminService
	) {
		$scope.conditionDataLoginAdmin = [];

		(function initController() {
			baseService.devicesActiveList();
			setTimeout(() => {
				if ($rootScope.audiomute == true) {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = true;
					}
				} else {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = false;
					}
				}
			}, 100);
		})();
		$scope.activePageTitle = $state.current.title;
		$scope.authen_password = $rootScope.globals.currentUser;
		$scope.ChkPassword = "";
		$scope.focus = true;
		$scope.password;
		$scope.progressFunction = function () {
			return $timeout(function () {}, 3000);
		};
		$scope.next = function () {
			$scope.playAudio();
			// $location.path('/devices/weighing-machines');
			baseService.nextDevices();
		};
		$scope.cancel = function () {
			$scope.playAudio();
			$location.path("/authen_admin");
		};
		$scope.start = function () {
			if ($scope.password.length == 6) {
				$scope.playAudio();
				$location.path("/staff");
			} else {
				$scope.ChkPassword = "รหัสผ่านไม่ถูกต้อง";
			}
		};

		$scope.playAudio = function () {
			var audio = new Audio("./assets/kiosk/audio/click.mp3");
			audio.play();
		};

		$scope.keyPassword = function (key) {
			if (key == null) {
				key = "";
				$scope.password = $scope.password.slice(0, -1);
			} else {
				$scope.password = $scope.password + key;
				// console.log($scope.password, "-", $scope.password.length);
				if ($scope.password.length == 6) {
					conLoginAdminService.getConLoginAdmin(
						{ password: $scope.password },
						function (result) {
							if (result.message.length > 0) {
								$scope.ChkPassword = "";
								$scope.start();
								// alert("รหัสถูก");
							} else {
								// alert("รหัสผิด");
								$scope.ChkPassword = "รหัสผ่านไม่ถูกต้อง";
								$scope.resetModel();
								document.getElementById("input-box-1").focus();
							}
						}
					);
				}
			}
		};

		$scope.resetModel = function () {
			$scope.passkey = {
				number1: "",
				number2: "",
				number3: "",
				number4: "",
				number5: "",
				number6: "",
			};
			$scope.password = "";
		};
		$scope.resetSearch = function () {
			$scope.ChkPassword = "";
			$scope.resetModel();
			document.getElementById("input-box-1").focus();
		};

		$scope.init = function () {
			$scope.resetModel();

			// ไปเอา password จาก database
		};
		$scope.init();

		document.onkeypress = function (e) {
			e = e || window.event;
			let key = e.key.toLowerCase();
			switch (page) {
				case "DisplayDevice":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(514, 906).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
					}
					if (key == "c" || key == "แ") {
						// green
					}
					break;

				default:
			}
		};
		var page = "DisplayDevice";
	}
})();
