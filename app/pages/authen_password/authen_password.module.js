/**
 * @author a.demeshko
 * created on 12/18/15
 */
(function() {
    "use strict";

    var pageName = "authen_password";
    var pageCtrl = pageName + "Ctrl";
    angular.module("BlurAdmin.pages.authen_password", []).config(routeConfig);
    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider.state(pageName, {
            url: "/" + pageName,
            templateUrl: "app/pages/authen_password/authen_password.html",
            controller: pageCtrl,
            title: "ข้อมูลผู้ใช้งาน",
            sidebarMeta: {
                icon: "ion-gear-a",
                order: 3,
            },
        });
    }
})();