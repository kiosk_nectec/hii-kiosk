(function () {
	"use strict";

	angular
		.module("BlurAdmin.pages.staff_card_regis")
		.controller("staff_card_regisCtrl", staff_card_regisCtrl);

	/** @ngInject */
	function staff_card_regisCtrl(
		$scope,
		baseService,
		$rootScope,
		$uibModal,
		$filter,
		$timeout,
		$location,
		$state,
		devicesService,
		$sce,
		$configuration,
		AuthenticationService,
		personService
	) {
		var isCallback = true;
		(function initController() {
			baseService.devicesActiveList();
			setTimeout(() => {
				if ($rootScope.audiomute == true) {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = true;
					}
				} else {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = false;
					}
				}
				var insertcardregisvideo = document.getElementById(
					"insertcardregisvideo"
				);
				if (typeof insertcardregisvideo.loop == "boolean") {
					// loop supported
					insertcardregisvideo.loop = true;
				} else {
					// loop property not supported
					insertcardregisvideo.addEventListener(
						"ended",
						function () {
							this.currentTime = 0;
							this.play();
						},
						false
					);
				}
				insertcardregisvideo.play();
				$scope.chkReaderFlag();
			}, 100);
		})();
		var myVar;
		$scope.chkReaderFlag = function () {
			if (page == "DisplayDevice") {
				var i = 1;

				myVar = setInterval(function () {
					devicesService.chkReaderFlag(null, function (result) {
						if (result.status == true) {
							if (i == 20) {
								clearInterval(myVar);
							} else {
								if (result.status == true) {
									if (result.message[0]["reader_flag"] == "Y") {
										$scope.Start();
									} else {
									}
								} else {
								}
								i = i + 1;
							}
						} else {
							clearInterval(myVar);
						}
					});
				}, 5000);
			}
		};
		$scope.videoConfig = {
			sources: [
				{
					src: $sce.trustAsResourceUrl(
						"assets/vdo/SampleVideo_720x480_2mb.mp4"
					),
					type: "video/mp4",
				},
				// { src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.webm"), type: "video/webm" },
				// { src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.ogg"), type: "video/ogg" }
			],
			tracks: [
				{
					src: "http://www.videogular.com/assets/subs/pale-blue-dot.vtt",
					kind: "subtitles",
					srclang: "en",
					label: "English",
					default: "",
				},
			],

			theme:
				$configuration.rootpath +
				"theme/bower_components/videogular-themes-default/videogular.css",
			plugins: {
				poster: "http://www.videogular.com/assets/images/videogular.png",
			},
		};
		$scope.activePageTitle = $state.current.title;
		$scope.staff_regis = $rootScope.globals.currentUser;
		$scope.video = null;

		$scope.img_profile_name = "";

		$scope.resetModel = function () {};
		$scope.loginByIdcard = function () {
			AuthenticationService.setProfile({
				name: "test name",
				sex: "ชาย",
				age: "60",
			});
			$location.path("/staff_new_regis");
			// console.log("test");
		};
		$scope.prepare = function () {
			page = "DisplayDevice";
			$(".DisplayDevice").show();
			$(".Loading").hide();
			$(".ResultDevice").hide();
			$scope.resetModel();
		};

		$scope.onPlayerReady = function (API) {
			$scope.video = API;
			// // console.log(API.stop())
			API.stop();
			setTimeout(() => {
				$scope.videoPlay();
			}, 1000);
			// // console.log(API)
		};

		$scope.videoPlay = function () {
			// console.log("PLAY");
			$scope.video.play();
		};
		$scope.videoStop = function () {
			// console.log("STOP");
			$scope.video.stop();
		};
		$scope.recheck = function () {
			$scope.videoPlay();
			$scope.prepare();
		};

		$rootScope.$on("CallMy2ControllerMethod", function (event, param1) {
			$scope.My2ControllerMethod(param1);
		});

		$scope.My2ControllerMethod = function (param1) {
			alert(param1.message);
		};
		$scope.progressFunction = function () {
			return $timeout(function () {}, 3000);
		};
		$scope.next = function () {
			// $location.path('/devices/weighing-machines');
			baseService.nextDevices();
		};

		// $scope.cancel = function() {
		//     $location.path('/authen');
		// }

		$scope.Home = function () {
			$location.path("/authen");
		};

		$scope.Cancel = function () {
			isCallback = false;
			$scope.playAudio();
			devicesService.stopDevice(
				{ device: "readeridcard" },
				function (result) {}
			);
			$location.path("/staff");
		};

		$scope.Start = function () {
			clearInterval(myVar);
			page = "Loading_card";
			$(".DisplayDevice").hide();
			$(".Loading").hide();
			$(".Loading_card").show();
			isCallback = true;
			devicesService.getDataIdcardReader(null, function (resultFromAPI) {
				if (isCallback) {
					console.log(resultFromAPI);

					if (resultFromAPI.id_card != undefined) {
						$scope.profile = resultFromAPI;
						
						$scope.Y = resultFromAPI.birthday.substr(0, 4);
						$scope.M = resultFromAPI.birthday.substr(4, 2);
						$scope.D = resultFromAPI.birthday.substr(6, 2);
						if ($scope.M == "00") {
							$scope.M = "01";
						}
						if ($scope.D == "00") {
							$scope.D = "01";
						}
						resultFromAPI.birthday = $scope.Y + $scope.M + $scope.D;
						$scope.profile.age = $scope.dateDiff(
							resultFromAPI.birthday,
							new Date()
						);
						$scope.profile.birthday =
							resultFromAPI.birthday.substring(0, 4) -
							543 +
							resultFromAPI.birthday.substring(4, 8);
						$scope.profile.profile_img = $rootScope.profile_img_b64;
						personService.savePerson($scope.profile, function (result) {
							console.log(result);
							if (result.status == true) {
								$rootScope.profile_img_b64 = "";
								$scope.sex = result.SEX;
								$scope.profile.profile_img = result.profile_img;
								$scope.profile.sex = result.SEX;
								$scope.results = '<img src="' + result.profile_img + '" />';
								AuthenticationService.setProfile($scope.profile);

								personService.getPerson(
									{ PID: result.PID },
									function (history) {
										if (history.message.length > 0) {
											// AuthenticationService.setProfile({ name: "test name", sex: "ชาย", age: "60" });
											$rootScope.globals.history = history.message[0];
										}
										$rootScope.globals.PID = result.PID;
										console.log($rootScope.globals);
										$scope.profile.PID = result.PID;

										$scope.profile.titleName = resultFromAPI.titlenameth;

										$scope.profile.SEX = result.SEX;
										$scope.profile.usertype = 1;

										if ($scope.profile.age.years) {
											if ($scope.profile.age.years > 60) {
												if ($scope.profile.sex == 1) {
													$scope.profile.titleName = "คุณลุง";
												} else if ($scope.profile.sex == 2) {
													$scope.profile.titleName = "คุณป้า";
												} else {
													$scope.profile.titleName = "";
												}
											} else {
												$scope.profile.titleName = $scope.profile.titlenameth;
											}
										}
										console.log($rootScope);
										$(".Success").show();
										page = "Success";
										$scope.playAudiofaceregisteridsuccess();
										AuthenticationService.setProfile($scope.profile);
									}
								);
								devicesService.updateReaderFlag(
									{ reader_flag: "N" },
									function (result) {
										// if(result.status == true){
											
										// }
									}
								);
							} else {
								page = "UnSuccess";
								$(".UnSuccess").show();
								$scope.playAudiofaceregisterfail();
								// $(".Success").show();
								// page = "Success";
							}
						});
						// $(".Success").show();
						// 				page = "Success";
						// 		page = "UnSuccess";
						// $(".UnSuccess").show();

						// });
					} else {
						// alert("เชื่อมต่อไม่ได้")
						$(".DisplayDevice").hide();
						$(".Loading").hide();
						$(".ResultDevice").hide();
						$(".Condition").hide();
						$(".Confirm").hide();
						$(".error_idcard").show();
						// $scope.errorAudio();
					}
				}
			});

			// setTimeout(function () { $(".Success").show(); }, 3000);
			// $scope.resetModel();
		};
		$scope.playAudiofaceregisterfail = function () {
			var display = document.getElementById("display");
			display.pause();

			var display = document.getElementById("success");
			display.pause();

			var sample = document.getElementById("unsuccess");
			sample.currentTime = 0;
			sample.play();
		};
		$scope.playAudiofaceregisteridsuccess = function () {
			var display = document.getElementById("display");
			display.pause();

			var sample = document.getElementById("success");
			sample.currentTime = 0;
			sample.play();
		};
		$scope.playAudioconfirmconsent = function () {
			var display = document.getElementById("display");
			display.pause();

			var display = document.getElementById("success");
			display.pause();

			var display = document.getElementById("unsuccess");
			display.pause();

			var sample = document.getElementById("condition");
			sample.currentTime = 0;
			sample.play();
		};
		$scope.playAudiofaceregisterconfirmsuccess = function () {
			var display = document.getElementById("display");
			display.pause();

			var display = document.getElementById("success");
			display.pause();

			var display = document.getElementById("unsuccess");
			display.pause();

			var display = document.getElementById("condition");
			display.pause();

			var sample = document.getElementById("faceregisterconfirmsuccess");
			sample.currentTime = 0;
			sample.play();
		};
		$scope.Finish = function () {
			$(".DisplayDevice").hide();
			$(".Loading").hide();
			$(".Loading_card").hide();
			$(".Confirm_face").hide();
			$(".Success").hide();
			$(".UnSuccess").hide();
			$(".Condition").show();
			page = "Condition";
			$scope.resetModel();
			document.querySelector(".box-text-detail").focus();
			$scope.playAudioconfirmconsent();
		};
		$scope.replayAudio = function () {
			var sample = document.getElementById("display");
			sample.pause();
			sample.currentTime = 0;
			sample.play();
		};
		// $scope.Cancel = function () {
		//     $scope.playAudio();
		//     $location.path('/authen');
		// }
		$scope.dateDiff = function (startdate, enddate) {
			//define moments for the startdate and enddate
			var startdateMoment = moment(startdate).subtract(543, "year");
			var enddateMoment = moment(enddate);
			// // console.log(enddateMoment);

			if (
				startdateMoment.isValid() === true &&
				enddateMoment.isValid() === true
			) {
				//getting the difference in years
				var years = enddateMoment.diff(startdateMoment, "years");

				//moment returns the total months between the two dates, subtracting the years
				var months = enddateMoment.diff(startdateMoment, "months") - years * 12;

				//to calculate the days, first get the previous month and then subtract it
				startdateMoment.add(years, "years").add(months, "months");
				var days = enddateMoment.diff(startdateMoment, "days");

				return {
					years: years,
					months: months,
					days: days,
				};
			} else {
				return undefined;
			}
		};

		$scope.profileByStaff = function () {
			$location.path("/profile");
			AuthenticationService.setProfile({ name: "xxx", sex: "xxx", age: "xxx" });
		};

		$scope.logout = function () {
			page = "UnSuccess";
			$scope.playAudio();
			$location.path("/home");
		};

		$scope.again = function () {
			page = "UnSuccess";
			$scope.playAudio();
			$location.path("/staff_face_regis");
		};

		$scope.confirm_card = function () {
			$scope.playAudio();
			$location.path("/numpad");
		};
		$scope.agree = function () {
			page = "Confirm_face";
			$(".DisplayDevice").hide();
			$(".Loading").hide();
			$(".Loading_card").hide();
			$(".Confirm_face").show();
			$(".Success").hide();
			$(".UnSuccess").hide();
			$(".Condition").hide();
			// Webcam.snap(function (data_uri) {
			// 	$scope.results = '<img src="' + data_uri + '" />';
			// display results in page
			// document.getElementById('results').innerHTML =
			// '<img src="' + data_uri + '"/>';
			// alert(data_uri)
			// });
			$scope.resetModel();
			$scope.playAudiofaceregisterconfirmsuccess();
		};
		document.onkeypress = function (e) {
			e = e || window.event;
			let key = e.key.toLowerCase();
			console.log(page);
			switch (page) {
				case "DisplayDevice":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(380, 940).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(960, 940).click();
					}
					if (key == "c" || key == "แ") {
						// green
						document.elementFromPoint(1564, 924).click();
					}
					break;
				case "Loading_card":
					if (key == "z" || key == "ผ") {
						//red
					}
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(940, 932).click();
					}
					if (key == "c" || key == "แ") {
						// green
					}
					break;
				case "UnSuccess":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(1268, 916).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(648, 916).click();
					}
					if (key == "c" || key == "แ") {
						// green
					}
					break;
				case "Success":
					if (key == "z" || key == "ผ") {
						//red
					}
					if (key == "x" || key == "ป") {
						//yellow
					}
					if (key == "c" || key == "แ") {
						// green
						document.elementFromPoint(968, 908).click();
					}
					break;
				case "Condition":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(460, 936).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
					}
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(1558, 923).click();
					}
					break;
				case "Confirm_face":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(436, 864).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(940, 876).click();
					}
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(1460, 876).click();
					}
					break;

				default:
			}
		};
		var page = "DisplayDevice";
	}
})();
