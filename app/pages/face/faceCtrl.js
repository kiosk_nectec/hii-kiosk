(function () {
	"use strict";

	angular.module("BlurAdmin.pages.face").controller("faceCtrl", faceCtrl);

	/** @ngInject */
	function faceCtrl(
		$scope,
		baseService,
		$rootScope,
		$uibModal,
		$filter,
		$timeout,
		$location,
		$state,
		devicesService,
		$sce,
		$configuration,
		AuthenticationService,
		personService
	) {
		(function initController() {
			// var sample = document.getElementById("ircamera");
			// console.log(sample)
			// sample.currentTime = 0;
			// sample.play();
			setTimeout(() => {
				if ($rootScope.audiomute == true) {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = true;
					}
				} else {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = false;
					}
				}
				var loginfacevideo = document.getElementById("loginfacevideo");
				if (typeof loginfacevideo.loop == "boolean") {
					// loop supported
					loginfacevideo.loop = true;
				} else {
					// loop property not supported
					loginfacevideo.addEventListener(
						"ended",
						function () {
							this.currentTime = 0;
							this.play();
						},
						false
					);
				}
				loginfacevideo.play();
			}, 100);
		})();
		$scope.img_b64 = "";
		$scope.PID = "";
		$scope.ListPID = [];
		$scope.videoConfig = {
			sources: [
				{
					src: $sce.trustAsResourceUrl(
						"assets/vdo/SampleVideo_720x480_2mb.mp4"
					),
					type: "video/mp4",
				},
				// { src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.webm"), type: "video/webm" },
				// { src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.ogg"), type: "video/ogg" }
			],
			tracks: [
				{
					src: "http://www.videogular.com/assets/subs/pale-blue-dot.vtt",
					kind: "subtitles",
					srclang: "en",
					label: "English",
					default: "",
				},
			],

			theme:
				$configuration.rootpath +
				"theme/bower_components/videogular-themes-default/videogular.css",
			plugins: {
				poster: "http://www.videogular.com/assets/images/videogular.png",
			},
		};
		$scope.profile = {};
		$scope.activePageTitle = $state.current.title;
		// $scope.profile = $rootScope.globals.currentUser;
		$scope.video = null;

		$scope.resetModel = function () {
			$scope.height = "";
		};
		// $scope.init = function () {
		//     $scope.height = ""
		// }
		$scope.loginByIdface = function () {
			$scope.playAudio();
			//AuthenticationService.setProfile({ name: "test name", sex: "ชาย", age: "60" });
			// $location.path('/profile');
			// console.log('test')
			$(".DisplayDevice").hide();
			$(".Loading").hide();
			$(".ResultDevice").hide();
			$(".Condition").show();
			$scope.resetModel();
			document.querySelector(".box-text-detail-face").focus();
		};
		$scope.prepare = function () {
			$(".DisplayDevice").show();
			$(".Loading").hide();
			$(".ResultDevice").hide();
			$scope.resetModel();
		};

		$scope.dateDiff = function (startdate, enddate) {
			//define moments for the startdate and enddate
			var startdateMoment = moment(startdate).subtract(543, "year");
			var enddateMoment = moment(enddate);
			console.log(enddateMoment);

			if (
				startdateMoment.isValid() === true &&
				enddateMoment.isValid() === true
			) {
				//getting the difference in years
				var years = enddateMoment.diff(startdateMoment, "years");

				//moment returns the total months between the two dates, subtracting the years
				var months = enddateMoment.diff(startdateMoment, "months") - years * 12;

				//to calculate the days, first get the previous month and then subtract it
				startdateMoment.add(years, "years").add(months, "months");
				var days = enddateMoment.diff(startdateMoment, "days");

				return {
					years: years,
					months: months,
					days: days,
				};
			} else {
				return undefined;
			}
		};

		$scope.start = function () {
			$scope.playAudio();
			$scope.videoStop();
			$scope.showload();
			Webcam.snap(function (data_uri) {
				// $scope.results = '<img src="' + data_uri + '" />';
				$scope.img_b64 = data_uri;
			});
			// var rs = {
			// 	data: [
			// 		{
			// 			distance: "['0.25632372', '0.30661106', '0.27661106']",
			// 			id: "['2021000001', '2019000001', '2019000001']",
			// 			status: "more than 1",
			// 			total: "2",
			// 		},
			// 	],
			// };
			// $scope.PID = "2021000001";
			// $scope.getPersonLoginFace();
			// $scope.showwelcome();
			// $scope.showNumpadID();
			personService.loginfaceApi(
				{ profile_img: $scope.img_b64 },
				function (result) {
					// console.log(result)
					// $scope.PID = "2019000001";
					if (result.status == true) {
						if (
							parseFloat(result.message.data[0]["total"]) == 0 ||
							undefined == result.message.data[0]["total"] ||
							NaN == result.message.data[0]["total"]
						) {
							$scope.showUnSuccess();
						} else if (parseFloat(result.message.data[0]["total"]) == 1) {
							$scope.PID = result.message.data[0].id_no;
							$scope.getPersonLoginFace();
							$scope.showwelcome();
							$scope.playAudiofaceloginsuccess();
						} else if (parseFloat(result.message.data[0]["total"]) > 1) {
							result.message.data[0]["distance"] = JSON.parse(
								result.message.data[0]["distance"].replaceAll("'", '"')
							);
							result.message.data[0]["id"] = JSON.parse(
								result.message.data[0]["id"].replaceAll("'", '"')
							);
							$scope.Listdistance = [];
							$scope.tempdistance = {};
							var i;
							for (i = 0; i < result.message.data[0]["distance"].length; i++) {
								$scope.tempdistance = {
									distance: parseFloat(result.message.data[0]["distance"][i]),
									PID: parseFloat(result.message.data[0]["id"][i]),
									index: i,
								};
								$scope.Listdistance.push($scope.tempdistance);
							}
							$scope.ListPID = $scope.Listdistance.sort((x, y) => {
								return x.distance - y.distance;
							});
							// $scope.getPersonLoginFace();
							$scope.showNumpadID();
							$scope.playAudiofaceconfirmid();
						}
					} else {
						$scope.showUnSuccess();
						$scope.playAudiofaceloginfail();
					}
				}
			);
		};

		$scope.showwelcome = function () {
			$(".DisplayDevice").hide();
			$(".Confirm").hide();
			$(".Camera").hide();
			$(".error_face").hide();
			$(".NumpadID").hide();
			$(".ResultDevice").hide();
			$(".SimilarSuccess").hide();
			$(".Success").hide();
			$(".UnSuccess").hide();
			$(".UnSimilarSuccess").hide();
			$(".Condition").hide();
			$(".Loading").hide();
			$(".Welcome").show();
			page = "Welcome";
			$scope.resetModel();
		};

		$scope.showload = function () {
			$(".DisplayDevice").hide();
			$(".Confirm").hide();
			$(".Camera").hide();
			$(".error_face").hide();
			$(".NumpadID").hide();
			$(".ResultDevice").hide();
			$(".SimilarSuccess").hide();
			$(".Success").hide();
			$(".UnSuccess").hide();
			$(".UnSimilarSuccess").hide();
			$(".Condition").hide();
			$(".Loading").show();
			$(".Welcome").hide();
			page = "Loading";
			$scope.resetModel();
		};

		$scope.showSimilar = function () {
			$(".DisplayDevice").hide();
			$(".Confirm").hide();
			$(".Camera").hide();
			$(".error_face").hide();
			$(".NumpadID").hide();
			$(".ResultDevice").hide();
			$(".SimilarSuccess").show();
			$(".Success").hide();
			$(".UnSuccess").hide();
			$(".UnSimilarSuccess").hide();
			$(".Condition").hide();
			$(".Loading").hide();
			$(".Welcome").hide();

			$scope.resetModel();
		};

		$scope.showUnSuccess = function () {
			$(".DisplayDevice").hide();
			$(".Confirm").hide();
			$(".Camera").hide();
			$(".error_face").hide();
			$(".NumpadID").hide();
			$(".ResultDevice").hide();
			$(".SimilarSuccess").hide();
			$(".Success").hide();
			$(".UnSimilarSuccess").hide();
			$(".Condition").hide();
			$(".Loading").hide();
			$(".UnSuccess").show();
			$(".Welcome").hide();
			page = "UnSuccess";
			$scope.resetModel();
		};
		$scope.showSuccess = function () {
			$(".DisplayDevice").hide();
			$(".Confirm").hide();
			$(".Camera").hide();
			$(".error_face").hide();
			$(".NumpadID").hide();
			$(".ResultDevice").hide();
			$(".SimilarSuccess").hide();
			$(".Success").show();
			$(".UnSuccess").hide();
			$(".UnSimilarSuccess").hide();
			$(".Condition").hide();
			$(".Loading").hide();
			$(".Welcome").hide();
			page = "Welcome";
			$scope.resetModel();
		};
		$scope.showNumpadID = function () {
			$(".DisplayDevice").hide();
			$(".Confirm").hide();
			$(".Camera").hide();
			$(".error_face").hide();
			$(".NumpadID").show();
			$(".ResultDevice").hide();
			$(".SimilarSuccess").hide();
			$(".Success").hide();
			$(".UnSuccess").hide();
			$(".UnSimilarSuccess").hide();
			$(".Condition").hide();
			$(".Loading").hide();
			$(".Welcome").hide();
			page = "NumpadID";
			$scope.resetModel();
		};
		$scope.showUnSimilar = function () {
			$(".DisplayDevice").hide();
			$(".Confirm").hide();
			$(".Camera").hide();
			$(".error_face").hide();
			$(".NumpadID").hide();
			$(".ResultDevice").hide();
			$(".SimilarSuccess").hide();
			$(".Success").hide();
			$(".UnSuccess").hide();
			$(".UnSimilarSuccess").show();
			$(".Condition").hide();
			$(".Loading").hide();
			$(".Welcome").hide();
			page = "UnSimilar";
			$scope.resetModel();
		};
		$scope.Next = function () {
			$(".DisplayDevice").hide();
			$(".Confirm").hide();
			$(".Camera").hide();
			$(".error_face").hide();
			$(".NumpadID").hide();
			$(".ResultDevice").hide();
			$(".SimilarSuccess").hide();
			$(".Success").hide();
			$(".UnSuccess").hide();
			$(".UnSimilarSuccess").hide();
			$(".Condition").show();
			$(".Loading").hide();
			$(".Welcome").hide();
			$scope.resetModel();
		};

		$scope.another = function () {
			$scope.playAudio();
			$location.path("/authen");
		};

		$scope.Again = function () {
			$(".DisplayDevice").show();
			$(".Confirm").hide();
			$(".Camera").hide();
			$(".error_face").hide();
			$(".NumpadID").hide();
			$(".ResultDevice").hide();
			$(".SimilarSuccess").hide();
			$(".Success").hide();
			$(".UnSuccess").hide();
			$(".UnSimilarSuccess").hide();
			$(".Condition").hide();
			$(".Loading").hide();
			$(".Welcome").hide();
			page = "DisplayDevice";
			$scope.resetModel();
			$scope.playAudioreplay();
		};
		$scope.NextID = function () {
			$(".DisplayDevice").hide();
			$(".Confirm").hide();
			$(".Camera").hide();
			$(".error_face").hide();
			$(".NumpadID").show();
			$(".ResultDevice").hide();
			$(".SimilarSuccess").hide();
			$(".Success").hide();
			$(".UnSuccess").hide();
			$(".UnSimilarSuccess").hide();
			$(".Condition").hide();
			$(".Loading").hide();
			$(".Welcome").hide();
			page = "NumpadID";
			$scope.height = "";
			$scope.resetModel();
			$scope.playAudiofaceconfirmid();
		};

		//เช็คต่อจากกรณีที่ 3 DEVDEE1
		$scope.nextcase = function () {
			console.log($rootScope.globals.currentUser);
			personService.confirmfaceApi(
				{ ListPID: $scope.ListPID, CID: $scope.height },
				function (result) {
					console.log(result);
					if (result.status == true) {
						$scope.showwelcome();
						$scope.playAudiofaceloginsuccess();
						$scope.PID = result.message[0]["PID"];
						$scope.getPersonLoginFace();
						$scope.resetModel();
					} else {
						$scope.showUnSimilar();
						$scope.playAudiofaceconfirmidfail();
					}
				}
			);
		};

		$scope.onPlayerReady = function (API) {
			$scope.video = API;
			// console.log(API.stop())
			API.stop();
			setTimeout(() => {
				// $scope.videoPlay();
			}, 1000);
			// console.log(API)
		};

		$scope.heightFuntion = function (Key) {
			$scope.playAudio();

			console.log($scope.height);
			if ($scope.height == null) {
				$scope.height = "";
			}
			if ($scope.height.replace(".", "").length <= 3) {
				$scope.height = $scope.height.replace(".", "");
				// if ($scope.height.replace(".", "").length == 3) {
				//     $scope.height = $scope.height + "."
				// }
				$scope.height = $scope.height + Key;
				$scope.$apply();
			}
		};

		$scope.deleteFuntion = function () {
			$scope.playAudio();
			// $scope.height = $scope.height.replace(".", "")
			$scope.height = $scope.height.slice(0, -1);
		};

		$scope.videoPlay = function () {
			var sample = document.getElementById("ircamera");
			sample.pause();
			sample.currentTime = 0;
			sample.play();
			// console.log("PLAY");
			$scope.video.play();
		};
		$scope.videoStop = function () {
			// console.log("STOP");
			$scope.video.stop();
		};
		$scope.recheck = function () {
			$scope.videoPlay();
			$scope.prepare();
		};
		$scope.prepare();

		$scope.Cancel = function () {
			$scope.playAudio();
			$location.path("/authen");
		};

		$scope.logout = function () {
			$scope.playAudio();
			$location.path("/home");
		};

		$scope.erconfirm = function () {
			$scope.playAudio();
			$location.path("/authen");
		};
		$scope.getPersonLoginFace = function () {
			personService.getPersonLoginFace({ PID: $scope.PID }, function (history) {
				let profile = history.message[0];
				$rootScope.globals.PID = $scope.PID;
				$scope.profile = {
					id_card: history.item["CID"],
					titlenameth: history.item["titlenameth"],
					firstnameth: history.item["NAME"],
					lastnameth: history.item["LNAME"],
					profile_img: history.item["profile_img"],
					birthday:
						parseInt(history.item["BIRTH"].substring(0, 4)) +
						543 +
						"" +
						history.item["BIRTH"].substring(4, 8),
				};
				$scope.profile.age = $scope.dateDiff(
					$scope.profile.birthday,
					new Date()
				);
				$scope.results = '<img src="' + $scope.profile.profile_img + '" />';
				$scope.sex = history.message[0]["SEX"];
				$scope.profile.sex = history.message[0]["SEX"];

				AuthenticationService.setProfile($scope.profile);
				$rootScope.globals.PID = $scope.PID;
				// console.log($rootScope);
				$scope.profile.titleName = history.message[0]["NAME"];

				$rootScope.globals.information.SEX = history.message[0]["SEX"];
				$rootScope.globals.usertype = 1;
				if (history.message.length > 0) {
					$rootScope.globals.history = history.message[0];
				}

				if ($scope.profile.age.years) {
					if ($scope.profile.age.years > 60) {
						if ($scope.profile.sex == 1) {
							$scope.profile.titleName = "คุณลุง";
						} else if ($scope.profile.sex == 2) {
							$scope.profile.titleName = "คุณป้า";
						} else {
							$scope.profile.titleName = "";
						}
					} else {
						$scope.profile.titleName = $scope.profile.titlenameth;
					}
				}
			});
		};
		$scope.confirm = function () {
			$scope.playAudio();
			$location.path("/numpad");
		};

		$scope.welcome = function () {
			$scope.confirm();
		};

		$scope.dateDiff = function (startdate, enddate) {
			//define moments for the startdate and enddate
			var startdateMoment = moment(startdate).subtract(543, "year");
			var enddateMoment = moment(enddate);
			// console.log(enddateMoment);

			if (
				startdateMoment.isValid() === true &&
				enddateMoment.isValid() === true
			) {
				//getting the difference in years
				var years = enddateMoment.diff(startdateMoment, "years");

				//moment returns the total months between the two dates, subtracting the years
				var months = enddateMoment.diff(startdateMoment, "months") - years * 12;

				//to calculate the days, first get the previous month and then subtract it
				startdateMoment.add(years, "years").add(months, "months");
				var days = enddateMoment.diff(startdateMoment, "days");

				return {
					years: years,
					months: months,
					days: days,
				};
			} else {
				return undefined;
			}
		};
		$scope.startCamera = function () {
			$scope.playAudio();
			$scope.playAudiofacelogincapture();
			page = "Camera";

			$(".DisplayDevice").hide();
			$(".Loading").hide();
			$(".ResultDevice").hide();
			$(".Camera").show();
			$(".Welcome").hide();
			// $scope.cameraAudio();
			$scope.resetModel();
		};

		$scope.xbar = function () {
			$scope.playAudio();
			$(".DisplayDevice").show();
			$(".Loading").hide();
			$(".ResultDevice").hide();
			$(".Condition").hide();
			$(".Confirm").hide();
			$(".error_face").hide();
			$(".Welcome").hide();
			$scope.resetModel();
		};

		$scope.agree = function () {
			$scope.playAudio();

			personService.savePerson($scope.profile, function (result) {
				if (result.status == true) {
					$scope.sex = result.SEX;
					$scope.profile.profile_img = result.profile_img;
					$scope.profile.sex = result.SEX;
					console.log($rootScope.globals);
					AuthenticationService.setProfile($scope.profile);
					personService.getPerson({ PID: result.PID }, function (history) {
						console.log(history);
						if (history.message.length > 0) {
							$rootScope.globals.history = history.message[0];
						}
						$rootScope.globals.PID = result.PID;

						$scope.profile.titleName =
							$rootScope.globals.currentUser.titlenameth;

						$rootScope.globals.information.SEX = result.SEX;
						$rootScope.globals.usertype = 1;

						if ($scope.profile.age.years) {
							if ($scope.profile.age.years > 60) {
								if ($scope.profile.sex == 1) {
									$scope.profile.titleName = "คุณลุง";
								} else if ($scope.profile.sex == 2) {
									$scope.profile.titleName = "คุณป้า";
								} else {
									$scope.profile.titleName = "";
								}
							} else {
								$scope.profile.titleName = $scope.profile.titlenameth;
							}
						}
					});
					// alert(result.message)
					$(".DisplayDevice").hide();
					$(".Loading").hide();
					$(".ResultDevice").hide();
					$(".Condition").hide();
					$(".Confirm").show();
					$(".Welcome").hide();
					$scope.confirmAudio();
					$scope.resetModel();
				} else {
					$scope.showUnSuccess();
					$scope.playAudiofaceloginfail();
				}
			});
		};

		document.onkeypress = function (e) {
			e = e || window.event;
			let key = e.key.toLowerCase();
			switch (page) {
				case "DisplayDevice":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(384, 928).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(964, 936).click();
					}
					if (key == "c" || key == "แ") {
						// green
						document.elementFromPoint(1540, 924).click();
					}
					break;
				case "Camera":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(364, 944).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
					}
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(1528, 932).click();
					}
					break;
				case "Loading":
					if (key == "z" || key == "ผ") {
						//red
					}
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(948, 932).click();
					}
					if (key == "c" || key == "แ") {
						//green
					}
					break;
				case "Welcome":
					if (key == "z" || key == "ผ") {
						//red
					}
					if (key == "x" || key == "ป") {
						//yellow
					}
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(1444, 868).click();
					}
					break;
				case "UnSuccess":
					if (key == "z" || key == "ผ") {
						//red
					}
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(660, 904).click();
					}
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(1264, 916).click();
					}
					break;
				case "NumpadID":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(332, 896).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
					}
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(848, 892).click();
					}
					break;
				case "UnSimilar":
					if (key == "z" || key == "ผ") {
						//red
					}
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(668, 916).click();
					}
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(1256, 912).click();
					}
					break;
				default:
			}
		};
		// $scope.init =function(){

		// 	if($stateParams.page=="Condition"){
		// 		$scope.unknown()
		// 	}else{
		// 		$scope.displayAudio()
		// 	}
		// }
		var page = "DisplayDevice";

		$scope.playAudio = function () {
			var audio = new Audio("./assets/kiosk/audio/click.mp3");
			audio.play();
		};
		$scope.playAudiofacelogincapture = function () {
			var display = document.getElementById("ircamera");
			display.pause();

			var sample = document.getElementById("facelogincapture");
			sample.currentTime = 0;
			sample.play();
		};
		$scope.playAudiofaceloginfail = function () {
			var display = document.getElementById("ircamera");
			display.pause();

			var display = document.getElementById("facelogincapture");
			display.pause();

			var sample = document.getElementById("faceloginfail");
			sample.currentTime = 0;
			sample.play();
		};
		$scope.playAudioreplay = function () {
			var display = document.getElementById("faceloginfail");
			display.pause();

			var display = document.getElementById("facelogincapture");
			display.pause();

			var sample = document.getElementById("ircamera");
			sample.currentTime = 0;
			sample.play();
		};
		$scope.playAudiofaceloginsuccess = function () {
			var display = document.getElementById("faceloginfail");
			display.pause();

			var display = document.getElementById("facelogincapture");
			display.pause();

			var display = document.getElementById("ircamera");
			display.pause();

			var display = document.getElementById("faceconfirmid");
			display.pause();

			var sample = document.getElementById("faceloginsuccess");
			sample.currentTime = 0;
			sample.play();
		};
		$scope.playAudiofaceconfirmid = function () {
			var display = document.getElementById("faceloginfail");
			display.pause();

			var display = document.getElementById("facelogincapture");
			display.pause();

			var display = document.getElementById("ircamera");
			display.pause();

			var display = document.getElementById("faceloginsuccess");
			display.pause();

			var sample = document.getElementById("faceconfirmid");
			sample.currentTime = 0;
			sample.play();
		};
		$scope.playAudiofaceconfirmidfail = function () {
			var display = document.getElementById("faceloginfail");
			display.pause();

			var display = document.getElementById("facelogincapture");
			display.pause();

			var display = document.getElementById("ircamera");
			display.pause();

			var display = document.getElementById("faceloginsuccess");
			display.pause();

			var display = document.getElementById("faceconfirmid");
			display.pause();

			var sample = document.getElementById("faceconfirmidfail");
			sample.currentTime = 0;
			sample.play();
		};
	}
})();
