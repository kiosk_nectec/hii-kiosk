/**
 * @author a.demeshko
 * created on 12/18/15
 */
(function() {
    "use strict";

    var pageName = "staff_face_regis";
    var pageCtrl = pageName + "Ctrl";
    angular.module("BlurAdmin.pages.staff_face_regis", []).config(routeConfig);
    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider.state(pageName, {
            url: "/" + pageName,
            templateUrl: "app/pages/staff_face_regis/staff_face_regis.html",
            controller: pageCtrl,
            title: "เข้าสู่ระบบด้วยใบหน้า",
            sidebarMeta: {
                icon: "ion-gear-a",
                order: 3,
            },
        });
    }
})();