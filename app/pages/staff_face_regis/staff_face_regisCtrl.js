(function () {
	"use strict";

	angular
		.module("BlurAdmin.pages.staff_face_regis")
		.controller("staff_face_regisCtrl", staff_face_regisCtrl);

	/** @ngInject */
	function staff_face_regisCtrl(
		$scope,
		baseService,
		$rootScope,
		$uibModal,
		$filter,
		$timeout,
		$location,
		$state,
		devicesService,
		$sce,
		$configuration,
		AuthenticationService,
		personService
	) {
		(function initController() {
			baseService.devicesActiveList();
			setTimeout(() => {
				if ($rootScope.audiomute == true) {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = true;
					}
				} else {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = false;
					}
				}
				var registerfacevideo = document.getElementById("registerfacevideo");
				if (typeof registerfacevideo.loop == 'boolean') { // loop supported
					registerfacevideo.loop = true;
				  } else { // loop property not supported
					registerfacevideo.addEventListener('ended', function () {
					  this.currentTime = 0;
					  this.play();
					}, false);
				  }
				registerfacevideo.play();
			}, 100);
		})();
		$scope.img_b64 = "";

		$scope.videoConfig = {
			sources: [
				{
					src: $sce.trustAsResourceUrl(
						"assets/vdo/SampleVideo_720x480_2mb.mp4"
					),
					type: "video/mp4",
				},
				// { src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.webm"), type: "video/webm" },
				// { src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.ogg"), type: "video/ogg" }
			],
			tracks: [
				{
					src: "http://www.videogular.com/assets/subs/pale-blue-dot.vtt",
					kind: "subtitles",
					srclang: "en",
					label: "English",
					default: "",
				},
			],

			theme:
				$configuration.rootpath +
				"theme/bower_components/videogular-themes-default/videogular.css",
			plugins: {
				poster: "http://www.videogular.com/assets/images/videogular.png",
			},
		};
		$scope.activePageTitle = $state.current.title;
		$scope.staff_regis = $rootScope.globals.currentUser;
		$scope.video = null;
		$scope.audio;
		$scope.resetModel = function () {};
		$scope.loginByIdcard = function () {
			AuthenticationService.setProfile({
				name: "test name",
				sex: "ชาย",
				age: "60",
			});
			$location.path("/staff_card_regis");
			// console.log('test')
		};
		$scope.prepare = function () {
			page = "DisplayDevice";
			$(".DisplayDevice").show();
			$(".Loading").hide();
			$(".ResultDevice").hide();
			$scope.resetModel();
		};
		$scope.start = function () {
			$scope.playAudio();
			$scope.videoStop();
			page = "Success";
			$(".DisplayDevice").hide();
			$(".Confirm").hide();
			$(".Camera").hide();
			$(".error_face").hide();
			$(".Success").show();
			// $(".error_face").show();

			// take snapshot and get image data
			Webcam.snap(function (data_uri) {
				$scope.results = '<img src="' + data_uri + '" />';
				// display results in page
				// document.getElementById('results').innerHTML =
				// '<img src="' + data_uri + '"/>';
				// alert(data_uri)
				// console.log(data_uri)
				$scope.img_b64 = data_uri;
			});
			var display = document.getElementById("camera");
			display.pause();

			var display = document.getElementById("display");
			display.pause();

			var sample = document.getElementById("success");
			sample.currentTime = 0;
			sample.play();
			// $scope.playAudiofaceregisterconfirm();
			// console.log($scope.results)
		};
		$scope.onPlayerReady = function (API) {
			$scope.video = API;
			// console.log(API.stop())
			API.stop();
			setTimeout(() => {
				$scope.videoPlay();
			}, 1000);
			// console.log(API)
		};

		$scope.videoPlay = function () {
			console.log("PLAY");
			$scope.video.play();
		};
		$scope.videoStop = function () {
			console.log("STOP");
			// $scope.video.stop();
		};
		$scope.recheck = function () {
			$scope.videoPlay();
			$scope.prepare();
		};

		$scope.progressFunction = function () {
			return $timeout(function () {}, 3000);
		};
		$scope.next = function () {
			// $location.path('/devices/weighing-machines');
			baseService.nextDevices();
		};

		// $scope.cancel = function() {
		//     $location.path('/authen');
		// }

		$scope.Cancel = function () {
			$location.path("/staff");
		};

		$scope.Next = function () {
			$rootScope.profile_img_b64 = $scope.img_b64;
			$location.path("/staff_card_regis");
		};
		$scope.again = function () {
			$scope.playAudio();
			$(".DisplayDevice").hide();
			$(".Loading_face").hide();
			$(".Camera").show();
			$(".Success").hide();
			page = "Camera";

			var display = document.getElementById("success");
			display.pause();

			var display = document.getElementById("display");
			display.pause();

			var sample = document.getElementById("camera");
			sample.currentTime = 0;
			sample.play();
			// $scope.playAudiofaceregiscapture();
			// $scope.cameraAudio();
			// $scope.resetModel();
		};

		$scope.startCamera = function () {
			$scope.playAudio();

			var sample = document.getElementById("display");
			sample.pause();

			var sample = document.getElementById("camera");
			sample.currentTime = 0;
			sample.play();

			page = "Camera";
			$(".DisplayDevice").hide();
			$(".ResultDevice").hide();
			$(".Camera").show();
			// $scope.playAudiofaceregiscapture();
			// $scope.cameraAudio();
			$scope.resetModel();
		};
		$scope.logout = function () {
			$location.path("/home");
		};

		$scope.Start = function () {
			$(".DisplayDevice").hide();
			$(".Loading_face").show();
			setTimeout(function () {
				$(".Success").show();
				page = "Success";
			}, 3000);
			$scope.resetModel();
		};
		$scope.playAudiofaceregisterconfirm = function () {
			$scope.audio = new Audio("./assets/kiosk/audio/faceregisterconfirm.mp3");
			$scope.audio.play();
		};
		$scope.playAudiofaceregiscapture = function () {
			$scope.audio = new Audio("./assets/kiosk/audio/faceregiscapture.mp3");
			$scope.audio.play();
		};

		$scope.replayAudio = function () {
			var sample = document.getElementById("display");
			sample.pause();
			sample.currentTime = 0;
			sample.play();
		};

		document.onkeypress = function (e) {
			e = e || window.event;
			let key = e.key.toLowerCase();
			switch (page) {
				case "DisplayDevice":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(380, 940).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(960, 940).click();
					}
					if (key == "c" || key == "แ") {
						// green
						document.elementFromPoint(1564, 924).click();
					}
					break;
				case "Camera":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(388, 940).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
					}
					if (key == "c" || key == "แ") {
						// green
						document.elementFromPoint(1524, 924).click();
					}
					break;
				case "Success":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(416, 916).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(968, 916).click();
					}
					if (key == "c" || key == "แ") {
						// green
						document.elementFromPoint(1496, 908).click();
					}
					break;

				default:
			}
		};
		var page = "DisplayDevice";
	}
})();
