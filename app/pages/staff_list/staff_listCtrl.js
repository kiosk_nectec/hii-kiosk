(function () {
	"use strict";

	angular
		.module("BlurAdmin.pages.staff_list")
		.controller("staff_listCtrl", homeCtrl);

	/** @ngInject */
	function homeCtrl(
		$scope,
		baseService,
		$rootScope,
		$uibModal,
		$filter,
		$timeout,
		$location,
		$state,
		staffService,
		AuthenticationService,
		personService
	) {
		(function initController() {
			baseService.devicesActiveList();
			setTimeout(() => {
				if ($rootScope.audiomute == true) {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = true;
					}
				} else {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = false;
					}
				}
			}, 100);
		})();
		$scope.activePageTitle = $state.current.title;
		$scope.authen_admin = $rootScope.globals.currentUser;

		$scope.progressFunction = function () {
			return $timeout(function () {}, 3000);
		};
		$scope.profile = {};
		$scope.next = function () {
			$scope.playAudio();
			// $location.path('/devices/weighing-machines');
			baseService.nextDevices();
		};

		$scope.ListPerson = [];
		$scope.SearchModel = {};

		$scope.dateDiff = function (startdate, enddate) {
			//define moments for the startdate and enddate
			var startdateMoment = moment(startdate).subtract(543, "year");
			var enddateMoment = moment(enddate);
			if (
				startdateMoment.isValid() === true &&
				enddateMoment.isValid() === true
			) {
				//getting the difference in years
				var years = enddateMoment.diff(startdateMoment, "years");
				//moment returns the total months between the two dates, subtracting the years
				var months = enddateMoment.diff(startdateMoment, "months") - years * 12;
				//to calculate the days, first get the previous month and then subtract it
				startdateMoment.add(years, "years").add(months, "months");
				var days = enddateMoment.diff(startdateMoment, "days");
				return {
					years: years,
					months: months,
					days: days,
				};
			} else {
				return undefined;
			}
		};
		$scope.showDisplayDevice = function () {
			$(".DisplayDevice").show();
			$(".Welcome").hide();
		};

		$scope.showwelcome = function () {
			page = "welcome";
			$(".DisplayDevice").hide();
			$(".Welcome").show();
		};
		$scope.Back = function () {
			$location.path("/staff");
		};
		$scope.resetModel = function () {
			$scope.ListPerson = [];
			$scope.SearchModel = {};
		};

		$scope.Search = function () {
			if (
				undefined == $scope.SearchModel.Name ||
				$scope.SearchModel.Name == ""
			) {
				$scope.ListPerson = [];
			} else {
				$scope.ListPerson = [];
				$scope.tempListPerson = [];
				$scope.tempPerson = {};
				staffService.SearchPerson($scope.SearchModel, function (result) {
					if (result.status === true) {
						$scope.tempListPerson = result.message;
						if ($scope.tempListPerson.length > 0) {
							$scope.tempListPerson.forEach(function (entry, index) {
								$scope.tempPerson = {
									BIRTH:
										parseInt(entry.BIRTH.substring(0, 4)) +
										543 +
										"" +
										entry.BIRTH.substring(4, 8),
									age: $scope.dateDiff(
										parseInt(entry.BIRTH.substring(0, 4)) +
											543 +
											"" +
											entry.BIRTH.substring(4, 8),
										new Date()
									),
									CID: entry.CID,
									LNAME: entry.LNAME,
									NAME: entry.NAME,
									PID: entry.PID,
									profile_img: entry.profile_img,
									titlenameth: entry.titlenameth,
								};
								$scope.ListPerson.push($scope.tempPerson);
							});
						}
					} else {
						$scope.ListPerson = [];
					}
				});
			}
		};
		$scope.getPersonLoginFace = function (item) {
			personService.getPersonLoginFace({ PID: item.PID }, function (history) {
				if (history.status == true) {
					$scope.showwelcome();
					let profile = history.message[0];
					$rootScope.globals.PID = item.PID;
					$scope.profile = {
						id_card: history.item["CID"],
						titlenameth: history.item["titlenameth"],
						firstnameth: history.item["NAME"],
						lastnameth: history.item["LNAME"],
						profile_img: history.item["profile_img"],
						birthday:
							parseInt(history.item["BIRTH"].substring(0, 4)) +
							543 +
							"" +
							history.item["BIRTH"].substring(4, 8),
					};
					$scope.profile.age = $scope.dateDiff(
						$scope.profile.birthday,
						new Date()
					);
					$scope.results = '<img src="' + $scope.profile.profile_img + '" />';
					$scope.sex = history.message[0]["SEX"];
					$scope.profile.sex = history.message[0]["SEX"];

					AuthenticationService.setProfile($scope.profile);
					$rootScope.globals.PID = item.PID;
					// console.log($rootScope);
					$scope.profile.titleName = history.message[0]["NAME"];

					$rootScope.globals.information.SEX = history.message[0]["SEX"];
					$rootScope.globals.usertype = 1;
					if (history.message.length > 0) {
						$rootScope.globals.history = history.message[0];
					}

					if ($scope.profile.age.years) {
						if ($scope.profile.age.years > 60) {
							if ($scope.profile.sex == 1) {
								$scope.profile.titleName = "คุณลุง";
							} else if ($scope.profile.sex == 2) {
								$scope.profile.titleName = "คุณป้า";
							} else {
								$scope.profile.titleName = "";
							}
						} else {
							$scope.profile.titleName = $scope.profile.titlenameth;
						}
					}
				} else {
					$scope.showUnSuccess();
				}
			});
		};

		$scope.onit = function () {
			$scope.showDisplayDevice();
			$scope.resetModel();
			// $scope.Search();
		};

		$scope.welcome = function () {
			page = "welcome";
			$scope.playAudio();
			$location.path("/numpad");
		};

		$scope.Home = function () {
			$scope.playAudio();
			$location.path("/authen");
		};
		$scope.Home = function () {
			$scope.playAudio();
			$location.path("/authen");
		};

		$scope.Cancel = function () {
			// $scope.showDisplayDevice();
			$location.path("/staff");
		};

		$scope.playAudio = function () {
			var audio = new Audio("./assets/kiosk/audio/click.mp3");
			audio.play();
		};

		$scope.onit();
		document.onkeypress = function (e) {
			var getElement = document.getElementById("StaffSearchName");
			if (document.activeElement != getElement) {
				e = e || window.event;
				let key = e.key.toLowerCase();
				switch (page) {
					case "DisplayDevice":
						if (key == "z" || key == "ผ") {
							//red
						}
						if (key == "x" || key == "ป") {
							//yellow
							document.elementFromPoint(960, 990).click();
						}
						if (key == "c" || key == "แ") {
							// green
						}
						break;
					case "welcome":
						if (key == "z" || key == "ผ") {
							//red
							document.elementFromPoint(434, 878).click();
						}
						if (key == "x" || key == "ป") {
							//yellow
						}
						if (key == "c" || key == "แ") {
							// green
							document.elementFromPoint(1456, 878).click();
						}
						break;

					default:
				}
			}
		};
		var page = "DisplayDevice";
	}
})();
