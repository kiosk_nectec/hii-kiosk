/**
 * @author a.demeshko
 * created on 12/18/15
 */
(function() {
    "use strict";

    var pageName = "staff_list";
    var pageCtrl = pageName + "Ctrl";
    angular.module("BlurAdmin.pages.staff_list", []).config(routeConfig);
    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider.state(pageName, {
            url: "/" + pageName,
            templateUrl: "app/pages/staff_list/staff_list.html",
            controller: pageCtrl,
            title: "ค้นหารายชื่อผู้ใช้งาน",
            sidebarMeta: {
                icon: "ion-gear-a",
                order: 3,
            },
        });
    }
})();