/**
 * @author a.demeshko
 * created on 12/18/15
 */
(function() {
    "use strict";

    var pageName = "results_unknown";
    var pageCtrl = pageName + "Ctrl";
    angular.module("BlurAdmin.pages.results_unknown", []).config(routeConfig);
    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider.state(pageName, {
            url: "/" + pageName,
            templateUrl: "app/pages/results_unknown/results_unknown.html",
            controller: pageCtrl,
            title: "ข้อมูลผู้ใช้งาน",
            sidebarMeta: {
                icon: "ion-gear-a",
                order: 3,
            },
        });
    }
})();