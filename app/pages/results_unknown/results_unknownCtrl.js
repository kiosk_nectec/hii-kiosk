(function () {
	"use strict";

	angular
		.module("BlurAdmin.pages.results_unknown")
		.controller("results_unknownCtrl", homeCtrl);

	/** @ngInject */
	function homeCtrl(
		$scope,
		baseService,
		$rootScope,
		$uibModal,
		$filter,
		$timeout,
		$location,
		$state
	) {
		(function initController() {
			$scope.profile = {};
			setTimeout(() => {
				if ($rootScope.audiomute == true) {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = true;
					}
				} else {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = false;
					}
				}
			}, 100);
		})();

		$scope.profile = angular.copy($rootScope.globals.currentUser);

		$scope.results = $rootScope.globals.information;
		$scope.recheckAll = function () {
			$rootScope.globals.information = {};
			baseService.recheckAll();
		};
		$scope.profilePage = function () {
			$location.path("/profile");
		};
		$scope.logout = function () {
			$location.path("/authen");
		};
		$scope.authen_adminByResults = function () {
			// AuthenticationService.setProfile({ name: "test name", sex: "ชาย", age: "60" });
			$location.path("/authen_admin");
		};
		$scope.detailByProfile = function () {
			// AuthenticationService.setProfile({ name: "test name", sex: "ชาย", age: "60" });
			$location.path("/detail");
		};
	}
})();
