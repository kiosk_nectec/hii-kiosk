(function () {
	"use strict";

	angular.module("BlurAdmin.pages.staff").controller("staffCtrl", homeCtrl);

	/** @ngInject */
	function homeCtrl(
		$scope,
		baseService,
		$rootScope,
		$uibModal,
		$filter,
		$timeout,
		$location,
		$state,
		$window
	) {
		(function initController() {
			baseService.devicesActiveList();
			setTimeout(() => {
				if ($rootScope.audiomute == true) {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = true;
					}
				} else {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = false;
					}
				}
			}, 100);
		})();
		$scope.activePageTitle = $state.current.title;
		$scope.authen_admin = $rootScope.globals.currentUser;

		$scope.progressFunction = function () {
			return $timeout(function () {}, 3000);
		};
		$scope.next = function () {
			$scope.playAudio();
			// $location.path('/devices/weighing-machines');
			baseService.nextDevices();
		};

		// $scope.cancel = function() {
		//     $location.path('/authen');
		// }

		$scope.Home = function () {
			$scope.playAudio();
			$location.path("/authen");
		};

		$scope.Homekiosmode = function (event) {
			console.log(event);
			// var keycode=event.keycode;
			// Window.minimize()
			// $window.top.close();
			// document.getElementsByTagName ('html') [0] .remove ();
			// $myService.onclose();
			// $window.open('','_parent','');
			// $window.close();
			// $window.location.href = 'http://www.google.com';
			// document.close();
			// window.location.href = '/closekiosk';
		};
		// $window.Homekiosmode = function (evt) {
		//     $myService.onclose();
		//   }

		$scope.settingBystaff = function () {
			$scope.playAudio();
			$location.path("/staff_set");
		};

		$scope.regisBystaff = function () {
			$scope.playAudio();
			$location.path("/staff_face_regis");
		};

		$scope.listBystaff = function () {
			$scope.playAudio();
			$location.path("/staff_list");
		};

		$scope.playAudio = function () {
			var audio = new Audio("./assets/kiosk/audio/click.mp3");
			audio.play();
		};
		document.onkeypress = function (e) {
			e = e || window.event;
			let key = e.key.toLowerCase();
			switch (page) {
				case "DisplayDevice":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(380, 824).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(976, 836).click();
					}
					if (key == "c" || key == "แ") {
						// green
						document.elementFromPoint(1500, 836).click();
					}
					break;

				default:
			}
		};
		var page = "DisplayDevice";
	}
})();
