/**
 * @author a.demeshko
 * created on 12/18/15
 */
(function() {
    "use strict";

    var pageName = "staff";
    var pageCtrl = pageName + "Ctrl";
    angular.module("BlurAdmin.pages.staff", []).config(routeConfig);
    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider.state(pageName, {
            url: "/" + pageName,
            templateUrl: "app/pages/staff/staff.html",
            controller: pageCtrl,
            title: "เมนูเจ้าหน้าที่",
            sidebarMeta: {
                icon: "ion-gear-a",
                order: 3,
            },
        });
    }
})();