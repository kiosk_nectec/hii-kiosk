(function () {
	"use strict";

	angular.module("BlurAdmin.pages.numpad").controller("numpadCtrl", homeCtrl);

	/** @ngInject */
	function homeCtrl(
		$scope,
		baseService,
		$rootScope,
		$uibModal,
		$filter,
		$timeout,
		$location,
		$state,
		d_serviceService
	) {
		(function initController() {
			baseService.devicesActiveList();
			setTimeout(() => {
				if ($rootScope.audiomute == true) {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = true;
					}
				} else {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = false;
					}
				}
			}, 100);
		})();
		$scope.activePageTitle = $state.current.title;
		$scope.numpad = angular.copy($rootScope.globals.currentUser);
		// console.log($rootScope.globals);
		if ($rootScope.globals.history != undefined) {
			$scope.height = angular.copy($rootScope.globals.history.HEIGHT);
		} else {
			$scope.height = "";
		}

		$scope.heightFuntion = function (Key) {
			$scope.playAudio();

			console.log($scope.height);
			if ($scope.height == null) {
				$scope.height = "";
			}
			if ($scope.height.replace(".", "").length <= 3) {
				$scope.height = $scope.height.replace(".", "");
				if ($scope.height.replace(".", "").length == 3) {
					$scope.height = $scope.height + ".";
				}
				$scope.height = $scope.height + Key;
				$scope.$apply();
			}
		};

		//เก็บน้ำหนัก

		$scope.deleteFuntion = function () {
			$scope.playAudio();
			$scope.height = $scope.height.replace(".", "");
			$scope.height = $scope.height.slice(0, -1);
		};

		$scope.progressFunction = function () {
			return $timeout(function () {}, 3000);
		};
		$scope.next = function () {
			$scope.playAudio();

			// $location.path('/devices/weighing-machines');
			baseService.nextDevices();
		};

		// $scope.cancel = function() {
		//     $location.path('/authen');
		// }

		$scope.select = function () {
			$scope.playAudio();
			// console.log($rootScope.globals);
			$rootScope.globals.information.height = angular.copy($scope.height);
			d_serviceService.saveD_service($rootScope.globals, function (result) {
				if (result.status == true) {
					$location.path("/profile");
				}
			});
			// d_serviceService.saveD_service($rootScope.globals)

			//
		};

		$scope.Cancel = function () {
			$scope.playAudio();
			$location.path("/authen");
		};

		$scope.playAudio = function () {
			var audio = new Audio("./assets/kiosk/audio/click.mp3");
			audio.play();
		};

		document.onkeypress = function (e) {
			e = e || window.event;
			let key = e.key.toLowerCase();
			switch (page) {
				case "DisplayDevice":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(348, 909).click();
					}
					// if (key == "x" || key=="ป") {
					// 	//yellow
					// }
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(852, 903).click();
					}
					break;

				default:
			}
		};

		var page = "DisplayDevice";
	}
})();
