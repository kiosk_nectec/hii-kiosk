(function () {
	"use strict";

	angular.module("BlurAdmin.pages.profile").controller("profileCtrl", homeCtrl);

	/** @ngInject */
	function homeCtrl(
		$scope,
		baseService,
		$rootScope,
		$uibModal,
		$filter,
		$timeout,
		$location,
		$state,
		conBloodPressureService,
		conPulseRateService,
		conOxygenService,
		conTemperatureService,
		conWeighingMachinesService
	) {
		(function initController() {
			baseService.devicesActiveList();
			setTimeout(() => {
				if ($rootScope.audiomute == true) {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = true;
					}
				} else {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = false;
					}
				}
			}, 100);
		})();
		$scope.activePageTitle = $state.current.title;

		$scope.profile = angular.copy($rootScope.globals.currentUser);

		$scope.PID = $rootScope.globals.PID;

		$scope.profile.titleName = $rootScope.globals.currentUser.titlenameth;
		console.log($rootScope.globals.history)
		if ($rootScope.globals.history) {
			$scope.history = $rootScope.globals.history;
			
			// $scope.history.BMR = parseInt($scope.history.BMR);
			// $scope.sex = $rootScope.globals.history.SEX;
		} else {
			$scope.history = "";
		}
		console.log($rootScope.globals);

		if ($scope.profile.age.years) {
			if ($scope.profile.age.years > 60) {
				if ($scope.profile.sex == 1) {
					$scope.profile.titleName = "คุณลุง";
				} else if ($scope.profile.sex == 2) {
					$scope.profile.titleName = "คุณป้า";
				} else {
					$scope.profile.titleName = "";
				}
			} else {
				$scope.profile.titleName = $scope.profile.titlenameth;
			}
		}

		console.log($rootScope.globals);

		console.log($scope.history);
		$scope.results = $rootScope.globals.information;

		$scope.progressFunction = function () {
			return $timeout(function () {}, 3000);
		};
		$scope.authen_adminByProfile = function () {
			$location.path("/authen_admin");
		};
		$scope.next = function () {
			$scope.playAudio();
			// $location.path('/devices/weighing-machines');
			baseService.nextDevices();
		};

		$scope.logout = function () {
			$scope.playAudio();
			$location.path("/home");
		};

		$scope.other_services = function () {
			$scope.playAudio();
			$location.path("/other_services");
		};

		$scope.select = function () {
			$scope.selectAudio();
			$scope.playAudio();
			// $(".DisplayDevice").hide();
			// $(".Select_Weighing").show();
			// $(".Select_Option").hide();
			if ($rootScope.devicesActiveList.length > 0) {
				baseService.nextDevices();
			} else {
				$(".DisplayDevice").hide();
				$(".Select_Weighing").hide();
				$(".Select_Option").hide();
				$(".UnSuccess").show();
			}
		};

		$scope.option = function () {
			$scope.playAudio();
			$(".DisplayDevice").hide();
			$(".Select_Weighing").show();
			$(".Select_Option").show();
			$(".UnSuccess").hide();
			$scope.resetModel();
		};

		$scope.back_select = function () {
			$scope.playAudio();
			$(".DisplayDevice").hide();
			$(".Select_Weighing").show();
			$(".Select_Option").hide();
			$(".UnSuccess").hide();
			$scope.resetModel();
		};

		$scope.Cancel = function () {
			$scope.playAudio();
			$(".DisplayDevice").show();
			$(".Select_Weighing").hide();
			$(".Select_Option").hide();
			$(".UnSuccess").hide();
			$scope.resetModel();
		};

		$scope.playAudio = function () {
			var audio = new Audio("./assets/kiosk/audio/click.mp3");
			audio.play();
		};

		$scope.conditionDataBloodPressure = [];
		$scope.bloodPressureStyleEmoji = {};
		$scope.convertDataBloodPressure = {};

		$scope.conditionDataPulseRate = [];
		$scope.pulseRateStyleEmoji = {};
		$scope.convertDataPulseRate = {};

		$scope.conditionDataOxygen = [];
		$scope.oxygenStyleEmoji = {};
		$scope.convertDataOxygen = {};

		$scope.conditionDataTemperature = [];
		$scope.convertDataTemperature = {};
		$scope.temperatureStyleEmoji = {};

		$scope.conditionDataWeighingMachines = [];
		$scope.convertDataWeighingMachines = {};
		$scope.weighingMachinesStyleEmoji = {};

		$scope.init = function () {
			conBloodPressureService.getConBloodPressure({}, function (result) {
				$scope.conditionDataBloodPressure = result.message;
				console.log($scope.conditionDataBloodPressure);
				// $scope.convertDataBloodPressure = $scope.conditionDataBloodPressure[0]

				// $scope.bloodPressureStyleEmoji = {
				//     'background-image': 'url('+$scope.convertDataBloodPressure.Image+')','width':'200px'
				// }
				$scope.convertResult();
			});

			conPulseRateService.getConPulseRate({}, function (result) {
				$scope.conditionDataPulseRate = result.message;
				// console.log($scope.conditionDataBloodPressure)
				// $scope.convertDataBloodPressure = $scope.conditionDataBloodPressure[0]

				// $scope.bloodPressureStyleEmoji = {
				//     'background-image': 'url('+$scope.convertDataBloodPressure.Image+')','width':'200px'
				// }
				$scope.convertResult();
			});

			conOxygenService.getConOxygen({}, function (result) {
				$scope.conditionDataOxygen = result.message;
				// console.log($scope.conditionDataOxygen)
				// $scope.convertDataOxygen = $scope.conditionDataOxygen[0]

				// $scope.oxygenStyleEmoji = {
				//     'background-image': 'url('+$scope.convertDataOxygen.Image+')','width':'200px'
				// }
				$scope.convertResult();
			});

			conTemperatureService.getConTemperature({}, function (result) {
				$scope.conditionDataTemperature = result.message;
				// console.log($scope.conditionDataOxygen)
				// $scope.convertDataOxygen = $scope.conditionDataOxygen[0]

				// $scope.oxygenStyleEmoji = {
				//     'background-image': 'url('+$scope.convertDataOxygen.Image+')','width':'200px'
				// }

				$scope.convertResult();
			});

			conWeighingMachinesService.getConWeighingMachines({}, function (result) {
				$scope.conditionDataWeighingMachines = result.message;
				console.log($scope.conditionDataWeighingMachines);
				// console.log($scope.conditionDataWeighingMachines)
				// $scope.convertDataWeighingMachines = $scope.conditionDataWeighingMachines[0]

				// $scope.oxygenStyleEmoji = {
				//     'background-image': 'url('+$scope.convertDataWeighingMachines.Image+')','width':'200px'
				// }

				$scope.convertResult();
			});
		};

		$scope.convertResult = function () {
			// alert("Data")
			// $scope.data.systolic = 70
			// $scope.data.diastolic = 100
			console.log($scope.conditionDataBloodPressure);

			var indexofSBP;
			var indexofDBP;

			if ($scope.history != undefined) {
				$scope.conditionDataBloodPressure.forEach(function (item, index) {
					console.log(
						parseFloat(item.SBP_min),
						item.SBP_max,
						$scope.history.SBP,
						parseFloat(item.Importance)
					);

					if (
						parseFloat(item.SBP_min) <= parseFloat($scope.history.SBP) &&
						parseFloat(item.SBP_max) >= parseFloat($scope.history.SBP)
					) {
						$scope.convertDataBloodPressure = item;

						$scope.bloodPressureStyle = JSON.parse(
							$scope.convertDataBloodPressure.Results_style
						);
						// Object.assign($scope.bloodPressureStyle, JSON.parse($scope.convertDataBloodPressure.Recommendation_style));

						$scope.bloodPressureStyleEmoji = {
							"background-image":
								"url(" + $scope.convertDataBloodPressure.Image + ")",
						};
						indexofSBP = item;
						console.log(indexofSBP);
					}
					if (
						parseFloat(item.DBP_min) <= parseFloat($scope.history.DBP) &&
						parseFloat(item.DBP_max) >= parseFloat($scope.history.DBP)
					) {
						$scope.convertDataBloodPressure = item;

						$scope.bloodPressureStyle = JSON.parse(
							$scope.convertDataBloodPressure.Results_style
						);
						// Object.assign($scope.bloodPressureStyle, JSON.parse($scope.convertDataBloodPressure.Recommendation_style));

						$scope.bloodPressureStyleEmoji = {
							"background-image":
								"url(" + $scope.convertDataBloodPressure.Image + ")",
						};
						indexofDBP = item;
						console.log(indexofDBP);
					}
				});
			}

			if ($scope.history != undefined) {
				$scope.conditionDataPulseRate.forEach((item) => {
					// $scope.data.pulse = 50
					// console.log(parseFloat(item.Min), item.Max, $scope.data.pulse)
					if (
						parseFloat(item.Min) <= parseFloat($scope.history.PR) &&
						parseFloat(item.Max) >= parseFloat($scope.history.PR)
					) {
						$scope.convertDataPulseRate = item;

						$scope.pulseRateStyle = JSON.parse(
							$scope.convertDataPulseRate.Device_style
						);

						$scope.pulseRateStyleEmoji = {
							"background-image":
								"url(" + $scope.convertDataPulseRate.Image + ")",
						};
					}
				});
			}

			if ($scope.history != undefined) {
				$scope.conditionDataOxygen.forEach((item) => {
					console.log(parseFloat(item.Min), item.Max, $scope.history.OXYGEN);
					if (
						parseFloat(item.Min) <= parseFloat($scope.history.OXYGEN) &&
						parseFloat(item.Max) >= parseFloat($scope.history.OXYGEN)
					) {
						$scope.convertDataOxygen = item;

						$scope.oxygenStyle = JSON.parse(
							$scope.convertDataOxygen.Results_style
						);

						$scope.oxygenStyleEmoji = {
							"background-image": "url(" + $scope.convertDataOxygen.Image + ")",
						};
					}
				});
			}

			console.log($scope.conditionDataTemperature);
			if ($scope.history != undefined) {
				$scope.conditionDataTemperature.forEach((item) => {
					console.log(parseFloat(item.Min), item.Max, $scope.history.BTEMP);
					if (
						parseFloat(item.Min) <= parseFloat($scope.history.BTEMP) &&
						parseFloat(item.Max) >= parseFloat($scope.history.BTEMP)
					) {
						$scope.convertDataTemperature = item;

						$scope.temperatureStyle = JSON.parse(
							$scope.convertDataTemperature.Results_style
						);

						$scope.temperatureStyleEmoji = {
							"background-image":
								"url(" + $scope.convertDataTemperature.Image + ")",
						};
					}
				});
			}

			if ($scope.history != undefined && $scope.conditionDataWeighingMachines!=undefined) {
				$scope.conditionDataWeighingMachines.forEach((item) => {
					// alert("condition")
					console.log(parseFloat(item.Min), item.Max, $scope.history.BMI);
					if (
						parseFloat(item.Min) <= parseFloat($scope.history.BMI) &&
						parseFloat(item.Max) >= parseFloat($scope.history.BMI)
					) {
						$scope.convertDataWeighingMachines = item;

						$scope.weighingMachinesStyle = JSON.parse(
							$scope.convertDataWeighingMachines.Results_style
						);

						$scope.weighingMachinesStyleEmoji = {
							"background-image":
								"url(" + $scope.convertDataWeighingMachines.Image + ")",
						};

						// for (const style in $scope.weighingMachinesStyle.emojiposition) {
						//     Object.assign($scope.weighingMachinesStyleEmoji, {
						//         [style]: $scope.weighingMachinesStyle.emojiposition[style],
						//     });
						// }
						console.log($scope.weighingMachinesStyle);
					}
				});
			}
		};

		$scope.init();

		$scope.selectAudio = function () {
			var sample = document.getElementById("display");
			sample.pause();
		};

		document.onkeypress = function (e) {
			e = e || window.event;
			let key = e.key.toLowerCase();
			switch (page) {
				case "DisplayDevice":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(384, 975).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(951, 969).click();
					}
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(1545, 978).click();
					}
					break;

				default:
			}
		};

		var page = "DisplayDevice";
	}
})();
