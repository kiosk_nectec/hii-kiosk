(function () {
	"use strict";

	angular.module("BlurAdmin.pages.detail").controller("detailCtrl", detailCtrl);

	/** @ngInject */
	function detailCtrl(
		$scope,
		baseService,
		$rootScope,
		$uibModal,
		$filter,
		$timeout,
		$location,
		$state,
		detailService
	) {
		(function initController() {
			baseService.devicesActiveList();
			setTimeout(() => {
				if ($rootScope.audiomute == true) {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = true;
					}
				} else {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = false;
					}
				}
			}, 100);
		})();

		$scope.activePageTitle = $state.current.title;
		$scope.detail = $rootScope.globals.currentUser;
		$scope.title = "";
		$scope.titlegraph = "";

		$scope.urlimgday = "";
		$scope.urlimgmonth = "";
		$scope.urlimgyear = "";
		$scope.TypeGraph = "";
		$scope.tapId = "";
		$scope.remark = "";
		var D_Type = "";

		$scope.backward = false;
		$scope.forward = true;

		$scope.DataType = {};

		$scope.listDataGraph = [];

		$scope.options = {};

		$scope.listMonth = [
			{ m: "01", M: "ม.ค." },
			{ m: "02", M: "ก.พ." },
			{ m: "03", M: "มี.ค." },
			{ m: "04", M: "เม.ย." },
			{ m: "05", M: "พ.ค." },
			{ m: "06", M: "มิ.ย." },
			{ m: "07", M: "ก.ค." },
			{ m: "08", M: "ส.ค." },
			{ m: "09", M: "ก.ย." },
			{ m: "10", M: "ต.ค." },
			{ m: "11", M: "พ.ย." },
			{ m: "12", M: "ธ.ค." },
		];
		$scope.listDay = [
			{ d: "01", D: "1" },
			{ d: "02", D: "2" },
			{ d: "03", D: "3" },
			{ d: "04", D: "4" },
			{ d: "05", D: "5" },
			{ d: "06", D: "6" },
			{ d: "07", D: "7" },
			{ d: "08", D: "8" },
			{ d: "09", D: "9" },
			{ d: "10", D: "10" },
			{ d: "11", D: "11" },
			{ d: "12", D: "12" },
			{ d: "13", D: "13" },
			{ d: "14", D: "14" },
			{ d: "15", D: "15" },
			{ d: "16", D: "16" },
			{ d: "17", D: "17" },
			{ d: "18", D: "18" },
			{ d: "19", D: "19" },
			{ d: "20", D: "20" },
			{ d: "21", D: "21" },
			{ d: "22", D: "22" },
			{ d: "23", D: "23" },
			{ d: "24", D: "24" },
			{ d: "25", D: "25" },
			{ d: "26", D: "26" },
			{ d: "27", D: "27" },
			{ d: "28", D: "28" },
			{ d: "29", D: "29" },
			{ d: "30", D: "30" },
			{ d: "31", D: "31" },
		];

		$scope.liststandard = [];
		var now = new Date();
		$scope.progressFunction = function () {
			return $timeout(function () {}, 3000);
		};
		$scope.next = function () {
			baseService.nextDevices();
		};
		$scope.Home = function () {
			$location.path("/authen");
		};
		$scope.LoginBypassword = function () {
			$location.path("/authen_password");
		};
		$scope.qrBypassword = function () {
			$location.path("/authen_qr");
		};
		$scope.Back = function () {
			$scope.playAudio();
			$location.path("/results");
		};
		$scope.playAudio = function () {
			var audio = new Audio("./assets/kiosk/audio/click.mp3");
			audio.play();
		};
		$scope.CreateGarph = function (DataType, listDataGraph, liststandard) {
			var thmonth = new Array(
				"มกราคม",
				"กุมภาพันธ์",
				"มีนาคม",
				"เมษายน",
				"พฤษภาคม",
				"มิถุนายน",
				"กรกฎาคม",
				"สิงหาคม",
				"กันยายน",
				"ตุลาคม",
				"พฤศจิกายน",
				"ธันวาคม"
			);
			$scope.label = [];
			$scope.value1 = [];
			$scope.datasets = [];
			$scope.MaxMinvalue1 = [];
			$scope.MaxMinvalue2 = [];
			$scope.BSP = [];
			$scope.DSP = [];
			$scope.MediumSP = [];

			$scope.standardmin = 0;
			$scope.standardmax = 0;
			$scope.liststandardmin = [];
			$scope.liststandardmax = [];

			$scope.valuemin = [];
			$scope.valuemid = [];
			$scope.valuemax = [];

			$scope.max = 120;
			if (DataType == "DAY") {console.log(listDataGraph)
				$scope.remark = "";
				$scope.title =
					$scope.DataType.date.getDate() +
					" " +
					thmonth[$scope.DataType.date.getMonth()] +
					" " +
					(parseFloat($scope.DataType.date.getFullYear()) + 543);
				$scope.urlimgday = "assets/kiosk/detail/bt_grap_day1.png";
				$scope.urlimgmonth = "assets/kiosk/detail/bt_grap_mon2.png";
				$scope.urlimgyear = "assets/kiosk/detail/bt_grap_year2.png";
				// if (listDataGraph.length > 0) {
				if ($scope.tapId == "menu-1") {
					$scope.TypeGraph = "line";
					$scope.titlegraph = "ผลการชั่งน้ำหนัก";
					console.log(listDataGraph)
					if (listDataGraph.length > 0) {
						$scope.label.push(" ");
						$scope.value1.push(null);
						for (var i = 0; i < listDataGraph.length; i++) {
							$scope.label.push(listDataGraph[i]["TIME"]);
							if (
								listDataGraph[i]["WEIGHT"] == null ||
								listDataGraph[i]["WEIGHT"] == undefined
							) {
								$scope.value1.push(null);
							} else {
								$scope.value1.push(parseFloat(listDataGraph[i]["WEIGHT"]));
							}
						}
						$scope.label.push(" ");
						$scope.value1.push(null);
						//คำนวณ max min value
						for (var i = 0; i < listDataGraph.length; i++) {
							if (
								listDataGraph[i]["WEIGHT"] == null ||
								listDataGraph[i]["WEIGHT"] == undefined
							) {
							} else {
								$scope.MaxMinvalue1.push(
									parseFloat(listDataGraph[i]["WEIGHT"])
								);
							}
						}
						$scope.min =
							Math.round(
								(Math.min.apply(null, $scope.MaxMinvalue1) - 10) / 10
							) *
								10 +
							5;
						$scope.max =
							Math.round(
								(Math.max.apply(null, $scope.MaxMinvalue1) + 10) / 10
							) *
								10 -
							5;
						var tempdatasets = {
							backgroundColor: "#28bcbc",
							borderColor: "#28bcbc",
							data: $scope.value1,
							fill: false,
							pointStyle: "circle",
							pointBorderWidth: 20,
							pointBackgroundColor: "#28bcbc",
							pointBorderColor: "#28bcbc",
							pointHoverRadius: 5,
							showLine: true,
							datalabels: {
								color: "#FFFFFF",
								anchor: "end",
								align: "end",
								font: { size: 20 },
							},
						};
						$scope.datasets.push(tempdatasets);
					}
				} else if ($scope.tapId == "menu-2") {
					$scope.TypeGraph = "bar";
					$scope.titlegraph = "ค่าความดันโลหิต";
					if (listDataGraph.length > 0) {
						if (liststandard.length > 0) {
							$scope.standardmin = liststandard[0]["MIN"];
							$scope.standardmax = liststandard[0]["MAX"];
						} else {
							$scope.standardmin = 80;
							$scope.standardmax = 120;
						}
						$scope.label.push(" ");
						$scope.BSP.push([null, null]);
						$scope.MediumSP.push([null, null]);
						$scope.DSP.push([null, null]);
						$scope.liststandardmin.push($scope.standardmin);
						$scope.liststandardmax.push($scope.standardmax);
						for (var i = 0; i < listDataGraph.length; i++) {
							$scope.TempSBP = [];
							$scope.TempMediumSP = [];
							$scope.TempDBP = [];
							$scope.label.push(listDataGraph[i]["TIME"]);
							$scope.liststandardmin.push($scope.standardmin);
							$scope.liststandardmax.push($scope.standardmax);
							$scope.TempSBP.push(
								parseFloat(listDataGraph[i]["SBP"]),
								parseFloat(listDataGraph[i]["SBP"]) + 1
							);
							$scope.BSP.push($scope.TempSBP);
							$scope.TempMediumSP.push(
								parseFloat(listDataGraph[i]["SBP"]),
								parseFloat(listDataGraph[i]["DBP"]) + 1
							);
							$scope.MediumSP.push($scope.TempMediumSP);
							$scope.TempDBP.push(
								parseFloat(listDataGraph[i]["DBP"]),
								parseFloat(listDataGraph[i]["DBP"]) + 1
							);
							$scope.DSP.push($scope.TempDBP);
						}
						$scope.label.push(" ");
						$scope.BSP.push([null, null]);
						$scope.MediumSP.push([null, null]);
						$scope.DSP.push([null, null]);
						$scope.liststandardmin.push($scope.standardmin);
						$scope.liststandardmax.push($scope.standardmax);
						//คำนวณ max min value
						for (var i = 0; i < listDataGraph.length; i++) {
							if (
								listDataGraph[i]["DBP"] == null ||
								listDataGraph[i]["DBP"] == undefined
							) {
							} else {
								$scope.MaxMinvalue1.push(parseFloat(listDataGraph[i]["DBP"]));
							}
							if (
								listDataGraph[i]["SBP"] == null ||
								listDataGraph[i]["SBP"] == undefined
							) {
							} else {
								$scope.MaxMinvalue2.push(parseFloat(listDataGraph[i]["SBP"]));
							}
						}
						$scope.min =
							Math.round(
								(Math.min.apply(null, $scope.MaxMinvalue1) - 10) / 10
							) * 10;
						if ($scope.min - Math.min.apply(null, $scope.MaxMinvalue1)) {
							$scope.min = $scope.min - 10;
						}
						if ($scope.min == $scope.standardmin) {
							$scope.min = $scope.min - 10;
						}
						$scope.max =
							Math.round(
								(Math.max.apply(null, $scope.MaxMinvalue2) + 10) / 10
							) * 10;
						if ($scope.max == $scope.standardmax) {
							$scope.max = $scope.max + 10;
						}
						$scope.datasets = [
							{
								type: "line",
								data: $scope.liststandardmin,
								lineTension: 0,
								fill: false,
								borderColor: "#FFFFFF",
								pointStyle: "dash",
								datalabels: {
									display: false,
								},
								borderDash: [10, 5],
							},
							{
								type: "line",
								data: $scope.liststandardmax,
								lineTension: 0,
								fill: false,
								borderColor: "#FFFFFF",
								pointStyle: "dash",
								datalabels: {
									display: false,
								},
								borderDash: [10, 5],
							},
							{
								barPercentage: 0.2,
								type: "bar",
								data: $scope.BSP,
								backgroundColor: "#ff545d",
								datalabels: {
									color: "#FFFFFF",
									anchor: "end",
									align: "end",
									formatter: (val) => {
										return val[0];
									},
									font: { size: 20 },
								},
							},
							{
								barPercentage: 0.1,
								data: $scope.MediumSP,
								type: "bar",
								backgroundColor: "#fff2e5",
								datalabels: {
									display: false,
								},
							},
							{
								barPercentage: 0.2,
								type: "bar",
								data: $scope.DSP,
								backgroundColor: "#3fdbcb",
								datalabels: {
									color: "#FFFFFF",
									anchor: "start",
									align: "start",
									formatter: (val) => {
										return val[0];
									},
									font: { size: 20 },
								},
							},
						];
					}
				} else if ($scope.tapId == "menu-3") {
					$scope.TypeGraph = "line";
					$scope.titlegraph = "ค่าออกซิเจนในเลือด";
					if (listDataGraph.length > 0) {
						if (liststandard.length > 0) {
							$scope.standardmin = liststandard[0]["MIN"];
							$scope.standardmax = liststandard[0]["MAX"];
						} else {
							$scope.standardmin = 95;
							$scope.standardmax = 100;
						}

						$scope.label.push(" ");
						$scope.value1.push(null);
						$scope.liststandardmin.push($scope.standardmin);
						$scope.liststandardmax.push($scope.standardmax);
						for (var i = 0; i < listDataGraph.length; i++) {
							$scope.label.push(listDataGraph[i]["TIME"]);
							$scope.liststandardmin.push($scope.standardmin);
							$scope.liststandardmax.push($scope.standardmax);
							if (
								listDataGraph[i]["OXYGEN"] == null ||
								listDataGraph[i]["OXYGEN"] == undefined
							) {
								$scope.value1.push(null);
							} else {
								$scope.value1.push(parseFloat(listDataGraph[i]["OXYGEN"]));
							}
						}
						$scope.label.push(" ");
						$scope.value1.push(null);
						$scope.liststandardmin.push($scope.standardmin);
						$scope.liststandardmax.push($scope.standardmax);
						//คำนวณ max min value
						for (var i = 0; i < listDataGraph.length; i++) {
							if (
								listDataGraph[i]["OXYGEN"] == null ||
								listDataGraph[i]["OXYGEN"] == undefined
							) {
							} else {
								$scope.MaxMinvalue1.push(
									parseFloat(listDataGraph[i]["OXYGEN"])
								);
							}
						}
						$scope.min = 85;
						$scope.max = 100;
						var tempdatasets = {
							backgroundColor: "#28bcbc",
							borderColor: "#28bcbc",
							data: $scope.value1,
							fill: false,
							pointStyle: "circle",
							pointBorderWidth: 20,
							pointBackgroundColor: "#28bcbc",
							pointBorderColor: "#28bcbc",
							pointHoverRadius: 5,
							showLine: true,
							datalabels: {
								color: "#FFFFFF",
								anchor: "end",
								align: "end",
								font: { size: 20 },
							},
						};
						var tempdatastandardmin = {
							type: "line",
							data: $scope.liststandardmin,
							lineTension: 0,
							fill: false,
							borderColor: "#FFFFFF",
							pointStyle: "dash",
							datalabels: {
								display: false,
							},
							borderDash: [10, 5],
						};
						var tempdatastandardmax = {
							type: "line",
							data: $scope.liststandardmax,
							lineTension: 0,
							fill: false,
							borderColor: "#FFFFFF",
							pointStyle: "dash",
							datalabels: {
								display: false,
							},
							borderDash: [10, 5],
						};
						$scope.datasets.push(tempdatasets);
						$scope.datasets.push(tempdatastandardmin);
						$scope.datasets.push(tempdatastandardmax);
					}
				} else if ($scope.tapId == "menu-4") {
					$scope.TypeGraph = "line";
					$scope.titlegraph = "ค่าอุณหภูมิร่างกาย";
					if (listDataGraph.length > 0) {
						$scope.color = [];
						if (liststandard.length > 0) {
							$scope.standardmin = liststandard[0]["MIN"];
							$scope.standardmax = liststandard[0]["MAX"];
						} else {
							$scope.standardmin = 36.5;
							$scope.standardmax = 37.5;
						}
						$scope.label.push(" ");
						$scope.value1.push(null);
						$scope.color.push(null);
						$scope.liststandardmin.push($scope.standardmin);
						$scope.liststandardmax.push($scope.standardmax);

						for (var i = 0; i < listDataGraph.length; i++) {
							$scope.label.push(listDataGraph[i]["TIME"]);
							$scope.liststandardmin.push($scope.standardmin);
							$scope.liststandardmax.push($scope.standardmax);
							if (
								listDataGraph[i]["BTEMP"] == null ||
								listDataGraph[i]["BTEMP"] == undefined
							) {
								$scope.color.push(null);
								$scope.value1.push(null);
							} else {
								if (
									parseFloat(listDataGraph[i]["BTEMP"]) > $scope.standardmax
								) {
									$scope.color.push("#f04e6b");
								} else {
									$scope.color.push("#28bcbc");
								}

								$scope.value1.push(parseFloat(listDataGraph[i]["BTEMP"]));
							}
						}
						$scope.label.push(" ");
						$scope.value1.push(null);
						$scope.color.push(null);
						console.log($scope.color);
						$scope.liststandardmin.push($scope.standardmin);
						$scope.liststandardmax.push($scope.standardmax);
						$scope.min = 35;
						$scope.max = 40;
						var tempdatasets = {
							backgroundColor: "#28bcbc",
							borderColor: "#28bcbc",
							data: $scope.value1,
							fill: false,
							pointStyle: "circle",
							pointBorderWidth: 20,
							pointBackgroundColor: $scope.color,
							pointBorderColor: $scope.color,
							pointHoverRadius: 5,
							showLine: true,
							datalabels: {
								color: "#FFFFFF",
								anchor: "end",
								align: "end",
								font: { size: 20 },
							},
						};
						var tempdatastandardmin = {
							type: "line",
							data: $scope.liststandardmin,
							lineTension: 0,
							fill: false,
							borderColor: "#FFFFFF",
							pointStyle: "dash",
							datalabels: {
								display: false,
							},
							borderDash: [10, 5],
						};
						var tempdatastandardmax = {
							type: "line",
							data: $scope.liststandardmax,
							lineTension: 0,
							fill: false,
							borderColor: "#FFFFFF",
							pointStyle: "dash",
							datalabels: {
								display: false,
							},
							borderDash: [10, 5],
						};
						$scope.datasets.push(tempdatasets);
						$scope.datasets.push(tempdatastandardmin);
						$scope.datasets.push(tempdatastandardmax);
					}
				}
				// }
				$scope.XfontSize = 30;
			} else if (DataType == "MONTH") {
				$scope.remark =
					"* ข้อมูลที่แสดง คือ ค่ามากที่สุดและค่าน้อยที่สุด ของช่วงเวลานั้นๆ";
				$scope.title =
					thmonth[$scope.DataType.date.getMonth()] +
					" " +
					(parseFloat($scope.DataType.date.getFullYear()) + 543);
				$scope.urlimgday = "assets/kiosk/detail/bt_grap_day2.png";
				$scope.urlimgmonth = "assets/kiosk/detail/bt_grap_mon1.png";
				$scope.urlimgyear = "assets/kiosk/detail/bt_grap_year2.png";
				// if (listDataGraph.length > 0) {
				if ($scope.tapId == "menu-1") {
					$scope.TypeGraph = "bar";
					$scope.titlegraph = "ผลการชั่งน้ำหนัก";
					if (listDataGraph.length > 0) {
						$scope.label.push(" ");
						$scope.M = "";
						$scope.listMonth.forEach(function (entry, index) {
							if (listDataGraph[0]["MONTH"] === entry.m) {
								$scope.M = entry.M;
							}
						});
						$scope.lastDayOfMonth = new Date(
							listDataGraph[0]["YEAR"],
							listDataGraph[0]["MONTH"],
							0
						).getDate();
						for (var a = 0; a < $scope.lastDayOfMonth; a++) {
							$scope.label.push($scope.listDay[a]["D"] + " " + $scope.M);
						}

						$scope.Tempvalue1 = [];
						$scope.listDay.forEach(function (entry, index) {
							if (index < $scope.lastDayOfMonth) {
								$scope.Tempvalueofday = [];
								for (var i = 0; i < listDataGraph.length; i++) {
									if (listDataGraph[i]["DAY"] === entry.d) {
										$scope.Tempvalueofday.push(listDataGraph[i]);
									}
								}
								if ($scope.Tempvalueofday.length > 0) {
									$scope.Tempvalue1.push($scope.Tempvalueofday);
								}
							}
						});
						var tempdata = [];
						$scope.Tempvalue1.forEach(function (entry, index) {
							$scope.Tempvalue = [];
							var min = Math.min.apply(
								Math,
								entry.map(function (o) {
									return o.WEIGHT;
								})
							);
							var max = Math.max.apply(
								Math,
								entry.map(function (o) {
									return o.WEIGHT;
								})
							);
							var tempminvalue = {};
							var tempmaxvalue = {};
							entry.forEach(function (ety, index) {
								if (ety["WEIGHT"] == min) {
									if (undefined == tempminvalue.type) {
										tempminvalue = ety;
										tempminvalue.type = "min";
										tempdata.push(tempminvalue);
									}
								}
								if (ety["WEIGHT"] == max) {
									if (undefined == tempmaxvalue.type) {
										tempmaxvalue = ety;
										tempmaxvalue.type = "max";
										tempdata.push(tempmaxvalue);
									}
								}
							});
						});
						$scope.valuemin.push([null, null]);
						$scope.valuemid.push([null, null]);
						$scope.valuemax.push([null, null]);
						$scope.listDay.forEach(function (ety, index) {
							if (index < $scope.lastDayOfMonth) {
								var minvalue = [];
								var maxvalue = [];
								for (var a = 0; a < tempdata.length; a++) {
									if (ety.d == tempdata[a]["DAY"]) {
										if (tempdata[a]["type"] == "min") {
											minvalue.push(
												parseFloat(tempdata[a]["WEIGHT"]),
												parseFloat(tempdata[a]["WEIGHT"]) + 0.2
											);
										}
										if (tempdata[a]["type"] == "max") {
											maxvalue.push(
												parseFloat(tempdata[a]["WEIGHT"]),
												parseFloat(tempdata[a]["WEIGHT"]) + 0.2
											);
										}
									}
								}
								if (minvalue.length > 0) {
									$scope.valuemin.push(minvalue);
								} else {
									$scope.valuemin.push([null, null]);
								}
								if (maxvalue.length > 0) {
									$scope.valuemax.push(maxvalue);
								} else {
									$scope.valuemax.push([null, null]);
								}
								$scope.valuemid.push([
									$scope.valuemin[index + 1][0],
									$scope.valuemax[index + 1][0],
								]);
							}
						});
						$scope.label.push(" ");
						$scope.valuemin.push([null, null]);
						$scope.valuemid.push([null, null]);
						$scope.valuemax.push([null, null]);
						//คำนวณ max min value
						for (var i = 0; i < listDataGraph.length; i++) {
							if (
								listDataGraph[i]["WEIGHT"] == null ||
								listDataGraph[i]["WEIGHT"] == undefined
							) {
							} else {
								$scope.MaxMinvalue1.push(
									parseFloat(listDataGraph[i]["WEIGHT"])
								);
							}
						}
						$scope.min =
							Math.round(
								(Math.min.apply(null, $scope.MaxMinvalue1) - 10) / 10
							) *
								10 +
							5;
						$scope.max =
							Math.round(
								(Math.max.apply(null, $scope.MaxMinvalue1) + 10) / 10
							) *
								10 -
							5;
						$scope.datasets = [
							{
								barPercentage: 0.3,
								data: $scope.valuemin,
								backgroundColor: "#3fdbcb",
								datalabels: {
									color: "#FFFFFF",
									anchor: "start",
									align: "start",
									formatter: (val) => {
										return val[0];
									},
									font: { size: 20 },
								},
							},
							{
								barPercentage: 0.2,
								data: $scope.valuemid,
								backgroundColor: "#fff2e5",
								datalabels: {
									display: false,
								},
							},
							{
								barPercentage: 0.3,
								data: $scope.valuemax,
								backgroundColor: "#ff545d",
								datalabels: {
									color: "#FFFFFF",
									anchor: "end",
									align: "end",
									formatter: (val) => {
										return val[0];
									},
									font: { size: 20 },
								},
							},
						];
					}
				} else if ($scope.tapId == "menu-2") {
					$scope.TypeGraph = "bar";
					$scope.titlegraph = "ค่าความดันโลหิต";
					if (listDataGraph.length > 0) {
						if (liststandard.length > 0) {
							$scope.standardmin = liststandard[0]["MIN"];
							$scope.standardmax = liststandard[0]["MAX"];
						} else {
							$scope.standardmin = 80;
							$scope.standardmax = 120;
						}
						$scope.label.push(" ");
						$scope.liststandardmin.push($scope.standardmin);
						$scope.liststandardmax.push($scope.standardmax);
						$scope.M = "";
						$scope.listMonth.forEach(function (entry, index) {
							if (listDataGraph[0]["MONTH"] === entry.m) {
								$scope.M = entry.M;
							}
						});
						$scope.lastDayOfMonth = new Date(
							listDataGraph[0]["YEAR"],
							listDataGraph[0]["MONTH"],
							0
						).getDate();
						for (var a = 0; a < $scope.lastDayOfMonth; a++) {
							$scope.label.push($scope.listDay[a]["D"] + " " + $scope.M);
						}
						var templistDataGraph = [];
						var tempDataGraph = {};
						for (var i = 0; i < listDataGraph.length; i++) {
							tempDataGraph = {
								DAY: listDataGraph[i]["DAY"],
								BP: listDataGraph[i]["SBP"],
							};
							templistDataGraph.push(tempDataGraph);
							tempDataGraph = {
								DAY: listDataGraph[i]["DAY"],
								BP: listDataGraph[i]["DBP"],
							};
							tempDataGraph.BP = listDataGraph[i]["DBP"];
							templistDataGraph.push(tempDataGraph);
						}

						$scope.Tempvalue1 = [];
						$scope.listDay.forEach(function (entry, index) {
							if (index < $scope.lastDayOfMonth) {
								$scope.Tempvalueofday = [];
								for (var i = 0; i < templistDataGraph.length; i++) {
									if (templistDataGraph[i]["DAY"] === entry.d) {
										$scope.Tempvalueofday.push(templistDataGraph[i]);
									}
								}
								if ($scope.Tempvalueofday.length > 0) {
									$scope.Tempvalue1.push($scope.Tempvalueofday);
								}
							}
						});
						var tempdata = [];
						$scope.Tempvalue1.forEach(function (entry, index) {
							$scope.Tempvalue = [];
							var min = Math.min.apply(
								Math,
								entry.map(function (o) {
									return o.BP;
								})
							);
							var max = Math.max.apply(
								Math,
								entry.map(function (o) {
									return o.BP;
								})
							);
							var tempminvalue = {};
							var tempmaxvalue = {};
							entry.forEach(function (ety, index) {
								if (ety["BP"] == min) {
									if (undefined == tempminvalue.type) {
										tempminvalue = ety;
										tempminvalue.type = "min";
										tempdata.push(tempminvalue);
									}
								}
								if (ety["BP"] == max) {
									if (undefined == tempmaxvalue.type) {
										tempmaxvalue = ety;
										tempmaxvalue.type = "max";
										tempdata.push(tempmaxvalue);
									}
								}
							});
						});
						$scope.valuemin.push([null, null]);
						$scope.valuemid.push([null, null]);
						$scope.valuemax.push([null, null]);
						$scope.listDay.forEach(function (ety, index) {
							if (index < $scope.lastDayOfMonth) {
								var minvalue = [];
								var maxvalue = [];
								for (var a = 0; a < tempdata.length; a++) {
									if (ety.d == tempdata[a]["DAY"]) {
										if (tempdata[a]["type"] == "min") {
											minvalue.push(
												parseFloat(tempdata[a]["BP"]),
												parseFloat(tempdata[a]["BP"]) + 2
											);
										}
										if (tempdata[a]["type"] == "max") {
											maxvalue.push(
												parseFloat(tempdata[a]["BP"]),
												parseFloat(tempdata[a]["BP"]) + 2
											);
										}
									}
								}
								if (minvalue.length > 0) {
									$scope.valuemin.push(minvalue);
								} else {
									$scope.valuemin.push([null, null]);
								}
								if (maxvalue.length > 0) {
									$scope.valuemax.push(maxvalue);
								} else {
									$scope.valuemax.push([null, null]);
								}
								$scope.valuemid.push([
									$scope.valuemin[index + 1][0],
									$scope.valuemax[index + 1][0],
								]);
								$scope.liststandardmin.push($scope.standardmin);
								$scope.liststandardmax.push($scope.standardmax);
							}
						});
						$scope.label.push(" ");
						$scope.valuemin.push([null, null]);
						$scope.valuemid.push([null, null]);
						$scope.valuemax.push([null, null]);
						$scope.liststandardmin.push($scope.standardmin);
						$scope.liststandardmax.push($scope.standardmax);
						//คำนวณ max min value
						for (var i = 0; i < templistDataGraph.length; i++) {
							if (
								templistDataGraph[i]["BP"] == null ||
								templistDataGraph[i]["BP"] == undefined
							) {
							} else {
								$scope.MaxMinvalue1.push(
									parseFloat(templistDataGraph[i]["BP"])
								);
							}
						}
						$scope.min =
							Math.round(
								(Math.min.apply(null, $scope.MaxMinvalue1) - 10) / 10
							) * 10;
						if ($scope.min == $scope.standardmin) {
							$scope.min = $scope.min - 10;
						}
						$scope.max =
							Math.round(
								(Math.max.apply(null, $scope.MaxMinvalue1) + 10) / 10
							) * 10;
						$scope.datasets = [
							{
								type: "line",
								data: $scope.liststandardmin,
								lineTension: 0,
								fill: false,
								borderColor: "#FFFFFF",
								pointStyle: "dash",
								datalabels: {
									display: false,
								},
								borderDash: [10, 5],
							},
							{
								type: "line",
								data: $scope.liststandardmax,
								lineTension: 0,
								fill: false,
								borderColor: "#FFFFFF",
								pointStyle: "dash",
								datalabels: {
									display: false,
								},
								borderDash: [10, 5],
							},
							{
								barPercentage: 0.3,
								data: $scope.valuemin,
								backgroundColor: "#3fdbcb",
								datalabels: {
									color: "#FFFFFF",
									anchor: "start",
									align: "start",
									formatter: (val) => {
										return val[0];
									},
									font: { size: 20 },
								},
							},
							{
								barPercentage: 0.2,
								data: $scope.valuemid,
								backgroundColor: "#fff2e5",
								datalabels: {
									display: false,
								},
							},
							{
								barPercentage: 0.3,
								data: $scope.valuemax,
								backgroundColor: "#ff545d",
								datalabels: {
									color: "#FFFFFF",
									anchor: "end",
									align: "end",
									formatter: (val) => {
										return val[0];
									},
									font: { size: 20 },
								},
							},
						];
					}
				} else if ($scope.tapId == "menu-3") {
					$scope.TypeGraph = "bar";
					$scope.titlegraph = "ค่าออกซิเจนในเลือด";
					if (listDataGraph.length > 0) {
						if (liststandard.length > 0) {
							$scope.standardmin = liststandard[0]["MIN"];
							$scope.standardmax = liststandard[0]["MAX"];
						} else {
							$scope.standardmin = 95;
							$scope.standardmax = 100;
						}
						$scope.label.push(" ");
						$scope.liststandardmin.push($scope.standardmin);
						$scope.liststandardmax.push($scope.standardmax);
						$scope.M = "";
						$scope.listMonth.forEach(function (entry, index) {
							if (listDataGraph[0]["MONTH"] === entry.m) {
								$scope.M = entry.M;
							}
						});
						$scope.lastDayOfMonth = new Date(
							listDataGraph[0]["YEAR"],
							listDataGraph[0]["MONTH"],
							0
						).getDate();
						for (var a = 0; a < $scope.lastDayOfMonth; a++) {
							$scope.label.push($scope.listDay[a]["D"] + " " + $scope.M);
						}

						$scope.Tempvalue1 = [];
						$scope.listDay.forEach(function (entry, index) {
							if (index < $scope.lastDayOfMonth) {
								$scope.Tempvalueofday = [];
								for (var i = 0; i < listDataGraph.length; i++) {
									if (listDataGraph[i]["DAY"] === entry.d) {
										$scope.Tempvalueofday.push(listDataGraph[i]);
									}
								}
								if ($scope.Tempvalueofday.length > 0) {
									$scope.Tempvalue1.push($scope.Tempvalueofday);
								}
							}
						});
						var tempdata = [];
						$scope.Tempvalue1.forEach(function (entry, index) {
							$scope.Tempvalue = [];
							var min = Math.min.apply(
								Math,
								entry.map(function (o) {
									return o.OXYGEN;
								})
							);
							var max = Math.max.apply(
								Math,
								entry.map(function (o) {
									return o.OXYGEN;
								})
							);
							var tempminvalue = {};
							var tempmaxvalue = {};
							entry.forEach(function (ety, index) {
								if (ety["OXYGEN"] == min) {
									if (undefined == tempminvalue.type) {
										tempminvalue = ety;
										tempminvalue.type = "min";
										tempdata.push(tempminvalue);
									}
								}
								if (ety["OXYGEN"] == max) {
									if (undefined == tempmaxvalue.type) {
										tempmaxvalue = ety;
										tempmaxvalue.type = "max";
										tempdata.push(tempmaxvalue);
									}
								}
							});
						});
						$scope.valuemin.push([null, null]);
						$scope.valuemid.push([null, null]);
						$scope.valuemax.push([null, null]);
						$scope.listDay.forEach(function (ety, index) {
							if (index < $scope.lastDayOfMonth) {
								var minvalue = [];
								var maxvalue = [];
								for (var a = 0; a < tempdata.length; a++) {
									if (ety.d == tempdata[a]["DAY"]) {
										if (tempdata[a]["type"] == "min") {
											minvalue.push(
												parseFloat(tempdata[a]["OXYGEN"]),
												parseFloat(tempdata[a]["OXYGEN"]) + 0.2
											);
										}
										if (tempdata[a]["type"] == "max") {
											maxvalue.push(
												parseFloat(tempdata[a]["OXYGEN"]),
												parseFloat(tempdata[a]["OXYGEN"]) + 0.2
											);
										}
									}
								}
								if (minvalue.length > 0) {
									$scope.valuemin.push(minvalue);
								} else {
									$scope.valuemin.push([null, null]);
								}
								if (maxvalue.length > 0) {
									$scope.valuemax.push(maxvalue);
								} else {
									$scope.valuemax.push([null, null]);
								}
								$scope.valuemid.push([
									$scope.valuemin[index + 1][0],
									$scope.valuemax[index + 1][0],
								]);
								$scope.liststandardmin.push($scope.standardmin);
								$scope.liststandardmax.push($scope.standardmax);
							}
						});
						$scope.label.push(" ");
						$scope.valuemin.push([null, null]);
						$scope.valuemid.push([null, null]);
						$scope.valuemax.push([null, null]);
						$scope.liststandardmin.push($scope.standardmin);
						$scope.liststandardmax.push($scope.standardmax);
						//คำนวณ max min value
						for (var i = 0; i < listDataGraph.length; i++) {
							if (
								listDataGraph[i]["OXYGEN"] == null ||
								listDataGraph[i]["OXYGEN"] == undefined
							) {
							} else {
								$scope.MaxMinvalue1.push(
									parseFloat(listDataGraph[i]["OXYGEN"])
								);
							}
						}
						$scope.min = 85;
						$scope.max = 100;
						$scope.datasets = [
							{
								type: "line",
								data: $scope.liststandardmin,
								lineTension: 0,
								fill: false,
								borderColor: "#FFFFFF",
								pointStyle: "dash",
								datalabels: {
									display: false,
								},
								borderDash: [10, 5],
							},
							{
								type: "line",
								data: $scope.liststandardmax,
								lineTension: 0,
								fill: false,
								borderColor: "#FFFFFF",
								pointStyle: "dash",
								datalabels: {
									display: false,
								},
								borderDash: [10, 5],
							},
							{
								barPercentage: 0.3,
								data: $scope.valuemin,
								backgroundColor: "#3fdbcb",
								datalabels: {
									color: "#FFFFFF",
									anchor: "start",
									align: "start",
									formatter: (val) => {
										return val[0];
									},
									font: { size: 20 },
								},
							},
							{
								barPercentage: 0.2,
								data: $scope.valuemid,
								backgroundColor: "#fff2e5",
								datalabels: {
									display: false,
								},
							},
							{
								barPercentage: 0.3,
								data: $scope.valuemax,
								backgroundColor: "#ff545d",
								datalabels: {
									color: "#FFFFFF",
									anchor: "end",
									align: "end",
									formatter: (val) => {
										return val[0];
									},
									font: { size: 20 },
								},
							},
						];
					}
				} else if ($scope.tapId == "menu-4") {
					$scope.TypeGraph = "bar";
					$scope.titlegraph = "ค่าอุณหภูมิร่างกาย";
					if (listDataGraph.length > 0) {
						if (liststandard.length > 0) {
							$scope.standardmin = liststandard[0]["MIN"];
							$scope.standardmax = liststandard[0]["MAX"];
						} else {
							$scope.standardmin = 36.5;
							$scope.standardmax = 37.5;
						}
						$scope.label.push(" ");
						$scope.liststandardmin.push($scope.standardmin);
						$scope.liststandardmax.push($scope.standardmax);
						$scope.M = "";
						$scope.listMonth.forEach(function (entry, index) {
							if (listDataGraph[0]["MONTH"] === entry.m) {
								$scope.M = entry.M;
							}
						});
						$scope.lastDayOfMonth = new Date(
							listDataGraph[0]["YEAR"],
							listDataGraph[0]["MONTH"],
							0
						).getDate();
						for (var a = 0; a < $scope.lastDayOfMonth; a++) {
							$scope.label.push($scope.listDay[a]["D"] + " " + $scope.M);
						}

						$scope.Tempvalue1 = [];
						$scope.listDay.forEach(function (entry, index) {
							if (index < $scope.lastDayOfMonth) {
								$scope.Tempvalueofday = [];
								for (var i = 0; i < listDataGraph.length; i++) {
									if (listDataGraph[i]["DAY"] === entry.d) {
										$scope.Tempvalueofday.push(listDataGraph[i]);
									}
								}
								if ($scope.Tempvalueofday.length > 0) {
									$scope.Tempvalue1.push($scope.Tempvalueofday);
								}
							}
						});
						var tempdata = [];
						$scope.Tempvalue1.forEach(function (entry, index) {
							$scope.Tempvalue = [];
							var min = Math.min.apply(
								Math,
								entry.map(function (o) {
									return o.BTEMP;
								})
							);
							var max = Math.max.apply(
								Math,
								entry.map(function (o) {
									return o.BTEMP;
								})
							);
							var tempminvalue = {};
							var tempmaxvalue = {};
							entry.forEach(function (ety, index) {
								if (ety["BTEMP"] == min) {
									if (undefined == tempminvalue.type) {
										tempminvalue = ety;
										tempminvalue.type = "min";
										tempdata.push(tempminvalue);
									}
								}
								if (ety["BTEMP"] == max) {
									if (undefined == tempmaxvalue.type) {
										tempmaxvalue = ety;
										tempmaxvalue.type = "max";
										tempdata.push(tempmaxvalue);
									}
								}
							});
						});
						$scope.valuemin.push([null, null]);
						$scope.valuemid.push([null, null]);
						$scope.valuemax.push([null, null]);
						$scope.listDay.forEach(function (ety, index) {
							if (index < $scope.lastDayOfMonth) {
								var minvalue = [];
								var maxvalue = [];

								for (var a = 0; a < tempdata.length; a++) {
									if (ety.d == tempdata[a]["DAY"]) {
										if (tempdata[a]["type"] == "min") {
											minvalue.push(
												parseFloat(tempdata[a]["BTEMP"]),
												parseFloat(tempdata[a]["BTEMP"]) + 0.1
											);
										}
										if (tempdata[a]["type"] == "max") {
											maxvalue.push(
												parseFloat(tempdata[a]["BTEMP"]),
												parseFloat(tempdata[a]["BTEMP"]) + 0.1
											);
										}
									}
								}
								if (minvalue.length > 0) {
									$scope.valuemin.push(minvalue);
								} else {
									$scope.valuemin.push([null, null]);
								}
								if (maxvalue.length > 0) {
									$scope.valuemax.push(maxvalue);
								} else {
									$scope.valuemax.push([null, null]);
								}
								$scope.valuemid.push([
									$scope.valuemin[index + 1][0],
									$scope.valuemax[index + 1][0],
								]);
								$scope.liststandardmin.push($scope.standardmin);
								$scope.liststandardmax.push($scope.standardmax);
							}
						});
						$scope.label.push(" ");
						$scope.valuemin.push([null, null]);
						$scope.valuemid.push([null, null]);
						$scope.valuemax.push([null, null]);
						$scope.liststandardmin.push($scope.standardmin);
						$scope.liststandardmax.push($scope.standardmax);
						$scope.min = 35;
						$scope.max = 40;
						$scope.datasets = [
							{
								type: "line",
								data: $scope.liststandardmin,
								lineTension: 0,
								fill: false,
								borderColor: "#FFFFFF",
								pointStyle: "dash",
								datalabels: {
									display: false,
								},
								borderDash: [10, 5],
							},
							{
								type: "line",
								data: $scope.liststandardmax,
								lineTension: 0,
								fill: false,
								borderColor: "#FFFFFF",
								pointStyle: "dash",
								datalabels: {
									display: false,
								},
								borderDash: [10, 5],
							},
							{
								barPercentage: 0.3,
								data: $scope.valuemin,
								backgroundColor: "#3fdbcb",
								datalabels: {
									color: "#FFFFFF",
									anchor: "start",
									align: "start",
									formatter: (val) => {
										return val[0];
									},
									font: { size: 20 },
								},
							},
							{
								barPercentage: 0.2,
								data: $scope.valuemid,
								backgroundColor: "#fff2e5",
								datalabels: {
									display: false,
								},
							},
							{
								barPercentage: 0.3,
								data: $scope.valuemax,
								backgroundColor: "#ff545d",
								datalabels: {
									color: "#FFFFFF",
									anchor: "end",
									align: "end",
									formatter: (val) => {
										return val[0];
									},
									font: { size: 20 },
								},
							},
						];
					}
				}
				// }
				$scope.XfontSize = 21;
			} else if (DataType == "YEAR") {
				$scope.remark =
					"* ข้อมูลที่แสดง คือ ค่ามากที่สุดและค่าน้อยที่สุด ของช่วงเวลานั้นๆ";
				$scope.title =
					"พ.ศ. " + (parseFloat($scope.DataType.date.getFullYear()) + 543);
				$scope.urlimgday = "assets/kiosk/detail/bt_grap_day2.png";
				$scope.urlimgmonth = "assets/kiosk/detail/bt_grap_mon2.png";
				$scope.urlimgyear = "assets/kiosk/detail/bt_grap_year1.png";
				// if (listDataGraph.length > 0) {
				$scope.label.push(" ");
				$scope.listMonth.forEach(function (entry, index) {
					$scope.label.push(entry.M);
				});
				if ($scope.tapId == "menu-1") {
					$scope.TypeGraph = "bar";
					$scope.titlegraph = "ผลการชั่งน้ำหนัก";
					if (listDataGraph.length > 0) {
						$scope.Tempvalue1 = [];
						$scope.listMonth.forEach(function (entry, index) {
							$scope.TempvalueofMonth = [];
							for (var i = 0; i < listDataGraph.length; i++) {
								if (listDataGraph[i]["MONTH"] === entry.m) {
									$scope.TempvalueofMonth.push(listDataGraph[i]);
								}
							}
							if ($scope.TempvalueofMonth.length > 0) {
								$scope.Tempvalue1.push($scope.TempvalueofMonth);
							}
						});
						var tempdata = [];
						$scope.Tempvalue1.forEach(function (entry, index) {
							$scope.Tempvalue = [];
							var min = Math.min.apply(
								Math,
								entry.map(function (o) {
									return o.WEIGHT;
								})
							);
							var max = Math.max.apply(
								Math,
								entry.map(function (o) {
									return o.WEIGHT;
								})
							);
							var tempminvalue = {};
							var tempmaxvalue = {};
							entry.forEach(function (ety, index) {
								if (ety["WEIGHT"] == min) {
									if (undefined == tempminvalue.type) {
										tempminvalue = ety;
										tempminvalue.type = "min";
										tempdata.push(tempminvalue);
									}
								}
								if (ety["WEIGHT"] == max) {
									if (undefined == tempmaxvalue.type) {
										tempmaxvalue = ety;
										tempmaxvalue.type = "max";
										tempdata.push(tempmaxvalue);
									}
								}
							});
						});
						$scope.valuemin.push([null, null]);
						$scope.valuemid.push([null, null]);
						$scope.valuemax.push([null, null]);
						$scope.listMonth.forEach(function (ety, index) {
							var minvalue = [];
							var maxvalue = [];
							for (var a = 0; a < tempdata.length; a++) {
								if (ety.m == tempdata[a]["MONTH"]) {
									if (tempdata[a]["type"] == "min") {
										minvalue.push(
											parseFloat(tempdata[a]["WEIGHT"]),
											parseFloat(tempdata[a]["WEIGHT"]) + 0.2
										);
									}
									if (tempdata[a]["type"] == "max") {
										maxvalue.push(
											parseFloat(tempdata[a]["WEIGHT"]),
											parseFloat(tempdata[a]["WEIGHT"]) + 0.2
										);
									}
								}
							}
							if (minvalue.length > 0) {
								$scope.valuemin.push(minvalue);
							} else {
								$scope.valuemin.push([null, null]);
							}
							if (maxvalue.length > 0) {
								$scope.valuemax.push(maxvalue);
							} else {
								$scope.valuemax.push([null, null]);
							}
							$scope.valuemid.push([
								$scope.valuemin[index + 1][0],
								$scope.valuemax[index + 1][0],
							]);
						});
						$scope.label.push(" ");
						$scope.valuemin.push([null, null]);
						$scope.valuemid.push([null, null]);
						$scope.valuemax.push([null, null]);
						//คำนวณ max min value
						for (var i = 0; i < listDataGraph.length; i++) {
							if (
								listDataGraph[i]["WEIGHT"] == null ||
								listDataGraph[i]["WEIGHT"] == undefined
							) {
							} else {
								$scope.MaxMinvalue1.push(
									parseFloat(listDataGraph[i]["WEIGHT"])
								);
							}
						}
						$scope.min =
							Math.round(
								(Math.min.apply(null, $scope.MaxMinvalue1) - 10) / 10
							) *
								10 +
							5;
						$scope.max =
							Math.round(
								(Math.max.apply(null, $scope.MaxMinvalue1) + 10) / 10
							) *
								10 -
							5;
						$scope.datasets = [
							{
								barPercentage: 0.3,
								data: $scope.valuemin,
								backgroundColor: "#3fdbcb",
								datalabels: {
									color: "#FFFFFF",
									anchor: "start",
									align: "start",
									formatter: (val) => {
										return val[0];
									},
									font: { size: 20 },
								},
							},
							{
								barPercentage: 0.2,
								data: $scope.valuemid,
								backgroundColor: "#fff2e5",
								datalabels: {
									display: false,
								},
							},
							{
								barPercentage: 0.3,
								data: $scope.valuemax,
								backgroundColor: "#ff545d",
								datalabels: {
									color: "#FFFFFF",
									anchor: "end",
									align: "end",
									formatter: (val) => {
										return val[0];
									},
									font: { size: 20 },
								},
							},
						];
					}
				} else if ($scope.tapId == "menu-2") {
					$scope.TypeGraph = "bar";
					$scope.titlegraph = "ค่าความดันโลหิต";
					if (listDataGraph.length > 0) {
						if (liststandard.length > 0) {
							$scope.standardmin = liststandard[0]["MIN"];
							$scope.standardmax = liststandard[0]["MAX"];
						} else {
							$scope.standardmin = 80;
							$scope.standardmax = 120;
						}
						$scope.liststandardmin.push($scope.standardmin);
						$scope.liststandardmax.push($scope.standardmax);
						var templistDataGraph = [];
						var tempDataGraph = {};
						for (var i = 0; i < listDataGraph.length; i++) {
							tempDataGraph = {
								MONTH: listDataGraph[i]["MONTH"],
								BP: listDataGraph[i]["SBP"],
							};
							templistDataGraph.push(tempDataGraph);
							tempDataGraph = {
								MONTH: listDataGraph[i]["MONTH"],
								BP: listDataGraph[i]["DBP"],
							};
							tempDataGraph.BP = listDataGraph[i]["DBP"];
							templistDataGraph.push(tempDataGraph);
						}

						$scope.Tempvalue1 = [];
						$scope.listMonth.forEach(function (entry, index) {
							$scope.Tempvalueofmonth = [];
							for (var i = 0; i < templistDataGraph.length; i++) {
								if (templistDataGraph[i]["MONTH"] === entry.m) {
									$scope.Tempvalueofmonth.push(templistDataGraph[i]);
								}
							}
							if ($scope.Tempvalueofmonth.length > 0) {
								$scope.Tempvalue1.push($scope.Tempvalueofmonth);
							}
						});
						var tempdata = [];
						$scope.Tempvalue1.forEach(function (entry, index) {
							$scope.Tempvalue = [];
							var min = Math.min.apply(
								Math,
								entry.map(function (o) {
									return o.BP;
								})
							);

							var max = Math.max.apply(
								Math,
								entry.map(function (o) {
									return o.BP;
								})
							);
							var tempminvalue = {};
							var tempmaxvalue = {};
							entry.forEach(function (ety, index) {
								if (ety["BP"] == min) {
									if (undefined == tempminvalue.type) {
										tempminvalue = ety;
										tempminvalue.type = "min";
										tempdata.push(tempminvalue);
									}
								}
								if (ety["BP"] == max) {
									if (undefined == tempmaxvalue.type) {
										tempmaxvalue = ety;
										tempmaxvalue.type = "max";
										tempdata.push(tempmaxvalue);
									}
								}
							});
						});
						$scope.valuemin.push([null, null]);
						$scope.valuemid.push([null, null]);
						$scope.valuemax.push([null, null]);
						$scope.listMonth.forEach(function (ety, index) {
							var minvalue = [];
							var maxvalue = [];
							for (var a = 0; a < tempdata.length; a++) {
								if (ety.m == tempdata[a]["MONTH"]) {
									if (tempdata[a]["type"] == "min") {
										minvalue.push(
											parseFloat(tempdata[a]["BP"]),
											parseFloat(tempdata[a]["BP"]) + 2
										);
									}
									if (tempdata[a]["type"] == "max") {
										maxvalue.push(
											parseFloat(tempdata[a]["BP"]),
											parseFloat(tempdata[a]["BP"]) + 2
										);
									}
								}
							}
							if (minvalue.length > 0) {
								$scope.valuemin.push(minvalue);
							} else {
								$scope.valuemin.push([null, null]);
							}
							if (maxvalue.length > 0) {
								$scope.valuemax.push(maxvalue);
							} else {
								$scope.valuemax.push([null, null]);
							}
							$scope.valuemid.push([
								$scope.valuemin[index + 1][0],
								$scope.valuemax[index + 1][0],
							]);
							$scope.liststandardmin.push($scope.standardmin);
							$scope.liststandardmax.push($scope.standardmax);
						});
						$scope.valuemin.push([null, null]);
						$scope.valuemid.push([null, null]);
						$scope.valuemax.push([null, null]);
						$scope.liststandardmin.push($scope.standardmin);
						$scope.liststandardmax.push($scope.standardmax);
						//คำนวณ max min value
						for (var i = 0; i < templistDataGraph.length; i++) {
							if (
								templistDataGraph[i]["BP"] == null ||
								templistDataGraph[i]["BP"] == undefined
							) {
							} else {
								$scope.MaxMinvalue1.push(
									parseFloat(templistDataGraph[i]["BP"])
								);
							}
						}
						$scope.min =
							Math.round(
								(Math.min.apply(null, $scope.MaxMinvalue1) - 10) / 10
							) * 10;
						if ($scope.min == $scope.standardmin) {
							$scope.min = $scope.min - 10;
						}
						$scope.max =
							Math.round(
								(Math.max.apply(null, $scope.MaxMinvalue1) + 10) / 10
							) * 10;
						$scope.datasets = [
							{
								type: "line",
								data: $scope.liststandardmin,
								lineTension: 0,
								fill: false,
								borderColor: "#FFFFFF",
								pointStyle: "dash",
								datalabels: {
									display: false,
								},
								borderDash: [10, 5],
							},
							{
								type: "line",
								data: $scope.liststandardmax,
								lineTension: 0,
								fill: false,
								borderColor: "#FFFFFF",
								pointStyle: "dash",
								datalabels: {
									display: false,
								},
								borderDash: [10, 5],
							},
							{
								barPercentage: 0.3,
								data: $scope.valuemin,
								backgroundColor: "#3fdbcb",
								datalabels: {
									color: "#FFFFFF",
									anchor: "start",
									align: "start",
									formatter: (val) => {
										return val[0];
									},
									font: { size: 20 },
								},
							},
							{
								barPercentage: 0.2,
								data: $scope.valuemid,
								backgroundColor: "#fff2e5",
								datalabels: {
									display: false,
								},
							},
							{
								barPercentage: 0.3,
								data: $scope.valuemax,
								backgroundColor: "#ff545d",
								datalabels: {
									color: "#FFFFFF",
									anchor: "end",
									align: "end",
									formatter: (val) => {
										return val[0];
									},
									font: { size: 20 },
								},
							},
						];
					}
				} else if ($scope.tapId == "menu-3") {
					$scope.TypeGraph = "bar";
					$scope.titlegraph = "ค่าออกซิเจนในเลือด";
					if (listDataGraph.length > 0) {
						if (liststandard.length > 0) {
							$scope.standardmin = liststandard[0]["MIN"];
							$scope.standardmax = liststandard[0]["MAX"];
						} else {
							$scope.standardmin = 95;
							$scope.standardmax = 100;
						}
						$scope.liststandardmin.push($scope.standardmin);
						$scope.liststandardmax.push($scope.standardmax);

						$scope.Tempvalue1 = [];
						$scope.listMonth.forEach(function (entry, index) {
							$scope.TempvalueofMonth = [];
							for (var i = 0; i < listDataGraph.length; i++) {
								if (listDataGraph[i]["MONTH"] === entry.m) {
									$scope.TempvalueofMonth.push(listDataGraph[i]);
								}
							}
							if ($scope.TempvalueofMonth.length > 0) {
								$scope.Tempvalue1.push($scope.TempvalueofMonth);
							}
						});
						var tempdata = [];

						$scope.Tempvalue1.forEach(function (entry, index) {
							$scope.Tempvalue = [];
							var min = Math.min.apply(
								Math,
								entry.map(function (o) {
									return o.OXYGEN;
								})
							);
							var max = Math.max.apply(
								Math,
								entry.map(function (o) {
									return o.OXYGEN;
								})
							);
							var tempminvalue = {};
							var tempmaxvalue = {};
							entry.forEach(function (ety, index) {
								if (ety["OXYGEN"] == min) {
									if (undefined == tempminvalue.type) {
										tempminvalue = ety;
										tempminvalue.type = "min";
										tempdata.push(tempminvalue);
									}
								}
								if (ety["OXYGEN"] == max) {
									if (undefined == tempmaxvalue.type) {
										tempmaxvalue = ety;
										tempmaxvalue.type = "max";
										tempdata.push(tempmaxvalue);
									}
								}
							});
						});
						$scope.valuemin.push([null, null]);
						$scope.valuemid.push([null, null]);
						$scope.valuemax.push([null, null]);
						$scope.listMonth.forEach(function (ety, index) {
							var minvalue = [];
							var maxvalue = [];
							for (var a = 0; a < tempdata.length; a++) {
								if (ety.m == tempdata[a]["MONTH"]) {
									if (tempdata[a]["type"] == "min") {
										minvalue.push(
											parseFloat(tempdata[a]["OXYGEN"]),
											parseFloat(tempdata[a]["OXYGEN"]) + 0.2
										);
									}
									if (tempdata[a]["type"] == "max") {
										maxvalue.push(
											parseFloat(tempdata[a]["OXYGEN"]),
											parseFloat(tempdata[a]["OXYGEN"]) + 0.2
										);
									}
								}
							}
							if (minvalue.length > 0) {
								$scope.valuemin.push(minvalue);
							} else {
								$scope.valuemin.push([null, null]);
							}
							if (maxvalue.length > 0) {
								$scope.valuemax.push(maxvalue);
							} else {
								$scope.valuemax.push([null, null]);
							}
							$scope.valuemid.push([
								$scope.valuemin[index + 1][0],
								$scope.valuemax[index + 1][0],
							]);
							$scope.liststandardmin.push($scope.standardmin);
							$scope.liststandardmax.push($scope.standardmax);
						});
						$scope.valuemin.push([null, null]);
						$scope.valuemid.push([null, null]);
						$scope.valuemax.push([null, null]);
						$scope.liststandardmin.push($scope.standardmin);
						$scope.liststandardmax.push($scope.standardmax);
						$scope.min = 85;
						$scope.max = 100;
						$scope.datasets = [
							{
								type: "line",
								data: $scope.liststandardmin,
								lineTension: 0,
								fill: false,
								borderColor: "#FFFFFF",
								pointStyle: "dash",
								datalabels: {
									display: false,
								},
								borderDash: [10, 5],
							},
							{
								type: "line",
								data: $scope.liststandardmax,
								lineTension: 0,
								fill: false,
								borderColor: "#FFFFFF",
								pointStyle: "dash",
								datalabels: {
									display: false,
								},
								borderDash: [10, 5],
							},
							{
								barPercentage: 0.3,
								data: $scope.valuemin,
								backgroundColor: "#3fdbcb",
								datalabels: {
									color: "#FFFFFF",
									anchor: "start",
									align: "start",
									formatter: (val) => {
										return val[0];
									},
									font: { size: 20 },
								},
							},
							{
								barPercentage: 0.2,
								data: $scope.valuemid,
								backgroundColor: "#fff2e5",
								datalabels: {
									display: false,
								},
							},
							{
								barPercentage: 0.3,
								data: $scope.valuemax,
								backgroundColor: "#ff545d",
								datalabels: {
									color: "#FFFFFF",
									anchor: "end",
									align: "end",
									formatter: (val) => {
										return val[0];
									},
									font: { size: 20 },
								},
							},
						];
					}
				} else if ($scope.tapId == "menu-4") {
					$scope.TypeGraph = "bar";
					$scope.titlegraph = "ค่าอุณหภูมิร่างกาย";
					if (listDataGraph.length > 0) {
						if (liststandard.length > 0) {
							$scope.standardmin = liststandard[0]["MIN"];
							$scope.standardmax = liststandard[0]["MAX"];
						} else {
							$scope.standardmin = 36.5;
							$scope.standardmax = 37.5;
						}
						$scope.liststandardmin.push($scope.standardmin);
						$scope.liststandardmax.push($scope.standardmax);

						$scope.Tempvalue1 = [];
						$scope.listMonth.forEach(function (entry, index) {
							$scope.TempvalueofMonth = [];
							for (var i = 0; i < listDataGraph.length; i++) {
								if (listDataGraph[i]["MONTH"] === entry.m) {
									$scope.TempvalueofMonth.push(listDataGraph[i]);
								}
							}
							if ($scope.TempvalueofMonth.length > 0) {
								$scope.Tempvalue1.push($scope.TempvalueofMonth);
							}
						});
						var tempdata = [];
						$scope.Tempvalue1.forEach(function (entry, index) {
							$scope.Tempvalue = [];
							var min = Math.min.apply(
								Math,
								entry.map(function (o) {
									return o.BTEMP;
								})
							);
							var max = Math.max.apply(
								Math,
								entry.map(function (o) {
									return o.BTEMP;
								})
							);
							var tempminvalue = {};
							var tempmaxvalue = {};
							entry.forEach(function (ety, index) {
								if (ety["BTEMP"] == min) {
									if (undefined == tempminvalue.type) {
										tempminvalue = ety;
										tempminvalue.type = "min";
										tempdata.push(tempminvalue);
									}
								}
								if (ety["BTEMP"] == max) {
									if (undefined == tempmaxvalue.type) {
										tempmaxvalue = ety;
										tempmaxvalue.type = "max";
										tempdata.push(tempmaxvalue);
									}
								}
							});
						});
						$scope.valuemin.push([null, null]);
						$scope.valuemid.push([null, null]);
						$scope.valuemax.push([null, null]);
						$scope.listMonth.forEach(function (ety, index) {
							var minvalue = [];
							var maxvalue = [];

							for (var a = 0; a < tempdata.length; a++) {
								if (ety.m == tempdata[a]["MONTH"]) {
									if (tempdata[a]["type"] == "min") {
										minvalue.push(
											parseFloat(tempdata[a]["BTEMP"]),
											parseFloat(tempdata[a]["BTEMP"]) + 0.1
										);
									}
									if (tempdata[a]["type"] == "max") {
										maxvalue.push(
											parseFloat(tempdata[a]["BTEMP"]),
											parseFloat(tempdata[a]["BTEMP"]) + 0.1
										);
									}
								}
							}
							if (minvalue.length > 0) {
								$scope.valuemin.push(minvalue);
							} else {
								$scope.valuemin.push([null, null]);
							}
							if (maxvalue.length > 0) {
								$scope.valuemax.push(maxvalue);
							} else {
								$scope.valuemax.push([null, null]);
							}
							$scope.valuemid.push([
								$scope.valuemin[index + 1][0],
								$scope.valuemax[index + 1][0],
							]);
							$scope.liststandardmin.push($scope.standardmin);
							$scope.liststandardmax.push($scope.standardmax);
						});
						$scope.valuemin.push([null, null]);
						$scope.valuemid.push([null, null]);
						$scope.valuemax.push([null, null]);
						$scope.liststandardmin.push($scope.standardmin);
						$scope.liststandardmax.push($scope.standardmax);
						$scope.min = 35;
						$scope.max = 40;
						$scope.datasets = [
							{
								type: "line",
								data: $scope.liststandardmin,
								lineTension: 0,
								fill: false,
								borderColor: "#FFFFFF",
								pointStyle: "dash",
								datalabels: {
									display: false,
								},
								borderDash: [10, 5],
							},
							{
								type: "line",
								data: $scope.liststandardmax,
								lineTension: 0,
								fill: false,
								borderColor: "#FFFFFF",
								pointStyle: "dash",
								datalabels: {
									display: false,
								},
								borderDash: [10, 5],
							},
							{
								barPercentage: 0.3,
								data: $scope.valuemin,
								backgroundColor: "#3fdbcb",
								datalabels: {
									color: "#FFFFFF",
									anchor: "start",
									align: "start",
									formatter: (val) => {
										return val[0];
									},
									font: { size: 20 },
								},
							},
							{
								barPercentage: 0.2,
								data: $scope.valuemid,
								backgroundColor: "#fff2e5",
								datalabels: {
									display: false,
								},
							},
							{
								barPercentage: 0.3,
								data: $scope.valuemax,
								backgroundColor: "#ff545d",
								datalabels: {
									color: "#FFFFFF",
									anchor: "end",
									align: "end",
									formatter: (val) => {
										return val[0];
									},
									font: { size: 20 },
								},
							},
						];
					}
				}
				$scope.label.push(" ");
				// }
				$scope.XfontSize = 30;
			}
			if (DataType == "DAY" && $scope.tapId != "menu-2") {
				$scope.options = {
					responsive: true,
					title: {
						display: true,
						text: $scope.titlegraph,
						fontSize: 45,
						fontColor: "#FFFFFF",
						fontFamily: "Prompt",
						fontStyle: "normal",
					},
					legend: {
						display: false,
					},
					hover: {
						mode: "nearest",
						intersect: true,
					},
					scales: {
						xAxes: [
							{
								display: true,
								scaleLabel: {
									display: true,
								},
								ticks: {
									fontSize: $scope.XfontSize,
									fontColor: "#FFFFFF",
									fontFamily: "Prompt",
								},
							},
						],
						yAxes: [
							{
								position: "right",
								display: true,
								scaleLabel: {
									display: true,
								},
								ticks: {
									min: $scope.min,
									max: $scope.max,
									fontSize: 30,
									fontColor: "#FFFFFF",
								},
							},
						],
					},
				};
			} else {
				$scope.options = {
					responsive: true,
					legend: {
						display: false,
					},
					title: {
						display: true,
						text: $scope.titlegraph,
						fontSize: 45,
						fontColor: "#FFFFFF",
						fontFamily: "Prompt",
						fontStyle: "normal",
					},
					tooltips: { enabled: false },
					hover: {
						mode: "nearest",
						intersect: true,
					},
					scales: {
						xAxes: [
							{
								stacked: true,
								ticks: {
									fontSize: $scope.XfontSize,
									fontColor: "#FFFFFF",
									fontFamily: "Prompt",
								},
							},
						],
						yAxes: [
							{
								position: "right",
								display: true,
								scaleLabel: {
									display: true,
								},
								ticks: {
									min: $scope.min,
									max: $scope.max,
									fontSize: 30,
									fontColor: "#FFFFFF",
								},
							},
						],
					},
				};
			}
			$scope.OptionGraph = {
				type: $scope.TypeGraph,
				data: {
					labels: $scope.label,
					datasets: $scope.datasets,
				},
				options: $scope.options,
			};
			var ctx = document.getElementById("canvas");
			if (window.myBar1) window.myBar1.destroy();
			window.myBar1 = new Chart(ctx, $scope.OptionGraph);
		};

		$scope.GetDataGraph = function (DataType) {
			if (D_Type != DataType) {
				$scope.DataType.date = new Date();
			}
			D_Type = DataType;
			$scope.DataType.type = DataType;
			$scope.DataType.tapId = $scope.tapId;
			$scope.DataType.PID = $rootScope.globals.PID;
			$scope.DataType.usertype = $rootScope.globals.usertype;
			detailService.GetDataGraph($scope.DataType, function (result) {
				if (result.status === true) {
					$scope.listDataGraph = result.message;
					$scope.liststandard = result.standard;
					var tempnow = new Date();
					if ($scope.DataType.type == "DAY") {
						if (tempnow.getDate() == $scope.DataType.date.getDate()) {
							$scope.forward = false;
						} else if (tempnow.getDate() > $scope.DataType.date.getDate()) {
							$scope.forward = true;
						}
					} else if ($scope.DataType.type == "MONTH") {
						if (tempnow.getMonth() == $scope.DataType.date.getMonth()) {
							$scope.forward = false;
						} else if (tempnow.getMonth() > $scope.DataType.date.getMonth()) {
							$scope.forward = true;
						}
					} else if ($scope.DataType.type == "YEAR") {
						if (tempnow.getFullYear() == $scope.DataType.date.getFullYear()) {
							$scope.forward = false;
						} else if (
							tempnow.getFullYear() > $scope.DataType.date.getFullYear()
						) {
							$scope.forward = true;
						}
					}
					$scope.CreateGarph(
						DataType,
						$scope.listDataGraph,
						$scope.liststandard
					);
				} else {
				}
			});
		};
		$scope.openTap = function (evt, cityName) {
			$scope.tapId = cityName;
			var i, tabcontent, tablinks;
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(" active", "");
			}
			document.getElementById(cityName).style.display = "block";
			evt.currentTarget.className += " active";
			$scope.DataType.date = new Date();
			$scope.GetDataGraph("DAY");
		};
		$scope.onInit = function () {
			$(".DisplayDevice").show();
			$scope.backward = true;
			$scope.forward = false;
			D_Type = "";
			$timeout(function () {
				document.getElementById("m1").click();
			}, 600);
		};
		$scope.Previous = function () {
			var tempnow = new Date();
			if ($scope.DataType.type == "DAY") {
				if (tempnow.getDate() == $scope.DataType.date.getDate()) {
					var temp = $scope.DataType.date.setDate(
						$scope.DataType.date.getDate() - 1
					);
					var DateSrearch = new Date(temp);
					$scope.DataType.date = DateSrearch;
					$scope.GetDataGraph("DAY");
				} else {
					var temp = $scope.DataType.date.setDate(
						$scope.DataType.date.getDate() - 1
					);
					var DateSrearch = new Date(temp);
					$scope.DataType.date = DateSrearch;
					$scope.GetDataGraph("DAY");
				}
			} else if ($scope.DataType.type == "MONTH") {
				if (tempnow.getMonth() == $scope.DataType.date.getMonth()) {
					var temp = $scope.DataType.date.setMonth(
						$scope.DataType.date.getMonth() - 1
					);
					var DateSrearch = new Date(temp);
					$scope.DataType.date = DateSrearch;
					$scope.GetDataGraph("MONTH");
				} else {
					var temp = $scope.DataType.date.setMonth(
						$scope.DataType.date.getMonth() - 1
					);
					var DateSrearch = new Date(temp);
					$scope.DataType.date = DateSrearch;
					$scope.GetDataGraph("MONTH");
				}
			} else if ($scope.DataType.type == "YEAR") {
				if (tempnow.getFullYear() == $scope.DataType.date.getFullYear()) {
					var temp = $scope.DataType.date.setFullYear(
						$scope.DataType.date.getFullYear() - 1
					);
					var DateSrearch = new Date(temp);
					$scope.DataType.date = DateSrearch;
					$scope.GetDataGraph("YEAR");
				} else {
					var temp = $scope.DataType.date.setFullYear(
						$scope.DataType.date.getFullYear() - 1
					);
					var DateSrearch = new Date(temp);
					$scope.DataType.date = DateSrearch;
					$scope.GetDataGraph("YEAR");
				}
			}
		};
		$scope.Next = function () {
			console.log($scope.DataType);
			var tempnow = new Date();

			if ($scope.DataType.type == "DAY") {
				if (tempnow.getDate() == $scope.DataType.date.getDate()) {
					var temp = $scope.DataType.date.setDate(
						$scope.DataType.date.getDate() + 1
					);
					var DateSrearch = new Date(temp);
					$scope.DataType.date = DateSrearch;
					$scope.GetDataGraph("DAY");
				} else {
					var temp = $scope.DataType.date.setDate(
						$scope.DataType.date.getDate() + 1
					);
					var DateSrearch = new Date(temp);
					$scope.DataType.date = DateSrearch;
					$scope.GetDataGraph("DAY");
				}
			} else if ($scope.DataType.type == "MONTH") {
				if (tempnow.getMonth() == $scope.DataType.date.getMonth()) {
					var temp = $scope.DataType.date.setMonth(
						$scope.DataType.date.getMonth() + 1
					);
					var DateSrearch = new Date(temp);
					$scope.DataType.date = DateSrearch;
					$scope.GetDataGraph("MONTH");
				} else {
					var temp = $scope.DataType.date.setMonth(
						$scope.DataType.date.getMonth() + 1
					);
					var DateSrearch = new Date(temp);
					$scope.DataType.date = DateSrearch;
					$scope.GetDataGraph("MONTH");
				}
			} else if ($scope.DataType.type == "YEAR") {
				if (tempnow.getFullYear() == $scope.DataType.date.getFullYear()) {
					var temp = $scope.DataType.date.setFullYear(
						$scope.DataType.date.getFullYear() + 1
					);
					var DateSrearch = new Date(temp);
					$scope.DataType.date = DateSrearch;
					$scope.GetDataGraph("YEAR");
				} else {
					var temp = $scope.DataType.date.setFullYear(
						$scope.DataType.date.getFullYear() + 1
					);
					var DateSrearch = new Date(temp);
					$scope.DataType.date = DateSrearch;
					$scope.GetDataGraph("YEAR");
				}
			}
		};
		$scope.onInit();
	}
})();

