(function () {
	"use strict";

	angular
		.module("BlurAdmin.pages.other_services")
		.controller("other_servicesCtrl", homeCtrl);

	/** @ngInject */
	function homeCtrl(
		$scope,
		baseService,
		$rootScope,
		$uibModal,
		$filter,
		$timeout,
		$location,
		$state
	) {
		(function initController() {
			baseService.devicesActiveList();
			setTimeout(() => {
				if ($rootScope.audiomute == true) {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = true;
					}
				} else {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = false;
					}
				}
			}, 100);
		})();
		$scope.activePageTitle = $state.current.title;
		$scope.other_services = $rootScope.globals.currentUser;

		$scope.progressFunction = function () {
			return $timeout(function () {}, 3000);
		};
		$scope.next = function () {
			// $location.path('/devices/weighing-machines');
			baseService.nextDevices();
		};

		// $scope.cancel = function() {
		//     $location.path('/authen');
		// }

		$scope.authen_adminByOther_services = function () {
			$scope.playAudio();
			$location.path("/authen_admin");
		};

		$scope.Back = function () {
			$scope.playAudio();
			$location.path("/profile");
		};

		$scope.playAudio = function () {
			var audio = new Audio("./assets/kiosk/audio/click.mp3");
			audio.play();
		};

		document.onkeypress = function (e) {
			e = e || window.event;
			let key = e.key.toLowerCase();
			switch (page) {
				case "DisplayDevice":
					// if (key == "z" || key=="ผ") {
					//     //red
					//     document.elementFromPoint(384, 975).click();
					// }
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(951, 969).click();
					}
					// if (key == "c" || key=="แ") {
					//     //green
					//     document.elementFromPoint(1545, 978).click();
					// }
					break;

				default:
			}
		};

		var page = "DisplayDevice";
	}
})();
