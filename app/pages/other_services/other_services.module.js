/**
 * @author a.demeshko
 * created on 12/18/15
 */
(function() {
    "use strict";

    var pageName = "other_services";
    var pageCtrl = pageName + "Ctrl";
    angular.module("BlurAdmin.pages.other_services", []).config(routeConfig);
    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider.state(pageName, {
            url: "/" + pageName,
            templateUrl: "app/pages/other_services/other_services.html",
            controller: pageCtrl,
            title: "บริการอื่นๆ",
            sidebarMeta: {
                icon: "ion-gear-a",
                order: 3,
            },
        });
    }
})();