(function () {
	"use strict";

	angular.module("BlurAdmin.pages.authen").controller("authenCtrl", authenCtrl);

	/** @ngInject */
	function authenCtrl(
		$scope,
		baseService,
		AuthenticationService,
		$uibModal,
		$filter,
		$timeout,
		$location,
		$state,
		$rootScope,
		$stateParams
	) {
		(function initController() {
			// reset login status
			AuthenticationService.ClearCredentials();
			$scope.urlbackgroundimg = "";
			setTimeout(() => {
				if ($rootScope.audiomute == true) {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = true;
					}
				} else {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = false;
					}
				}
				var condition = navigator.onLine ? "ONLINE" : "OFFLINE";
				if (condition == "ONLINE") {
					$scope.urlbackgroundimg = "assets/kiosk/auth/bt_wifi_open.svg";
				} else {
					$scope.urlbackgroundimg = "assets/kiosk/auth/bt_wifi_close.png";
				}
			}, 100);
			$scope.chkaudio = $rootScope.audiomute;
		})();

		$scope.activePageTitle = $state.current.title;
		$scope.height = "";

		$scope.heightFuntion = function (Key) {
			if ($scope.height.replace(".", "").length <= 3) {
				$scope.height = $scope.height.replace(".", "");
				if ($scope.height.replace(".", "").length == 3) {
					$scope.height = $scope.height + ".";
				}
				$scope.height = $scope.height + Key;
				$rootScope.globals.information.height = $scope.height;
				$scope.$apply();
			}
		};
		// $scope.delete= "";
		$scope.deleteFuntion = function () {
			$scope.height = $scope.height.replace(".", "");
			$scope.height = $scope.height.slice(0, -1);
		};

		$scope.progressFunction = function () {
			return $timeout(function () {}, 3000);
		};
		$scope.unknownUser = function () {
			$scope.playAudio();
			AuthenticationService.setProfile({ name: "xxx", sex: "xxx", age: "xxx" });
			$location.path("/profile");
		};
		$scope.loginByIdcard = function () {
			$rootScope.globals.usertype = "1";
			// window.onload();
			// AuthenticationService.setProfile({ name: "test name", sex: "ชาย", age: "60" });
			$scope.playAudio();
			// console.log($rootScope.globals)
			$location.path("/idcard");
		};
		$scope.loginByface = function () {
			// console.log($rootScope.globals)
			$rootScope.globals.usertype = "1";
			// AuthenticationService.setProfile({ name: "test name", sex: "ชาย", age: "60" });
			$scope.playAudio();
			$location.path("/face");
		};

		$scope.authen_adminByIdcard = function () {
			// AuthenticationService.setProfile({ name: "test name", sex: "ชาย", age: "60" });
			$scope.playAudio();
			$location.path("/authen_admin");
		};

		$scope.authen_adminBySelect_Weigh = function () {
			// AuthenticationService.setProfile({ name: "test name", sex: "ชาย", age: "60" });
			$scope.playAudio();
			$location.path("/authen_admin");
		};
		$scope.OnOffAudio = function () {
			$scope.playAudio();
			if ($rootScope.audiomute == true) {
				$rootScope.audiomute = false;
			} else {
				$rootScope.audiomute = true;
			}
		};
		$scope.resetModel = function () {};

		$scope.next = function () {
			$scope.playAudio();
			// $location.path('/devices/weighing-machines');
			baseService.nextDevices();
		};

		$scope.logout = function () {
			$scope.playAudio();
			$location.path("/home");
		};

		$scope.Cancel = function () {
			$scope.playAudio();
			$scope.cancelNumpadAudio();

			page = "DisplayDevice";

			$(".DisplayDevice").show();
			$(".Condition").hide();
			$(".Numpad").hide();
			$(".Select_Weighing").hide();
			$(".Select_Option").hide();
			$scope.resetModel();
		};

		$scope.unknown = function () {
			$scope.conditionAudio();

			page = "Condition";
			baseService.devicesActiveList();

			$(".DisplayDevice").hide();
			$(".Condition").show();
			$(".Numpad").hide();
			$(".Select_Weighing").hide();
			$(".Select_Option").hide();
			AuthenticationService.setProfile({
				name: "test name",
				sex: "ชาย",
				age: "60",
			});
			document.querySelector(".box-text-detail").focus();
			$rootScope.globals.usertype = "0";
			$scope.resetModel();
			$scope.playAudio();
		};

		$scope.agree = function () {
			$scope.playAudio();
			$scope.numpadAudio();

			page = "Numpad";
			$(".DisplayDevice").hide();
			$(".Condition").hide();
			$(".Numpad").show();
			$(".Select_Weighing").hide();
			$(".Select_Option").hide();
			$scope.resetModel();
		};

		$scope.select = function () {
			$scope.playAudio();
			$(".DisplayDevice").hide();
			$(".Condition").hide();
			$(".Numpad").hide();
			$(".Select_Weighing").show();
			$(".Select_Option").hide();
			$scope.resetModel();
		};

		$scope.option = function () {
			$scope.playAudio();
			$(".DisplayDevice").hide();
			$(".Condition").hide();
			$(".Numpad").hide();
			$(".Select_Weighing").show();
			$(".Select_Option").show();
			$scope.resetModel();
		};

		$scope.playAudio = function () {
			var audio = new Audio("./assets/kiosk/audio/click.mp3");
			audio.play();
		};

		// window.onload = function () {
		//     var audio = new Audio('./assets/kiosk/audio/insert_idcard.mp3');
		//     audio.play();

		// };
		$scope.displayAudio = function () {
			// var audio = new Audio('./assets/kiosk/audio/please_login.mp3');
			// audio.play();
			var display = document.getElementById("display");
			// var display =angular.element(document.getElementById('display')).scope()
			// console.log(display)
			display.play();
		};
		$scope.conditionAudio = function () {
			var display = document.getElementById("display");
			display.pause();

			var sample = document.getElementById("condition");
			sample.currentTime = 0;
			sample.play();
		};

		$scope.numpadAudio = function () {
			var display = document.getElementById("condition");
			display.pause();

			var numpad = document.getElementById("numpad");
			numpad.currentTime = 0;
			numpad.play();
		};

		$scope.cancelNumpadAudio = function () {
			var numpad = document.getElementById("numpad");
			numpad.pause();

			// var sample = document.getElementById("numpad");
			// sample.currentTime = 0;
			// sample.play();
		};

		document.onkeypress = function (e) {
			e = e || window.event;
			let key = e.key.toLowerCase();
			switch (page) {
				case "DisplayDevice":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(433, 548).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(963, 581).click();
					}
					if (key == "c" || key == "แ") {
						// green
						document.elementFromPoint(1520, 400).click();
					}
					break;
				case "Numpad":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(348, 909).click();
					}
					// if (key == "x" || key=="ป") {
					// 	//yellow
					// }
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(852, 903).click();
					}

					break;
				case "Condition":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(492, 900).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
					}
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(1532, 908).click();
					}

					break;
				default:
			}
		};
		$scope.init = function () {
			if ($stateParams.page == "Condition") {
				$scope.unknown();
			} else {
				$scope.displayAudio();
			}
		};
		var page = "DisplayDevice";
	}
})();
