/**
 * @author a.demeshko
 * created on 12/18/15
 */
(function() {
    "use strict";

    var pageName = "authen";
    var pageCtrl = pageName + "Ctrl";
    angular.module("BlurAdmin.pages.authen", []).config(routeConfig);
    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider.state(pageName, {
            url: "/" + pageName,
            templateUrl: "app/pages/authen/authen.html",
            params: {
                page: null,
            },
            controller: pageCtrl,
            title: "เลือกรูปแบบการเข้าสู่ระบบ",
            sidebarMeta: {
                icon: "ion-gear-a",
                order: 1,
            },
        });
        
    }
})();