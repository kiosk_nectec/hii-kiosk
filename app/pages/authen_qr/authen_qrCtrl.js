(function () {
	"use strict";

	angular
		.module("BlurAdmin.pages.authen_qr")
		.controller("authen_qrCtrl", homeCtrl);

	/** @ngInject */
	function homeCtrl(
		$scope,
		baseService,
		$rootScope,
		$uibModal,
		$filter,
		$timeout,
		$location,
		$state
	) {
		(function initController() {
			baseService.devicesActiveList();
			setTimeout(() => {
				if ($rootScope.audiomute == true) {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = true;
					}
				} else {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = false;
					}
				}
			}, 100);
		})();
		$scope.activePageTitle = $state.current.title;
		$scope.authen_qr = $rootScope.globals.currentUser;

		$scope.progressFunction = function () {
			return $timeout(function () {}, 3000);
		};
		$scope.next = function () {
			$scope.playAudio();
			// $location.path('/devices/weighing-machines');
			baseService.nextDevices();
		};
		$scope.cancel = function () {
			$scope.playAudio();
			$location.path("/authen_admin");
		};
		// $scope.scanqr = function () {
		// 	let scanner = new Instascan.Scanner({
		//         video: document.getElementById("preview"),
		//     });
		//     scanner.addListener("scan", function (content) {

		//         alert(content);
		//     });
		//     Instascan.Camera.getCameras()
		//         .then(function (cameras) {console.log(cameras);console.log(cameras.length);
		//             if (cameras.length > 0) {
		//                 scanner.start(cameras[0]);
		//             } else {
		//                 console.error("No cameras found.");
		//             }
		//         })
		//         .catch(function (e) {
		//             console.error(e);
		//         });
		// };
		// $scope.scanqr();
		$scope.playAudio = function () {
			var audio = new Audio("./assets/kiosk/audio/click.mp3");
			audio.play();
		};

		document.onkeypress = function (e) {
			e = e || window.event;
			let key = e.key.toLowerCase();
			switch (page) {
				case "DisplayDevice":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(946, 906).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
					}
					if (key == "c" || key == "แ") {
						// green
					}
					break;

				default:
			}
		};
		var page = "DisplayDevice";
	}
})();
