(function () {
	"use strict";

	angular
		.module("BlurAdmin.pages.devices")
		.controller("weighingMachinesCtrl", weighingMachinesCtrl);

	/** @ngInject */
	function weighingMachinesCtrl(
		$scope,
		baseService,
		$rootScope,
		$uibModal,
		$filter,
		$timeout,
		$location,
		$state,
		devicesService,
		$sce,
		$configuration,
		d_serviceService,
		conWeighingMachinesService
	) {
		(function initController() {
			setTimeout(() => {
				if ($rootScope.audiomute == true) {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = true;
					}
					console.log(elems, "true");
				} else {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = false;
					}
					console.log(elems, "false");
				}
				var weighingmachinesvideo = document.getElementById("weighingmachinesvideo");
				console.log(weighingmachinesvideo)
				if (typeof weighingmachinesvideo.loop == 'boolean') { // loop supported
					weighingmachinesvideo.loop = true;
				  } else { // loop property not supported
					weighingmachinesvideo.addEventListener('ended', function () {
					  this.currentTime = 0;
					  this.play();
					}, false);
				  }
				weighingmachinesvideo.play();
			}, 200);
		})();
		$scope.data = {};
		var isCallback = true;
		var isCallbackPrepare = true;
		$scope.cancelShow = false;
		$scope.conditionDataWeighingMachines = [];
		$scope.convertDataWeighingMachines = {};
		$scope.weighingMachinesStyleEmoji = {};
		$scope.RFIDWheelchair = "";
		$scope.Show_RFIDWheelchair = "";
		$scope.TextRFIDError = "";
		$scope.ListRFID = [];
		$scope.init = function () {
			conWeighingMachinesService.getConWeighingMachines({}, function (result) {
				$scope.conditionDataWeighingMachines = result.message;
				console.log($scope.conditionDataWeighingMachines);
				// console.log($scope.conditionDataWeighingMachines)
				// $scope.convertDataWeighingMachines = $scope.conditionDataWeighingMachines[0]

				// $scope.oxygenStyleEmoji = {
				//     'background-image': 'url('+$scope.convertDataWeighingMachines.Image+')','width':'200px'
				// }
			});
		};
		$scope.backtoprofile = function () {
			$scope.playAudio();
			if ($rootScope.globals.usertype == 0) {
				$location.path("/authen");
			} else {
				$location.path("/profile");
			}
		};
		$scope.regiscard = function () {
			$scope.playAudio();
			$location.path("/idcard");
		};
		$scope.convertResult = function () {
			//    $scope.data.BMI = 25
			//  alert("convertResult")
			$scope.conditionDataWeighingMachines.forEach((item) => {
				// alert("condition")
				// console.log($scope.convertDataWeighingMachines)
				console.log(parseFloat(item.Min), item.Max, $scope.data.BMI);
				if (
					parseFloat(item.Min) <= parseFloat($scope.data.BMI) &&
					parseFloat(item.Max) >= parseFloat($scope.data.BMI)
				) {
					$scope.convertDataWeighingMachines = item;

					$scope.weighingMachinesStyle = JSON.parse(
						$scope.convertDataWeighingMachines.Device_style
					);

					$scope.weighingMachinesStyleEmoji = {
						"background-image":
							"url(" + $scope.convertDataWeighingMachines.Image + ")",
					};

					for (const style in $scope.weighingMachinesStyle.emojiposition) {
						Object.assign($scope.weighingMachinesStyleEmoji, {
							[style]: $scope.weighingMachinesStyle.emojiposition[style],
						});
					}
					console.log($scope.weighingMachinesStyleEmoji);
					// $scope.weighingMachinesStyleBg = {
					//     'background-color': '(' + $scope.convertDataWeighingMachines.Color_code_bg + ')'
					// }

					// $scope.weighingMachinesStyleDisplay = {
					//     'color': '(' + $scope.convertDataWeighingMachines.Color_code_display + ')'
					// }

					// $scope.weighingMachinesStyleRec = {
					//     'color': '(' + $scope.convertDataWeighingMachines.Color_code_rec + ')'
					// }

					// $scope.weighingMachinesStyleRec2 = {
					//     'color': '(' + $scope.convertDataWeighingMachines.Color_code_rec_2 + ')'
					// }

					$rootScope.globals.information.BMICODE = item.Code;
					d_serviceService.saveD_service($rootScope.globals);
				}
			});
		};

		$scope.videoConfig = {
			sources: [
				{
					src: $sce.trustAsResourceUrl(
						"assets/vdo/SampleVideo_720x480_2mb.mp4"
					),
					type: "video/mp4",
				},
				// { src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.webm"), type: "video/webm" },
				// { src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.ogg"), type: "video/ogg" }
			],
			tracks: [
				{
					src: "http://www.videogular.com/assets/subs/pale-blue-dot.vtt",
					kind: "subtitles",
					srclang: "en",
					label: "English",
					default: "",
				},
			],

			theme:
				$configuration.rootpath +
				"theme/bower_components/videogular-themes-default/videogular.css",
			plugins: {
				poster: "http://www.videogular.com/assets/images/videogular.png",
			},
		};
		$scope.activePageTitle = $state.current.title;
		$scope.profile = $rootScope.globals.currentUser;
		$scope.usertype = $rootScope.globals.usertype;

		$scope.video = null;

		console.log($rootScope.globals);

		console.log($rootScope.globals.usertype);

		$scope.resetModel = function () {
			$scope.weight = 0;
		};
		$scope.prepare = function () {
			$(".DisplayDevice").show();
			$(".Loading").hide();
			$(".ResultDevice").hide();
			$scope.resetModel();
		};

		// $scope.Weighing = function () {
		// 	$(".DisplayDevice").hide();
		// 	$(".Loading").show();
		// 	$(".error").hide();
		// 	// $scope.playAudio();
		// 	// $scope.videoStop()
		// 	$scope.selectAudio();
		// 	isCallbackPrepare = true;
		// 	devicesService.getDataWeighingMachines({}, function (result) {
		// 		if (isCallbackPrepare) {
		// 			if (result.data[0].rfid == "yes") {
		// 				$scope.start();
		// 			} else {
		// 				$scope.wheelchairWeigh = undefined;
		// 				$scope.wheelchair = "";
		// 				$(".DisplayDevice").hide();
		// 				$(".Loading").hide();
		// 				$(".Select_Weighing").show();
		// 				// $scope.resultAudio();
		// 			}
		// 		}
		// 	});
		// };

		$scope.start = function () {
			$scope.playAudio();
			$scope.videoStop();

			page = "Loading";

			$(".DisplayDevice").hide();
			$(".Loading").show();
			$(".Select_Weighing").hide();
			$(".error").hide();
			$scope.loadAudio();
			isCallback = true;
			$scope.cancelShow = false;
			devicesService.getDataWeighingMachines({}, function (result) {
				if (isCallback) {
					page = "error_device";
					console.log(result);
					if (result.success == false) {
						$(".Loading").hide();
						$(".error").show();
						$scope.errorAudio();
					} else if (result.data[0].magnet == undefined) {
						$(".Loading").hide();
						$(".error").show();
						$scope.errorAudio();
					} else if (result.data[0].weight_total != undefined) {
						// $scope.$apply(function () {
						// $scope.weight = 59;

						// alert(JSON.stringify(result))
						$scope.data = result.data[0];
						// result.data[0].magnet = "yes"
						// result.data[0].magnet = "Notfound";
						if (result.data[0].magnet != "Notfoun") {
							$scope.data.weight = result.data[0].weight_total;
							$scope.data.BMI = $scope.calBMI(
								$scope.data.weight,
								$rootScope.globals.information.height
							);
							$scope.data.BMR = $scope.calBMR(
								$rootScope.globals.information.SEX,
								$scope.data.weight,
								$rootScope.globals.information.height,
								$rootScope.globals.currentUser.age.years
							);

							$scope.setData();

							page = "ResultDevice";

							$(".DisplayDevice").hide();
							$(".Select_Weighing").hide();
							$(".Loading").hide();
							$(".ResultDevice").show();
							$scope.resultAudio();
						} else {
							page = "Select_Weighing";

							$scope.weight_total = result.data[0].weight_total;
							$scope.wheelchairWeigh = undefined;
							$scope.wheelchair = "";
							$(".DisplayDevice").hide();
							$(".Loading").hide();
							$(".Select_Weighing").show();
						}
						// });
						// console.log($scope.weight);
					} else {
						$(".Loading").hide();
						$(".error").show();
						$scope.errorAudio();
					}
				}
			});
		};

		$scope.skip = function () {
			baseService.nextDevices();
		};

		$scope.showResult = function () {
			// console.log($rootScope.globals.currentUser.profile_img)
			if(undefined != $rootScope.globals.currentUser.profile_img){
				$scope.profile.profile_img = $rootScope.globals.currentUser.profile_img;
			}else{
				$scope.profile.profile_img = "assets/kiosk/results_unknown/profile-result.png";
			}
			$scope.data.weight = (
				$scope.weight_total - $scope.wheelchairWeigh
			).toFixed(1);
			$scope.data.BMI = $scope.calBMI(
				$scope.data.weight,
				$rootScope.globals.information.height
			);
			$scope.data.BMR = $scope.calBMR(
				$rootScope.globals.information.SEX,
				$scope.data.weight,
				$rootScope.globals.information.height,
				$rootScope.globals.currentUser.age.years
			);
			$scope.setData();

			page = "ResultDevice";

			$(".DisplayDevice").hide();
			$(".Select_Weighing").hide();
			$(".Select_Option").hide();
			$(".Loading").hide();
			$(".ResultDevice").show();
			$scope.resultAudio();
		};
		$scope.setData = function () {
			$rootScope.globals.information.weight = $scope.data;
			d_serviceService.saveD_service($rootScope.globals);
			$scope.convertResult();
		};
		$scope.calBMI = function (weight, heigth) {
			console.log(weight, heigth);
			let bmi = parseFloat(weight) / Math.pow(parseFloat(heigth) / 100, 2);
			return bmi
				.toFixed(1)
				.toString()
				.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
		};
		$scope.calBMR = function (sex, weight, heigth, age) {
			var bmr;
			// sex = 1;
			// age = 25;
			if (sex == 1) {
				bmr = 66 + 13.7 * weight + 5 * heigth - 6.8 * age;
			} else {
				bmr = 665 + 9.6 * weight + 1.8 * heigth - 4.7 * age;
			}

			return isNaN(bmr)
				? "-"
				: Math.round(bmr)
						.toFixed(1)
						.toString()
						.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
		};

		$scope.onPlayerReady = function (API) {
			$scope.video = API;
			console.log(API.stop());
			API.stop();

			setTimeout(() => {
				$scope.videoPlay();
			}, 1000);
			// console.log(API)
		};

		$scope.Info = function () {
			$scope.playAudio();
			$(".DisplayDevice").hide();
			$(".DisplayDevice_Info").show();
			$(".ResultDevice").hide();
			$(".Blood_Info").hide();
			$scope.resetModel();
		};

		$scope.Back = function () {
			$scope.playAudio();
			$(".DisplayDevice").show();
			$(".DisplayDevice_Info").hide();
			$(".ResultDevice").hide();
			$(".Blood_Info").hide();
			$scope.resetModel();
		};

		$scope.locationpath_W = function () {
			$scope.playAudio();
			$location.path("/devices/weighing-machines");
		};
		$scope.locationpath_B = function () {
			$scope.playAudio();
			$location.path("/devices/blood-pressure");
		};
		$scope.locationpath_P = function () {
			$scope.playAudio();
			$location.path("/devices/pulse-oximeter");
		};
		$scope.locationpath_T = function () {
			$scope.playAudio();
			$location.path("/devices/thermometer");
		};
		$scope.videoPlay = function () {
			// console.log("PLAY");
			// $scope.video.play();
		};
		$scope.videoStop = function () {
			// console.log("STOP");
			// $scope.video.stop();
		};
		$scope.recheck = function () {
			page = "DisplayDevice";
			$scope.wheelchairWeigh = undefined;
			$scope.wheelchair = "";
			$scope.playAudio();
			$scope.videoPlay();
			$scope.prepare();
		};
		$scope.prepare();

		$scope.Cancel = function () {
			$scope.playAudio();
			isCallback = false;
			isCallbackPrepare = false;
			$scope.cancelShow = true;
			page = "DisplayDevice";

			devicesService.stopDevice(
				{ device: "weighingmachine" },
				function (result) {
					$(".DisplayDevice").show();
					$(".Loading").hide();
					$(".ResultDevice").hide();
					$(".Select_Weighing").hide();
					$scope.resetModel();
				}
			);
		};

		$scope.option = function () {
			$scope.playAudio();
			page = "Select_Option";
			$scope.RFIDWheelchair = "";
			$scope.Show_RFIDWheelchair = "";
			$scope.TextRFIDError = "";
			$scope.ListRFID = [];
			$(".DisplayDevice").hide();
			$(".Loading").hide();
			$(".Select_Weighing").show();
			$(".Select_Option").show();
			$scope.resetModel();
		};

		$scope.confirmRfid = function () {
			$scope.wheelchair = $scope.ListRFID[0]["item"];
			$scope.wheelchairWeigh = $scope.ListRFID[0]["value"];
			$scope.showResult();
			// page = "Select_Weighing";
			// $scope.select($scope.ListRFID[0]["item"], $scope.ListRFID[0]["value"]);
		};

		$scope.Cancelrfid = function () {
			$scope.playAudio();

			$(".DisplayDevice").hide();
			$(".Loading").hide();
			$(".Select_Weighing").show();
			$(".Select_Option").hide();
			$scope.resetModel();
		};

		$scope.select = function (item, value) {
			$scope.wheelchair = item;
			$scope.wheelchairWeigh = value;

			$scope.playAudio();

			$(".DisplayDevice").hide();
			$(".Loading").hide();
			$(".Select_Weighing").show();
			$(".Select_Option").hide();
			$scope.resetModel();
		};

		$scope.logout = function () {
			$scope.playAudio();
			$location.path("/home");
		};

		$scope.playAudio = function () {
			var audio = new Audio("./assets/kiosk/audio/click.mp3");
			audio.play();
			var load = document.getElementById("load");
			load.pause();
			var error = document.getElementById("error");
			error.pause();
		};

		$scope.init();

		// $scope.replayAudio = function () {
		//     var sample = document.getElementById("foobar");
		//     sample.play();
		// };

		$scope.selectAudio = function () {
			var display = document.getElementById("display");
			display.pause();

			// var sample = document.getElementById("load");
			// sample.currentTime = 0
			// sample.play();
		};

		$scope.loadAudio = function () {
			var display = document.getElementById("display");
			display.pause();

			var sample = document.getElementById("load");
			sample.currentTime = 0;
			sample.play();
		};

		$scope.resultAudio = function () {
			var load = document.getElementById("load");
			load.pause();

			var sample = document.getElementById("result");
			sample.currentTime = 0;
			sample.play();
		};

		$scope.errorAudio = function () {
			var load = document.getElementById("load");
			load.pause();

			var sample = document.getElementById("error");
			sample.currentTime = 0;
			sample.play();
		};

		document.onkeypress = function (e) {
			e = e || window.event;
			let key = e.key.toLowerCase();
			switch (page) {
				case "DisplayDevice":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(384, 975).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(951, 969).click();
					}
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(1545, 978).click();
					}
					break;
				case "Loading":
					// if (key == "z" || key=="ผ") {
					// 	//red
					// 	document.elementFromPoint(348, 909).click();
					// }
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(957, 939).click();
					}
					// if (key == "c" || key=="แ") {
					// 	//green
					// 	document.elementFromPoint(852, 903).click();
					// }

					break;
				case "Select_Weighing":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(372, 975).click();
					}
					// if (key == "x" || key=="ป") {
					// 	//yellow
					// 	document.elementFromPoint(957, 939).click();
					// }
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(1533, 981).click();
					}

					break;
				case "ResultDevice":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(429, 867).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(954, 861).click();
					}
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(1485, 864).click();
					}

					break;
				case "error_device":
					// if (key == "z" || key=="ผ") {
					// 	//red
					// 	document.elementFromPoint(384, 975).click();
					// }
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(942, 870).click();
					}
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(1440, 870).click();
					}
					break;
				case "Select_Option":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(526, 836).click();
					}
					// if (key == "x" || key == "ป") {
					// 	//yellow
					// 	document.elementFromPoint(954, 861).click();
					// }
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(1394, 840).click();
					}

					$scope.RFIDWheelchair = $scope.RFIDWheelchair + key;
					if ($scope.RFIDWheelchair.length == 10) {
						devicesService.GetChkRfid(
							{ RFID: $scope.RFIDWheelchair },
							function (result) {
								$scope.ListRFID = result.message;
								if (result.status == true && $scope.ListRFID.length > 0) {
									$scope.Show_RFIDWheelchair = $scope.ListRFID[0]["item"];
									$scope.RFIDWheelchair = "";
									$scope.TextRFIDError = "";
								} else {
									$scope.TextRFIDError =
										"เกิดข้อผิดพลาด กรุณาแตะบัตรใหม่อีกครัง";
									$scope.ListRFID = [];
									$scope.RFIDWheelchair = "";
									$scope.Show_RFIDWheelchair = "";
								}
							}
						);
					}
					break;
				default:
			}
		};

		var page = "DisplayDevice";
	}
})();
