/**
 * @author a.demeshko
 * created on 12/18/15
 */
(function() {
    "use strict";

    var pageName = "weighing-machines";
    var pageCtrl = pageName + "Ctrl";
    angular.module("BlurAdmin.pages.devices.weighingmachines", []).config(routeConfig);
    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider.state("devices." + pageName, {
            url: "/" + pageName,
            templateUrl: "app/pages/devices/weighing-machines/weighingMachines.html",
            controller: pageCtrl,
            title: "วัดน้ำหนัก",
            sidebarMeta: {
                order: 800,
            },
        });
    }
})();