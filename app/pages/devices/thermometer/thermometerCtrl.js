(function () {
	"use strict";

	angular
		.module("BlurAdmin.pages.devices")
		.controller("thermometerCtrl", thermometerCtrl);

	/** @ngInject */
	function thermometerCtrl(
		$scope,
		baseService,
		$rootScope,
		$uibModal,
		$filter,
		$timeout,
		$location,
		$state,
		devicesService,
		$sce,
		$configuration,
		d_serviceService,
		conTemperatureService,
		WebcamService
	) {
		(function initController() {
			setTimeout(() => {
				if ($rootScope.audiomute == true) {
					var elems = document.querySelectorAll("video, audio");
					console.log(elems, "true");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = true;
					}
				} else {
					var elems = document.querySelectorAll("video, audio");
					console.log(elems, "false");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = false;
					}
				}
				var thermometervideo = document.getElementById("thermometervideo");
				console.log(thermometervideo);
				if (typeof thermometervideo.loop == "boolean") {
					// loop supported
					thermometervideo.loop = true;
				} else {
					// loop property not supported
					thermometervideo.addEventListener(
						"ended",
						function () {
							this.currentTime = 0;
							this.play();
						},
						false
					);
				}
				thermometervideo.play();
			}, 200);
		})();
		var display = new Audio("./assets/kiosk/audio/welcome_thermometer.mp3");
		// // document.getElementById("display");
		var load = new Audio("./assets/kiosk/audio/load_thermometer.mp3");
		// // document.getElementById("load");
		var result = new Audio("./assets/kiosk/audio/result_thermometer.mp3");
		// // document.getElementById("result");
		var ircamera = new Audio("./assets/kiosk/audio/camera_thermometer.mp3");

		var countdown = new Audio("./assets/kiosk/audio/countdown_ir.mp3");
		// document.getElementById("ircamera");
		var error = new Audio("./assets/kiosk/audio/error_devices.mp3");
		// document.getElementById("error");
		$scope.DisplayDevice = false;
		$scope.data = {};

		$scope.conditionDataTemperature = [];
		$scope.convertDataTemperature = {};
		$scope.temperatureStyleEmoji = {};
		var isCallback = true;
		var getdata;
		// $scope.oxygenStyleBg = {}
		// $scope.oxygenStyleDisplay = {}
		// $scope.oxygenStyleRec = {}
		// $scope.oxygenStyleRec2 = {}
		$scope.cancelShow = false;
		$scope.init = function () {
			conTemperatureService.getConTemperature({}, function (result) {
				$scope.conditionDataTemperature = result.message;
				// console.log($scope.conditionDataOxygen)
				// $scope.convertDataOxygen = $scope.conditionDataOxygen[0]

				// $scope.oxygenStyleEmoji = {
				//     'background-image': 'url('+$scope.convertDataOxygen.Image+')','width':'200px'
				// }
			});
			// WebcamService.checkWebcamStatus(function (result) {
			// 	// alert(result.status);
			// 	if (result.status) {

			$scope.prepare();
			// 	} else {
			// 		$scope.error();
			// 		// display.pause();
			// 	}
			// });
			// setTimeout(() => {
			// 	if ($rootScope.audiomute == true) {
			// 		var elems = document.querySelectorAll("video, audio");
			// 		console.log(elems, "true");
			// 		for (var i = 0; i < elems.length; ++i) {
			// 			elems[i].muted = true;
			// 		}
			// 		display.muted();
			// 		load.muted();
			// 		result.muted();
			// 		ircamera.muted();
			// 		countdown.muted();
			// 		error.muted();
			// 	} else {
			// 		var elems = document.querySelectorAll("video, audio");
			// 		console.log(elems, "false");
			// 		for (var i = 0; i < elems.length; ++i) {
			// 			elems[i].muted = false;
			// 		}
			// 	}
			// }, 200);
		};
		$scope.Info = function () {
			$scope.playAudio();
			$(".DisplayDevice").hide();
			$(".DisplayDevice_Info").show();
			$(".ResultDevice").hide();
			$(".Blood_Info").hide();
			$scope.resetModel();
		};

		//เหลื่อทำนับถอยหลัง 10วิ
		$scope.useGun = function () {
			$scope.showGun();
		};
		$scope.showGun = function () {
			$scope.playAudio();
			$(".DisplayDevice").hide();
			$(".DisplayDevice_Info").hide();
			$(".Loading").hide();
			$(".error_camera").hide();
			$(".IRCamera").hide();
			$(".ResultDevice").hide();
			$(".Success").show();
			$(".SuccessError").hide();
			$scope.resetModel();
			function count(timetocall) {
				setTimeout(() => {
					$scope.guncounter = timetocall;
					$scope.$apply();
					if (timetocall > 0) {
						console.log($scope.guncounter);
						count(timetocall - 1);
					} else {
						devicesService.getTemperatureFromGun(
							{ session: $rootScope.globals.session_id },
							function (result) {
								console.log(result);
								$scope.overlayShow = false;
								// clearInterval(stopped);

								// alert(JSON.stringify(result))
								if (result.status == false) {
									$scope.showGunError();
								} else {
									if (result.message.length > 0) {
										if (
											result.message[0].temperature.length >= 2 &&
											result.message[0].temperature.length < 4
										) {
											let vaule = "";
											for (
												var i = 0;
												i < result.message[0].temperature.length;
												i++
											) {
												vaule += result.message[0].temperature.charAt(i);
												if (
													result.message[0].temperature.length == 3 &&
													i == 1
												) {
													vaule += ".";
												}
												if (
													result.message[0].temperature.length == 2 &&
													i == 1
												) {
													vaule += ".0";
												}
											}

											$scope.data = {};
											$scope.data.value = vaule;

											$scope.data.picture = "";
											$rootScope.globals.information.temperature = $scope.data;

											$scope.convertResult();
											$scope.showResult();
											// d_serviceService.saveD_service($rootScope.globals);
										} else {
											$scope.showGunError();
										}
									} else {
										$scope.showGunError();
									}
								}
							}
						);
					}
				}, 1000);
			}
			count(10);
		};

		//หน้าerror แล้วกลับไปหน้านับเวลา
		$scope.showGunError = function () {
			$scope.playAudio();
			$(".DisplayDevice").hide();
			$(".DisplayDevice_Info").hide();
			$(".Loading").hide();
			$(".error_camera").hide();
			$(".IRCamera").hide();
			$(".ResultDevice").hide();
			$(".Success").hide();
			$(".SuccessError").show();
			$scope.resetModel();
		};

		$scope.Back = function () {
			$scope.playAudio();
			$(".DisplayDevice").show();
			$(".DisplayDevice_Info").hide();
			$(".ResultDevice").hide();
			$(".Blood_Info").hide();
			$scope.resetModel();
		};

		$scope.locationpath_W = function () {
			$scope.playAudio();
			display.pause();
			result.pause();
			countdown.pause();
			ircamera.pause();
			error.pause();
			load.pause();
			countdown.pause();
			$location.path("/devices/weighing-machines");
		};
		$scope.locationpath_B = function () {
			$scope.playAudio();
			display.pause();
			result.pause();
			countdown.pause();
			ircamera.pause();
			error.pause();
			load.pause();
			countdown.pause();
			$location.path("/devices/blood-pressure");
		};
		$scope.locationpath_P = function () {
			$scope.playAudio();
			display.pause();
			result.pause();
			countdown.pause();
			ircamera.pause();
			error.pause();
			load.pause();
			countdown.pause();
			$location.path("/devices/pulse-oximeter");
		};
		$scope.locationpath_T = function () {
			$scope.playAudio();
			display.pause();
			result.pause();
			countdown.pause();
			ircamera.pause();
			error.pause();
			load.pause();
			countdown.pause();
			$location.path("/devices/thermometer");
		};
		$scope.convertResult = function () {
			//  $scope.data.value = 39.7
			$scope.conditionDataTemperature.forEach((item) => {
				console.log(parseFloat(item.Min), item.Max, $scope.data.value);
				if (
					parseFloat(item.Min) <= parseFloat($scope.data.value) &&
					parseFloat(item.Max) >= parseFloat($scope.data.value)
				) {
					$scope.convertDataTemperature = item;

					$scope.temperatureStyle = JSON.parse(
						$scope.convertDataTemperature.Device_style
					);

					$scope.temperatureStyleEmoji = {
						"background-image":
							"url(" + $scope.convertDataTemperature.Image + ")",
					};

					for (const style in $scope.temperatureStyle.emojiposition) {
						Object.assign($scope.temperatureStyleEmoji, {
							[style]: $scope.temperatureStyle.emojiposition[style],
						});
					}
					// $scope.oxygenStyleBg = {
					//     'background-color': '(' + $scope.convertDataOxygen.Color_code_bg + ')'
					// }

					// $scope.oxygenStyleDisplay = {
					//     'color': '(' + $scope.convertDataOxygen.Color_code_display + ')'
					// }

					// $scope.oxygenStyleRec = {
					//     'color': '(' + $scope.convertDataOxygen.Color_code_rec + ')'
					// }

					// $scope.oxygenStyleRec2 = {
					//     'color': '(' + $scope.convertDataOxygen.Color_code_rec_2 + ')'
					// }

					$rootScope.globals.information.BTEMPCODE = item.Code;
					d_serviceService.saveD_service($rootScope.globals);
				}
			});
		};

		var stopped;

		$scope.countdown = function (delay, callback) {
			stopped = setInterval(function () {
				console.log($scope.counter);
				delay -= 1000;
				if ($scope.counter > 0) {
					$scope.counter--;
					$scope.$apply();
				}
				if (delay <= -1000) {
					$scope.overlayShow = true;
					callback();
					clearInterval(stopped);
				}
				if (isCallback == false) {
					clearInterval(stopped);
				}
			}, 1000);
		};

		$scope.videoConfig = {
			sources: [
				{
					src: $sce.trustAsResourceUrl(
						"assets/vdo/SampleVideo_720x480_2mb.mp4"
					),
					type: "video/mp4",
				},
				// { src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.webm"), type: "video/webm" },
				// { src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.ogg"), type: "video/ogg" }
			],
			tracks: [
				{
					src: "http://www.videogular.com/assets/subs/pale-blue-dot.vtt",
					kind: "subtitles",
					srclang: "en",
					label: "English",
					default: "",
				},
			],

			theme:
				$configuration.rootpath +
				"theme/bower_components/videogular-themes-default/videogular.css",
			plugins: {
				poster: "http://www.videogular.com/assets/images/videogular.png",
			},
		};
		$scope.activePageTitle = $state.current.title;
		$scope.profile = $rootScope.globals.currentUser;
		$scope.video = null;

		$scope.resetModel = function () {
			$scope.temperature = 0;
			$scope.counter = 10;
			$scope.guncounter = 10;
		};
		$scope.displayAudio = function () {
			if ($rootScope.audiomute == false) {
				result.pause();
				load.pause();
				countdown.pause();
				ircamera.pause();
				error.pause();
				display.currentTime = 0;
				display.play();
			}
			// result.pause();
			// load.pause();
			// countdown.pause();
			// ircamera.pause();
			// error.pause();
			// display.currentTime = 0;
			// display.play();

			// var result = document.getElementById("result");
			// result.pause();
			// var load = document.getElementById("load");
			// load.pause();
			// var countdown = document.getElementById("countdown");
			// countdown.pause();
			// var ircamera = document.getElementById("ircamera");
			// ircamera.pause();
			// var error = document.getElementById("error");
			// error.pause();

			// var display = document.getElementById("display");
			// display.currentTime = 0;
			// display.play();
		};
		$scope.prepare = function () {
			$(".error").hide();
			$(".Loading").hide();
			$(".ResultDevice").hide();
			$(".IRCamera").hide();
			$(".error_camera").hide();
			$(".DisplayDevice").show();
			page = "DisplayDevice";
			$scope.displayAudio();
			$scope.resetModel();
		};
		$scope.ircameraAudio = function () {
			if ($rootScope.audiomute == false) {
				display.pause();
				load.pause();
				countdown.pause();
				result.pause();
				error.pause();
				ircamera.currentTime = 0;
				ircamera.play();
			}
			// var display = document.getElementById("display");
			// display.pause();

			// var ircamera = document.getElementById("ircamera");
			// ircamera.currentTime = 0;
			// ircamera.play();
		};
		$scope.IRCamera = function () {
			$(".error_camera").hide();
			$(".error").hide();
			$(".DisplayDevice").hide();
			$(".Loading").hide();
			$(".ResultDevice").hide();
			$(".IRCamera").show();
			page = "startCamera";
			$scope.ircameraAudio();
		};
		$scope.errorAudio = function () {
			if ($rootScope.audiomute == false) {
				display.pause();
				load.pause();
				result.pause();
				countdown.pause();
				ircamera.pause();
				error.currentTime = 0;
				error.play();
			}
		};
		$scope.error = function () {
			page = "error_camera";

			$(".Loading").hide();
			$(".IRCamera").hide();
			$(".DisplayDevice").hide();
			$(".error").hide();
			$(".ResultDevice").hide();
			$(".error_camera").show();
			clearInterval(initCamera);
			$scope.errorAudio();
			// $("#my_camera").hide();
		};
		$scope.resultAudio = function () {
			if ($rootScope.audiomute == false) {
				display.pause();
				load.pause();
				countdown.pause();
				ircamera.pause();
				error.pause();
				result.currentTime = 0;
				result.play();
			}
		};
		$scope.showResult = function () {
			page = "ResultDevice";

			$(".DisplayDevice").hide();
			$(".DisplayDevice_Info").hide();
			$(".Loading").hide();
			$(".error_camera").hide();
			$(".IRCamera").hide();
			$(".ResultDevice").show();
			$(".Success").hide();
			$(".SuccessError").hide();
			$scope.resultAudio();
			$scope.resetModel();
		};
		$scope.loadAudio = function () {
			if ($rootScope.audiomute == false) {
				display.pause();
				result.pause();
				countdown.pause();
				ircamera.pause();
				error.pause();
				load.currentTime = 0;
				load.play();
			}
		};
		$scope.loading = function () {
			$(".error_camera").hide();
			$(".error").hide();
			$(".DisplayDevice").hide();
			$(".IRCamera").hide();
			$(".ResultDevice").hide();
			$(".Loading").show();
			page = "Loading";
			$scope.loadAudio();
		};
		$scope.countdownAudio = function () {
			if ($rootScope.audiomute == false) {
				display.pause();
				result.pause();
				countdown.pause();
				ircamera.pause();
				error.pause();
				load.pause();
				countdown.currentTime = 0;
				countdown.play();
			}
		};
		var isStart;

		$scope.start = function () {
			$scope.cancelShow = false;
			WebcamService.checkWebcamStatus(function (result) {
				// alert(result.status);
				if (result.status && $scope.cameraActive) {
					if (isStart == false) {
						isStart = true;
						var apistatus = "wait";
						isCallback = true;
						$scope.countdownAudio();
						$scope.counter = 10;
						$scope.overlayShow = false;
						$("#snapshot").hide();
						$("#snapshotcencel").hide();
						devicesService.getDataIRCamera({}, function (result) {
							console.log(result);
							$scope.overlayShow = false;
							// clearInterval(stopped);
							// result.success = true;

							// result.data = [
							// 	{
							// 		value: "35.94",
							// 		picture: "http://128.199.73.202/hii-asset/temp_picture.png",
							// 	},
							// ];
							if (isCallback) {
								// alert(JSON.stringify(result))
								if (result.success == false) {
									// $(".Loading").hide();
									// $(".error").show();
									apistatus = "error";
									// alert('error_if')
									// $scope.errorAudio();
								} else if (result.data[0].value != undefined) {
									console.log(result.data[0]);
									$scope.$apply(function () {
										$scope.data = result.data[0];
										// $scope.data.value = parseFloat($scope.data.value).toFixed(
										// 	1
										// );
										$scope.data.picture =
											$scope.data.picture + "?time=" + new Date().getTime();
										console.log($scope.data);
										$rootScope.globals.information.temperature = result.data[0];
										$rootScope.globals.information.temperature.BTEMPFROMAPI =
											$scope.data.value;
										devicesService.GetSettingTemp(null, function (result) {
											if (result.status) {
												if (result.message.length > 0) {
													if (result.message[0]["TEMP_ACTIVE"] == 1) {
														if (result.message[0]["MATH_SIGN"] == 0) {
															$rootScope.globals.information.temperature.value =
																(
																	$rootScope.globals.information.temperature
																		.value - result.message[0]["TEMP_VALUE"]
																).toFixed(1);
															$rootScope.globals.information.temperature.TEMPCOMPENSATE =
																result.message[0]["TEMP_VALUE_TEXT"];
															$scope.convertResult();
														} else {
															$rootScope.globals.information.temperature.value =
																$rootScope.globals.information.temperature
																	.value + result.message[0]["TEMP_VALUE"];
															$rootScope.globals.information.temperature.TEMPCOMPENSATE =
																result.message[0]["TEMP_VALUE_TEXT"];
															$scope.data.value = (
																parseFloat($scope.data.value) +
																parseFloat(result.message[0]["TEMP_VALUE"])
															).toFixed(1);
															$scope.convertResult();
														}
													} else {
														$scope.data.value = parseFloat(
															$scope.data.value
														).toFixed(1);
														$scope.convertResult();
													}
												} else {
													$scope.data.value = parseFloat(
														$scope.data.value
													).toFixed(1);
													$scope.convertResult();
												}
											} else {
												$scope.data.value = parseFloat(
													$scope.data.value
												).toFixed(1);
												$scope.convertResult();
											}
										});
										// $scope.convertResult();
										// d_serviceService.saveD_service($rootScope.globals);
									});
									apistatus = "ok";
								} else {
									apistatus = "error";
								}
							}
						});
						$scope.countdown(10000, function () {
							$scope.playAudio();
							$scope.videoStop();
							$scope.loading();
							isCallback = true;

							getdata = setInterval(() => {
								console.log(apistatus);

								if (apistatus == "wait") {
								} else if (apistatus == "ok") {
									$scope.showResult();
									clearInterval(getdata);
								} else {
									$(".Loading").hide();
									$(".error").show();
									page = "error_camera";
									$scope.errorAudio();
									clearInterval(getdata);
									console.log(apistatus);
								}
							}, 5000);
						});
					}
				} else {
					$scope.error();
				}
			});
		};

		$scope.skip = function () {
			baseService.nextDevices();
		};

		$scope.onPlayerReady = function (API) {
			$scope.video = API;
			API.stop();

			setTimeout(() => {
				$scope.videoPlay();
			}, 1000);
			// console.log(API)
		};

		$scope.videoPlay = function () {
			// console.log("PLAY");
			// $scope.video.play();
		};
		$scope.backtoprofile = function () {
			$scope.playAudio();
			display.pause();
			result.pause();
			countdown.pause();
			ircamera.pause();
			error.pause();
			load.pause();
			countdown.pause();
			// $location.path("/profile");
			if ($rootScope.globals.usertype == 0) {
				$location.path("/authen");
			} else {
				$location.path("/profile");
			}
		};
		$scope.videoStop = function () {
			// console.log("STOP");
			// $scope.video.stop();
		};
		$scope.recheck = function () {
			page = "DisplayDevice";

			$scope.playAudio();
			$scope.videoPlay();

			$scope.prepare();
			clearInterval(stopped);
			clearInterval(getdata);
			clearInterval(initCamera);
			isCallback = false;
			devicesService.stopDevice(
				{ device: "infraredcameraheatdetection" },
				function (result) {}
			);
		};
		// $scope.prepare();

		$scope.startCamera = function () {
			isStart = false;

			page = "startCamera";

			$scope.overlayShow = true;

			$(".btn-face-cap").hide();
			$("#my_camera").hide();
			$(".camera-loading").hide();
			$("#snapshot").hide();

			// WebcamService.checkWebcamStatus(function (result) {
			// 	// alert(result.status);
			// 	if (result.status) {
			// 		devicesService.getStatus(
			// 			{ device: "infraredcameraheatdetection" },
			// 			function (result) {
			// 				if (result.data) {
			// 					if (result.data[0].status != "success") {
			// 						$scope.error();
			// 					} else {
			// 						$scope.resetModel();
			// 						$scope.playAudio();
			// 						$scope.IRCamera();
			// 						$scope.initCamera();
			// 					}
			// 				} else {
			// 					if (result.status == false) {
			// 						if (result.success == false) {
			// 							$scope.error();
			// 						}
			// 					} else {
			// 						$scope.error();
			// 					}
			// 				}
			// 			}
			// 		);
			// 	} else {
			// 		$scope.error();
			// 	}
			// });
			$scope.resetModel();
			$scope.playAudio();
			$scope.IRCamera();
			$scope.initCamera();
		};
		var initCamera;
		$scope.initCamera = function () {
			// $scope.cameraActive = false;
			// $(".camera-loading").show();

			// Webcam.loaded = false;
			// Webcam.live = false;
			// let count = 0;
			// initCamera = setInterval(() => {
			// 	console.log(Webcam);

			// 	if (count == 0) {

			// 		Webcam.attach("#my_camera");
			// 	} else if (Webcam.live != true) {

			// 		Webcam.attach("#my_camera");
			// 	} else {
			// 		$("#my_camera").fadeIn(3000);
			// 		$(".btn-face-cap").fadeIn(3000);
			// 		$("#snapshot").fadeIn(3000);
			// 		$(".camera-loading").hide();

			// 		$scope.cameraActive = true;
			// 		$scope.$apply();
			// 		clearInterval(initCamera);
			// 	}
			// 	count++;
			// }, 500);
			Webcam.attach("#my_camera");
			$("#my_camera").fadeIn(3000);
			$(".btn-face-cap").fadeIn(3000);
			$("#snapshot").fadeIn(3000);
			$(".camera-loading").hide();
			$scope.cameraActive = true;
		};

		$scope.Cancel = function () {
			$scope.cancelShow = true;
			$scope.playAudio();
			clearInterval(stopped);
			clearInterval(getdata);
			clearInterval(initCamera);
			isCallback = false;
			devicesService.stopDevice(
				{ device: "infraredcameraheatdetection" },
				function (result) {
					// $scope.resetModel();
					$scope.startCamera();
					// $(".DisplayDevice").show();
					// $(".Loading").hide();
					// $(".ResultDevice").hide();
					// $(".IRCamera").hide();
					// $(".error").hide();
				}
			);
		};
		$scope.logout = function () {
			$scope.playAudio();
			display.pause();
			result.pause();
			countdown.pause();
			ircamera.pause();
			error.pause();
			load.pause();
			countdown.pause();
			$location.path("/home");
		};
		$scope.playAudio = function () {
			var audio = new Audio("./assets/kiosk/audio/click.mp3");
			audio.play();
		};
		$scope.init();

		document.onkeypress = function (e) {
			e = e || window.event;
			let key = e.key.toLowerCase();
			switch (page) {
				case "DisplayDevice":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(384, 975).click();
					}
					// if (key == "x" || key=="ป") {
					// 	//yellow
					// 	document.elementFromPoint(951, 969).click();
					// }
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(1545, 978).click();
					}
					break;
				case "startCamera":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(384, 975).click();
					}
					// if (key == "x" || key=="ป") {
					// 	//yellow
					// 	document.elementFromPoint(951, 969).click();
					// }
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(1545, 978).click();
					}
					break;
				case "Loading":
					// if (key == "z" || key=="ผ") {
					// 	//red
					// 	document.elementFromPoint(348, 909).click();
					// }
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(957, 939).click();
					}
					// if (key == "c" || key=="แ") {
					// 	//green
					// 	document.elementFromPoint(852, 903).click();
					// }

					break;
				case "ResultDevice":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(429, 867).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(954, 861).click();
					}
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(1485, 864).click();
					}

					break;
				case "error_device":
					// if (key == "z" || key=="ผ") {
					// 	//red
					// 	document.elementFromPoint(384, 975).click();
					// }
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(942, 870).click();
					}
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(1440, 870).click();
					}
					break;
				case "error_camera":
					// if (key == "z" || key=="ผ") {
					// 	//red
					// 	document.elementFromPoint(384, 975).click();
					// }
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(942, 870).click();
					}
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(1440, 870).click();
					}
					break;

				default:
			}
		};

		var page = "DisplayDevice";
	}
})();
