/**
 * @author a.demeshko
 * created on 12/18/15
 */
(function() {
    "use strict";

    var pageName = "thermometer";
    var pageCtrl = pageName + "Ctrl";
    angular.module("BlurAdmin.pages.devices.thermometer", []).config(routeConfig);
    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider.state("devices." + pageName, {
            url: "/" + pageName,
            templateUrl: "app/pages/devices/thermometer/thermometer.html",
            controller: pageCtrl,
            title: "วัดอุณหภูมิ",
            sidebarMeta: {
                order: 800,
            },
        });
    }
})();