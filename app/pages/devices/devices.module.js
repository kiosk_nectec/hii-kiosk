/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function() {
    "use strict";

    angular
        .module("BlurAdmin.pages.devices", ["ui.select", "ngSanitize"])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state("devices", {
                url: "/devices",
                abstract: true,
                template: '<div ui-view  autoscroll="true" autoscroll-body-top></div>',
                title: "อุปกรณ์",
                sidebarMeta: {
                    icon: "ion-gear-a",
                    order: 4,
                },
            })
            .state("devices.weighingMachines", {
                url: "/weighing-machines",
                templateUrl: "app/pages/devices/weighing-machines/weighingMachines.html",
                controller: "weighingMachinesCtrl",
                controllerAs: "vm",
                title: "วัดน้ำหนัก",
                sidebarMeta: {
                    order: 5,
                },
            })
            .state("devices.bloodPressure", {
                url: "/blood-pressure",
                templateUrl: "app/pages/devices/blood-pressure/bloodPressure.html",
                controller: "bloodPressureCtrl",
                controllerAs: "vm",
                title: "วัดความดันโลหิต",
                sidebarMeta: {
                    order: 5,
                },
            })
            .state("devices.pulseOximeter", {
                url: "/pulse-oximeter",
                templateUrl: "app/pages/devices/pulse-oximeter/pulseOximeter.html",
                controller: "pulseOximeterCtrl",
                controllerAs: "vm",
                title: "วัดเปอร์เซ็นต์ออกซิเจนในเลือด อัตราการเต้นหัวใจ",
                sidebarMeta: {
                    order: 5,
                },
            })
            .state("devices.thermometer", {
                url: "/thermometer",
                templateUrl: "app/pages/devices/thermometer/thermometer.html",
                controller: "thermometerCtrl",
                controllerAs: "vm",
                title: "วัดอุณหภูมิร่างกาย",
                sidebarMeta: {
                    order: 5,
                },
            });;

    }
})();