/**
 * @author a.demeshko
 * created on 12/18/15
 */
(function() {
    "use strict";

    var pageName = "blood-pressure";
    var pageCtrl = pageName + "Ctrl";
    angular.module("BlurAdmin.pages.devices.bloodpressure", []).config(routeConfig);
    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider.state("devices." + pageName, {
            url: "/" + pageName,
            templateUrl: "app/pages/devices/blood-pressure/bloodPressure.html",
            controller: pageCtrl,
            title: "วัดความดันโลหิต",
            sidebarMeta: {
                order: 800,
            },
        });
    }
})();