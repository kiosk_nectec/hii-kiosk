(function () {
	"use strict";

	angular
		.module("BlurAdmin.pages.devices")
		.controller("bloodPressureCtrl", bloodPressureCtrl);

	/** @ngInject */
	function bloodPressureCtrl(
		$scope,
		baseService,
		$rootScope,
		$uibModal,
		$filter,
		$timeout,
		$location,
		$state,
		devicesService,
		$sce,
		$configuration,
		d_serviceService,
		conBloodPressureService,
		conPulseRateService
	) {
		(function initController() {
			setTimeout(() => {
				if ($rootScope.audiomute == true) {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = true;
					}
					console.log(elems, "true");
				} else {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = false;
					}
					console.log(elems, "false");
				}
				var bloodpressurevideo = document.getElementById("bloodpressurevideo");
				console.log(bloodpressurevideo);
				if (typeof bloodpressurevideo.loop == 'boolean') { // loop supported
					bloodpressurevideo.loop = true;
				  } else { // loop property not supported
					bloodpressurevideo.addEventListener('ended', function () {
					  this.currentTime = 0;
					  this.play();
					}, false);
				  }
				bloodpressurevideo.play();
			}, 200);
		})();
		var isCallback = true;
		$scope.conditionDataBloodPressure = [];
		$scope.convertDataBloodPressure = {};
		$scope.bloodPressureStyleEmoji = {};
		$scope.bloodPressureStyleBg = {};
		$scope.bloodPressureStyleDisplay = {};
		$scope.bloodPressureStyleRec = {};
		$scope.bloodPressureStyleRec2 = {};

		$scope.conditionDataPulseRate = [];
		$scope.convertDataPulseRate = {};
		$scope.pulseRateStyleEmoji = {};
		$scope.pulseRateStyleBg = {};
		$scope.pulseRateStyleDisplay = {};
		$scope.pulseRateStyleRec = {};
		$scope.pulseRateStyleNum = {};
		$scope.cancelShow = false;
		$scope.init = function () {
			conBloodPressureService.getConBloodPressure({}, function (result) {
				$scope.conditionDataBloodPressure = result.message;
				console.log($scope.conditionDataBloodPressure);
				// $scope.convertDataBloodPressure = $scope.conditionDataBloodPressure[0]

				// $scope.bloodPressureStyleEmoji = {
				//     'background-image': 'url('+$scope.convertDataBloodPressure.Image+')','width':'200px'
				// }
			});

			conPulseRateService.getConPulseRate({}, function (result) {
				$scope.conditionDataPulseRate = result.message;
				// console.log($scope.conditionDataBloodPressure)
				// $scope.convertDataBloodPressure = $scope.conditionDataBloodPressure[0]

				// $scope.bloodPressureStyleEmoji = {
				//     'background-image': 'url('+$scope.convertDataBloodPressure.Image+')','width':'200px'
				// }
			});
		};
		$scope.backtoprofile = function () {
			$scope.playAudio();
			// $location.path("/profile");
			if ($rootScope.globals.usertype == 0) {
				$location.path("/authen");
			} else {
				$location.path("/profile");
			}
		};
		$scope.convertResult = function () {
			// // $scope.data.systolic = 130
			// $scope.data.diastolic = 0

			var indexofSBP;
			var indexofDBP;

			$scope.conditionDataBloodPressure.forEach(function (item, index) {
				console.log(
					parseFloat(item.SBP_min),
					item.SBP_max,
					$scope.data.systolic,
					parseFloat(item.Importance)
				);

				if (
					parseFloat(item.SBP_min) <= parseFloat($scope.data.systolic) &&
					parseFloat(item.SBP_max) >= parseFloat($scope.data.systolic)
				) {
					$scope.convertDataBloodPressure = item;

					$scope.bloodPressureStyle = JSON.parse(
						$scope.convertDataBloodPressure.Device_style
					);
					$scope.bloodPressureStyleSTL = JSON.parse(
						$scope.convertDataBloodPressure.Device_style
					);
					// Object.assign($scope.bloodPressureStyle, JSON.parse($scope.convertDataBloodPressure.Recommendation_style));

					$scope.bloodPressureStyleEmoji = {
						"background-image":
							"url(" + $scope.convertDataBloodPressure.Image + ")",
					};

					for (const style in $scope.bloodPressureStyle.emojiposition) {
						Object.assign($scope.bloodPressureStyleEmoji, {
							[style]: $scope.bloodPressureStyle.emojiposition[style],
						});
					}
					indexofSBP = item;
					console.log(indexofSBP);

					$rootScope.globals.information.BPCODE = item.Code;
					d_serviceService.saveD_service($rootScope.globals);
				}
				if (
					parseFloat(item.DBP_min) <= parseFloat($scope.data.diastolic) &&
					parseFloat(item.DBP_max) >= parseFloat($scope.data.diastolic)
				) {
					$scope.convertDataBloodPressure = item;

					$scope.bloodPressureStyle = JSON.parse(
						$scope.convertDataBloodPressure.Device_style
					);
					$scope.bloodPressureStyleDTL = JSON.parse(
						$scope.convertDataBloodPressure.Device_style
					);
					// Object.assign($scope.bloodPressureStyle, JSON.parse($scope.convertDataBloodPressure.Recommendation_style));

					$scope.bloodPressureStyleEmoji = {
						"background-image":
							"url(" + $scope.convertDataBloodPressure.Image + ")",
					};

					for (const style in $scope.bloodPressureStyle.emojiposition) {
						Object.assign($scope.bloodPressureStyleEmoji, {
							[style]: $scope.bloodPressureStyle.emojiposition[style],
						});
					}
					indexofDBP = item;
					console.log(indexofDBP);

					$rootScope.globals.information.BPCODE = item.Code;
					d_serviceService.saveD_service($rootScope.globals);
				}
			});

			$scope.conditionDataPulseRate.forEach((item) => {
				//  $scope.data.pulse = 110
				// console.log(parseFloat(item.Min), item.Max, $scope.data.pulse)
				if (
					parseFloat(item.Min) <= parseFloat($scope.data.pulse) &&
					parseFloat(item.Max) >= parseFloat($scope.data.pulse)
				) {
					$scope.convertDataPulseRate = item;

					$scope.pulseRateStyle = JSON.parse(
						$scope.convertDataPulseRate.Device_style
					);

					$scope.pulseRateStyleEmoji = {
						"background-image":
							"url(" + $scope.convertDataPulseRate.Image + ")",
					};
					for (const style in $scope.pulseRateStyle.emojiposition) {
						Object.assign($scope.pulseRateStyleEmoji, {
							[style]: $scope.pulseRateStyle.emojiposition[style],
						});
					}

					$rootScope.globals.information.PRCODE = item.Code;
					d_serviceService.saveD_service($rootScope.globals);
				}
			});
		};

		$scope.data = {};
		$scope.videoConfig = {
			sources: [
				{
					src: $sce.trustAsResourceUrl(
						"assets/vdo/SampleVideo_720x480_2mb.mp4"
					),
					type: "video/mp4",
				},
				// { src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.webm"), type: "video/webm" },
				// { src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.ogg"), type: "video/ogg" }
			],
			tracks: [
				{
					src: "http://www.videogular.com/assets/subs/pale-blue-dot.vtt",
					kind: "subtitles",
					srclang: "en",
					label: "English",
					default: "",
				},
			],

			theme:
				$configuration.rootpath +
				"theme/bower_components/videogular-themes-default/videogular.css",
			plugins: {
				poster: "http://www.videogular.com/assets/images/videogular.png",
			},
		};
		$scope.activePageTitle = $state.current.title;
		$scope.profile = $rootScope.globals.currentUser;
		$scope.usertype = $rootScope.globals.usertype;

		$scope.video = null;

		$scope.resetModel = function () {
			$scope.bloodPressure = "";
		};
		$scope.prepare = function () {
			$(".DisplayDevice").show();
			$(".Loading").hide();
			$(".ResultDevice").hide();
			$scope.resetModel();
		};
		$scope.start = function () {
			page = "Loading";

			$scope.videoStop();
			$(".DisplayDevice").hide();
			$(".Loading").show();
			$(".error").hide();
			$(".ResultDevice").hide();
			$scope.loadAudio();
			isCallback = true;
			$scope.cancelShow = false;
			devicesService.getDataBloodPressure({}, function (result) {
				console.log(result);
				// alert(JSON.stringify(result));
				if (isCallback) {
					page = "error_device";

					if (result.success == false) {
						$(".Loading").hide();
						$(".error").show();
						$scope.errorAudio();
						$scope.stopDevice(function () {});
					} else if (result.data[0].diastolic != undefined) {
						console.log(result.data[0]);
						$scope.$apply(function () {
							page = "ResultDevice";

							$scope.data = result.data[0];
							$scope.convertResult();
							$rootScope.globals.information.bloodPressure = result.data[0];
							d_serviceService.saveD_service($rootScope.globals);
							$(".Loading").hide();
							$(".ResultDevice").show();
							$scope.resultAudio();
						});
						// console.log($scope.weight);
					} else {
						$(".Loading").hide();
						$(".error").show();
						$scope.errorAudio();
						$scope.stopDevice(function () {});
					}
				}
			});
		};
		$scope.skip = function () {
			baseService.nextDevices();
		};

		$scope.onPlayerReady = function (API) {
			$scope.video = API;
			API.stop();

			setTimeout(() => {
				$scope.videoPlay();
			}, 1000);
			// console.log(API)
		};
		$scope.select = function () {
			$scope.playAudio();
			$(".DisplayDevice").hide();
			$(".ResultDevice").show();
			$(".Blood_Info").hide();
			$scope.resetModel();
		};

		$scope.option = function () {
			$scope.playAudio();
			$(".DisplayDevice").hide();
			$(".ResultDevice").show();
			$(".Blood_Info").show();
			$scope.resetModel();
		};

		$scope.Info = function () {
			$scope.playAudio();
			$(".DisplayDevice").hide();
			$(".DisplayDevice_Info").show();
			$(".ResultDevice").hide();
			$(".Blood_Info").hide();
			$scope.resetModel();
		};

		$scope.Back = function () {
			$scope.playAudio();
			$(".DisplayDevice").show();
			$(".DisplayDevice_Info").hide();
			$(".ResultDevice").hide();
			$(".Blood_Info").hide();
			$scope.resetModel();
		};

		$scope.locationpath_W = function () {
			$scope.playAudio();
			$location.path("/devices/weighing-machines");
		};
		$scope.locationpath_B = function () {
			$scope.playAudio();
			$location.path("/devices/blood-pressure");
		};
		$scope.locationpath_P = function () {
			$scope.playAudio();
			$location.path("/devices/pulse-oximeter");
		};
		$scope.locationpath_T = function () {
			$scope.playAudio();
			$location.path("/devices/thermometer");
		};

		$scope.back_select = function () {
			$scope.playAudio();
			$(".DisplayDevice").hide();
			$(".ResultDevice").show();
			$(".Blood_Info").hide();
			$scope.resetModel();
		};

		$scope.videoPlay = function () {
			// console.log("PLAY");
			// $scope.video.play();
		};
		$scope.videoStop = function () {
			// console.log("STOP");
			// $scope.video.stop();
		};
		$scope.recheck = function () {
			page = "DisplayDevice";

			$scope.videoPlay();
			$scope.replayAudio();
			$scope.prepare();
		};
		$scope.prepare();
		$scope.playAudio = function () {
			var audio = new Audio("./assets/kiosk/audio/click.mp3");
			audio.play();

			var load = document.getElementById("load");
			load.pause();
		};
		$scope.Cancel = function () {
			$scope.playAudio();
			isCallback = false;

			page = "DisplayDevice";
			$scope.cancelShow = true;
			$scope.stopDevice(function () {
				$(".DisplayDevice").show();
				$(".Loading").hide();
				$(".ResultDevice").hide();
				$scope.resetModel();
			});
		};
		$scope.stopDevice = function (callback) {
			devicesService.stopDevice({ device: "pressuregauge" }, function (result) {
				callback();
			});
		};
		$scope.logout = function () {
			$scope.playAudio();
			$location.path("/home");
		};

		$scope.init();

		$scope.loadAudio = function () {
			var display = document.getElementById("display");
			display.pause();

			var sample = document.getElementById("load");
			sample.currentTime = 0;
			sample.play();
			var error = document.getElementById("error");
			error.pause();
		};

		$scope.resultAudio = function () {
			var load = document.getElementById("load");
			load.pause();

			var sample = document.getElementById("result");
			sample.currentTime = 0;
			sample.play();
		};

		$scope.replayAudio = function () {
			var result = document.getElementById("result");
			result.pause();

			var display = document.getElementById("display");
			display.currentTime = 0;
			display.play();
		};

		$scope.errorAudio = function () {
			var load = document.getElementById("load");
			load.pause();

			var sample = document.getElementById("error");
			sample.currentTime = 0;
			sample.play();
		};

		document.onkeypress = function (e) {
			e = e || window.event;
			let key = e.key.toLowerCase();
			switch (page) {
				case "DisplayDevice":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(384, 975).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(951, 969).click();
					}
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(1545, 978).click();
					}
					break;
				case "Loading":
					// if (key == "z" || key=="ผ") {
					// 	//red
					// 	document.elementFromPoint(348, 909).click();
					// }
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(957, 939).click();
					}
					// if (key == "c" || key=="แ") {
					// 	//green
					// 	document.elementFromPoint(852, 903).click();
					// }

					break;
				case "ResultDevice":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(429, 867).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(954, 861).click();
					}
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(1485, 864).click();
					}

					break;
				case "error_device":
					// if (key == "z" || key=="ผ") {
					// 	//red
					// 	document.elementFromPoint(384, 975).click();
					// }
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(942, 870).click();
					}
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(1440, 870).click();
					}
					break;

				default:
			}
		};

		var page = "DisplayDevice";
	}
})();
