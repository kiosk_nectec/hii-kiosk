/**
 * @author a.demeshko
 * created on 12/18/15
 */
(function() {
    "use strict";

    var pageName = "pulse-oximeter";
    var pageCtrl = pageName + "Ctrl";
    angular.module("BlurAdmin.pages.devices.bloodpressure", []).config(routeConfig);
    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider.state("devices." + pageName, {
            url: "/" + pageName,
            templateUrl: "app/pages/devices/pulse-oximeter/pulseOximeter.html",
            controller: pageCtrl,
            title: "",
            sidebarMeta: {
                order: 800,
            },
        });
    }
})();