(function () {
	"use strict";

	angular
		.module("BlurAdmin.pages.devices")
		.controller("pulseOximeterCtrl", pulseOximeterCtrl);

	/** @ngInject */
	function pulseOximeterCtrl(
		$scope,
		baseService,
		$rootScope,
		$uibModal,
		$filter,
		$timeout,
		$location,
		$state,
		devicesService,
		$sce,
		$configuration,
		d_serviceService,
		conOxygenService
	) {
		(function initController() {
			setTimeout(() => {
				if ($rootScope.audiomute == true) {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = true;
					}
					console.log(elems, "true");
				} else {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = false;
					}
					console.log(elems, "true");
				}
				var pulseoximetervideo = document.getElementById("pulseoximetervideo");
				console.log(pulseoximetervideo);
				if (typeof pulseoximetervideo.loop == 'boolean') { // loop supported
					pulseoximetervideo.loop = true;
				  } else { // loop property not supported
					pulseoximetervideo.addEventListener('ended', function () {
					  this.currentTime = 0;
					  this.play();
					}, false);
				  }
				pulseoximetervideo.play();
			}, 200);
		})();
		$scope.data = {};

		$scope.conditionDataOxygen = [];
		$scope.convertDataOxygen = {};
		$scope.oxygenStyleEmoji = {};
		var isCallback = true;
		$scope.cancelShow = false;
		// $scope.oxygenStyleBg = {}
		// $scope.oxygenStyleDisplay = {}
		// $scope.oxygenStyleRec = {}
		// $scope.oxygenStyleRec2 = {}
		$scope.init = function () {
			conOxygenService.getConOxygen({}, function (result) {
				$scope.conditionDataOxygen = result.message;
				// console.log($scope.conditionDataOxygen)
				// $scope.convertDataOxygen = $scope.conditionDataOxygen[0]

				// $scope.oxygenStyleEmoji = {
				//     'background-image': 'url('+$scope.convertDataOxygen.Image+')','width':'200px'
				// }
			});
		};

		$scope.convertResult = function () {
			// $scope.data.SpO2 = 80
			$scope.conditionDataOxygen.forEach((item) => {
				console.log(parseFloat(item.Min), item.Max, $scope.data.SpO2);
				if (
					parseFloat(item.Min) <= parseFloat($scope.data.SpO2) &&
					parseFloat(item.Max) >= parseFloat($scope.data.SpO2)
				) {
					$scope.convertDataOxygen = item;

					$scope.oxygenStyle = JSON.parse(
						$scope.convertDataOxygen.Device_style
					);

					$scope.oxygenStyleEmoji = {
						"background-image": "url(" + $scope.convertDataOxygen.Image + ")",
					};

					for (const style in $scope.oxygenStyle.emojiposition) {
						Object.assign($scope.oxygenStyleEmoji, {
							[style]: $scope.oxygenStyle.emojiposition[style],
						});
					}
					console.log($scope.oxygenStyleEmoji);
					// $scope.oxygenStyleBg = {
					//     'background-color': '(' + $scope.convertDataOxygen.Color_code_bg + ')'
					// }

					// $scope.oxygenStyleDisplay = {
					//     'color': '(' + $scope.convertDataOxygen.Color_code_display + ')'
					// }

					// $scope.oxygenStyleRec = {
					//     'color': '(' + $scope.convertDataOxygen.Color_code_rec + ')'
					// }

					// $scope.oxygenStyleRec2 = {
					//     'color': '(' + $scope.convertDataOxygen.Color_code_rec_2 + ')'
					// }

					$rootScope.globals.information.OXYGENCODE = item.Code;
					$rootScope.globals.information.RRCODE = item.Code;
					d_serviceService.saveD_service($rootScope.globals);
				}
			});
		};

		var stopped;
		// var stoppederror;
		// $scope.countdown = function () {

		// 	stopped = $timeout(function () {
		// 		console.log($scope.counter);

		// 		if ($scope.counter > 0) {
		// 			$scope.counter--;
		// 			$scope.countdown();
		// 		}
		// 	}, 1000);
		// };
		$scope.countdown = function (delay, callback) {
			stopped = setInterval(function () {
				delay -= 1000;
				if ($scope.counter > 0) {
					console.log($scope.counter);
					$scope.counter--;
					$scope.$apply();
				}
				if (delay <= -1000) {
					console.log(delay);
					clearInterval(stopped);
					$scope.overlayShow = true;
					$scope.$apply();
					// alert($scope.overlayShow)
					
					callback();
				}
			}, 1000);
		};
		// $scope.countdownerror = function (delay, callback) {
		// 	stoppederror = setInterval(function () {
		// 		delay -= 1000;
		// 		if ($scope.countererror > 0) {
		// 			$scope.countererror--;
		// 			$scope.$apply();
		// 		}
		// 		if (delay <= -1000) {
		// 			clearInterval(stoppederror);
		// 			callback();
		// 		}
		// 	}, 1000);
		// };
		// $scope.countdown = function (delay, callback) {
		// 	stopped = setInterval(function () {
		// 		console.log($scope.counter);
		// 		delay -= 1000;
		// 		if ($scope.counter > 0) {
		// 			$scope.counter--;
		// 			$scope.$apply();
		// 		}
		// 		if (delay <= -1000) {
		// 			$scope.overlayShow = true
		// 			callback();
		// 			clearInterval(stopped);
		// 		}
		// 		if (isCallback == false) {
		// 			clearInterval(stopped);
		// 		}
		// 	}, 1000);
		// };

		$scope.videoConfig = {
			sources: [
				{
					src: $sce.trustAsResourceUrl(
						"assets/vdo/SampleVideo_720x480_2mb.mp4"
					),
					type: "video/mp4",
				},
				// { src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.webm"), type: "video/webm" },
				// { src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.ogg"), type: "video/ogg" }
			],
			tracks: [
				{
					src: "http://www.videogular.com/assets/subs/pale-blue-dot.vtt",
					kind: "subtitles",
					srclang: "en",
					label: "English",
					default: "",
				},
			],

			theme:
				$configuration.rootpath +
				"theme/bower_components/videogular-themes-default/videogular.css",
			plugins: {
				poster: "http://www.videogular.com/assets/images/videogular.png",
			},
		};
		$scope.activePageTitle = $state.current.title;
		$scope.profile = $rootScope.globals.currentUser;
		$scope.usertype = $rootScope.globals.usertype;

		$scope.video = null;

		$scope.resetModel = function () {
			$scope.pulseOximeter = "";
			$scope.pulseRate = "";
			$scope.counter = 15;
			// $scope.countererror = 15;
		};
		$scope.prepare = function () {
			$(".DisplayDevice").show();
			$(".Loading").hide();
			$(".ResultDevice").hide();
			// $scope.resultAudio();
			$scope.resetModel();
		};
		$scope.start = function () {
			$scope.counter = 15;
			// $scope.countererror = 15;
			$scope.overlayShow = false;
			$scope.playAudio();
			$scope.videoStop();
			$(".DisplayDevice").hide();
			$(".Loading").show();
			$(".error").hide();
			$(".error_low").hide();
			$scope.loadAudio();
			isCallback = true;
			$scope.cancelShow = false;
			page = "error_device";

			$scope.countdown(15000, function () {});
			devicesService.getDataPulseOximeter({}, function (result) {
				// 		document.getElementById("erconfirmtext").disabled = true;
				// document.getElementById("erconfirm").disabled = true;
				// $("#erconfirm").addClass("disabledbutton");
				// $("btn-repeat-erconfirm *").disable();
				console.log(result);
				$scope.overlayShow = true;
				clearInterval(stopped);
				// alert(JSON.stringify(isCallback))
				// result.data[0].SpO2 = 80

				if (isCallback) {
					if (result.success == false) {
						$(".Loading").hide();
						$(".error").show();
						// var i;
						// for (i = 0; i < 5; i++) {
						// 	setTimeout(() => {
						// 		conOxygenService.getApiOxygenStatus({}, function (result) {
						// 			console.log(result);
						// 			if(result.status == "success"){
						// 				$scope.start();

						// 			}
						// 		});
						// 	}, 30000);
						// 	// break;
						// }
						$scope.errorAudio();
					} else if (result.data[0].SpO2 <= 89 || result.data[0].SpO2 > 100) {
						$(".Loading").hide();
						$(".error").hide();
						$(".error_low").show();
						// alert("die")
						$scope.errorAudio();
					} else if (result.data[0].SpO2 != undefined) {
						console.log(result.data[0]);
						// $scope.$apply(function() {

						page = "ResultDevice";

						$scope.data = result.data[0];
						$scope.convertResult();
						console.log($scope.data);
						$rootScope.globals.information.pulseOximeter = result.data[0];
						d_serviceService.saveD_service($rootScope.globals);
						// });
						// console.log($scope.weight);
						$(".Loading").hide();
						$(".ResultDevice").show();
						$scope.resultAudio();
					} else {
						$(".Loading").hide();
						$(".error_low").hide();
						$(".error").show();
						$scope.errorAudio();
					}
				}
			});
			// $scope.countdownerror(17000,function () {
			// 	isCallback=false
			// 	clearInterval(stoppederror);
			// 	$(".Loading").hide();
			// 	$(".error").show();
			// 	$scope.errorAudio();
			// });
		};

		$scope.skip = function () {
			baseService.nextDevices();
		};

		$scope.onPlayerReady = function (API) {
			$scope.video = API;
			API.stop();

			setTimeout(() => {
				$scope.videoPlay();
			}, 1000);
			// console.log(API)
		};

		$scope.Info = function () {
			$scope.playAudio();
			$(".DisplayDevice").hide();
			$(".DisplayDevice_Info").show();
			$(".ResultDevice").hide();
			$(".Blood_Info").hide();
			$scope.resetModel();
		};

		$scope.Back = function () {
			$scope.playAudio();
			$(".DisplayDevice").show();
			$(".DisplayDevice_Info").hide();
			$(".ResultDevice").hide();
			$(".Blood_Info").hide();
			$scope.resetModel();
		};

		$scope.locationpath_W = function () {
			$scope.playAudio();
			$location.path("/devices/weighing-machines");
		};
		$scope.locationpath_B = function () {
			$scope.playAudio();
			$location.path("/devices/blood-pressure");
		};
		$scope.locationpath_P = function () {
			$scope.playAudio();
			$location.path("/devices/pulse-oximeter");
		};
		$scope.locationpath_T = function () {
			$scope.playAudio();
			$location.path("/devices/thermometer");
		};
		$scope.videoPlay = function () {
			// console.log("PLAY");
			// $scope.video.play();
		};
		$scope.videoStop = function () {
			// console.log("STOP");
			// $scope.video.stop();
		};
		$scope.recheck = function () {
			page = "DisplayDevice";

			$scope.playAudio();
			$scope.replayAudio();
			$scope.videoPlay();
			$scope.prepare();
		};
		$scope.prepare();

		$scope.Cancel = function () {
			$scope.overlayShow = true;
			$scope.cancelShow = true;
			clearInterval(stopped);
			$scope.playAudio();
			isCallback = false;
			devicesService.stopDevice(
				{ device: "bloodoxygenmeter" },
				function (result) {
					$(".DisplayDevice").show();
					$(".Loading").hide();
					$(".ResultDevice").hide();
					$scope.resetModel();
				}
			);
			// $scope.overlayShow = true;
		};

		$scope.logout = function () {
			$scope.playAudio();
			$location.path("/home");
		};

		$scope.backtoprofile = function () {
			$scope.playAudio();
			// $location.path("/profile");
			if ($rootScope.globals.usertype == 0) {
				$location.path("/authen");
			} else {
				$location.path("/profile");
			}
		};

		$scope.playAudio = function () {
			var audio = new Audio("./assets/kiosk/audio/click.mp3");
			audio.play();
			var load = document.getElementById("load");
			load.pause();
		};

		$scope.init();

		$scope.loadAudio = function () {
			var display = document.getElementById("display");
			display.pause();
			var error = document.getElementById("error");
			error.pause();

			var sample = document.getElementById("load");
			sample.currentTime = 0;
			sample.play();
		};

		$scope.resultAudio = function () {
			var load = document.getElementById("load");
			load.pause();

			var sample = document.getElementById("result");
			sample.currentTime = 0;
			sample.play();
		};

		$scope.replayAudio = function () {
			var result = document.getElementById("result");
			result.pause();

			var sample = document.getElementById("display");
			sample.currentTime = 0;
			sample.play();
		};

		$scope.errorAudio = function () {
			var load = document.getElementById("load");
			load.pause();

			var sample = document.getElementById("error");
			sample.currentTime = 0;
			sample.play();
		};

		document.onkeypress = function (e) {
			e = e || window.event;
			let key = e.key.toLowerCase();
			switch (page) {
				case "DisplayDevice":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(384, 975).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(951, 969).click();
					}
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(1545, 978).click();
					}
					break;
				case "Loading":
					// if (key == "z" || key=="ผ") {
					// 	//red
					// 	document.elementFromPoint(348, 909).click();
					// }
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(957, 939).click();
					}
					// if (key == "c" || key=="แ") {
					// 	//green
					// 	document.elementFromPoint(852, 903).click();
					// }

					break;
				case "ResultDevice":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(429, 867).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(954, 861).click();
					}
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(1485, 864).click();
					}

					break;
				case "error_device":
					// if (key == "z" || key=="ผ") {
					// 	//red
					// 	document.elementFromPoint(384, 975).click();
					// }
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(942, 870).click();
					}
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(1440, 870).click();
					}
					break;

				default:
			}
		};

		var page = "DisplayDevice";
	}
})();
