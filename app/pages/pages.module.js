/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
;(function () {
  'use strict'

  angular
    .module('BlurAdmin.pages', [
      'BlurAdmin.pages.home',
      'BlurAdmin.pages.authen',
      'BlurAdmin.pages.profile',
      'BlurAdmin.pages.devices',
      'BlurAdmin.pages.results',
      'BlurAdmin.pages.idcard',
      'BlurAdmin.pages.authen_admin',
      'BlurAdmin.pages.authen_password',
      'BlurAdmin.pages.authen_qr',
      'BlurAdmin.pages.staff',
      'BlurAdmin.pages.staff_set',
      'BlurAdmin.pages.staff_face_regis',
      'BlurAdmin.pages.staff_card_regis',
      'BlurAdmin.pages.staff_new_regis',
      'BlurAdmin.pages.staff_list',
      'BlurAdmin.pages.other_services',
      'BlurAdmin.pages.detail',
      'BlurAdmin.pages.numpad',
      'BlurAdmin.pages.face',
      'BlurAdmin.pages.results_unknown',
      'BlurAdmin.pages.profile_new',
      'BlurAdmin.pages.error_device',
    ])
    .config(routeConfig)
    .run(run)

  /** @ngInject */
  function routeConfig($urlRouterProvider) {
    $urlRouterProvider.otherwise('home')
  }

  run.$inject = [
    '$rootScope',
    '$location',
    '$cookies',
    '$http',
    '$state',
    '$timeout',
    '$document',
    'devicesService',
    'BackgroundService',
  ]

  function run(
    $rootScope,
    $location,
    $cookies,
    $http,
    $state,
    $timeout,
    $document,
    devicesService,
    BackgroundService,
  ) {
    var history = []
    // keep user logged in after page refresh
    $rootScope.globals = $cookies.getObject('globals') || {}
    if ($rootScope.globals.currentUser) {
      $http.defaults.headers.common['Authorization'] =
        'Basic ' + $rootScope.globals.currentUser.authdata
    }

    $rootScope.audiomute = false
    // console.log($rootScope.audiomute)
    var baseurl = window.location.origin + window.location.pathname
    // Timeout timer value
    var TimeOutTimerValue = 60000
    var url
    var power
    devicesService.GetSettingScreen(null, function (result) {
      if (result.status == true) {
        var Listscreen = result.message
        if (Listscreen.length > 0) {
          // // POWER = 0 คือ ปิด POWER = 1 คือ เปิด
          if (Listscreen[0]['POWER'] == 1) {
            power = true
          } else {
            power = false
          }
          // // TYPE = 0 คือ image TYPE = 1 คือ video
          // if (Listscreen[0]["TYPE"] == 1) {
          // 	url = baseurl + "backgroundvideo/";
          // } else {
          // 	url = baseurl + "backgroundimage/";
          // }
          TimeOutTimerValue = parseFloat(Listscreen[0]['TIMER']) * 60000
        }
      } else {
      }
    })

    // Start a timeout
    var TimeOut_Thread = $timeout(function () {
      LogoutByTimer()
    }, TimeOutTimerValue)
    var bodyElement = angular.element($document)
    angular.forEach(
      [
        'keydown',
        'keyup',
        'click',
        'mousemove',
        'DOMMouseScroll',
        'mousewheel',
        'mousedown',
        'touchstart',
        'touchmove',
        'scroll',
        'focus',
      ],
      function (EventName) {
        bodyElement.bind(EventName, function (e) {
          if (undefined != $rootScope.TimeOutscreen) {
            TimeOutTimerValue = parseFloat($rootScope.TimeOutscreen) * 60000
            delete $rootScope['TimeOutscreen']
          } else {
          }
          // console.log($rootScope.audiomute)
          if ($rootScope.audiomute == true) {
            var elems = document.querySelectorAll('video, audio')
            for (var i = 0; i < elems.length; ++i) {
              elems[i].muted = true
            }
          } else {
            var elems = document.querySelectorAll('video, audio')
            for (var i = 0; i < elems.length; ++i) {
              elems[i].muted = false
            }
          }
          //console.log(url);
          //console.log(e);
          TimeOut_Resetter(e)
        })
      },
    )
    function LogoutByTimer() {
      devicesService.GetSettingScreen(null, function (result) {
        if (result.status == true) {
          var Listscreen = result.message
          if (Listscreen.length > 0) {
            TimeOutTimerValue = parseFloat(Listscreen[0]['TIMER']) * 60000
            // POWER = 0 คือ ปิด POWER = 1 คือ เปิด
            if (Listscreen[0]['POWER'] == 1) {
              devicesService.SetWeighingmachine(
                { func: 'setzero', action: 'ScreenServer' },
                function (result) {},
              )
              // TYPE = 0 คือ image TYPE = 1 คือ video
              if (Listscreen[0]['TYPE'] == 1) {
                url = baseurl + 'backgroundvideo/'
              } else {
                url = baseurl + 'backgroundimage/'
              }
              // console.log(TimeOutTimerValue);
              // console.log("on power");
              BackgroundService.BackUpData(null, function (result) {})
              BackgroundService.alert_line_notify(null, function (result) {
                location.href = url
              })

              // power = true;
            } else {
              // console.log(TimeOutTimerValue);
              // console.log("off power");
              TimeOut_Resetter()
              // power = false;
            }
          }
        } else {
          TimeOut_Resetter()
        }
      })
      // if (power == true) {
      // 	console.log("on power");
      // 	location.href = url;
      // } else {
      // 	TimeOut_Resetter(e);
      // 	console.log("off power");
      // }
      // location.href = baseurl+"backgroundvideo/";
      // location.href = baseurl + "backgroundimage/";
      ///////////////////////////////////////////////////
      /// redirect to another page(eg. Login.html) here
      ///////////////////////////////////////////////////
    }
    function TimeOut_Resetter(e) {
      /// Stop the pending timeout
      $timeout.cancel(TimeOut_Thread)

      /// Reset the timeout
      TimeOut_Thread = $timeout(function () {
        LogoutByTimer()
      }, TimeOutTimerValue)
    }

    $rootScope.$on('$routeChangeStart', function (evt) {
      lastDigestRun = Date.now()
    })

    // console.log($rootScope.globals);
    $rootScope.$on('$locationChangeStart', function (event, next, current) {
      // redirect to login page if not logged in and trying to access a restricted page
      // var restrictedPage = $.inArray($location.path(), ['/home', '/register']) === -1;
      var restrictedPage =
        $.inArray($location.path(), [
          '',
          '/home',
          '/authen',
          '/idcard',
          '/authen_admin',
          '/authen_password',
          '/authen_qr',
          '/staff',
          '/staff_set',
          '/staff_face_regis',
          '/staff_card_regis',
          '/staff_new_regis',
          '/staff_list',
          '/other_services',
          '/detail',
          '/numpad',
          '/face',
          '/results_unknown',
          '/error_device',
          '/profile_new',
        ]) === -1
      var loggedIn = $rootScope.globals.currentUser
      // console.log(restrictedPage, !loggedIn);
      if (restrictedPage && !loggedIn) {
        $state.go('authen')

        $location.path('/authen')
      }
    })

    document.onmousemove = getCursorXY
    function getCursorXY(e) {
      let x = window.Event
        ? e.pageX
        : event.clientX +
          (document.documentElement.scrollLeft
            ? document.documentElement.scrollLeft
            : document.body.scrollLeft)
      let y = window.Event
        ? e.pageY
        : event.clientY +
          (document.documentElement.scrollTop
            ? document.documentElement.scrollTop
            : document.body.scrollTop)
      // console.log(x, y);
    }

    $rootScope.authenPage = function () {
      $rootScope.playAudio()
      $rootScope.homeAudio()
      var prevUrl = history.length > 1 ? history.splice(-2)[0] : '/'
      // console.log(history);
      $location.path('/authen')
      // location.href = "http://localhost/kios-nectec/backgroundvideo/";
    }

    $rootScope.$on('$locationChangeSuccess', function (
      evt,
      absNewUrl,
      absOldUrl,
    ) {
      // console.log('success', evt, absNewUrl, absOldUrl);
      history.push($location.$$path)
    })

    $rootScope.back = function () {
      var prevUrl = history.length > 1 ? history.splice(-2)[0] : '/'
      // console.log(history);
      // $location.path(prevUrl);
      $location.path('/profile')
    }

    $rootScope.playAudio = function () {
      var audio = new Audio('./assets/kiosk/audio/click.mp3')
      audio.play()
    }

    $rootScope.homeAudio = function () {
      var sample = document.getElementById('display')
      sample.pause()
    }
    navigator.getMedia =
      navigator.getUserMedia || // use the proper vendor prefix
      navigator.webkitGetUserMedia ||
      navigator.mozGetUserMedia ||
      navigator.msGetUserMedia
    let camera

    // navigator.getMedia(
    // 	{ video: true },
    // 	function () {
    // 		camera = setInterval(() => {
    // 			console.log(Webcam);

    // 			if (Webcam.live != true) {
    // 				// $("#my_camera").show();
    // 				// Webcam.attach("#my_camera");
    // 			} else {
    // 				// $rootScope.cameraActive = true;

    // 				clearInterval(camera);
    // 			}
    // 		},500);
    // 	},
    // 	function () {
    // 		// onComplete.call(this, { status: false });
    // 		// webcam is not available
    // 	}
    // );
  }
})()
