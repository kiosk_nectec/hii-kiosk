(function() {
    "use strict";

    angular
        .module("BlurAdmin.pages.error_device")
        .controller("error_deviceCtrl", error_deviceCtrl)
        .component("apiErrorPage", {
            templateUrl: 'app/pages/error_device/error_device.html',
            controller: error_deviceCtrl,
            bindings: {
                txtSkip: "=",
                txtRetry: "=",
                funcSkip: "&",
                funcRetry: "&",
            }
        });

    /** @ngInject */
    function error_deviceCtrl(
        $scope,
        baseService,
        $rootScope,
        $uibModal,
        $filter,
        $timeout,
        $location,
        $state,
        devicesService,
        $sce,
        $configuration,
        AuthenticationService
    ) {

        var ctrl = this;
        console.log(ctrl)


    }


})();