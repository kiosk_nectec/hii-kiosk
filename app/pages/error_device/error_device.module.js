/**
 * @author a.demeshko
 * created on 12/18/15
 */
(function() {
    "use strict";

    var pageName = "error_device";
    var pageCtrl = pageName + "Ctrl";
    angular.module("BlurAdmin.pages.error_device", []).config(routeConfig);
    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider.state(pageName, {
            url: "/" + pageName,
            templateUrl: "app/pages/error_device/error_device.html",
            controller: pageCtrl,
            title: "เข้าสู่ระบบด้วยบัตรประชาชน",
            sidebarMeta: {
                icon: "ion-gear-a",
                order: 3,
            },
        });
    }
})();