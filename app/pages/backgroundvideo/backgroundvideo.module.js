/**
 * @author a.demeshko
 * created on 12/18/15
 */
(function() {
    "use strict";

    var pageName = "backgroundvideo";
    var pageCtrl = pageName + "Ctrl";
    angular.module("BlurAdmin.pages.backgroundvideo", []).config(routeConfig);
    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider.state(pageName, {
            url: "/" + pageName,
            templateUrl: "app/pages/backgroundvideo/backgroundvideo.html",
            controller: pageCtrl,
            title: "กรุณาเลือกรูปแบบการเข้าใช้",
            sidebarMeta: {
                icon: "ion-gear-a",
                order: 3,
            },
        });
    }
})();