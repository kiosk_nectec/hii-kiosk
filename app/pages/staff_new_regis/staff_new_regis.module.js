/**
 * @author a.demeshko
 * created on 12/18/15
 */
(function() {
    "use strict";

    var pageName = "staff_new_regis";
    var pageCtrl = pageName + "Ctrl";
    angular.module("BlurAdmin.pages.staff_new_regis", []).config(routeConfig);
    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider.state(pageName, {
            url: "/" + pageName,
            templateUrl: "app/pages/staff_new_regis/staff_new_regis.html",
            controller: pageCtrl,
            title: "คุณลุง ประสิทธิ คงความดี",
            sidebarMeta: {
                icon: "ion-gear-a",
                order: 3,
            },
        });
    }
})();