(function () {
	"use strict";

	angular
		.module("BlurAdmin.pages.staff_new_regis")
		.controller("staff_new_regisCtrl", homeCtrl);

	/** @ngInject */
	function homeCtrl(
		$scope,
		baseService,
		$rootScope,
		$uibModal,
		$filter,
		$timeout,
		$location,
		$state
	) {
		(function initController() {
			baseService.devicesActiveList();
			setTimeout(() => {
				if ($rootScope.audiomute == true) {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = true;
					}
				} else {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = false;
					}
				}
			}, 100);
		})();
		$scope.activePageTitle = $state.current.title;
		$scope.staff_new_regis = $rootScope.globals.currentUser;

		$scope.progressFunction = function () {
			return $timeout(function () {}, 3000);
		};
		$scope.next = function () {
			// $location.path('/devices/weighing-machines');
			baseService.nextDevices();
		};

		// $scope.cancel = function() {
		//     $location.path('/authen');
		// }

		$scope.Home = function () {
			$location.path("/authen");
		};

		$scope.LoginBypassword = function () {
			$location.path("/authen_password");
		};

		$scope.qrBypassword = function () {
			$location.path("/authen_qr");
		};

		$scope.Back = function () {
			$location.path("/authen");
		};
	}
})();
