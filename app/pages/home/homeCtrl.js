(function () {
	"use strict";

	angular.module("BlurAdmin.pages.home").controller("homeCtrl", homeCtrl);

	/** @ngInject */
	function homeCtrl(
		$scope,
		$rootScope,
		baseService,
		AuthenticationService,
		$uibModal,
		$filter,
		$timeout,
		$location,
		$state
	) {
		(function initController() {
			// reset login status
			AuthenticationService.ClearCredentials();
			setTimeout(() => {
				if ($rootScope.audiomute == true) {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = true;
					}
				} else {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = false;
					}
				}
			}, 100);
			$scope.chkaudio = $rootScope.audiomute;
		})();

		$scope.authen_adminByHome = function () {
			// AuthenticationService.setProfile({ name: "test name", sex: "ชาย", age: "60" });

			// setTimeout(function () {
			//     $scope.audio();
			// }, 1000);

			$scope.playAudio();
			$location.path("/authen_admin");
		};

		$scope.OnOffAudio = function () {
			$scope.playAudio();
			if ($rootScope.audiomute == true) {
				$rootScope.audiomute = false;
			} else {
				$rootScope.audiomute = true;
			}
		};

		$scope.playAudio = function () {
			var audio = new Audio("./assets/kiosk/audio/click.mp3");
			audio.play();
		};

		document.onkeypress = function (e) {
			e = e || window.event;
			let key = e.key.toLowerCase();
			if (e.key.toLowerCase() == "z" || key == "ผ") {
				// document.getElementById("myCheck").click();
			}
			if (e.key.toLowerCase() == "x" || key == "ป") {
				// document.getElementById("myCheck").click();
			}
			if (e.key.toLowerCase() == "c" || key == "แ") {
				document.getElementById("start").click();
			}
		};
	}
})();
