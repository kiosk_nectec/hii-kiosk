/**
 * @author a.demeshko
 * created on 12/18/15
 */
(function() {
    "use strict";

    var pageName = "home";
    var pageCtrl = pageName + "Ctrl";
    angular.module("BlurAdmin.pages.home", []).config(routeConfig);
    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider.state(pageName, {
            url: "/" + pageName,
            templateUrl: "app/pages/home/home.html",
            controller: pageCtrl,
            title: "Home",
            sidebarMeta: {
                icon: "ion-gear-a",
                order: 0,
            },
        });
    }
})();