(function () {
	"use strict";

	angular
		.module("BlurAdmin.pages.profile_new")
		.controller("profile_newCtrl", homeCtrl);

	/** @ngInject */
	function homeCtrl(
		$scope,
		baseService,
		$rootScope,
		$uibModal,
		$filter,
		$timeout,
		$location,
		$state
	) {
		(function initController() {
			baseService.devicesActiveList();
			setTimeout(() => {
				if ($rootScope.audiomute == true) {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = true;
					}
				} else {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = false;
					}
				}
			}, 100);
		})();
		$scope.activePageTitle = $state.current.title;
		$scope.profile = $rootScope.globals.currentUser;

		$scope.progressFunction = function () {
			return $timeout(function () {}, 3000);
		};
		$scope.authen_adminByProfile = function () {
			$location.path("/authen_admin");
		};
		$scope.next = function () {
			// $location.path('/devices/weighing-machines');
			baseService.nextDevices();
		};

		$scope.logout = function () {
			$location.path("/authen");
		};
		$scope.other_services = function () {
			$location.path("/other_services");
		};

		$scope.select = function () {
			$(".DisplayDevice").hide();
			$(".Select_Weighing").show();
			$(".Select_Option").hide();
			$scope.resetModel();
		};

		$scope.option = function () {
			$(".DisplayDevice").hide();
			$(".Select_Weighing").show();
			$(".Select_Option").show();
			$scope.resetModel();
		};

		$scope.back_select = function () {
			$(".DisplayDevice").hide();
			$(".Select_Weighing").show();
			$(".Select_Option").hide();
			$scope.resetModel();
		};

		$scope.Cancel = function () {
			$(".DisplayDevice").show();
			$(".Select_Weighing").hide();
			$(".Select_Option").hide();
			$scope.resetModel();
		};
	}
})();
