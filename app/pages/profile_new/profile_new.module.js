/**
 * @author a.demeshko
 * created on 12/18/15
 */
(function() {
    "use strict";

    var pageName = "profile_new";
    var pageCtrl = pageName + "Ctrl";
    angular.module("BlurAdmin.pages.profile_new", []).config(routeConfig);
    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider.state(pageName, {
            url: "/" + pageName,
            templateUrl: "app/pages/profile_new/profile_new.html",
            controller: pageCtrl,
            title: "",
            sidebarMeta: {
                icon: "ion-gear-a",
                order: 3,
            },
        });
    }
})();