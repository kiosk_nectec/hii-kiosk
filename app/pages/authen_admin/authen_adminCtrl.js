(function () {
	"use strict";

	angular
		.module("BlurAdmin.pages.authen_admin")
		.controller("authen_adminCtrl", homeCtrl);

	/** @ngInject */
	function homeCtrl(
		$scope,
		baseService,
		$rootScope,
		$uibModal,
		$filter,
		$timeout,
		$location,
		$state
	) {
		(function initController() {
			baseService.devicesActiveList();
			setTimeout(() => {
				if ($rootScope.audiomute == true) {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = true;
					}
				} else {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = false;
					}
				}
			}, 100);
		})();
		$scope.activePageTitle = $state.current.title;
		$scope.authen_admin = $rootScope.globals.currentUser;

		// $scope.resetModel = function () {
		//     $scope.number1 = ""
		//     $scope.number2 = ""
		//     $scope.number3 = ""
		//     $scope.number4 = ""
		//     $scope.number5 = ""
		//     $scope.number6 = ""
		// }

		$scope.progressFunction = function () {
			return $timeout(function () {}, 3000);
		};
		$scope.next = function () {
			$scope.playAudio();
			// $location.path('/devices/weighing-machines');
			baseService.nextDevices();
		};

		// $scope.cancel = function() {
		//     $location.path('/authen');
		// }

		$scope.Home = function () {
			$scope.playAudio();
			$location.path("/authen");
		};

		$scope.LoginBypassword = function () {
			$scope.playAudio();
			$location.path("/authen_password");
		};

		$scope.qrBypassword = function () {
			$scope.playAudio();
			$location.path("/authen_qr");
		};

		$scope.Back = function () {
			page = "DisplayDevice";
			$scope.playAudio();
			$location.path("/authen");
		};

		$scope.playAudio = function () {
			var audio = new Audio("./assets/kiosk/audio/click.mp3");
			audio.play();
		};
		document.onkeypress = function (e) {
			e = e || window.event;
			let key = e.key.toLowerCase();
			switch (page) {
				case "DisplayDevice":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(632, 528).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(960, 996).click();
					}
					if (key == "c" || key == "แ") {
						// green
						document.elementFromPoint(1304, 516).click();
					}
				default:
			}
		};
		var page = "DisplayDevice";
	}
})();
