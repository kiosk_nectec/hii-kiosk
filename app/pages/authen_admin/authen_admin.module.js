/**
 * @author a.demeshko
 * created on 12/18/15
 */
(function() {
    "use strict";

    var pageName = "authen_admin";
    var pageCtrl = pageName + "Ctrl";
    angular.module("BlurAdmin.pages.authen_admin", []).config(routeConfig);
    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider.state(pageName, {
            url: "/" + pageName,
            templateUrl: "app/pages/authen_admin/authen_admin.html",
            controller: pageCtrl,
            title: "กรุณาเลือกรูปแบบการเข้าใช้",
            sidebarMeta: {
                icon: "ion-gear-a",
                order: 3,
            },
        });
    }
})();