/**
 * @author a.demeshko
 * created on 12/18/15
 */
(function() {
    "use strict";

    var pageName = "idcard";
    var pageCtrl = pageName + "Ctrl";
    angular.module("BlurAdmin.pages.idcard", []).config(routeConfig);
    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider.state(pageName, {
            url: "/" + pageName,
            templateUrl: "app/pages/idcard/idcard.html",
            controller: pageCtrl,
            title: "เข้าสู่ระบบด้วยบัตรประชาชน",
            sidebarMeta: {
                icon: "ion-gear-a",
                order: 3,
            },
        });
    }
})();