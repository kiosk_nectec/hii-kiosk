(function () {
	"use strict";

	angular.module("BlurAdmin.pages.idcard").controller("idcardCtrl", idcardCtrl);

	/** @ngInject */
	function idcardCtrl(
		$scope,
		baseService,
		$rootScope,
		$uibModal,
		$filter,
		$timeout,
		$location,
		$state,
		devicesService,
		$sce,
		$configuration,
		AuthenticationService,
		personService
	) {
		(function initController() {
			setTimeout(() => {
				if ($rootScope.audiomute == true) {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = true;
					}
					console.log(elems, "true");
				} else {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = false;
					}
					console.log(elems, "false");
				}
				var insertcardvideo = document.getElementById("insertcardvideo");
				if (typeof insertcardvideo.loop == "boolean") {
					// loop supported
					insertcardvideo.loop = true;
				} else {
					// loop property not supported
					insertcardvideo.addEventListener(
						"ended",
						function () {
							this.currentTime = 0;
							this.play();
						},
						false
					);
				}
				insertcardvideo.play();
				$scope.chkReaderFlag();
			}, 100);
		})();
		var myVar;
		$scope.chkReaderFlag = function () {
			if (page == "DisplayDevice") {
				var i = 1;

				myVar = setInterval(function () {
					devicesService.chkReaderFlag(null, function (result) {
						if (result.status == true) {
							if (i == 20) {
								clearInterval(myVar);
							} else {
								if (result.status == true) {
									if (result.message[0]["reader_flag"] == "Y") {
										$scope.start();
									} else {
									}
								} else {
								}
								i = i + 1;
							}
						} else {
							clearInterval(myVar);
						}
					});
				}, 5000);
			}
		};
		// 		setTimeout(() => {
		// var insertcardvideo = document.getElementById("insertcardvideo");
		// 				if (typeof insertcardvideo.loop == 'boolean') { // loop supported
		// 					insertcardvideo.loop = true;
		// 				  } else { // loop property not supported
		// 					insertcardvideo.addEventListener('ended', function () {
		// 					  this.currentTime = 0;
		// 					  this.play();
		// 					}, false);
		// 				  }
		// 				insertcardvideo.play();}, 1);
		var isCallback = true;
		$scope.cancelShow = false;
		$scope.videoConfig = {
			sources: [
				{
					src: $sce.trustAsResourceUrl(
						"assets/vdo/SampleVideo_720x480_2mb.mp4"
					),
					type: "video/mp4",
				},
				// { src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.webm"), type: "video/webm" },
				// { src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.ogg"), type: "video/ogg" }
			],
			tracks: [
				{
					src: "http://www.videogular.com/assets/subs/pale-blue-dot.vtt",
					kind: "subtitles",
					srclang: "en",
					label: "English",
					default: "",
				},
			],

			theme:
				$configuration.rootpath +
				"theme/bower_components/videogular-themes-default/videogular.css",
			plugins: {
				poster: "http://www.videogular.com/assets/images/videogular.png",
			},
		};
		$scope.activePageTitle = $state.current.title;

		$scope.profile = $rootScope.globals.currentUser;
		// console.log($scope.profile);
		$scope.conditionflag;

		$scope.video = null;

		$scope.resetModel = function () {};
		$scope.loginByIdcard = function () {
			var sample = document.getElementById("load");
			sample.pause();
			sample.currentTime = 0;
			personService.ChkConditionFlagPerson(
				{ CID: $scope.profile.id_card },
				function (result) {
					if (result.status == true) {
						if (result.message == 0) {
							$scope.profile.CONDITION_FLAG = true;
							$scope.playAudio();
							$(".DisplayDevice").hide();
							$(".Loading").hide();
							$(".ResultDevice").hide();
							$(".Condition").show();
							document.querySelector(".box-text-detail-idcard").focus();
							page = "Condition";
							$scope.conditionAudio();
							$scope.resetModel();
						} else {
							$scope.agree();
							$(".DisplayDevice").hide();
							$(".Loading").hide();
							$(".ResultDevice").hide();
							$(".Condition").hide();
							$(".Confirm").show();
							page = "Confirm";
							$scope.confirmAudio();
							$scope.resetModel();
						}
					}
				}
			);
			// console.log($scope.profile);
		};
		$scope.prepare = function () {
			// $scope.playAudio();
			page = "DisplayDevice";
			$(".DisplayDevice").show();
			$(".Loading").hide();
			$(".ResultDevice").hide();
			$scope.resetModel();
		};

		$scope.dateDiff = function (startdate, enddate) {
			//define moments for the startdate and enddate
			var startdateMoment = moment(startdate).subtract(543, "year");
			var enddateMoment = moment(enddate);
			// console.log(enddateMoment);

			if (
				startdateMoment.isValid() === true &&
				enddateMoment.isValid() === true
			) {
				//getting the difference in years
				var years = enddateMoment.diff(startdateMoment, "years");

				//moment returns the total months between the two dates, subtracting the years
				var months = enddateMoment.diff(startdateMoment, "months") - years * 12;

				//to calculate the days, first get the previous month and then subtract it
				startdateMoment.add(years, "years").add(months, "months");
				var days = enddateMoment.diff(startdateMoment, "days");

				return {
					years: years,
					months: months,
					days: days,
				};
			} else {
				return undefined;
			}
		};

		$scope.start = function () {
			clearInterval(myVar);
			$scope.playAudio();
			$scope.videoStop();
			page = "Loading";
			$(".DisplayDevice").hide();
			$(".Confirm").hide();
			$(".error_idcard").hide();
			$(".Loading").show();
			$scope.loadAudio();
			// $(".error_idcard").show();
			isCallback = true;
			$scope.cancelShow = false;
			devicesService.getDataIdcardReader(null, function (result) {
				if (isCallback) {
					// console.log(result.id_card);
					// result = {
					// 	id_card: "3110100670974",
					// 	titlenameth: "นาง",
					// 	firstnameth: "ละเอียด",
					// 	lastnameth: "ลาภอุทัยกาญจน์",
					// 	titlenameEn: "Mrs.",
					// 	firstnameEn: "Laaiad",
					// 	lastnameEn: "Laputhaikarn",
					// 	birthday: "24940000",
					// 	home: "69/788",
					// 	moo: "หมู่ที่ 3",
					// 	trok: "",
					// 	soi: "",
					// 	road: "",
					// 	tumbon: "ตำบลบึงยี่โถ",
					// 	amphoe: "อำเภอธัญบุรี",
					// 	province: "จังหวัดปทุมธานี",
					// 	photo: "http://127.0.0.1/hii-asset/3110100670974.png",
					// };
					if (result.id_card != undefined) {
						// alert(JSON.stringify(result))
						// $scope.$apply(function() {
						$scope.profile = result;
						
						$scope.Y = result.birthday.substr(0, 4);
						$scope.M = result.birthday.substr(4, 2);
						$scope.D = result.birthday.substr(6, 2);
						if ($scope.M == "00") {
							$scope.M = "01";
						}
						if ($scope.D == "00") {
							$scope.D = "01";
						}
						result.birthday = $scope.Y + $scope.M + $scope.D;
						$scope.profile.age = $scope.dateDiff(result.birthday, new Date());
						console.log(result)
						$scope.profile.birthday =
							result.birthday.substring(0, 4) -
							543 +
							result.birthday.substring(4, 8);
						AuthenticationService.setProfile($scope.profile);
						$rootScope.globals.usertype = "1";
						$scope.loginByIdcard();

						// });
					} else {
						page = "error_idcard";

						// alert("เชื่อมต่อไม่ได้")
						$(".DisplayDevice").hide();
						$(".Loading").hide();
						$(".ResultDevice").hide();
						$(".Condition").hide();
						$(".Confirm").hide();
						$(".error_idcard").show();
						$scope.errorAudio();
					}
				}
			});
		};

		$scope.onPlayerReady = function (API) {
			$scope.video = API;
			// console.log(API.stop())
			API.stop();
			setTimeout(() => {
				$scope.videoPlay();
			}, 1000);
			// console.log(API)
		};

		$scope.videoPlay = function () {
			console.log("PLAY");
			$scope.video.play();
		};
		$scope.videoStop = function () {
			// console.log("STOP");
			$scope.video.stop();
		};
		$scope.recheck = function () {
			// $scope.playAudio();
			$scope.videoPlay();
			$scope.prepare();
		};
		$scope.prepare();

		$scope.Cancel = function () {
			$scope.cancelShow = true;
			isCallback = false;
			$scope.playAudio();
			devicesService.stopDevice({ device: "readeridcard" }, function (result) {
				$location.path("/authen");
			});
		};

		$scope.logout = function () {
			$scope.playAudio();
			$location.path("/home");
		};

		$scope.confirm = function () {
			// personService.ChkHeightFlagPerson(
			// 	{ PID: $rootScope.PID },
			// 	function (result) {
			// 		console.log(result)
			// 		if (result.status == true) {
			// 			if (result.message.length == 0) {
			// 				$scope.playAudio();
			// 				$location.path("/numpad");
			// 			} else {
			// 				$scope.playAudio();
			// 				$rootScope.globals.information.height = result.message[0]['HEIGHT'];

			// 				// d_serviceService.saveD_service($rootScope.globals)

			// 				$location.path("/profile");
			// 			}
			// 		}
			// 	}
			// );
			$scope.playAudio();
			$location.path("/numpad");
		};

		$scope.erconfirm = function () {
			$scope.playAudio();
			$state.go("authen", {
				page: "Condition",
			});
			// $location.path("/authen");
		};

		$scope.xbar = function () {
			$scope.playAudio();
			page = "DisplayDevice";
			$(".DisplayDevice").show();
			$(".Loading").hide();
			$(".ResultDevice").hide();
			$(".Condition").hide();
			$(".Confirm").hide();
			$(".error_idcard").hide();
			$scope.resetModel();
		};

		$scope.agree = function () {
			$scope.playAudio();
			console.log($scope.profile);
			personService.savePerson($scope.profile, function (result) {
				console.log(result);
				if (result.status == true) {
					$scope.sex = result.SEX;
					$scope.profile.profile_img = result.profile_img;
					$scope.profile.sex = result.SEX;
					AuthenticationService.setProfile($scope.profile);
					personService.getPerson({ PID: result.PID }, function (history) {
						console.log(history);
						if (history.message.length > 0) {
							$rootScope.globals.history = history.message[0];
						}
						$rootScope.globals.PID = result.PID;

						$scope.profile.titleName =
							$rootScope.globals.currentUser.titlenameth;

						$rootScope.globals.information.SEX = result.SEX;
						$rootScope.globals.usertype = 1;

						if ($scope.profile.age.years) {
							if ($scope.profile.age.years > 60) {
								if ($scope.profile.sex == 1) {
									$scope.profile.titleName = "คุณลุง";
								} else if ($scope.profile.sex == 2) {
									$scope.profile.titleName = "คุณป้า";
								} else {
									$scope.profile.titleName = "";
								}
							} else {
								$scope.profile.titleName = $scope.profile.titlenameth;
							}
						}
					});
					devicesService.updateReaderFlag(
						{ reader_flag: "N" },
						function (result) {
							// if(result.status == true){
							// }
						}
					);

					page = "Confirm";

					$(".DisplayDevice").hide();
					$(".Loading").hide();
					$(".ResultDevice").hide();
					$(".Condition").hide();
					$(".Confirm").show();
					$scope.confirmAudio();
					$scope.resetModel();
				} else {
					page = "error_idcard";

					// alert("เชื่อมต่อไม่ได้")
					$(".DisplayDevice").hide();
					$(".Loading").hide();
					$(".ResultDevice").hide();
					$(".Condition").hide();
					$(".Confirm").hide();
					$(".error_idcard").show();
					$scope.errorAudio();
				}
			});
		};

		$scope.playAudio = function () {
			var audio = new Audio("./assets/kiosk/audio/click.mp3");
			audio.play();
		};

		// $scope.playAudioinsertidcard = function () {
		// 	var audio = new Audio("./assets/kiosk/audio/insert_idcard.mp3");
		// 	audio.play();
		// };
		// $scope.stopAudioinsertidcard = function () {
		// 	var audio = new Audio("./assets/kiosk/audio/insert_idcard.mp3");
		// 	audio.pause();
		// };
		// $scope.playAudioinsertidcard();
		$scope.replayAudio = function () {
			var sample = document.getElementById("display");
			sample.pause();
			sample.currentTime = 0;
			sample.play();
		};

		$scope.loadAudio = function () {
			var display = document.getElementById("confirm");
			display.pause();

			var display = document.getElementById("display");
			display.pause();

			var sample = document.getElementById("load");
			sample.currentTime = 0;
			sample.play();
		};

		$scope.conditionAudio = function () {
			var display = document.getElementById("load");
			display.pause();

			var sample = document.getElementById("condition");
			sample.currentTime = 0;
			sample.play();
		};

		$scope.confirmAudio = function () {
			var display = document.getElementById("condition");
			display.pause();

			var sample = document.getElementById("confirm");
			sample.currentTime = 0;
			sample.play();
		};

		$scope.errorAudio = function () {
			var load = document.getElementById("load");
			load.pause();

			var sample = document.getElementById("error");
			sample.currentTime = 0;
			sample.play();
		};

		document.onkeypress = function (e) {
			e = e || window.event;
			let key = e.key.toLowerCase();
			switch (page) {
				case "DisplayDevice":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(384, 933).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(975, 939).click();
					}
					if (key == "c" || key == "แ") {
						// green
						document.elementFromPoint(1524, 939).click();
					}
					break;
				case "Loading":
					// if (key == "z" || key=="ผ") {
					// 	//red
					// 	document.elementFromPoint(348, 909).click();
					// }
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(957, 939).click();
					}
					// if (key == "c" || key=="แ") {
					// 	//green
					// 	document.elementFromPoint(852, 903).click();
					// }

					break;
				case "Confirm":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(426, 864).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(963, 849).click();
					}
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(1449, 855).click();
					}

					break;
				case "error_idcard":
					// if (key == "z" || key=="ผ") {
					// 	//red
					// 	document.elementFromPoint(426, 864).click();
					// }
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(954, 861).click();
					}
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(1485, 864).click();
					}

					break;
				case "Condition":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(492, 900).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
					}
					if (key == "c" || key == "แ") {
						//green
						document.elementFromPoint(1532, 908).click();
					}

					break;
				default:
			}
		};
		var keys = {};
		window.addEventListener(
			"keydown",
			function (e) {
				keys[e.code] = true;
				switch (e.code) {
					case "ArrowUp":
					case "ArrowDown":
					case "ArrowLeft":
					case "ArrowRight":
					case "Space":
						e.preventDefault();
						break;
					default:
						break; // do not block other keys
				}
			},
			false
		);

		var page = "DisplayDevice";
	}
})();
