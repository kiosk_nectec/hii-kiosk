/**
 * @author a.demeshko
 * created on 12/18/15
 */
(function() {
    "use strict";

    var pageName = "staff_set";
    var pageCtrl = pageName + "Ctrl";
    angular.module("BlurAdmin.pages.staff_set", []).config(routeConfig);
    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider.state(pageName, {
            url: "/" + pageName,
            templateUrl: "app/pages/staff_set/staff_set.html",
            controller: pageCtrl,
            title: "ตั้งค่าอุปกรณ์",
            sidebarMeta: {
                icon: "ion-gear-a",
                order: 3,
            },
        });
    }
})();