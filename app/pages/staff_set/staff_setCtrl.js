(function () {
	"use strict";

	angular
		.module("BlurAdmin.pages.staff_set")
		.controller("staff_setCtrl", homeCtrl);

	/** @ngInject */
	function homeCtrl(
		$scope,
		baseService,
		$rootScope,
		$uibModal,
		$filter,
		$timeout,
		$location,
		$state,
		devicesService,
		BackgroundService
	) {
		(function initController() {
			baseService.devicesActiveList();
			setTimeout(() => {
				if ($rootScope.audiomute == true) {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = true;
					}
				} else {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = false;
					}
				}
			}, 100);
		})();
		$scope.activePageTitle = $state.current.title;
		$scope.authen_admin = $rootScope.globals.currentUser;
		$scope.listdevice = [];
		$scope.device = {
			blood_pressure: true,
			pulse_oximeter: true,
			ir_thermometer: true,
			idcard: true,
			web_cam: true,
			infa_thermometer: true,
			weighing_machines: true,
			rfid_reader: true,
		};
		$scope.CreateModelRFID = { value: 0, item: "", code: "" };
		$scope.CreateModel = { device: 0, active_flag: 0 };
		$scope.textsuccess = "";
		$scope.textunsuccess = "";
		$scope.texttimererror = "";
		$scope.urlbackgroundimg = "";
		$scope.textfilenotfound_H = "";
		$scope.textfilenotfound_D = "";
		$scope.texttimeintervalerror = "";

		$scope.textdeletesuccess = "";
		$scope.textdeleteunsuccess = "";
		$scope.textweigherror = "";

		$scope.textSetzeroSuccess = "";
		$scope.textSetzeroUnSuccess = "";

		$scope.textCalibrationSuccess = "";
		$scope.textCalibrationUnSuccess = "";

		$scope.CreateModelScreen = {
			POWER_flag: true,
			POWER: 0,
			TYPE: 0,
			TIMER: 3,
			TIME_Interval: 3,
		};
		$scope.DRFID = true;
		$scope.focus = true;
		$scope.weight_total;

		$scope.SearchModelRFID = { code: "" };

		$scope.Listscreen = [];
		$scope.ListRFID = [];
		$scope.tempRFID = [];

		$scope.CreateModelTemp = {
			TEMP_ACTIVE: 0,
			TEMP_ACTIVE_FLAG: false,
			MATH_SIGN: "",
			TEMP_VALUE: "",
		};
		$scope.urlbackgroundTempSuccessimg = "";
		$scope.textSetTempUnSuccessChkBox = "";
		$scope.textSetTempUnSuccess1 = "";
		$scope.textSetTempUnSuccess2 = "";
		$scope.textSetTempHeadSuccess = "";
		$scope.textSetTempHeadUnSuccess = "";
		$scope.progressFunction = function () {
			return $timeout(function () {}, 3000);
		};
		$scope.next = function () {
			// $location.path('/devices/weighing-machines');
			baseService.nextDevices();
		};

		// $scope.cancel = function() {
		//     $location.path('/authen');
		// }

		$scope.Home = function () {
			$scope.playAudio();
			$location.path("/authen");
		};

		$scope.Back = function () {
			page = "DisplayDevice";
			$scope.playAudio();
			$location.path("/staff");
		};

		$scope.Cancel = function () {
			page = "DisplayDevice";
			$(".DisplayDevice").show();
			$(".Loading").hide();
			$(".Loadingset").hide();
			$(".error_Device").hide();
			$(".ResultDevice").hide();
			$(".PopUp").hide();
			$(".PopUpSet").hide();
			$(".Success").hide();
			$(".PopUpSetRFID").hide();
			$(".PopUpSetTEMP").hide();
			$(".RFIDSuccess").hide();
			$(".ListRFIDSuccess").hide();
			$(".SetWeigthingSuccess").hide();
			$(".TempSuccess").hide();
		};

		$scope.CancelSetScreen = function () {
			page = "popupset";
			$(".DisplayDevice").hide();
			$(".Loading").hide();
			$(".Loadingset").hide();
			$(".error_Device").hide();
			$(".ResultDevice").hide();
			$(".PopUp").hide();
			$(".PopUpSet").show();
			$(".Success").hide();
			$(".PopUpSetRFID").hide();
			$(".PopUpSetTEMP").hide();
			$(".RFIDSuccess").hide();
			$(".ListRFIDSuccess").hide();
			$(".SetWeigthingSuccess").hide();
			$(".TempSuccess").hide();
		};
		$scope.CancelSetTemp = function () {
			page = "TEMP";
			$(".DisplayDevice").hide();
			$(".Loading").hide();
			$(".Loadingset").hide();
			$(".error_Device").hide();
			$(".ResultDevice").hide();
			$(".PopUp").hide();
			$(".PopUpSet").hide();
			$(".Success").hide();
			$(".PopUpSetRFID").hide();
			$(".PopUpSetTEMP").show();
			$(".RFIDSuccess").hide();
			$(".ListRFIDSuccess").hide();
			$(".SetWeigthingSuccess").hide();
			$(".TempSuccess").hide();
		};

		$scope.option = function () {
			page = "popup";
			$(".DisplayDevice").hide();
			$(".Loading").hide();
			$(".Loadingset").hide();
			$(".error_Device").hide();
			$(".ResultDevice").hide();
			$(".PopUp").show();
			$(".PopUpSet").hide();
			$(".Success").hide();
			$(".PopUpSetRFID").hide();
			$(".PopUpSetTEMP").hide();
			$(".RFIDSuccess").hide();
			$(".ListRFIDSuccess").hide();
			$(".SetWeigthingSuccess").hide();
			$(".TempSuccess").hide();
		};

		$scope.showsetRFID = function () {
			page = "RFID";
			// console.log((1234).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
			$(".DisplayDevice").hide();
			$(".Loading").hide();
			$(".Loadingset").hide();
			$(".error_Device").hide();
			$(".ResultDevice").hide();
			$(".PopUp").hide();
			$(".PopUpSet").hide();
			$(".Success").hide();
			$(".PopUpSetRFID").show();
			$(".PopUpSetTEMP").hide();
			$(".RFIDSuccess").hide();
			$(".ListRFIDSuccess").hide();
			$(".SetWeigthingSuccess").hide();
			$(".TempSuccess").hide();
			$scope.resetRFID();
		};

		$scope.showsetTEMP = function () {
			page = "TEMP";
			$(".DisplayDevice").hide();
			$(".Loading").hide();
			$(".Loadingset").hide();
			$(".error_Device").hide();
			$(".ResultDevice").hide();
			$(".PopUp").hide();
			$(".PopUpSet").hide();
			$(".Success").hide();
			$(".PopUpSetRFID").hide();
			$(".PopUpSetTEMP").show();
			$(".RFIDSuccess").hide();
			$(".ListRFIDSuccess").hide();
			$(".SetWeigthingSuccess").hide();
			$(".TempSuccess").hide();

			$scope.urlbackgroundTempSuccessimg = "";
			$scope.textSetTempUnSuccessChkBox = "";
			$scope.textSetTempUnSuccess1 = "";
			$scope.textSetTempUnSuccess2 = "";
			$scope.textSetTempHeadSuccess = "";
			$scope.textSetTempHeadUnSuccess = "";
			$(".Loading").show();
			devicesService.GetSettingTemp(null, function (result) {
				if (result.status) {
					if (result.message.length > 0) {
						$scope.CreateModelTemp = {
							TEMP_ACTIVE: result.message[0]["TEMP_ACTIVE"],
							MATH_SIGN: result.message[0]["MATH_SIGN"],
							TEMP_VALUE: result.message[0]["TEMP_VALUE"],
						};
						if (result.message[0]["TEMP_ACTIVE"] == 1) {
							$scope.CreateModelTemp.TEMP_ACTIVE_FLAG = false;
						} else {
							$scope.CreateModelTemp.TEMP_ACTIVE_FLAG = true;
						}
						$(".Loading").hide();
					} else {
						$scope.CreateModelTemp = {
							TEMP_ACTIVE: 0,
							TEMP_ACTIVE_FLAG: false,
							MATH_SIGN: "",
							TEMP_VALUE: "",
						};
						$(".Loading").hide();
					}
				} else {
					$scope.CreateModelTemp = {
						TEMP_ACTIVE: 0,
						TEMP_ACTIVE_FLAG: false,
						MATH_SIGN: "",
						TEMP_VALUE: "",
					};
					$(".Loading").hide();
				}
			});
		};

		$scope.resetRFID = function () {
			page = "RFID";
			$(".SRFID").hide();
			$(".DeRFID").hide();
			$(".ShowLRFID").hide();
			$(".LRFID").show();
			$scope.DRFID = true;
			$scope.CreateModelRFID = { value: "", code: "" };
			document.getElementById("input-box-RFID").placeholder = "";
			// console.log($scope.CreateModelRFID);
			// $scope.$apply();
		};

		$scope.ShowListRFID = function () {
			page = "ListRFID";
			$(".ShowLRFID").show();
			$(".RFIDnoinform").hide();
			$(".ListRFIDSuccess").hide();
			$scope.SearchModelRFID.code = "";
			document.getElementById("input-box-ListRFID").focus();

			$scope.GetListRFID();
		};

		$scope.GetListRFID = function () {
			devicesService.GetListRFID(null, function (result) {
				if (result.status == true) {
					$scope.ListRFID = result.message;
				} else {
				}
			});
		};

		$scope.showset = function () {
			$(".Loading").show();
			devicesService.GetSettingScreen(null, function (result) {
				if (result.status == true) {
					$scope.Listscreen = result.message;
					if ($scope.Listscreen.length > 0) {
						if ($scope.Listscreen[0]["POWER"] == 0) {
							$scope.CreateModelScreen.POWER_flag = true;
						} else {
							$scope.CreateModelScreen.POWER_flag = false;
						}
						$scope.CreateModelScreen.TYPE = $scope.Listscreen[0]["TYPE"];
						$scope.CreateModelScreen.TIMER = parseFloat(
							$scope.Listscreen[0]["TIMER"]
						);
						$scope.CreateModelScreen.TIME_Interval = parseFloat(
							$scope.Listscreen[0]["TIME_Interval"]
						);
					}
					page = "popupset";
					$(".DisplayDevice").hide();
					$(".Loading").hide();
					$(".Loadingset").hide();
					$(".error_Device").hide();
					$(".ResultDevice").hide();
					$(".PopUp").hide();
					$(".PopUpSet").show();
					$(".Success").hide();
					$(".PopUpSetRFID").hide();
					$(".PopUpSetTEMP").hide();
					$(".RFIDSuccess").hide();
					$(".ListRFIDSuccess").hide();
					$(".SetWeigthingSuccess").hide();
					$(".TempSuccess").hide();
				} else {
					page = "popupset";
					$(".DisplayDevice").hide();
					$(".Loading").hide();
					$(".Loadingset").hide();
					$(".error_Device").hide();
					$(".ResultDevice").hide();
					$(".PopUp").hide();
					$(".PopUpSet").show();
					$(".Success").hide();
					$(".PopUpSetRFID").hide();
					$(".PopUpSetTEMP").hide();
					$(".RFIDSuccess").hide();
					$(".ListRFIDSuccess").hide();
					$(".SetWeigthingSuccess").hide();
					$(".TempSuccess").hide();
				}
			});
		};

		$scope.ChkSaveScreen = function () {
			$(".Loading").show();
			if ($scope.CreateModelScreen.POWER_flag == false) {
				$scope.CreateModelScreen.POWER = 1;
			} else {
				$scope.CreateModelScreen.POWER = 0;
			}
			if ($scope.CreateModelScreen.POWER_flag == false) {
				if ($scope.CreateModelScreen.TYPE == 0) {
					var listimg = [];
					BackgroundService.getFileImageComboList(null, function (result) {
						listimg = result.message;
						if (listimg.length == 0) {
							page = "Loading";
							$scope.CreateModelScreen.TIMER = 3;
							$scope.textsuccess = "";
							$scope.textunsuccess = "";
							$scope.textfilenotfound_H = "การพักหน้าจอไม่สามารถใช้งานได้";
							$scope.textfilenotfound_D = "ไม่พบรูปภาฟในระบบ";
							$scope.texttimererror = "";
							$scope.texttimeintervalerror = "";
							$scope.textSetzeroSuccess = "";
							$scope.textSetzeroUnSuccess = "";
							$scope.textCalibrationSuccess = "";
							$scope.textCalibrationUnSuccess = "";
							$scope.urlbackgroundimg = "assets/kiosk/face/icon-check-2.svg";
							$(".Success").show();
							$(".Loading").hide();
							$scope.CreateModelScreen.POWER_flag = true;
						} else {
							if ($scope.CreateModelScreen.TIME_Interval < 1) {
								page = "Loading";
								$scope.CreateModelScreen.TIME_Interval = 0;
								$scope.CreateModelScreen.TIMER = 3;
								$scope.textunsuccess = "บันทึกไม่สำเร็จ";
								$scope.textsuccess = "";
								$scope.textfilenotfound_H = "";
								$scope.textfilenotfound_D = "";
								$scope.texttimererror = "";
								$scope.texttimeintervalerror = "กรุณาระบุระยะเวลาการแสดง";
								$scope.textSetzeroSuccess = "";
								$scope.textSetzeroUnSuccess = "";
								$scope.textCalibrationSuccess = "";
								$scope.textCalibrationUnSuccess = "";
								$scope.urlbackgroundimg = "assets/kiosk/face/icon-check-2.svg";
								$(".Success").show();
								$(".Loading").hide();
							} else {
								if (
									undefined != $scope.CreateModelScreen.TIMER &&
									$scope.CreateModelScreen.TIMER < 60 &&
									$scope.CreateModelScreen.TIMER >= 3
								) {
									$scope.SaveScreen();
								} else {
									page = "Loading";
									$scope.CreateModelScreen.TIMER = 3;
									$scope.textunsuccess = "บันทึกไม่สำเร็จ";
									$scope.textsuccess = "";
									$scope.textfilenotfound_H = "";
									$scope.textfilenotfound_D = "";
									$scope.texttimererror =
										"ระยะเวลาการรอต้องมากกว่า 3 นาที และน้อยกว่า 60 นาที";
									$scope.texttimeintervalerror = "";
									$scope.textSetzeroSuccess = "";
									$scope.textSetzeroUnSuccess = "";
									$scope.textCalibrationSuccess = "";
									$scope.textCalibrationUnSuccess = "";
									$scope.urlbackgroundimg =
										"assets/kiosk/face/icon-check-2.svg";
									$(".Success").show();
									$(".Loading").hide();
								}
							}
						}
					});
				} else {
					var listvideo = [];
					BackgroundService.getFileVideoComboList(null, function (result) {
						listvideo = result.message;
						if (listvideo.length == 0) {
							page = "Loading";
							$scope.CreateModelScreen.TIMER = 3;
							$scope.textsuccess = "";
							$scope.textunsuccess = "";
							$scope.textfilenotfound_H = "การพักหน้าจอไม่สามารถใช้งานได้";
							$scope.textfilenotfound_D = "ไม่พบวีดีโอในระบบ";
							$scope.texttimererror = "";
							$scope.texttimeintervalerror = "";
							$scope.textSetzeroSuccess = "";
							$scope.textSetzeroUnSuccess = "";
							$scope.textCalibrationSuccess = "";
							$scope.textCalibrationUnSuccess = "";
							$scope.urlbackgroundimg = "assets/kiosk/face/icon-check-2.svg";
							$(".Success").show();
							$scope.CreateModelScreen.POWER_flag = true;
						} else {
							if (
								undefined != $scope.CreateModelScreen.TIMER &&
								$scope.CreateModelScreen.TIMER < 60 &&
								$scope.CreateModelScreen.TIMER >= 3
							) {
								$scope.CreateModelScreen.TIME_Interval = 0;
								$scope.SaveScreen();
							} else {
								page = "Loading";
								$scope.CreateModelScreen.TIMER = 3;
								$scope.textunsuccess = "บันทึกไม่สำเร็จ";
								$scope.textsuccess = "";
								$scope.textfilenotfound_H = "";
								$scope.textfilenotfound_D = "";
								$scope.textSetzeroSuccess = "";
								$scope.textSetzeroUnSuccess = "";
								$scope.textCalibrationSuccess = "";
								$scope.textCalibrationUnSuccess = "";
								$scope.texttimererror =
									"ระยะเวลาการรอต้องมากกว่า 3 นาที และน้อยกว่า 60 นาที";
								$scope.texttimeintervalerror = "";
								$scope.urlbackgroundimg = "assets/kiosk/face/icon-check-2.svg";
								$(".Success").show();
							}
						}
					});
				}
			} else {
				$scope.SaveScreen();
			}
		};

		$scope.SaveScreen = function () {
			devicesService.SaveSettingScreen(
				$scope.CreateModelScreen,
				function (result) {
					if (result.status == true) {
						if ($scope.CreateModelScreen.POWER == 1) {
							$rootScope.TimeOutscreen = $scope.CreateModelScreen.TIMER;
						}
						page = "Loading";
						$scope.textsuccess = "บันทึกสำเร็จ";
						$scope.textfilenotfound_H = "";
						$scope.textfilenotfound_D = "";
						$scope.textunsuccess = "";
						$scope.texttimererror = "";
						$scope.texttimeintervalerror = "";
						$scope.textSetzeroSuccess = "";
						$scope.textSetzeroUnSuccess = "";
						$scope.textCalibrationSuccess = "";
						$scope.textCalibrationUnSuccess = "";
						$scope.urlbackgroundimg = "assets/kiosk/face/icon-check.svg";
						$(".Loading").hide();
						$(".Success").show();
					} else {
						page = "Loading";
						$scope.textunsuccess = "บันทึกไม่สำเร็จ";
						$scope.textfilenotfound_H = "";
						$scope.textfilenotfound_D = "";
						$scope.textsuccess = "";
						$scope.texttimererror = "";
						$scope.texttimeintervalerror = "";
						$scope.textSetzeroSuccess = "";
						$scope.textSetzeroUnSuccess = "";
						$scope.textCalibrationSuccess = "";
						$scope.textCalibrationUnSuccess = "";
						$scope.urlbackgroundimg = "assets/kiosk/face/icon-check-2.svg";
						$(".Loading").hide();
						$(".Success").show();
					}
				}
			);
		};
		$scope.SaveRFID = function () {
			$(".Loading").show();
			if (
				null == $scope.CreateModelRFID.value ||
				"" == $scope.CreateModelRFID.value
			) {
				page = "Loading";
				$scope.textsuccess = "";
				$scope.textunsuccess = "บันทึกไม่สำเร็จ";
				$scope.textdeletesuccess = "";
				$scope.textdeleteunsuccess = "";
				$scope.textweigherror = "กรุณาระบุน้ำหนักของวิลแชร์";
				$scope.textSetzeroSuccess = "";
				$scope.textSetzeroUnSuccess = "";
				$scope.textCalibrationSuccess = "";
				$scope.textCalibrationUnSuccess = "";
				$scope.urlbackgroundimg = "assets/kiosk/face/icon-check-2.svg";
				$(".Loading").hide();
				$(".RFIDSuccess").show();
			} else {
				$scope.CreateModelRFID.item =
					parseFloat($scope.CreateModelRFID.value).toFixed(1) + " kg";
				console.log($scope.CreateModelRFID);
				devicesService.SaveRFID($scope.CreateModelRFID, function (result) {
					if (result.status == true) {
						page = "Loading";
						$scope.textsuccess = "บันทึกสำเร็จ";
						$scope.textunsuccess = "";
						$scope.textdeletesuccess = "";
						$scope.textdeleteunsuccess = "";
						$scope.textweigherror = "";
						$scope.textSetzeroSuccess = "";
						$scope.textSetzeroUnSuccess = "";
						$scope.textCalibrationSuccess = "";
						$scope.textCalibrationUnSuccess = "";
						$scope.urlbackgroundimg = "assets/kiosk/face/icon-check.svg";
						$(".Loading").hide();
						$(".RFIDSuccess").show();
					} else {
						page = "Loading";
						$scope.textsuccess = "";
						$scope.textunsuccess = "บันทึกไม่สำเร็จ";
						$scope.textdeletesuccess = "";
						$scope.textdeleteunsuccess = "";
						$scope.textweigherror = "";
						$scope.textSetzeroSuccess = "";
						$scope.textSetzeroUnSuccess = "";
						$scope.textCalibrationSuccess = "";
						$scope.textCalibrationUnSuccess = "";
						$scope.urlbackgroundimg = "assets/kiosk/face/icon-check-2.svg";
						$(".Loading").hide();
						$(".RFIDSuccess").show();
					}
				});
			}
		};

		$scope.DeleteRFIDInList = function (item) {
			$(".Loading").show();
			devicesService.DeleteRFID({ code: item.code }, function (result) {
				if (result.status == true) {
					page = "Loading";
					$scope.textsuccess = "";
					$scope.textunsuccess = "";
					$scope.textdeletesuccess = "ลบสำเร็จ";
					$scope.textdeleteunsuccess = "";
					$scope.textweigherror = "";
					$scope.textSetzeroSuccess = "";
					$scope.textSetzeroUnSuccess = "";
					$scope.textCalibrationSuccess = "";
					$scope.textCalibrationUnSuccess = "";
					$scope.urlbackgroundimg = "assets/kiosk/face/icon-check.svg";
					$(".Loading").hide();
					$(".ListRFIDSuccess").show();
					$scope.GetListRFID();
				} else {
					page = "Loading";
					$scope.textsuccess = "";
					$scope.textunsuccess = "";
					$scope.textdeletesuccess = "";
					$scope.textdeleteunsuccess = "ลบไม่สำเร็จ";
					$scope.textweigherror = "";
					$scope.textSetzeroSuccess = "";
					$scope.textSetzeroUnSuccess = "";
					$scope.textCalibrationSuccess = "";
					$scope.textCalibrationUnSuccess = "";
					$scope.urlbackgroundimg = "assets/kiosk/face/icon-check-2.svg";
					$(".Loading").hide();
					$(".ListRFIDSuccess").show();
					$scope.GetListRFID();
				}
			});
		};
		$scope.DeleteRFID = function () {
			$(".Loading").show();
			$scope.CreateModelRFID.item =
				parseFloat($scope.CreateModelRFID.value).toFixed(1) + " kg";
			console.log($scope.CreateModelRFID);
			devicesService.DeleteRFID($scope.CreateModelRFID, function (result) {
				if (result.status == true) {
					page = "Loading";
					$scope.textsuccess = "";
					$scope.textunsuccess = "";
					$scope.textdeletesuccess = "ลบสำเร็จ";
					$scope.textdeleteunsuccess = "";
					$scope.textSetzeroSuccess = "";
					$scope.textSetzeroUnSuccess = "";
					$scope.textCalibrationSuccess = "";
					$scope.textCalibrationUnSuccess = "C";
					$scope.urlbackgroundimg = "assets/kiosk/face/icon-check.svg";
					$(".Loading").hide();
					$(".RFIDSuccess").show();
				} else {
					page = "Loading";
					$scope.textsuccess = "";
					$scope.textunsuccess = "";
					$scope.textdeletesuccess = "";
					$scope.textSetzeroSuccess = "";
					$scope.textSetzeroUnSuccess = "";
					$scope.textCalibrationSuccess = "";
					$scope.textCalibrationUnSuccess = "";
					$scope.textdeleteunsuccess = "ลบไม่สำเร็จ";
					$scope.urlbackgroundimg = "assets/kiosk/face/icon-check-2.svg";
					$(".Loading").hide();
					$(".RFIDSuccess").show();
				}
			});
		};

		$scope.checkdevice = function (api, device, check) {
			$(".Loading").show();
			if (check == false) {
				// alert(device);
				devicesService.getStatus({ device: api }, function (result) {
					// alert(JSON.stringify(result));
					if (result.data) {
						if (result.data[0].status != "success") {
							// alert("ตรวจสอบอุปกรณ์"); //ต่ออุปกรณไม่ได้
							$scope.disableDevice(device);
							$(".Loading").hide();
						} else {
							if (device == "weighing_machines") {
								$scope.CreateModel = { device: 1, active_flag: 1 };
							} else if (device == "blood_pressure") {
								$scope.CreateModel = { device: 2, active_flag: 1 };
							} else if (device == "pulse_oximeter") {
								$scope.CreateModel = { device: 3, active_flag: 1 };
							} else if (device == "ir_thermometer") {
								$scope.CreateModel = { device: 4, active_flag: 1 };
							}
							// console.log($scope.CreateModel)
							devicesService.setActiveDevice(
								$scope.CreateModel,
								function (result) {
									if (result.status == true) {
										$(".Loading").hide();
									} else {
										$(".Loading").hide();
									}
								}
							);

							// alert("เชื่อมต่อสำเร็จ"); //ต่อสำเร็จ
						}
					} else {
						if (result.status == false) {
							if (result.success == false) {
								$(".Loading").hide();
								// alert("not found api"); //ไม่มี api
							}
						} else {
							$(".Loading").hide();
							// alert("format api is wrong."); //return format api ไม่มี data
						}

						$scope.disableDevice(device);
					}
				});
			} else {
				if (device == "weighing_machines") {
					$scope.CreateModel = { device: 1, active_flag: 0 };
				} else if (device == "blood_pressure") {
					$scope.CreateModel = { device: 2, active_flag: 0 };
				} else if (device == "pulse_oximeter") {
					$scope.CreateModel = { device: 3, active_flag: 0 };
				} else if (device == "ir_thermometer") {
					$scope.CreateModel = { device: 4, active_flag: 0 };
				}
				devicesService.setActiveDevice($scope.CreateModel, function (result) {
					if (result.status == true) {
						$(".Loading").hide();
					} else {
						$(".Loading").hide();
					}
				});
			}
		};
		$scope.disableDevice = function (device) {
			if (device == "blood_pressure") {
				$scope.device.blood_pressure = true;
			}

			if (device == "pulse_oximeter") {
				$scope.device.pulse_oximeter = true;
			}

			if (device == "web_cam") {
				$scope.device.web_cam = true;
			}

			if (device == "ir_thermometer") {
				$scope.device.ir_thermometer = true;
			}

			if (device == "infa_thermometer") {
				$scope.device.infa_thermometer = true;
			}

			if (device == "weighing_machines") {
				$scope.device.weighing_machines = true;
			}

			if (device == "idcard") {
				$scope.device.idcard = true;
			}

			if (device == "rfid_reader") {
				$scope.device.rfid_reader = true;
			}
		};

		$scope.init = function () {
			$scope.ResetModel();
			$scope.reload();
		};
		$scope.reload = function () {
			devicesService.getActiveDevices(null, function (result) {
				if (result.status == true) {
					$scope.listdevice = result.message;
					$scope.listdevice.forEach(function (entry, index) {
						if (entry.id == 1) {
							if (entry.active_flag == 1) {
								$scope.device.weighing_machines = false;
							}
						}

						if (entry.id == 2) {
							if (entry.active_flag == 1) {
								$scope.device.blood_pressure = false;
							}
						}

						if (entry.id == 3) {
							if (entry.active_flag == 1) {
								$scope.device.pulse_oximeter = false;
							}
						}

						if (entry.id == 4) {
							if (entry.active_flag == 1) {
								$scope.device.ir_thermometer = false;
							}
						}
					});
				} else {
					$scope.listdevice = result.message;
				}
			});
		};

		$scope.ResetModel = function () {
			$scope.device = {
				blood_pressure: true,
				pulse_oximeter: true,
				ir_thermometer: true,
				idcard: true,
				web_cam: true,
				infa_thermometer: true,
				weighing_machines: true,
				rfid_reader: true,
			};

			$scope.CreateModel = { device: 0, active_flag: 0 };
		};

		$scope.start = function () {
			page = "LoadingWeigh";
			$scope.playAudio();
			$(".DisplayDevice").hide();
			$(".Loadingset").show();
			$(".Loading").hide();
			$(".error_Device").hide();
			$(".ResultDevice").hide();
			$(".PopUp").hide();
			$(".PopUpSet").hide();
			$(".Success").hide();
			$(".PopUpSetRFID").hide();
			$(".PopUpSetTEMP").hide();
			$(".RFIDSuccess").hide();
			$(".ListRFIDSuccess").hide();
			$(".SetWeigthingSuccess").hide();
			$(".TempSuccess").hide();
			devicesService.getDataWeighingMachines({}, function (result) {
				if (result.data[0].weight_total != undefined) {
					$scope.weight_total = parseFloat(result.data[0].weight_total);
					$scope.$apply();
					// setTimeout(() => {
					page = "Weigh";
					$(".DisplayDevice").hide();
					$(".Loading").hide();
					$(".Loadingset").hide();
					$(".error_Device").hide();
					$(".ResultDevice").show();
					$(".PopUp").hide();
					$(".PopUpSet").hide();
					$(".Success").hide();
					$(".PopUpSetRFID").hide();
					$(".PopUpSetTEMP").hide();
					$(".RFIDSuccess").hide();
					$(".ListRFIDSuccess").hide();
					$(".SetWeigthingSuccess").hide();
					$(".TempSuccess").hide();
					// }, 2000);
				}
			});
		};

		$scope.Setzero = function () {
			console.log("Setzero");
			$(".Loading").show();
			devicesService.SetWeighingmachine(
				{ func: "setzero", action: "UserAction" },
				function (result) {
					if (result.data[0]["status"] == "success") {
						page = "Loading";
						$scope.textSetzeroSuccess = "Set zero สำเร็จ";
						$scope.textSetzeroUnSuccess = "";
						$scope.textCalibrationSuccess = "";
						$scope.textCalibrationUnSuccess = "";
						$scope.urlbackgroundimg = "assets/kiosk/face/icon-check.svg";
						$(".Loading").hide();
						$(".SetWeigthingSuccess").show();
						$(".TempSuccess").hide();
					} else {
						page = "Loading";
						$scope.textSetzeroUnSuccess = "Set zero ไม่สำเร็จ";
						$scope.textSetzeroSuccess = "";
						$scope.textCalibrationSuccess = "";
						$scope.textCalibrationUnSuccess = "";
						$scope.urlbackgroundimg = "assets/kiosk/face/icon-check-2.svg";
						$(".Loading").hide();
						$(".SetWeigthingSuccess").show();
						$(".TempSuccess").hide();
					}
				}
			);
		};

		$scope.Calibration = function () {
			console.log("Calibration");
			$(".Loading").show();
			devicesService.SetWeighingmachine(
				{ func: "calibration", action: "UserAction" },
				function (result) {
					if (result.data[0]["status"] == "success") {
						page = "Loading";
						$scope.textSetzeroSuccess = "";
						$scope.textSetzeroUnSuccess = "";
						$scope.textCalibrationSuccess = "Calibration สำเร็จ";
						$scope.textCalibrationUnSuccess = "";
						$scope.urlbackgroundimg = "assets/kiosk/face/icon-check.svg";
						$(".Loading").hide();
						$(".SetWeigthingSuccess").show();
						$(".TempSuccess").hide();
					} else {
						page = "Loading";
						$scope.textSetzeroUnSuccess = "";
						$scope.textSetzeroSuccess = "";
						$scope.textCalibrationSuccess = "";
						$scope.textCalibrationUnSuccess = "Calibration ไม่สำเร็จ";
						$scope.urlbackgroundimg = "assets/kiosk/face/icon-check-2.svg";
						$(".Loading").hide();
						$(".SetWeigthingSuccess").show();
						$(".TempSuccess").hide();
					}
				}
			);
		};

		$scope.ChkSaveTemp = function () {
			$(".Loading").show();
			if (!$scope.CreateModelTemp.TEMP_ACTIVE_FLAG) {
				if ($scope.CreateModelTemp.MATH_SIGN == "") {
					$scope.urlbackgroundTempSuccessimg =
						"assets/kiosk/face/icon-check-2.svg";
					$scope.textSetTempUnSuccessChkBox =
						"กรุณาเลือกค่าอุณหภูมิที่เป็นบวกหรือลบ";
					$scope.textSetTempUnSuccess1 = "";
					$scope.textSetTempUnSuccess2 = "";
					$scope.textSetTempHeadSuccess = "";
					$scope.textSetTempHeadUnSuccess = "บันทึกไม่สำเร็จ";
					page = "TempSuccess";
					$(".Loading").hide();
					$(".TempSuccess").show();
				} else {
					var strTemp = $scope.CreateModelTemp.TEMP_VALUE.toString();
					var I = Math.floor($scope.CreateModelTemp.TEMP_VALUE);
					var A = strTemp.substring(0, 1);
					var X = strTemp.substring(2, 3);
					if (
						Number.isInteger(parseFloat(A)) &&
						Number.isInteger(parseFloat(I)) &&
						(X == "" || X == "5" || X == "0") &&
						parseFloat($scope.CreateModelTemp.TEMP_VALUE) <= 10
					) {
						$scope.SaveTemp();
					} else {
						page = "TempSuccess";
						$scope.urlbackgroundTempSuccessimg =
							"assets/kiosk/face/icon-check-2.svg";
						$scope.textSetTempUnSuccessChkBox = "";
						$scope.textSetTempUnSuccess1 =
							"ค่าอุณหภูมิไม่สามารถกำหนดให้เกิน บวก 10 หรือน้อยกว่า ลบ 10 ได้";
						$scope.textSetTempUnSuccess2 =
							"และค่าอุณหภูมิจะเพิ่มหรือลดได้ทีละ 0.5 และทศนิยม 1 ตำแหน่ง";
						$scope.textSetTempHeadSuccess = "";
						$scope.textSetTempHeadUnSuccess = "บันทึกไม่สำเร็จ";
						$(".Loading").hide();
						$(".TempSuccess").show();
					}
				}
			} else {
				$scope.SaveTemp();
			}
		};
		$scope.SaveTemp = function () {
			if ($scope.CreateModelTemp.TEMP_ACTIVE_FLAG) {
				$scope.CreateModelTemp.TEMP_ACTIVE = 0;
			} else {
				$scope.CreateModelTemp.TEMP_ACTIVE = 1;
			}
			if ($scope.CreateModelTemp.MATH_SIGN == 0) {
				$scope.CreateModelTemp.TEMP_VALUE_TEXT =
					"-" + $scope.CreateModelTemp.TEMP_VALUE;
			} else {
				$scope.CreateModelTemp.TEMP_VALUE_TEXT =
					"+" + $scope.CreateModelTemp.TEMP_VALUE;
			}
			devicesService.SaveSettingTemp($scope.CreateModelTemp, function (result) {
				if (result.status == true) {
					$scope.urlbackgroundTempSuccessimg =
						"assets/kiosk/face/icon-check.svg";
					$scope.textSetTempUnSuccessChkBox = "";
					$scope.textSetTempUnSuccess1 = "";
					$scope.textSetTempUnSuccess2 = "";
					$scope.textSetTempHeadSuccess = "บันทึกสำเร็จ";
					$scope.textSetTempHeadUnSuccess = "";
					page = "TempSuccess";
					$(".Loading").hide();
					$(".TempSuccess").show();
				} else {
					$scope.urlbackgroundTempSuccessimg =
						"assets/kiosk/face/icon-check-2.svg";
					$scope.textSetTempUnSuccessChkBox = "";
					$scope.textSetTempUnSuccess1 = "";
					$scope.textSetTempUnSuccess2 = "";
					$scope.textSetTempHeadSuccess = "";
					$scope.textSetTempHeadUnSuccess = "บันทึกไม่สำเร็จ";
					page = "TempSuccess";
					$(".Loading").hide();
					$(".TempSuccess").show();
				}
			});
		};
		$scope.playAudio = function () {
			var audio = new Audio("./assets/kiosk/audio/click.mp3");
			audio.play();
		};
		document.onkeypress = function (e) {
			e = e || window.event;
			let key = e.key.toLowerCase();
			switch (page) {
				case "DisplayDevice":
					if (key == "z" || key == "ผ") {
						//red
					}
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(966, 988).click();
					}
					if (key == "c" || key == "แ") {
						// green
					}
					break;
				case "popup":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(956, 988).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
					}
					if (key == "c" || key == "แ") {
						// green
					}
					break;
				case "popupset":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(412, 972).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
					}
					if (key == "c" || key == "แ") {
						// green
						document.elementFromPoint(1512, 992).click();
					}
					break;
				case "TEMP":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(412, 972).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
					}
					if (key == "c" || key == "แ") {
						// green
						document.elementFromPoint(1512, 992).click();
					}
					break;
				case "TempSuccess":
					if (key == "z" || key == "ผ") {
						//red
					}
					if (key == "x" || key == "ป") {
						//yellow
					}
					if (key == "c" || key == "แ") {
						// green
						document.elementFromPoint(960, 910).click();
					}
					break;
				case "Loading":
					if (key == "z" || key == "ผ") {
						//red
					}
					if (key == "x" || key == "ป") {
						//yellow
					}
					if (key == "c" || key == "แ") {
						// green
						document.elementFromPoint(957, 918).click();
					}
					break;
				case "Weigh":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(423, 873).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
					}
					if (key == "c" || key == "แ") {
						// green
						document.elementFromPoint(1525, 873).click();
					}
					break;
				case "LoadingWeigh":
					if (key == "z" || key == "ผ") {
						//red
					}
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(950, 940).click();
					}
					if (key == "c" || key == "แ") {
						// green
					}
					break;

				case "RFID":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(412, 972).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(966, 988).click();
					}
					if (key == "c" || key == "แ") {
						// green
						document.elementFromPoint(1512, 992).click();
					}
					if ($scope.CreateModelRFID.code.length < 10) {
						$scope.CreateModelRFID.code = $scope.CreateModelRFID.code + key;
						$scope.$apply();
					}
					if ($scope.CreateModelRFID.code.length == 10) {
						$(".SRFID").show();
						$(".LRFID").hide();
						$scope.DRFID = false;
						document.getElementById("input-box-RFID").focus();
						$scope.$apply();
						devicesService.GetChkRfid(
							{ RFID: $scope.CreateModelRFID.code },
							function (result) {
								$scope.tempRFID = result.message;
								if (result.status == true && $scope.tempRFID.length > 0) {
									if ($scope.CreateModelRFID.value == "") {
										$scope.CreateModelRFID.value = parseFloat(
											$scope.tempRFID[0]["value"]
										);
										$(".DeRFID").show();
										$scope.$apply();
									}
								} else {
									document.getElementById("input-box-RFID").placeholder = "N/A";
								}
							}
						);
					}
					break;
				case "ListRFID":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(966, 988).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
					}
					if (key == "c" || key == "แ") {
						// green
					}
					$scope.SearchModelRFID.code = $scope.SearchModelRFID.code + key;

					$scope.$apply();
					if ($scope.SearchModelRFID.code.length == 10) {
						$(".RFIDnoinform").hide();
						devicesService.GetChkRfid(
							{ RFID: $scope.SearchModelRFID.code },
							function (result) {
								if (result.status == true && result.message.length > 0) {
									$scope.ListRFID = result.message;
									$scope.SearchModelRFID.code = "";
									$scope.$apply();
								} else {
									$scope.SearchModelRFID.code = "";

									$(".RFIDnoinform").show();
									$scope.ListRFID = [];
									$scope.$apply();
								}
							}
						);
					}
					break;
				default:
			}
		};
		var page = "DisplayDevice";
		$scope.init();
	}
})();
// ALTER TABLE `t_service` ADD `TEMPCOMPENSATE` VARCHAR(50) NULL AFTER `BTEMPCODE`;
// ALTER TABLE `t_service` ADD `BTEMPFROMAPI` DECIMAL(10,1) NULL AFTER `TEMPCOMPENSATE`;
