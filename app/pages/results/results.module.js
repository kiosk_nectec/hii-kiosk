/**
 * @author a.demeshko
 * created on 12/18/15
 */
(function() {
    "use strict";

    var pageName = "results";
    var pageCtrl = pageName + "Ctrl";
    angular.module("BlurAdmin.pages.results", []).config(routeConfig);
    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider.state(pageName, {
            url: "/" + pageName,
            templateUrl: "app/pages/results/results.html",
            controller: pageCtrl,
            title: "ข้อมูลผู้ใช้งาน",
            sidebarMeta: {
                icon: "ion-gear-a",
                order: 3,
            },
        });
    }
})();