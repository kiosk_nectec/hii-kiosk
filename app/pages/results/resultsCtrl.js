(function () {
	"use strict";

	angular.module("BlurAdmin.pages.results").controller("resultsCtrl", homeCtrl);

	/** @ngInject */
	function homeCtrl(
		$scope,
		baseService,
		$rootScope,
		$uibModal,
		$filter,
		$timeout,
		$location,
		$state,
		conBloodPressureService,
		conPulseRateService,
		conOxygenService,
		conTemperatureService,
		conWeighingMachinesService,
		d_serviceService
	) {
		(function initController() {
			$scope.profile = {};
			setTimeout(() => {
				if ($rootScope.audiomute == true) {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = true;
					}
				} else {
					var elems = document.querySelectorAll("video, audio");
					for (var i = 0; i < elems.length; ++i) {
						elems[i].muted = false;
					}
				}
			}, 100);
		})();

		$scope.profile = angular.copy($rootScope.globals.currentUser);

		$scope.results = $rootScope.globals.information;
		$scope.usertype = $rootScope.globals.usertype;

		$scope.PID = $rootScope.globals.PID;

		$scope.profile.titleName = $rootScope.globals.currentUser.titlenameth;

		if ($scope.profile.age.years) {
			if ($scope.profile.age.years > 60) {
				if ($scope.profile.sex == 1) {
					$scope.profile.titleName = "คุณลุง";
				} else if ($scope.profile.sex == 2) {
					$scope.profile.titleName = "คุณป้า";
				} else {
					$scope.profile.titleName = "";
				}
			} else {
				$scope.profile.titleName = $scope.profile.titlenameth;
			}
		}

		if ($scope.usertype == 1) {
			$scope.sex = $scope.profile.sex;
		} else {
			$scope.sex = 0;
		}

		console.log($scope.sex);

		console.log($scope.qrcode);
		console.log($scope.results);
		$scope.historyRef = $rootScope.globals.history;
		console.log($scope.historyRef);
		$scope.recheckAll = function () {
			$scope.playAudio();
			if ($rootScope.devicesActiveList.length > 0) {
				// $rootScope.globals.information = {};
				baseService.recheckAll();

				$scope.profile.age = $scope.dateDiff(result.birthday, new Date());
				$scope.profile.birthday =
					result.birthday.substring(0, 4) -
					543 +
					result.birthday.substring(4, 8);

				$scope.loginByIdcard();
			} else {
				$(".DisplayDevice").hide(hide);
				$(".UnSuccess").show();
			}
		};
		$scope.profilePage = function () {
			$scope.playAudio();
			$location.path("/profile");
		};
		$scope.logout = function () {
			$scope.playAudio();
			$location.path("/home");
		};
		$scope.authen_adminByResults = function () {
			$scope.playAudio();
			// AuthenticationService.setProfile({ name: "test name", sex: "ชาย", age: "60" });
			$location.path("/authen_admin");
		};
		$scope.detailByProfile = function () {
			$scope.playAudio();
			// AuthenticationService.setProfile({ name: "test name", sex: "ชาย", age: "60" });
			$location.path("/detail");
		};

		$scope.playAudio = function () {
			var audio = new Audio("./assets/kiosk/audio/click.mp3");
			audio.play();
		};
		$scope.dataprint = "";
		$scope.print = function () {
			console.log($scope.qrcode);
			let BTEMP = $scope.dataprint.BTEMP ? $scope.dataprint.BTEMP : "-";
			let SBP = $scope.dataprint.SBP ? $scope.dataprint.SBP : "-";
			let DBP = $scope.dataprint.DBP ? $scope.dataprint.DBP : "-";
			let PR = $scope.dataprint.PR ? $scope.dataprint.PR : "-";
			let OXYGEN = $scope.dataprint.OXYGEN ? $scope.dataprint.OXYGEN : "-";
			let WEIGHT = $scope.dataprint.WEIGHT ? $scope.dataprint.WEIGHT : "-";
			let HEIGHT = $scope.dataprint.HEIGHT ? $scope.dataprint.HEIGHT : "-";
			let BMI = $scope.dataprint.BMI ? $scope.dataprint.BMI : "-";
			let BMR = $scope.dataprint.BMR ? $scope.dataprint.BMR : "-";
			// let WEIGHT = 64.0;
			// let BMI = 22.1;
			// let BMR = "1,623.0";
			let PID = $scope.dataprint.PID ? $scope.dataprint.PID : "-";
			// $scope.d_print =
			// 	$scope.dataprint.PRENAME +
			// 	" " +
			// 	$scope.dataprint.NAME +
			// 	" " +
			// 	$scope.dataprint.LNAME +
			// 	" </br>";
			$scope.d_print = "KIOS</br>";
			$scope.d_print +=
				"วันที่ " +
				$scope.dataprint.DATE_SERV +
				" </br>เวลา" +
				$scope.dataprint.TIME_SERV +
				" </br></br>.............................</br></br>";
			$scope.d_print += "อุณหภูมิ " + BTEMP + " °C </br>";
			$scope.d_print += "ความดันโลหิต " + SBP + "/" + DBP + " mmHG </br>";
			// $scope.d_print += "ความดันโลหิตไดแอสโตลิก "+DBP+" </br>";
			$scope.d_print += "ชีพจร " + PR + " BPM </br>";
			$scope.d_print += "ออกซิเจนในเลือด " + OXYGEN + " % </br>";
			$scope.d_print += "น้ำหนัก " + WEIGHT + " kg</br>";
			$scope.d_print += "ส่วนสูง " + HEIGHT + " cm</br>";
			$scope.d_print += "BMI " + BMI + " </br>";
			$scope.d_print += "BMR " + BMR;
			// var newWin = window.open("", "Print-Window");
			var newWin = window.open("", "_blank");

			// newWin.document.open();

			// newWin.document.write(
			// 	'<html><body onload="window.print()">' +
			// 		$scope.d_print +
			// 		"</body></html>"
			// );

			newWin.document.write(
				'<html><head><style>table{width: 100%; text-align: left;margin-top:10px}.box-print{width: 182px;height: 500px;text-align: center;font-family: "Prompt", sans-serif;font-size: 14px;}</style></head><body onload="window.print()"><div class="box-print"><img width="100"style="overflow: hidden;" src="assets/kiosk/results/logo.jpg"><br>........................................<br><p>' +
					PID +
					'</p>........................................<br><table><tr><td style="text-align: left;">วันที่</td><td>' +
					$scope.dataprint.DATE_SERV +
					"</td></tr><tr><td>เวลา</td><td>" +
					$scope.dataprint.TIME_SERV +
					'</td></tr></table>........................................<table style="font-size: 12px;"><tr><td style="text-align: left;">อุณหภูมิ</td><td style="text-align: right;">' +
					BTEMP +
					' °C</td></tr><tr><td style="text-align: left;">ความดันโลหิต</td><td style="text-align: right;">' +
					SBP +
					"/" +
					DBP +
					' mmHG</td></tr><tr><td style="text-align: left;">ชีพจร</td><td style="text-align: right;">' +
					PR +
					' BPM</td></tr><tr><td style="text-align: left;">ออกซิเจนในเลือด</td><td style="text-align: right;">' +
					OXYGEN +
					' %</td></tr><tr><td style="text-align: left;">น้ำหนัก</td><td style="text-align: right;">' +
					WEIGHT +
					' kg</td></tr><tr><td style="text-align: left;">ส่วนสูง</td><td style="text-align: right;">' +
					HEIGHT +
					' cm</td></tr><tr><td style="text-align: left;">BMI</td><td style="text-align: right;">' +
					BMI +
					'</td></tr><tr><td style="text-align: left;">BMR</td><td style="text-align: right;">' +
					BMR +
					"</td></tr></table></div></body></html>"
			);

			newWin.document.close();
			setTimeout(function () {
				newWin.close();
			}, 40);
		};

		$scope.conditionDataBloodPressure = [];
		$scope.bloodPressureStyleEmoji = {};
		$scope.convertDataBloodPressure = {};

		$scope.conditionDataPulseRate = [];
		$scope.pulseRateStyleEmoji = {};
		$scope.convertDataPulseRate = {};

		$scope.conditionDataOxygen = [];
		$scope.oxygenStyleEmoji = {};
		$scope.convertDataOxygen = {};

		$scope.conditionDataTemperature = [];
		$scope.convertDataTemperature = {};
		$scope.temperatureStyleEmoji = {};

		$scope.conditionDataWeighingMachines = [];
		$scope.convertDataWeighingMachines = {};
		$scope.weighingMachinesStyleEmoji = {};

		$scope.ShowDevice = function () {
			$scope.playAudio();
			$(".DisplayDevice").show();
			$(".UnSuccess").hide();
		};

		$scope.init = function () {
			conBloodPressureService.getConBloodPressure({}, function (result) {
				$scope.conditionDataBloodPressure = result.message;
				// console.log($scope.conditionDataBloodPressure)
				// $scope.convertDataBloodPressure = $scope.conditionDataBloodPressure[0]

				// $scope.bloodPressureStyleEmoji = {
				//     'background-image': 'url('+$scope.convertDataBloodPressure.Image+')','width':'200px'
				// }
				$scope.convertResult();
				$scope.ShowDevice();
			});

			conPulseRateService.getConPulseRate({}, function (result) {
				$scope.conditionDataPulseRate = result.message;
				// console.log($scope.conditionDataBloodPressure)
				// $scope.convertDataBloodPressure = $scope.conditionDataBloodPressure[0]

				// $scope.bloodPressureStyleEmoji = {
				//     'background-image': 'url('+$scope.convertDataBloodPressure.Image+')','width':'200px'
				// }
				$scope.convertResult();
			});

			conOxygenService.getConOxygen({}, function (result) {
				$scope.conditionDataOxygen = result.message;
				// console.log($scope.conditionDataOxygen)
				// $scope.convertDataOxygen = $scope.conditionDataOxygen[0]

				// $scope.oxygenStyleEmoji = {
				//     'background-image': 'url('+$scope.convertDataOxygen.Image+')','width':'200px'
				// }
				$scope.convertResult();
			});

			conTemperatureService.getConTemperature({}, function (result) {
				$scope.conditionDataTemperature = result.message;
				// console.log($scope.conditionDataOxygen)
				// $scope.convertDataOxygen = $scope.conditionDataOxygen[0]

				// $scope.oxygenStyleEmoji = {
				//     'background-image': 'url('+$scope.convertDataOxygen.Image+')','width':'200px'
				// }

				$scope.convertResult();
			});

			conWeighingMachinesService.getConWeighingMachines({}, function (result) {
				$scope.conditionDataWeighingMachines = result.message;
				// console.log($scope.conditionDataWeighingMachines)
				// $scope.convertDataWeighingMachines = $scope.conditionDataWeighingMachines[0]

				// $scope.oxygenStyleEmoji = {
				//     'background-image': 'url('+$scope.convertDataWeighingMachines.Image+')','width':'200px'
				// }

				$scope.convertResult();
			});
			console.log($scope.results);
			// const {
			// 	bloodPressure,
			// 	height,
			// 	pulseOximeter,
			// 	temperature,
			// 	weight,
			// } = $scope.results;
			// let _prename = "";
			// let _fname = $scope.profile.firstnameth
			// 	? $scope.profile.firstnameth
			// 	: "unknown";
			// let _lname = $scope.profile.lastnameth
			// 	? $scope.profile.lastnameth
			// 	: "unknown";
			// let _height = "";
			// let _weight = "";
			// let _bmi = "";
			// let _bmr = "";
			// let _diastolic = "";
			// let _pulse = "";
			// let _systolic = "";
			// let _PRbpm = "";
			// let _SpO2 = "";
			// let _temperature = "";
			// if (height != undefined) {
			// 	_height = height;
			// }
			// if (weight != undefined) {
			// 	_weight = weight.weight;
			// 	_bmi = weight.BMI;
			// 	_bmr = weight.BMR;
			// }
			// if (bloodPressure != undefined) {
			// 	_diastolic = bloodPressure.diastolic;
			// 	_pulse = bloodPressure.pulse;
			// 	_systolic = bloodPressure.systolic;
			// }
			// if (pulseOximeter != undefined) {
			// 	_PRbpm = pulseOximeter.PRbpm;
			// 	_SpO2 = pulseOximeter.SpO2;
			// }
			// if (temperature != undefined) {
			// 	_temperature = temperature.value;
			// }
			// $scope.qrcode = `{PRENAME:"${_prename}",
			// NAME:"${_fname}",
			// LNAME:"${_lname}",
			// DATE_SERV:"",
			// TIME_SERV:"",
			// BTEMP:"${_temperature}",
			// BTEMPCODE:"",
			// SBP:"${_systolic}",
			// DBP:"${_diastolic}",
			// BPCODE:"",
			// PR:"${_pulse}",
			// PRCODE:"",
			// RR:"",
			// RRCODE:"",
			// OXYGEN:"${_SpO2}",
			// OXYGENCODE:"",
			// WEIGHT:"${_weight}",
			// HEIGHT:"${_height}",
			// BMI:"${_bmi}",
			// BMICODE:"",
			// BMR:"${_bmr}"}`;
			d_serviceService.getCurrentD_service(
				{ session_id: $rootScope.globals.session_id },
				function (result) {
					console.log(result);
					if (result.status == true) {
						if (result.message.length > 0) {
							let data = result.message[0];
							$scope.dataprint = result.message[0];
							console.log(data);
							// $scope.qrcode = JSON.stringify(data).replaceAll("null", '""');

							// 	"PRENAME": "003",
							// 	"NAME": "สมชาย",
							// 	"LNAME": "ดีใจ",
							// 	"DATE_SERV": "2021-04-29",
							// 	"TIME_SERV": "17:14:26",
							// 	"BTEMP": "35.9",
							// 	"SBP": "125",
							// 	"DBP": "95",
							// 	"PR": "80",
							// 	"OXYGEN": "97",
							// 	"WEIGHT": null,
							// 	"HEIGHT": "170.0",
							// 	"BMI": null,
							// 	"BMR": null
							let BTEMP = data.BTEMP ? data.BTEMP : "-";
							let SBP = data.SBP ? data.SBP : "-";
							let DBP = data.DBP ? data.DBP : "-";
							let PR = data.PR ? data.PR : "-";
							let OXYGEN = data.OXYGEN ? data.OXYGEN : "-";
							let WEIGHT = data.WEIGHT ? data.WEIGHT : "-";
							let HEIGHT = data.HEIGHT ? data.HEIGHT : "-";
							let BMI = data.BMI ? data.BMI : "-";
							let BMR = data.BMR ? data.BMR : "-";
							$scope.qrcode =
								"ชื่อ-สกุล " +
								data.PRENAME +
								" " +
								data.NAME +
								" " +
								data.LNAME +
								" \n";
							$scope.qrcode +=
								"วันที่ให้บริการ " +
								data.DATE_SERV +
								" " +
								data.TIME_SERV +
								" \n";
							$scope.qrcode += "อุณหภูมิ " + BTEMP + " °C \n";
							$scope.qrcode += "ความดันโลหิต " + SBP + "/" + DBP + " mmHG \n";
							// $scope.qrcode += "ความดันโลหิตไดแอสโตลิก "+DBP+" \n";
							$scope.qrcode += "ชีพจร " + PR + " BPM \n";
							$scope.qrcode += "ออกซิเจนในเลือด " + OXYGEN + " % \n";
							$scope.qrcode += "น้ำหนัก " + WEIGHT + " kg\n";
							$scope.qrcode += "ส่วนสูง " + HEIGHT + " cm\n";
							$scope.qrcode += "BMI " + BMI + " \n";
							$scope.qrcode += "BMR " + BMR;
						}
					}
				}
			);
		};

		$scope.convertResult = function () {
			// alert("Data")
			// $scope.data.systolic = 70
			// $scope.data.diastolic = 100
			console.log($scope.conditionDataBloodPressure);

			var indexofSBP;
			var indexofDBP;

			if ($scope.results.bloodPressure != undefined) {
				$scope.conditionDataBloodPressure.forEach(function (item, index) {
					// console.log(parseFloat(item.SBP_min), item.SBP_max, $scope.results.bloodPressure.systolic, parseFloat(item.Importance))

					if (
						parseFloat(item.SBP_min) <=
							parseFloat($scope.results.bloodPressure.systolic) &&
						parseFloat(item.SBP_max) >=
							parseFloat($scope.results.bloodPressure.systolic)
					) {
						$scope.convertDataBloodPressure = item;

						$scope.bloodPressureStyle = JSON.parse(
							$scope.convertDataBloodPressure.Results_style
						);
						// Object.assign($scope.bloodPressureStyle, JSON.parse($scope.convertDataBloodPressure.Recommendation_style));

						$scope.bloodPressureStyleEmoji = {
							"background-image":
								"url(" + $scope.convertDataBloodPressure.Image + ")",
						};
						indexofSBP = item;
						console.log(indexofSBP);
					}
					if (
						parseFloat(item.DBP_min) <=
							parseFloat($scope.results.bloodPressure.diastolic) &&
						parseFloat(item.DBP_max) >=
							parseFloat($scope.results.bloodPressure.diastolic)
					) {
						$scope.convertDataBloodPressure = item;

						$scope.bloodPressureStyle = JSON.parse(
							$scope.convertDataBloodPressure.Results_style
						);
						// Object.assign($scope.bloodPressureStyle, JSON.parse($scope.convertDataBloodPressure.Recommendation_style));

						$scope.bloodPressureStyleEmoji = {
							"background-image":
								"url(" + $scope.convertDataBloodPressure.Image + ")",
						};
						indexofDBP = item;
						console.log(indexofDBP);
					}
				});
			}

			if ($scope.results.bloodPressure != undefined) {
				$scope.conditionDataPulseRate.forEach((item) => {
					// $scope.data.pulse = 50
					// console.log(parseFloat(item.Min), item.Max, $scope.data.pulse)
					if (
						parseFloat(item.Min) <=
							parseFloat($scope.results.bloodPressure.pulse) &&
						parseFloat(item.Max) >=
							parseFloat($scope.results.bloodPressure.pulse)
					) {
						$scope.convertDataPulseRate = item;

						$scope.pulseRateStyle = JSON.parse(
							$scope.convertDataPulseRate.Device_style
						);

						$scope.pulseRateStyleEmoji = {
							"background-image":
								"url(" + $scope.convertDataPulseRate.Image + ")",
						};
					}
				});
			}

			if ($scope.results.pulseOximeter != undefined) {
				$scope.conditionDataOxygen.forEach((item) => {
					console.log(
						parseFloat(item.Min),
						item.Max,
						$scope.results.pulseOximeter.SpO2
					);
					if (
						parseFloat(item.Min) <=
							parseFloat($scope.results.pulseOximeter.SpO2) &&
						parseFloat(item.Max) >=
							parseFloat($scope.results.pulseOximeter.SpO2)
					) {
						$scope.convertDataOxygen = item;

						$scope.oxygenStyle = JSON.parse(
							$scope.convertDataOxygen.Results_style
						);

						$scope.oxygenStyleEmoji = {
							"background-image": "url(" + $scope.convertDataOxygen.Image + ")",
						};
					}
				});
			}

			console.log($scope.conditionDataTemperature);
			if ($scope.results.temperature != undefined) {
				$scope.conditionDataTemperature.forEach((item) => {
					console.log(
						parseFloat(item.Min),
						item.Max,
						$scope.results.temperature.value
					);
					if (
						parseFloat(item.Min) <=
							parseFloat($scope.results.temperature.value) &&
						parseFloat(item.Max) >= parseFloat($scope.results.temperature.value)
					) {
						$scope.convertDataTemperature = item;

						$scope.temperatureStyle = JSON.parse(
							$scope.convertDataTemperature.Results_style
						);

						$scope.temperatureStyleEmoji = {
							"background-image":
								"url(" + $scope.convertDataTemperature.Image + ")",
						};
					}
				});
			}

			console.log($scope.results.WeighingMachines);
			if ($scope.conditionDataWeighingMachines != undefined && $scope.results.weight!= undefined) {
				$scope.conditionDataWeighingMachines.forEach((item) => {
					console.log($scope.results.weight);
					// console.log(parseFloat(item.Min), item.Max, $scope.results.weight.BMI);
					// alert("condition")
					if (
						parseFloat(item.Min) <= parseFloat($scope.results.weight.BMI) &&
						parseFloat(item.Max) >= parseFloat($scope.results.weight.BMI)
					) {
						$scope.convertDataWeighingMachines = item;

						$scope.weighingMachinesStyle = JSON.parse(
							$scope.convertDataWeighingMachines.Results_style
						);

						$scope.weighingMachinesStyleEmoji = {
							"background-image":
								"url(" + $scope.convertDataWeighingMachines.Image + ")",
						};

						// for (const style in $scope.weighingMachinesStyle.emojiposition) {
						//     Object.assign($scope.weighingMachinesStyleEmoji, {
						//         [style]: $scope.weighingMachinesStyle.emojiposition[style],
						//     });
						// }
						console.log($scope.weighingMachinesStyle);
					}
				});
			}
		};

		$scope.init();

		document.onkeypress = function (e) {
			e = e || window.event;
			let key = e.key.toLowerCase();
			switch (page) {
				case "DisplayDevice":
					if (key == "z" || key == "ผ") {
						//red
						document.elementFromPoint(384, 933).click();
					}
					if (key == "x" || key == "ป") {
						//yellow
						document.elementFromPoint(975, 939).click();
					}
					if (key == "c" || key == "แ") {
						// green
						document.elementFromPoint(1524, 939).click();
					}
					break;
				default:
			}
		};

		var page = "DisplayDevice";
	}
})();
